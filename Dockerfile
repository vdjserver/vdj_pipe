# Base Image
FROM debian:jessie

MAINTAINER VDJServer <vdjserver@utsouthwestern.edu>

# uncomment these if behind UTSW proxy
#ENV http_proxy 'http://proxy.swmed.edu:3128/'
#ENV https_proxy 'https://proxy.swmed.edu:3128/'
#ENV HTTP_PROXY 'http://proxy.swmed.edu:3128/'
#ENV HTTPS_PROXY 'https://proxy.swmed.edu:3128/'

# Install OS Dependencies
RUN apt-get update && apt-get install -y --fix-missing\
    build-essential \
    doxygen \
    git \
    graphviz \
    libbz2-dev \
    libxml2-dev \
    libxslt-dev \
    python \
    python-dev \
    python-sphinx \
    python-pip \
    vim \
    wget \
    zlib1g-dev

RUN pip install \
    biopython \
    lxml \
    numpy \
    reportlab

# Set boost config vars and files
ENV BOOST_VERSION 1.57.0
ENV BOOST_VERSION_LINK 1_57_0

RUN mkdir /vdjpipe-root
COPY docker/boost/boost-build.jam /vdjpipe-root/
COPY docker/boost/user-config.jam /root/

# Install/bootstrap boost
RUN wget http://downloads.sourceforge.net/project/boost/boost/$BOOST_VERSION/boost_$BOOST_VERSION_LINK.tar.gz
RUN tar -xvzf boost_$BOOST_VERSION_LINK.tar.gz
RUN cd /boost_$BOOST_VERSION_LINK && ./bootstrap.sh --prefix=/usr/local
RUN cd /boost_$BOOST_VERSION_LINK && ./b2 install
RUN cd /boost_$BOOST_VERSION_LINK/tools/build && ./bootstrap.sh
RUN cd /boost_$BOOST_VERSION_LINK/tools/build && ./b2 install --prefix=/usr/local

# Copy source
COPY . /vdjpipe-root/vdjpipe/

# Build vdj_pipe
RUN cd /vdjpipe-root/vdjpipe && b2 release
RUN cd /vdjpipe-root/vdjpipe && b2 install

ENV PATH /vdjpipe-root/vdjpipe/out/bin:$PATH

# disable doc building
#RUN cd /vdjpipe-root/vdjpipe && b2 autodoc
#RUN mkdir /var/www && mkdir /var/www/html
#RUN mv /vdjpipe-root/vdjpipe/out/html /var/www/html/vdjpipe

#VOLUME ["/var/www/html/vdjpipe"]
