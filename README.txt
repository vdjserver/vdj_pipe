project vdj_pipe 

Copyright, The University of Texas Southwestern Medical Center, 2014

vdj_pipe is a pipeline for processing DNA sequence data

For building, see doc/build.txt.
For legal information see doc/license.txt.
For HTML documentation see out/html/interface.html
Contact: lindsay.cowell (at) utsouthwestern.edu

Development Process
===================

Git Structure and Versioning Process
------------------------------------

 * This project uses the Git Flow methodology for code management and development: <https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow>.

 * Do not directly modify or commit change to the **master** branch.

 * New development and features should be done on branches that are cloned from the **develop** branch. Create an issue for the bug or task being developed and name the branch **issue-#** where # is the issue number.

 * When you have completed development and testing, push your branch to bitbucket and file a pull request on your branch.

 * Email Scott.Christley (at) UTSouthwestern.edu, if you have any questions.

Development Setup
=================

You will need to clone down the project to create a local repository on your machine.

    - Clone project
        $ git clone username@bitbucket.org:vdjserver/vdj_pipe.git
        $ cd vdj_pipe

    - Reproduce a bug in the **develop** branch
        $ git checkout develop
        $ build and run tests

    - Create a new branch from the **develop** branch to work on an issue
        $ git checkout -b issue-# develop
        $ develop and test

Release Process
===============