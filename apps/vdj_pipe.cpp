/** @file "/vdj_pipe/apps/vdj_pipe.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#include <iostream>
#include <fstream>
#include "vdj_pipe/command_line_options.hpp"
#include "vdj_pipe/process_options.hpp"

/**
*******************************************************************************/
int main(int argc, char* argv[]) {
   try{
      vdj_pipe::Command_line_options clo(argc, argv);
      if( ! clo.args_provided() ) {
         vdj_pipe::Command_line_options::print_version(std::cout) << '\n';
         vdj_pipe::Command_line_options::print_info(std::cout) << '\n';
         vdj_pipe::Command_line_options::print_usage(std::cout) << '\n';
         std::cout << "use option -h for more details\n" << std::endl;
         std::cerr << "no arguments provided" << std::endl;
         return 1;
      }

      if( clo.is_help() ) {
         clo.print_help(std::cout) << std::endl;
         return 0;
      }

      if( clo.version() ) {
         vdj_pipe::Command_line_options::print_version(std::cout) << std::endl;
         return 0;
      }

      vdj_pipe::process_options(clo.tree());
   }catch(...) {
      std::cerr
      << boost::current_exception_diagnostic_information()
      << std::endl;
      return 1;
   }
   return 0;
}
