/** @file "/vdj_pipe/include/vdj_pipe/best_match_pair.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef BEST_MATCH_PAIR_HPP_
#define BEST_MATCH_PAIR_HPP_
#include <limits>
#include "boost/assert.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
template<class ID, class Score = int> class Best_match_pair {
   static Score default_score() {return std::numeric_limits<Score>::min();}
public:
   typedef ID id_type;
   typedef Score score_type;

   Best_match_pair()
   : id1_(),
     score1_(default_score()),
     id2_(),
     score2_(default_score())
   {}

   id_type const& id1() const {return id1_;}
   id_type const& id2() const {return id2_;}
   bool has_score() const {return score1_ != default_score();}
   score_type score1() const {return score1_;}
   score_type score2() const {return score2_;}

   bool is_acceptable(const score_type min_score, const bool require_best) const {
      return
               id1_ &&
               score1_>= min_score &&
               ( ! require_best || score1_ != score2_ )
               ;
   }

   /**
   keep two best scores and corresponding id-s
   @param id
   @param score
   @return true if new best score was found
   */
   bool combine(id_type const& id, const score_type score) {
      BOOST_ASSERT(score2_ <= score1_);
      if( score1_ < score ) {
         score2_ = score1_;
         id2_ = id1_;
         score1_ = score;
         id1_ = id;
         return true;
      }

      if( score2_ < score ) {
            score2_ = score;
            id2_ = id;
      }
      return false;
   }

   /**
   keep two best scores and corresponding id-s
   @param bmp the other best match pair
   @return true if new best score was found
   */
   bool combine( Best_match_pair const& bmp) {
      BOOST_ASSERT(score2_ <= score1_);
      BOOST_ASSERT(bmp.score2_ <= bmp.score1_);

      if( score1_ < bmp.score1_ ) {
         if( score1_ < bmp.score2_ ) {
            score2_ = bmp.score2_;
            id2_ = bmp.id2_;
         } else {
            score2_ = score1_;
            id2_ = id1_;
         }
         score1_ = bmp.score1_;
         id1_ = bmp.id1_;
         return true;
      }

      if( score2_ < bmp.score1_ ) {
            score2_ = bmp.score1_;
            id2_ = bmp.id1_;
      }

      return false;
   }

private:
   ID id1_;
   Score score1_;
   ID id2_;
   Score score2_;
};

}//namespace vdj_pipe
#endif /* BEST_MATCH_PAIR_HPP_ */
