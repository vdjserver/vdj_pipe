/** @file "/vdj_pipe/include/vdj_pipe/command_line_options.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3; see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef COMMAND_LINE_OPTIONS_HPP_
#define COMMAND_LINE_OPTIONS_HPP_
#include <iosfwd>
#include <string>
#include "boost/program_options/options_description.hpp"
#include "boost/program_options/variables_map.hpp"
#include "boost/property_tree/ptree.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/exception.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Command_line_options {
   BOOST_STATIC_CONSTANT(std::size_t, unset = static_cast<std::size_t>(-1));
public:
   struct Err : public base_exception {};

   static std::ostream& print_version(std::ostream& os);
   static std::ostream& print_info(std::ostream& os);
   static std::ostream& print_usage(std::ostream& os);

   Command_line_options()
   : help_(),
     max_reads_(unset),
     max_file_reads_(unset),
     version_(false),
     no_stamp_(false),
     args_provided_(false)
   {
      init_options();
   }

   Command_line_options(int argc, char* argv[]);
   bool is_help() const {return help_.size();}
   std::string const& help_opt() const {return help_;}
   bool version() const {return version_;}
   bool args_provided() const {return args_provided_;}
   std::ostream& print_help(std::ostream& os) const;
   std::size_t max_reads() const {return max_reads_;}
   std::size_t max_file_reads() const {return max_file_reads_;}
   boost::property_tree::ptree tree() const;
   std::vector<std::string> const& config_files() const {return config_fn_;}

   void add_config_file(std::string const& config_fn) {
      config_fn_.push_back(config_fn);
   }

   void set_path_input(std::string const& in_path) {in_path_ = in_path;}
   void set_path_output(std::string const& out_path) {out_path_ = out_path;}

private:
   boost::program_options::options_description od_;
   std::string out_path_;
   std::string in_path_;
   std::vector<std::string> config_fn_;
   std::vector<std::string> input_csv_;
   std::string help_;
   std::size_t max_reads_;
   std::size_t max_file_reads_;
   bool version_;
   bool no_stamp_;
   bool args_provided_;

   void init_options();

   boost::property_tree::ptree options_to_tree() const;
};

}//namespace vdj_pipe
#endif /* COMMAND_LINE_OPTIONS_HPP_ */
