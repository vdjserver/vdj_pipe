/** @file "/vdj_pipe/include/vdj_pipe/config.hpp"
part of vdj_pipe project.
Distributed under GNU General Public License, Version 3; see doc/license.txt.
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJ_PIPE_CONFIG_HPP_
#define VDJ_PIPE_CONFIG_HPP_

#include "boost/config.hpp"

#ifndef BOOST_SYMBOL_EXPORT
  #define BOOST_SYMBOL_EXPORT
  #define BOOST_SYMBOL_IMPORT
#endif

#if defined(VDJ_PIPE_DYN_LINK)
#  if defined(VDJ_PIPE_SOURCE)
#     define VDJ_PIPE_DECL BOOST_SYMBOL_EXPORT
#  else
#     define VDJ_PIPE_DECL BOOST_SYMBOL_IMPORT
#  endif
#else
#  define VDJ_PIPE_DECL
#endif

#endif /* VDJ_PIPE_CONFIG_HPP_ */
