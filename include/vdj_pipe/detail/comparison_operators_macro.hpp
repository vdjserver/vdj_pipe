/** @file "/vdj_pipe/include/vdj_pipe/detail/comparison_operators_macro.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef COMPARISON_OPERATORS_MACRO_HPP_
#define COMPARISON_OPERATORS_MACRO_HPP_

#define VDJ_PIPE_COMPARISON_OPERATOR_MEMBERS(name) \
   bool operator!=(name const& x) const {       \
      return !( *this == x );                   \
   }                                            \
   bool operator>(name const& x) const {        \
      return x < *this;                         \
   }                                            \
   bool operator<=(name const& x) const {       \
      return !( x < *this );                    \
   }                                            \
   bool operator>=(name const& x) const {       \
      return !( *this < x );                    \
   }                                            \
/* */

#endif /* COMPARISON_OPERATORS_MACRO_HPP_ */
