/** @file "/vdj_pipe/include/vdj_pipe/detail/disable_warnings.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef DISABLE_WARNINGS_HPP_
#define DISABLE_WARNINGS_HPP_
#include "boost/predef.h"

#if BOOST_COMP_INTEL && ! defined(BOOST_DISABLE_ASSERTS)
#  pragma warning(disable: 279) // controlling expression is constant
#endif

#if BOOST_COMP_MSVC
   #pragma warning (push)
   #pragma warning (disable : 4251) // class 'A<T>' needs to have dll-interface to be used by clients of class 'B'
   #pragma warning (disable : 4275) // non DLL-interface classkey "identifier" used as base for DLL-interface
   #pragma warning (disable : 4290) // C++ exception specification ignored except to ...
   #pragma warning (disable : 4355) // 'this' : used in base member initializer list
   #pragma warning (disable : 4800) // forcing value to bool 'true' or 'false'
   #pragma warning (disable : 4003) // not enough actual parameters for macro
   #pragma warning (disable : 4503) // not enough actual parameters for macro
#endif

#endif /* DISABLE_WARNINGS_HPP_ */
