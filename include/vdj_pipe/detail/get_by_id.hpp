/** @file "/vdj_pipe/include/vdj_pipe/detail/get_by_id.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef GET_BY_ID_HPP_
#define GET_BY_ID_HPP_
#include "boost/type_traits/remove_reference.hpp"
#include "boost/type_traits/remove_const.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief Extract object by its ID and apply member function
@details use as boost::multi_index key extractor for storing object IDs in
multi_index
*******************************************************************************/
template<
   class Stor,
   class Id,
   class Obj,
   typename Result,
   Result(Obj::*MemFunPtr)()const
>
class Getter {
public:
   typedef typename boost::remove_const<
               typename boost::remove_reference<Result>::type
            >::type result_type;

   explicit Getter(Stor const& stor) : stor_(stor) {}
   Result operator()(const Id id) const {return (stor_[id].*MemFunPtr)();}
private:
   Stor const& stor_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* GET_BY_ID_HPP_ */
