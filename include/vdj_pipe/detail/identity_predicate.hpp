/** @file "/vdj_pipe/include/vdj_pipe/detail/identity_predicate.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef IDENTITY_PREDICATE_HPP_
#define IDENTITY_PREDICATE_HPP_
#include <functional>

namespace vdj_pipe{

/**@brief why is it not in STL?
*******************************************************************************/
template<class T> struct Identity : public std::unary_function<T,T> {
   T const& operator()(T const& x) const {return x;}
   T& operator()(T& x) const {return x;}
};

}//namespace vdj_pipe
#endif /* IDENTITY_PREDICATE_HPP_ */
