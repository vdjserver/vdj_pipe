/** @file "/vdj_pipe/include/vdj_pipe/detail/keyword_struct_macro.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef KEYWORD_STRUCT_MACRO_HPP_
#define KEYWORD_STRUCT_MACRO_HPP_
#include "boost/preprocessor/seq/for_each.hpp"
#include "boost/preprocessor/stringize.hpp"

#define VDJ_PIPE_STATIC_STRING_METHOD(name, val)                        \
   static std::string const& name() {                                   \
      static const std::string s = val; return s;                       \
   }                                                                    \
/* */

#define VDJ_PIPE_KEYWORD_STRUCT_MACRO_(r, data, elem)                   \
   VDJ_PIPE_STATIC_STRING_METHOD(elem, BOOST_PP_STRINGIZE(elem))        \
/* */

#define VDJ_PIPE_KEYWORD_STRUCT(name, list)                             \
   struct name {                                                        \
      BOOST_PP_SEQ_FOR_EACH(VDJ_PIPE_KEYWORD_STRUCT_MACRO_, ,list)      \
   }                                                                    \
/*  */

#endif /* KEYWORD_STRUCT_MACRO_HPP_ */
