/** @file "/vdj_pipe/include/vdj_pipe/detail/named_value_map.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef NAMED_VALUE_MAP_HPP_
#define NAMED_VALUE_MAP_HPP_
#include <string>
#include "boost/foreach.hpp"
#include "boost/assert.hpp"
#include "vdj_pipe/detail/disable_warnings.hpp"
#include "vdj_pipe/detail/id_bimap.hpp"
#include "vdj_pipe/detail/id_iterator.hpp"
#include "vdj_pipe/detail/id_map.hpp"
#include "vdj_pipe/exception.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief Store values mapped against name strings and value IDs
*******************************************************************************/
template<class Id, class Val> class Named_value_map {
   typedef detail::Id_map<Id, Val> val_map;
   typedef detail::Id_bimap<Id, std::string> name_map;
public:
   typedef Id id_type;
   typedef Val value_type;
   typedef Id_iterator<id_type> iterator;
   typedef iterator const_iterator;

   Named_value_map()
   : vm_(id_type(1)),
     nm_(id_type(1))
   {}

   iterator begin() const {return iterator(vm_.min_id());}
   iterator end() const {return ++iterator(vm_.max_id());}
   std::size_t size() const {return nm_.size();}

   id_type insert_new_name(std::string const& name) {
      if( name.empty() ) return id_type(0);
      const std::pair<id_type, bool> p = nm_.insert(name);
      if( ! p.second ) BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("duplicate value name")
               << base_exception::str1_t(sanitize(name))
      );
      vm_.insert(p.first);
      return p.first;
   }

   id_type insert_name(std::string const& name) {
      if( name.empty() ) return id_type(0);
      const std::pair<id_type, bool> p = nm_.insert(name);
      if( p.second ) vm_.insert(p.first);
      return p.first;
   }

   std::string const& name(const id_type id) const {
      BOOST_ASSERT(id && "invalid ID");
      return nm_[id];
   }

   id_type const* find_id(std::string const& name) const {
      return nm_.find(name);
   }

   id_type value_id(std::string const& name) const {
      if( name.empty() ) return id_type(0);
      if( id_type const* vid = nm_.find(name) ) {
         return *vid;
      }
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("value name does not exist")
               << base_exception::str1_t(sanitize(name))
      );
   }

   value_type const& operator[](const id_type vid) const {
      BOOST_ASSERT(vid && "invalid ID");
      return vm_[vid];
   }

   value_type& operator[](const id_type vid) {
      BOOST_ASSERT(vid && "invalid ID");
      return vm_[vid];
   }

   void clear_values() {
      BOOST_FOREACH(const id_type vid, *this) {
         vm_[vid] = value_type();
      }
   }

private:
   val_map vm_;
   name_map nm_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* NAMED_VALUE_MAP_HPP_ */
