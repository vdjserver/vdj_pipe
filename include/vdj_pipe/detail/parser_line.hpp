/** @file "/vdj_pipe/include/vdj_pipe/detail/parser_line.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PARSER_LINE_HPP_
#define PARSER_LINE_HPP_
#include <istream>
#include <limits>
#include <cctype>
#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief Basic line-based parser; use to derive other parsers
*******************************************************************************/
class Parser_line {

public:
   struct Err : public base_exception {
      typedef boost::error_info<struct errinfo_line_n_, int> line_t;
   };

   int line_num() const {return line_;}
   bool has_next() const {return fis_.good();}

protected:
   explicit Parser_line(File_input const& fi)
   : fis_(fi), str_(), line_(1)
   {
      if( ! fis_.good() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error reading")
               << Err::str1_t(sanitize(fi.path()))
      );
   }

   explicit Parser_line(std::istream& is, const compression::Compression compr)
   : fis_(is, compr), str_(), line_(1)
   {
      if( ! fis_.good() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error reading")
      );
   }

   void skip_line() {
      fis_.istream().ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      ++line_;
   }

   /** seek line starting from tag */
   void seek_line(const char tag) {
      while(fis_.istream() && fis_.istream().peek() != tag) skip_line();
   }

   const boost::string_ref get_id(const char tag) {
      get_defstr(tag);
      std::size_t n = 0;
      for(; n != str_.length() && str_[n] != ' ' && str_[n] != '\t'; ++n){}
      return boost::string_ref(str_.data(), n);
   }

   void set_meta(Seq_meta& sm, const char tag) {
      get_defstr(tag);
      std::size_t n = 0;
      for(; n != str_.size() && str_[n] != ' ' && str_[n] != '\t'; ++n){}
      sm.id_ = str_.substr(0, n);
      for(; n != str_.size() && (str_[n] == ' ' || str_[n] == '\t'); ++n){}
      sm.comm_ = str_.substr(n);
   }

   const boost::string_ref get_defstr(const char tag) {
      if( ! fis_.istream() || fis_.istream().peek() != tag ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("parsing error")
               << Err::line_t(line_)
      );
      fis_.istream().get();
      getline(fis_.istream(), str_);
      ++line_;
      return str_;
   }

   std::string get_sequence(const char tag) {
      std::string seq;
      while( fis_.istream().peek() != tag && getline(fis_.istream(), str_) ) {
         ++line_;
         BOOST_FOREACH(const char c, str_) {
            if( std::isalpha(c) ) seq.push_back(c);
         }
      }
      return seq;
   }

protected:
   File_istream fis_;
   std::string str_;
   int line_;

   void check() const {
      if( ! fis_.good() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error reading")
      );
   }
};

}//namespace detail
}//namespace vdj_pipe
#endif /* PARSER_LINE_HPP_ */
