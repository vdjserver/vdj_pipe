/** @file "/vdj_pipe/include/vdj_pipe/detail/queable_ofstream_types.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_DETAIL_QUEABLE_OFSTREAM_TYPES_HPP_
#define VDJ_PIPE_DETAIL_QUEABLE_OFSTREAM_TYPES_HPP_
#include <string>
#include <vector>
#include "boost/ref.hpp"
#include "vdj_pipe/value_variant.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief 
*******************************************************************************/
struct Queable_ofstream_types {
   typedef value_variant value_type;
   typedef std::vector<value_type> val_vector;

   typedef std::vector<boost::reference_wrapper<value_type const> >
   val_ref_vector;

   typedef std::vector<std::string> path_template;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* VDJ_PIPE_DETAIL_QUEABLE_OFSTREAM_TYPES_HPP_ */
