/** @file "/vdj_pipe/include/vdj_pipe/detail/string_ref.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef STRING_REF_HPP_
#define STRING_REF_HPP_
#include "boost/utility/string_ref.hpp"
#include "boost/functional/hash_fwd.hpp"

namespace boost{

template<typename charT, typename traits>
inline std::size_t hash_value(basic_string_ref<charT,traits> const& str) {
   return boost::hash_range(str.begin(), str.end());
}

}//namespace boost

namespace vdj_pipe{ namespace detail{

struct Equal_string_ref {
   template<typename charT, typename traits, typename Allocator>
   bool operator()(
            std::basic_string<charT, traits, Allocator> const& s1,
            const boost::basic_string_ref<charT,traits> s2
   ) const {return s1 == s2;}

   template<typename charT, typename traits, typename Allocator>
   bool operator()(
            const boost::basic_string_ref<charT,traits> s1,
            std::basic_string<charT, traits, Allocator> const& s2
   ) const {return s1 == s2;}
};

}//namespace detail
}//namespace vdj_pipe
#endif /* STRING_REF_HPP_ */
