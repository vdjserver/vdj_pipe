/** @file "/vdj_pipe/include/vdj_pipe/detail/unused_variable.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef UNUSED_VARIABLE_HPP_
#define UNUSED_VARIABLE_HPP_

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
template<typename T> inline void unused_variable(T const& ) {}

}//namespace vdj_pipe
#endif /* UNUSED_VARIABLE_HPP_ */
