/** @file "/vdj_pipe/include/vdj_pipe/exception.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3; see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef EXCEPTION_HPP_
#define EXCEPTION_HPP_

#include <exception>
#include "boost/exception/all.hpp"
#include "vdj_pipe/sanitize_string.hpp"

namespace vdj_pipe{

/**
Example:
   BOOST_THROW_EXCEPTION(
         base_exception()
         << base_exception::msg_t("exception occurred")
         << base_exception::nested_t(boost::current_exception())
   );
*******************************************************************************/
struct base_exception : virtual public std::exception, virtual public boost::exception {
   typedef boost::error_info<struct errinfo_message_, std::string> msg_t;
   typedef boost::error_info<struct errinfo_str1_, std::string> str1_t;
   typedef boost::error_info<struct errinfo_str2_, std::string> str2_t;
   typedef boost::error_info<struct errinfo_str3_, std::string> str3_t;
   typedef boost::error_info<struct errinfo_int1_, int> int1_t;
   typedef boost::error_info<struct errinfo_int2_, int> int2_t;
   typedef boost::error_info<struct errinfo_line_, int> line_t;
   typedef boost::error_info<struct errinfo_float1_, double> float1_t;
   typedef boost::errinfo_nested_exception nested_t;
};

}//namespace vdj_pipe

#endif /* EXCEPTION_HPP_ */
