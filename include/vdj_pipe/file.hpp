/** @file "/vdj_pipe/include/vdj_pipe/file.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef FILE_HPP_
#define FILE_HPP_
#include <iosfwd>
#include <string>
#include <utility>
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/comparison_operators_macro.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file_properties.hpp"

namespace vdj_pipe{

/**@return compression based on file extension
*******************************************************************************/
VDJ_PIPE_DECL compression::Compression
guess_compression_ext(std::string const& path);

/**@return compression based on file magic number
*******************************************************************************/
VDJ_PIPE_DECL compression::Compression
guess_compression_magic(std::string const& path);

/**@return file compression and format based on file extension
*******************************************************************************/
VDJ_PIPE_DECL std::pair<compression::Compression, format::Format>
guess_compression_format(std::string const& path);

/**@return file compression and format based on file extension
*******************************************************************************/
VDJ_PIPE_DECL format::Format
guess_format(std::string const& path, const compression::Compression c);

/**@brief create file if does not exist along with parent directories if needed
*******************************************************************************/
VDJ_PIPE_DECL std::string ensure_path_writable(std::string const& path);

/**@brief if file does not exist, create and write header;
create parent directories if needed
*******************************************************************************/
VDJ_PIPE_DECL std::string ensure_path_writable(
         std::string const& path,
         std::string const& header
);

/**@return true if readable
*******************************************************************************/
VDJ_PIPE_DECL bool is_path_readable(std::string const& path);

/**@return canonical path name
@throw if path not readable
*******************************************************************************/
VDJ_PIPE_DECL std::string ensure_path_readable(std::string const& path);

/**@brief
*******************************************************************************/
class File {
public:
   struct Err : public base_exception {};

   explicit File(
            std::string const& path,
            const compression::Compression compr = compression::unknown,
            const format::Format fmt = format::unknown
   )
   : path_(path), compr_(compr), fmt_(fmt)
   {}

   std::string const& path() const {return path_;}
   format::Format format() const {return fmt_;}
   compression::Compression compression() const {return compr_;}
   bool operator==(File const& f2) const {return path_ == f2.path_;}
   bool operator<(File const& f2) const {return path_ < f2.path_;}
   VDJ_PIPE_COMPARISON_OPERATOR_MEMBERS(File)

protected:
   std::string path_;
   compression::Compression compr_;
   format::Format fmt_;
};

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::size_t hash_value(File const& f);

/**@brief File target is supposed to exist at construction time
*******************************************************************************/
class File_input : public File {
public:
   explicit File_input(std::string const& path)
   : File(ensure_path_readable(path))
   {
      compr_ = guess_compression_magic(path_);
      fmt_ = guess_format(path_, compr_);
   }
};

/**@brief File target is created if needed at construction time
*******************************************************************************/
class File_output : public File {
public:

   File_output(const format::Format fmt)
   : File("", compression::unknown, fmt)
   {}

   explicit File_output(
      std::string const& path,
      const compression::Compression compr = compression::unknown,
      const format::Format fmt = format::unknown
   )
   : File(
      ensure_path_writable(path),
      compr == compression::unknown ? guess_compression_ext(path) : compr,
      fmt == format::unknown ? guess_compression_format(path).second : fmt
   )
   {}
};

}//namespace vdj_pipe
#endif /* FILE_HPP_ */
