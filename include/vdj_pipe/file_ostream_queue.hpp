/** @file "/vdj_pipe/include/vdj_pipe/file_ostream_queue.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef FILE_OSTREAM_QUEUE_HPP_
#define FILE_OSTREAM_QUEUE_HPP_
#include <iosfwd>
#include "boost/multi_index_container.hpp"
#include "boost/multi_index/hashed_index.hpp"
#include "boost/multi_index/member.hpp"
#include "boost/multi_index/sequenced_index.hpp"
#include "boost/range.hpp"
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/queable_ofstream_types.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file_stream.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
struct VDJ_PIPE_DECL Queable_ofstream {
   typedef detail::Queable_ofstream_types::value_type value_type;
   typedef detail::Queable_ofstream_types::val_vector val_vector;
   typedef detail::Queable_ofstream_types::val_ref_vector val_ref_vector;
   typedef detail::Queable_ofstream_types::path_template path_template;

   struct Err : public base_exception {};

   static std::string make_path(
            path_template const& templ,
            val_ref_vector const& vals,
            std::string const& header
   );

   Queable_ofstream(
            path_template const& templ,
            val_ref_vector const& vals,
            const format::Format fmt,
            std::string const& header
   );

   val_vector sv_;
   mutable File_ostream fos_;
};


/**@brief stack of recently used output file streams
*******************************************************************************/
class VDJ_PIPE_DECL File_ostream_queue {
   typedef detail::Queable_ofstream_types::val_vector val_vector;
   typedef detail::Queable_ofstream_types::path_template path_template;

   typedef boost::multi_index_container<
            Queable_ofstream,
            boost::multi_index::indexed_by<
               boost::multi_index::sequenced<>,
               boost::multi_index::hashed_unique<
                  boost::multi_index::member<
                     Queable_ofstream, val_vector, &Queable_ofstream::sv_
                  >
               >
            >
   > map_t;
   typedef map_t::iterator seq_iterator;
   typedef map_t::nth_index<1>::type index;
   typedef index::iterator hash_iterator;
   typedef index::const_iterator hash_citerator;

public:
   typedef detail::Queable_ofstream_types::val_ref_vector val_ref_vector;
   typedef detail::Queable_ofstream_types::value_type value_type;

   struct Err : public base_exception {};

   template<class Range> explicit File_ostream_queue(
            Range const& r,
            std::string const& header = "",
            const format::Format fmt = format::unknown,
            const std::size_t sz = 100
   )
   : pt_(boost::begin(r), boost::end(r)),
     header_(header),
     vm_(),
     fmt_(
        fmt == format::unknown ?
                 guess_compression_format(pt_.back()).second :
                 fmt
     ),
     sz_(sz)
   {}

   File_ostream& ostream(val_ref_vector const& v);
   std::size_t size() const {return vm_.size();}

private:
   path_template pt_;
   std::string header_;
   map_t vm_;
   format::Format fmt_;
   std::size_t sz_;
};

}//namespace vdj_pipe
#endif /* FILE_OSTREAM_QUEUE_HPP_ */
