/** @file "/vdj_pipe/include/vdj_pipe/file_ostream_variant.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef FILE_OSTREAM_VARIANT_HPP_
#define FILE_OSTREAM_VARIANT_HPP_
#include <string>
#include <vector>
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/queable_ofstream_types.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{
class File_ostream_queue;
class File_ostream;

/**@brief Select output file based on variables in value map
@details
File_ostream_variant is constructed from a path string that may contain
{\a var } elements, where \a var is a variable name available from vm value map.
*******************************************************************************/
class VDJ_PIPE_DECL File_ostream_variant {
   typedef detail::Queable_ofstream_types::value_type value_type;
   typedef detail::Queable_ofstream_types::val_ref_vector val_ref_vector;
   typedef detail::Queable_ofstream_types::path_template path_template;
   typedef detail::Queable_ofstream_types::val_vector val_vector;

public:
   struct Err : public base_exception {};

   /**@return string that cannot be part of filesystem path */
   static std::string const& skip_empty() {
      static const std::string s = "<?|>";
      return s;
   }

   File_ostream_variant(const format::Format fmt);

   /**
   @param path path string that may contain {\a var } elements, where
   \a var is a variable name available from vm value map
   @param unset_element
   @param vm
   @param header
   @param compr
   @param fmt
   @param queue_size
    */
   File_ostream_variant(
            std::string const& path,
            std::string const& unset_element,
            Value_map const& vm,
            std::string const& header = "",
            const compression::Compression compr = compression::unknown,
            format::Format fmt = format::unknown,
            const std::size_t queue_size = 500
   );

   File_ostream_variant(
            std::vector<Val_id> const& var_ids,
            path_template const& pt,
            std::string const& unset_element,
            std::string const& header = "",
            const compression::Compression compr = compression::unknown,
            format::Format fmt = format::unknown,
            const std::size_t queue_size = 500
   );

   File_ostream& ostream() {return *fos_;}
   File_ostream& ostream(Value_map const& vm);
   File_ostream& ostream(val_vector const& vv);

   /**@return number of files */
   std::size_t size() const;
   std::vector<Val_id> const& variable_ids() const {return idv_;}

private:
   value_type unset_element_;

   //if true, no output if one of variables is blank
   bool skip_empty_;

   std::vector<Val_id> idv_;
   boost::shared_ptr<File_ostream> fos_;
   boost::shared_ptr<File_ostream_queue> fosq_;

   void init_single(
            std::string const& path,
            std::string const& header,
            const compression::Compression compr,
            format::Format fmt
   );

   void init_queue(
            std::string const& path,
            Value_map const& vm,
            std::string const& header,
            format::Format fmt,
            const std::size_t queue_size
   );

   void init_queue(
            path_template const& pt,
            std::string const& header,
            format::Format fmt,
            const std::size_t queue_size
   );
};

}//namespace vdj_pipe
#endif /* FILE_OSTREAM_VARIANT_HPP_ */
