/** @file "/vdj_pipe/include/vdj_pipe/file_properties.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef FILE_PROPERTIES_HPP_
#define FILE_PROPERTIES_HPP_
#include <iosfwd>
#include <string>
#include "vdj_pipe/config.hpp"

namespace vdj_pipe{

namespace compression {

/**@brief File compression types
*******************************************************************************/
enum Compression {
   none, unknown, gzip,
   bzip2, zlib, pkzip, rar, seven_z
};

}//namespace compression

/**@brief guess file compression from low case extension string
@details Example ".gz" ".bz2"
*******************************************************************************/
VDJ_PIPE_DECL compression::Compression
extension_to_compression(std::string const& ext);

/**@brief guess file compression from magic number
@details Example ".gz" ".bz2"
*******************************************************************************/
VDJ_PIPE_DECL compression::Compression
compression_magic(std::istream& is);

namespace format {

/**@brief File format types
*******************************************************************************/
enum Format {
   unknown, Fasta, Fastq, Qual, CSV
};

}//namespace format

/**@brief guess file format from low case extension string
@details Example ".fq" ".fasta"
*******************************************************************************/
VDJ_PIPE_DECL format::Format
extension_to_format(std::string const& ext);

namespace plot {

/**@brief File format types
*******************************************************************************/
enum Plot {
   bar, line, bar_n_wisker, heat_map
};

}//namespace plot

}//namespace vdj_pipe
#endif /* FILE_PROPERTIES_HPP_ */
