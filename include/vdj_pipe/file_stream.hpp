/** @file "/vdj_pipe/include/vdj_pipe/file_stream.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef FILE_STREAM_HPP_
#define FILE_STREAM_HPP_
#include <string>
#include <fstream>
#include "boost/iostreams/filtering_streambuf.hpp"
#include "boost/scoped_ptr.hpp"
#include "boost/utility/string_ref.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{
class File_output;

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL File_ostream : public File_output {
   typedef boost::scoped_ptr<std::ofstream> ofs_ptr;
   typedef boost::scoped_ptr<std::ostream> os_ptr;
   typedef boost::iostreams::filtering_ostreambuf fosb_t;
   typedef boost::scoped_ptr<fosb_t> fosb_ptr;

   static fosb_t* make_fosb(
            std::ostream& os,
            const compression::Compression compr
   );

   static std::ios_base::openmode guess_mode(
      const std::ios_base::openmode mode,
      const compression::Compression compr
   );

public:
   struct Err : public base_exception {};

   File_ostream(const format::Format fmt);

   explicit File_ostream(
      File_output const& fout,
      const std::ios_base::openmode mode = (std::ios_base::openmode)0
   );

   std::ostream& ostream() {return os_;}

   void write(
            std::string const& descr,
            Seq_record::sequence const& seq
   );

   void write(
            std::string const& descr,
            const boost::string_ref seq
   );

   void write(
            std::string const& descr,
            Qual_record::quality const& qual
   );

   void write(
            std::string const& descr,
            Seq_record::sequence const& seq,
            Qual_record::quality const& qual
   );

private:
   ofs_ptr ofs_;
   fosb_ptr fosb_;
   os_ptr fosb_os_;
   std::ostream& os_;
};

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL File_istream {
   typedef boost::scoped_ptr<std::istream> is_ptr;
   typedef boost::scoped_ptr<std::ifstream> ifs_ptr;
   typedef boost::iostreams::filtering_istreambuf fisb_t;
   typedef boost::scoped_ptr<fisb_t> fisb_ptr;

   static fisb_t* make_fisb(
            std::istream& is,
            const compression::Compression compr
   );

public:
   struct Err : public base_exception {};
   explicit File_istream(File_input const& fin);

   explicit File_istream(
            std::istream& is,
            const compression::Compression compr = compression::none
   );

   std::istream& istream() {return is_;}
   bool good() const {return is_;}

private:
   ifs_ptr ifs_;
   fisb_ptr fisb_;
   is_ptr fisb_is_;
   std::istream& is_;
};

}//namespace vdj_pipe
#endif /* FILE_STREAM_HPP_ */
