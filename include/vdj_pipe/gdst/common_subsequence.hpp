/** @file "/vdj_pipe/include/vdj_pipe/gdst/common_subsequence.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef COMMON_SUBSEQUENCE_HPP_
#define COMMON_SUBSEQUENCE_HPP_
//#include "boost/container/flat_set.hpp"
#include "boost/assert.hpp"
#include "vdj_pipe/detail/vector_set.hpp"
#include "vdj_pipe/sequence_position.hpp"

namespace vdj_pipe{ namespace gdst{

/**@brief Common subsequence
*******************************************************************************/
struct Common_subseq {
   explicit Common_subseq(const unsigned start = 0, const unsigned len = 0)
   : start_(start), len_(len)
   {}

   unsigned start_; ///< match starting position in pattern
   unsigned len_; ///< match length
   /** set of sequence IDs and match starting positions */
//   boost::container::flat_set<super_seq> seq_;
   detail::Vector_set<super_seq> seq_;
};

/**@brief
@param start1 start of the aligned region in the first sequence
@param len1 length of the first sequence
@param start2 start of the aligned region in the second sequence
@param len2 length of the second sequence
@param match_len length of the aligned region
@return true if two aligned sequences have one or both mismatching ends
*******************************************************************************/
inline bool mismatching_end(
         const unsigned start1,
         const unsigned len1,
         const unsigned start2,
         const unsigned len2,
         const unsigned match_len
) {
   BOOST_ASSERT(start1 + match_len <= len1);
   BOOST_ASSERT(start2 + match_len <= len2);
   return
            (start1 && start2) ||
            ( start1 + match_len != len1 && start2 + match_len != len2 )
            ;
}

}//namespace gdst
}//namespace vdj_pipe
#endif /* COMMON_SUBSEQUENCE_HPP_ */
