/** @file "/vdj_pipe/include/vdj_pipe/gdst/depth_first_iterator.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef DEPTH_FIRST_ITERATOR_HPP_
#define DEPTH_FIRST_ITERATOR_HPP_
#include <stack>
#include <utility>
#include "boost/assert.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/gdst/node_ids.hpp"

namespace vdj_pipe{ namespace gdst{
class Gdst;

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Depth_iter {
   typedef std::pair<Branch_id, unsigned> branch_t;
   typedef std::stack<branch_t> stack;
public:
   Depth_iter(Gdst const& t, const Branch_id bid) : t_(t) {
      BOOST_ASSERT(bid);
      s_.push(branch_t(bid, 0));
   }

   void next();

   Branch_id id() const {
      BOOST_ASSERT( ! at_end() );
      return s_.top().first;
   }

   bool at_end() const {return s_.empty();}

private:
   Gdst const& t_;
   stack s_;
};

}//namespace gdst
}//namespace vdj_pipe
#endif /* DEPTH_FIRST_ITERATOR_HPP_ */
