/** @file "/vdj_pipe/include/vdj_pipe/gdst/gdst_stats.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef GDST_STATS_HPP_
#define GDST_STATS_HPP_
#include <vector>
#include "vdj_pipe/config.hpp"

namespace vdj_pipe{ namespace gdst{
class Gdst;

/**@brief 
*******************************************************************************/
struct VDJ_PIPE_DECL Gdst_stats {
   Gdst_stats(Gdst const& tree);

   /** number of nodes with children */
   unsigned nc_;

   /** number of nodes with leafs */
   unsigned nl_;

   /** number of nodes with children and leafs */
   unsigned ncl_;

   /** edge length histogram */
   std::vector<unsigned> edge_len_;

   /** sequence length histogram */
   std::vector<unsigned> seq_len_;

   /** number of sequence leafs per node histogram */
   std::vector<unsigned> n_leafs_;
};

}//namespace gdst
}//namespace vdj_pipe
#endif /* GDST_STATS_HPP_ */
