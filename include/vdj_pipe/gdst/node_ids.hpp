/** @file "/vdj_pipe/include/vdj_pipe/gdst/node_ids.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef NODE_IDS_HPP_
#define NODE_IDS_HPP_
#include "vdj_pipe/detail/object_id_base.hpp"

namespace vdj_pipe{ namespace gdst{

VDJ_PIPE_OBJECT_ID(Leaf_id);

VDJ_PIPE_OBJECT_ID(Branch_id);

VDJ_PIPE_OBJECT_ID(Children_id);

}//namespace gdst
}//namespace vdj_pipe
#endif /* NODE_IDS_HPP_ */
