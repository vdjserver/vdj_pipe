/** @file "/vdj_pipe/include/vdj_pipe/gdst/nodes.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef NODES_HPP_
#define NODES_HPP_
#include "boost/array.hpp"
#include "vdj_pipe/detail/vector_set.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/nucleotide_index.hpp"
#include "vdj_pipe/gdst/node_ids.hpp"

namespace vdj_pipe{ namespace gdst{

/**@brief Store suffixes
Each leaf is attached to a branch and stores suffixes that have the length
of the branch' index.
Since the length of the suffixes is known, only the sequence ID is stored
*******************************************************************************/
class Leaf : public detail::Vector_set<Seq_id> {};

/**@brief
*******************************************************************************/
class Children {
   typedef boost::array<Branch_id,4> stor;

   static stor const& init() {
      const static stor x =
      {{Branch_id(),Branch_id(),Branch_id(),Branch_id()}};
      return x;
   }

public:
   Children() : b_(init()) {}
   Branch_id const& operator[](const Nucleotide n) const {return b_[n];}
   Branch_id& operator[](const Nucleotide n) {return b_[n];}
private:
   stor b_;
};

/**@brief
*******************************************************************************/
struct Branch {
   static const unsigned npos = static_cast<unsigned>(-1);
   explicit Branch(const unsigned n)
   : n_(n),
     c_(),
     sb_(),
     leaf_(),
     sl_()
   {}

   /** Suffix index for branching child nodes
   */
   unsigned n_;

   Children_id c_;

   /** Branch node containing the suffix for which this branch was created.
   This is used when the index of the parent branch is < n_ - 1
   */
   Branch_id sb_;

   /** only suffixes of length n_ may be present */
   Leaf_id leaf_;

   Branch_id sl_; /**< suffix link */

};

}//namespace gdst
}//namespace vdj_pipe
#endif /* NODES_HPP_ */
