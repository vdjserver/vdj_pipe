/** @file "/vdj_pipe/include/vdj_pipe/gdst/pattern_iterator.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PATTERN_ITERATOR_HPP_
#define PATTERN_ITERATOR_HPP_
#include "boost/assert.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/gdst/node_ids.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/nucleotide_index.hpp"

namespace vdj_pipe{ namespace gdst{
class Gdst;

/**@brief Navigate suffix tree according to pattern
*******************************************************************************/
class VDJ_PIPE_DECL Pattern_iter {
public:
   Pattern_iter(
            Gdst const& t,
            const boost::string_ref suff,
            const Branch_id bid
   ) : t_(t), patt_(suff), curr_(bid)
   {
      BOOST_ASSERT(curr_);
      check_state();
   }

   Branch_id current() const {return curr_;}

   /**@return true if current node is at the end of the pattern*/
   bool at_end() const {return at_end_;}

   Branch_id next_id() const {return next_;}
   bool at_pattern_end() const {return at_pattern_end_;}
   bool at_mismatch() const {return mismatch_;}
   unsigned index() const {return pi_;}
   Nucleotide pattern_char() const {return nucleotide_index(patt_[pi_]);}
   Nucleotide edge_char() const {return ec_;}
   Nucleotide node_char() const {return nc_;}

   bool has_next() const {
      return
               ! at_end() &&
               next_id() &&
               ! at_pattern_end() &&
               ! at_mismatch()
               ;
   }

   void next() {
      BOOST_ASSERT(has_next());
      curr_ = next_id();
      check_state();
   }

private:
   Gdst const& t_;
   const boost::string_ref patt_;
   Branch_id curr_;
   bool at_end_;
   Branch_id next_;
   bool at_pattern_end_;
   bool mismatch_;
   unsigned pi_;
   Nucleotide ec_;
   Nucleotide nc_;

   void check_state();
};

}//namespace gdst
}//namespace vdj_pipe
#endif /* PATTERN_ITERATOR_HPP_ */
