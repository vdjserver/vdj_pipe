/** @file "/vdj_pipe/include/vdj_pipe/gdst/print_gdst.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PRINT_GDST_HPP_
#define PRINT_GDST_HPP_
#include <iosfwd>
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/gdst/node_ids.hpp"

namespace vdj_pipe{ namespace gdst{
class Gdst;

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::ostream& operator<<(std::ostream& os, Gdst const& st);

/**@brief 
*******************************************************************************/
VDJ_PIPE_DECL std::ostream& print(std::ostream& os, const Branch_id bid, Gdst const& st);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::ostream& dot(
         std::ostream& os,
         Gdst const& st,
         const bool sl = true
);

}//namespace gdst
}//namespace vdj_pipe
#endif /* PRINT_GDST_HPP_ */
