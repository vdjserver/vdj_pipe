/** @file "/vdj_pipe/include/vdj_pipe/gdst_search.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef GDST_SEARCH_HPP_
#define GDST_SEARCH_HPP_
#include <string>
#include <vector>
#include "boost/algorithm/string/predicate.hpp" //all
#include "boost/algorithm/string/classification.hpp" //operator!
#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "vdj_pipe/detail/disable_warnings.hpp"
#include "vdj_pipe/gdst/gdst.hpp"
#include "vdj_pipe/sequence_store.hpp"

namespace vdj_pipe{

/**@brief identify unique sequences in sequence store, insert them into
suffix tree, and remove non-unique ones
*******************************************************************************/
template<class MinLength> inline void remove_subsequences(
         Seq_store& ss,
         gdst::Gdst& st,
         MinLength const& min_len
) {
   if( ss.empty() ) return;
   const unsigned shortest = ss[ss.by_size().front()].size();
   std::vector<Seq_id> sidv;
   for( unsigned len = ss[ss.by_size().back()].size(); len >= shortest; --len ) {
      BOOST_FOREACH(const Seq_id sid, ss.by_size(len)) sidv.push_back(sid);
      BOOST_FOREACH(const Seq_id sid, sidv) {
         std::string const& seq = ss[sid].sequence();
         if( ! all(seq, ! Is_ambiguous()) ) continue;
         const gdst::Common_subseq cs =
                  st.find_longest(seq, min_len(seq.size()));
         if( cs.seq_.empty() ) {
            st.insert(sid);
         } else {
            ss.remove_subsequence(sid, cs.seq_);
         }
      }
      sidv.clear();
   }
}

}//namespace vdj_pipe
#endif /* GDST_SEARCH_HPP_ */
