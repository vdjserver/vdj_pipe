/** @file "/vdj_pipe/include/vdj_pipe/histogram_1d.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef HISTOGRAM_1D_HPP_
#define HISTOGRAM_1D_HPP_
#include <vector>
#include <iosfwd>
#include <ostream>

namespace vdj_pipe{

/**@brief simple unsigned integer-based histogram
*******************************************************************************/
class Histogram_1d {
public:

   void add(const unsigned val) {
      if( v_.size() <= val ) v_.resize(val + 1, 0);
      ++v_[val];
   }

   std::ostream& print(std::ostream& os, const char delim) const {
      for(std::size_t n = 0; n != v_.size(); ++n) {
         os << n << delim << v_[n] << '\n';
      }
      return os;
   }

private:
   std::vector<unsigned> v_;
};

}//namespace vdj_pipe
#endif /* HISTOGRAM_1D_HPP_ */
