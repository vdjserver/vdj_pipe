/** @file "/vdj_pipe/include/vdj_pipe/input_manager.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef INPUT_MANAGER_HPP_
#define INPUT_MANAGER_HPP_
#include <string>
#include <vector>
#include "boost/filesystem/path.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file_properties.hpp"
#include "vdj_pipe/sequence_file_entry.hpp"

namespace vdj_pipe{
class Value_map;

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Input_manager {
   static std::string check_dir(std::string const& dir);

   typedef std::vector<Seq_file_entry> in_v_t;
public:
   typedef in_v_t::const_iterator iterator;
   typedef in_v_t::const_iterator const_iterator;
   struct Err : public base_exception {};
   static std::string path(std::string const& root, std::string const& fn);

   Input_manager(
            std::string const& base,
            boost::property_tree::ptree const& pt,
            Value_map& vm
   );

   Input_manager(
            std::string const& base,
            const bool is_paired = false,
            const bool has_emid = false,
            const bool has_qual = true
   )
   : root_(base),
     is_paired_(is_paired),
     has_emid_(has_emid),
     has_qual_(has_qual)
   {}

   std::size_t size() const {return iv_.size();}
   bool empty() const {return iv_.empty();}
   const_iterator begin() const {return iv_.begin();}
   const_iterator end() const {return iv_.end();}
   std::string path(std::string const& fn) const {return path(root_, fn);}

   /**@return true if all input entries are paired */
   bool is_paired() const {return is_paired_;}

   /**@return true if all input entries include eMID files */
   bool has_emid() const {return has_emid_;}

   /**@return true if all input entries have quality */
   bool has_quality() const {return has_qual_;}

private:
   std::string root_;
   in_v_t iv_;
   bool is_paired_;
   bool has_emid_;
   bool has_qual_;

};

}//namespace vdj_pipe
#endif /* INPUT_MANAGER_HPP_ */
