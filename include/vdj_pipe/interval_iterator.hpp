/** @file "/vdj_pipe/include/vdj_pipe/interval_iterator.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef INTERVAL_ITERATOR_HPP_
#define INTERVAL_ITERATOR_HPP_
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/nucleotide_index.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
class Unambiguous_interval_iter {
public:
   Unambiguous_interval_iter(
            const boost::string_ref seq,
            const std::size_t min_len
   )
   : min_len_(min_len), seq_(seq), pos_(0), len_(0)
   {
      next();
   }

   void next() {
      pos_ += len_;
      for( len_ = 0; ; ) {
         if( pos_ + len_ == seq_.size() ) {
            if( len_ < min_len_ ) {
               pos_ = seq_.size();
               len_ = 0;
            }
            return;
         }
         if( is_ambiguous(seq_[pos_ + len_]) ) {
            if( len_ < min_len_ ) {
               pos_ += len_ + 1;
               len_ = 0;
               continue;
            }
            return;
         }
         ++len_;
      }
   }

   boost::string_ref subseq() const {
      return boost::string_ref(seq_.substr(pos_, len_));
   }

   bool has_subseq() const {return len_;}
private:
   std::size_t min_len_;
   boost::string_ref seq_;
   std::size_t pos_;
   std::size_t len_;
};


}//namespace vdj_pipe
#endif /* INTERVAL_ITERATOR_HPP_ */
