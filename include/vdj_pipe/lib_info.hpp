/** @file "/vdj_pipe/include/vdj_pipe/lib_info.hpp"
part of vdj_pipe project.
@n Distributed under GNU General Public License, Version 3; see doc/license.txt.
@date 2013 @author Mikhail K Levin
@details
Main doxygen page and library info API.
*******************************************************************************/
#ifndef VDJ_PIPELIB_INFO_HPP_
#define VDJ_PIPELIB_INFO_HPP_
#include <string>
#include <iosfwd>
#include "vdj_pipe/config.hpp"

/**
@mainpage vdj_pipe

@namespace vdj_pipe
@brief Main namespace of %vdj_pipe library

@namespace vdj_pipe::test
@brief Namespace for unit-tests

*/

namespace vdj_pipe{

/**@brief library info
*******************************************************************************/
struct VDJ_PIPE_DECL Lib_info {
   static std::string const& name();
   static std::string const& version();
   static bool const& stamp_flag();
   static void set_stamp_flag(bool aFlag);
   static std::string const& description();
   static std::string const& copyright();
   static std::string const& license();
   static std::string const& other_libs();
   static std::string const& compiler();
   static int version_1();
   static int version_2();
   static int version_3();
   static std::string const& version_e();
   static int build();

   template<class Ch, class Tr> static std::basic_ostream<Ch,Tr>&
   print(std::basic_ostream<Ch,Tr>& os) {
      os << name() << " - " << description() << ' ';
      return print_version(os);
   }

   template<class Ch, class Tr> static std::basic_ostream<Ch,Tr>&
   print_version(std::basic_ostream<Ch,Tr>& os) {
      os
      << version()
      << ", " << compiler()
      << ", " << other_libs()
      << ", build:" << build()
      ;
      return os;
   }
};

}//namespace vdj_pipe
#endif /* VDJ_PIPELIB_INFO_HPP_ */
