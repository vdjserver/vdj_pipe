/** @file "/vdj_pipe/include/vdj_pipe/min_match_length.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef MIN_MATCH_LENGTH_HPP_
#define MIN_MATCH_LENGTH_HPP_
#include <algorithm>
#include <functional>
#include "vdj_pipe/exception.hpp"

namespace vdj_pipe{

/**@brief Require full sequence to match
*******************************************************************************/
struct Match_full_length :
         public std::unary_function<std::string, std::size_t> {
   int operator()(const int sz) const {return sz;}
};

/**@brief Set a minimal sequence length to match
*******************************************************************************/
class Match_min_length :
         public std::unary_function<std::string, std::size_t> {
public:
   Match_min_length(const std::size_t min)
   : min_(min)
   {}

   int operator()(const int sz) const {
      return std::min(sz, min_);
   }

private:
   int min_;
};

/**@brief Set a minimal sequence length to match allowing for some mismatch at
ends
*******************************************************************************/
class Match_ignore_ends :
         public std::unary_function<std::string, std::size_t> {
public:
   Match_ignore_ends(const int min, const int end)
   : min_(min), end_(end)
   {}

   int operator()(const int sz) const {
      return std::min(
               sz,
               std::max(
                        min_,
                        sz - end_
               )
      );
   }

private:
   int min_;
   int end_;
};

/**@brief Set a fraction of sequence length to match
*******************************************************************************/
class Match_fraction_length :
         public std::unary_function<std::string, std::size_t> {
public:
   struct Err : public base_exception {};
   Match_fraction_length(const int min, const double f)
   : min_(min), f_(f)
   {
      BOOST_ASSERT(f_ > 0.0 && f_ <= 1.0 && "invalid fraction");
   }

   int operator()(const int sz) const {
      return std::min(
               sz,
               std::max(
                        min_,
                        (int)(sz * f_ + 0.5)
               )
      );
   }

private:
   int min_;
   double f_;
};

/**@brief Set a fraction of sequence length to match
*******************************************************************************/
class Get_match_length :
         public std::unary_function<std::string, std::size_t> {
public:
   struct Err : public base_exception {};

   Get_match_length(
            const int min = 0,
            const int end = 0,
            const double f = 0.0
   )
   : min_(min),
     end_(end),
     f_(f)
   {
      if( min_ == 0 && end_ == 0 && f_ == 0.0 ) f_ = 1.0;

      if( min_ < 0 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid minimal match length")
               << Err::int1_t(min_)
      );

      if( end_ < 0 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid ignore end length")
               << Err::int1_t(end_)
      );

      if( f_ < 0.0 || f_ > 1.0 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("invalid minimal match fraction")
               << Err::float1_t(f_)
      );
   }

   int operator()(const int sz) const {
      return std::min(
               sz,
               std::max(
                        min_,
                        std::max(
                                 sz - end_,
                                 (int)(sz * f_ + 0.5)
                        )
               )
      );
   }

private:
   int min_;
   int end_;
   double f_;
};

}//namespace vdj_pipe
#endif /* MIN_MATCH_LENGTH_HPP_ */
