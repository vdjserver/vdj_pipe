/** @file "/vdj_pipe/include/vdj_pipe/object_ids.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef OBJECT_IDS_HPP_
#define OBJECT_IDS_HPP_
#include "vdj_pipe/detail/object_id_base.hpp"

namespace vdj_pipe{

/** Mapped value ID */
VDJ_PIPE_OBJECT_ID(Val_id);

/** Molecular identifier ID */
VDJ_PIPE_OBJECT_ID(Mid_id);

/** Filesystem path ID */
VDJ_PIPE_OBJECT_ID(Path_id);

/** Sequencing read ID */
VDJ_PIPE_OBJECT_ID(Read_id);

/** Sequence ID */
VDJ_PIPE_OBJECT_ID(Seq_id);

VDJ_PIPE_OBJECT_ID(Mapped_id);

}//namespace vdj_pipe
#endif /* OBJECT_IDS_HPP_ */
