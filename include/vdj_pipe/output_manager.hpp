/** @file "/vdj_pipe/include/vdj_pipe/output_manager.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef OUTPUT_MANAGER_HPP_
#define OUTPUT_MANAGER_HPP_
#include <map>
#include <string>
#include "boost/filesystem/path.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file_properties.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Output_manager {
   typedef boost::filesystem::path path_t;
   static path_t make_dir(std::string const& dir);
   typedef std::map<std::string,plot::Plot> plot_type_map;
   typedef plot_type_map::value_type plot_pair;

public:
   struct Err : public base_exception {};

   explicit Output_manager(
            std::string const& base_out,
            std::string const& plot_list_path = "",
            const char delim = '\t'
   )
   : root_(make_dir(base_out)),
     plot_list_(path(plot_list_path)),
     csv_delim_(delim)
   {}

   char delimiter() const {return csv_delim_;}

   std::string path(std::string const& fn) const {
      if( root_.empty() ) return fn;
      const path_t p(fn);
      if( p.is_absolute() ) return p.string();
      return (root_ / p).string();
   }

   void register_plot(std::string const& path, const plot::Plot pt) {
      if( ! ptm_.insert(plot_pair(path, pt)).second ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("output already registered")
               << Err::str1_t(sanitize(path))
      );
   }

   /** get path and register file as plot */
   std::string path(std::string const& fn, const plot::Plot pt) {
      const std::string p = path(fn);
      register_plot(p, pt);
      return p;
   }

   void finish() const;

private:
   path_t root_;
   plot_type_map ptm_;
   std::string plot_list_;
   char csv_delim_;
};

}//namespace vdj_pipe
#endif /* OUTPUT_MANAGER_HPP_ */
