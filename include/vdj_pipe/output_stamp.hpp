/** @file "/vdj_pipe/include/vdj_pipe/output_stamp.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef OUTPUT_STAMP_HPP_
#define OUTPUT_STAMP_HPP_
#include <iosfwd>
#include <string>
#include "vdj_pipe/config.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
VDJ_PIPE_DECL std::ostream& stamp(
         std::ostream& os,
         std::string const& pref = "## ",
         std::string const& delim = "; ",
         std::string const& suff = "\n"
);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::string stamp(
         std::string const& pref = "## ",
         std::string const& delim = "; ",
         std::string const& suff = "\n"
);


}//namespace vdj_pipe
#endif /* OUTPUT_STAMP_HPP_ */
