/** @file "/vdj_pipe/include/vdj_pipe/parser_fasta.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PARSER_FASTA_HPP_
#define PARSER_FASTA_HPP_
#include "vdj_pipe/detail/parser_line.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{

/**
*******************************************************************************/
class Parser_fasta : public detail::Parser_line {
public:
   typedef Seq_record record;
   typedef record::sequence sequence;

   explicit Parser_fasta(File_input const& fi)
   : detail::Parser_line(fi)
   {
      if( fi.format() != format::Fasta ) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("wrong file format for FASTA parser")
         << Err::str1_t(sanitize(fi.path()))
         << Err::int1_t(fi.format())
      );
      next_record();
   }

   explicit Parser_fasta(
            std::istream& is,
            const compression::Compression compr = compression::none
   )
   : detail::Parser_line(is, compr)
   {
      next_record();
   }

   const boost::string_ref get_id() {return Parser_line::get_id('>');}
   void next_record() {Parser_line::seek_line('>');}
   const boost::string_ref get_defstr() {return Parser_line::get_defstr('>');}
   sequence get_sequence() {return Parser_line::get_sequence('>');}

   record get_record() {
      record sr;
      Parser_line::set_meta(sr, '>');
      sr.seq_ = get_sequence();
      return sr;
   }
};

}//namespace vdj_pipe
#endif /* PARSER_FASTA_HPP_ */
