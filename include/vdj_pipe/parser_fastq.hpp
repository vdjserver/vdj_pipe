/** @file "/vdj_pipe/include/vdj_pipe/parser_fastq.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PARSER_FASTQ_HPP_
#define PARSER_FASTQ_HPP_
#include <vector>
#include "boost/foreach.hpp"
#include "vdj_pipe/detail/parser_line.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{

/**@brief Parser for FASTQ files
@details Read, possibly compressed, FASTQ files
*******************************************************************************/
class Parser_fastq : public detail::Parser_line {
   enum State {Def, Seq, Qual, End};
public:
   typedef Seq_qual_record record;
   typedef record::sequence sequence;
   typedef record::quality quality;
   static const char qual_offset = -33;

   explicit Parser_fastq(File_input const& fi, const char offset = qual_offset)
   : detail::Parser_line(fi), offset_(offset), state_(End)
   {
      if( fi.format() != format::Fastq ) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("wrong file format for FASTQ parser")
         << Err::str1_t(sanitize(fi.path()))
         << Err::int1_t(fi.format())
      );
      next_record();
   }

   explicit Parser_fastq(
            std::istream& is,
            const compression::Compression compr = compression::none,
            const char offset = qual_offset
   )
   : detail::Parser_line(is, compr), offset_(offset), state_(End)
   {
      next_record();
   }

   void next_record() {
      if( ! has_next() ) return;
      switch (state_) {
      case Def:
         skip_line();
         if( ! has_next() ) return;
         /* no break */
      case Seq:
         skip_line();
         if( ! has_next() ) return;
         Parser_line::seek_line('+');
         if( ! has_next() ) return;
         /* no break */
      case Qual:
         skip_line();
         if( ! has_next() ) return;
         skip_line();
         if( ! has_next() ) return;
         /* no break */
      case End:
         state_ = Def;
      }
      Parser_line::seek_line('@');
   }

   const boost::string_ref get_id() {
      if( state_ != Def ) next_record();
      if( ! has_next() ) return "";
      state_ = Seq;
      return Parser_line::get_id('@');
   }

   const boost::string_ref get_defstr() {
      if( state_ != Def ) next_record();
      if( ! has_next() ) return "";
      state_ = Seq;
      return Parser_line::get_defstr('@');
   }

   sequence get_sequence() {
      if( ! has_next() ) return "";
      switch (state_) {
      case Qual:
      case End:
         next_record();
         if( ! has_next() ) return "";
         /* no break */
      case Def:
         skip_line();
         if( ! has_next() ) return "";
         /* no break */
         case Seq: break;
      }
      state_ = Qual;
      return Parser_line::get_sequence('+');
   }

   quality get_qual() {
      quality q;
      get_qual(back_inserter(q));
      return q;
   }

   template<class InsertIter> void get_qual(InsertIter i) {
      if( ! has_next() ) return;
      switch (state_) {
      case End:
         next_record();
         if( ! has_next() ) return;
         /* no break */
      case Def:
         skip_line();
         if( ! has_next() ) return;
         /* no break */
      case Seq:
         skip_line();
         if( ! has_next() ) return;
         Parser_line::seek_line('+');
         if( ! has_next() ) return;
         /* no break */
      case Qual:
         skip_line();
         if( ! has_next() ) return;
      }
      getline(fis_.istream(), str_);
      ++line_;
      BOOST_FOREACH(const char c, str_) {
         if( c < '!' || c > '~') BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("invalid quality character")
                  << Err::str1_t(sanitize(c))
                  << Err::line_t(line_num() - 1)
         );
         i = c + offset_;
      }
      state_ = End;
   }

   record get_record() {
      record qr;
      set_meta(qr, '@');
      state_ = Seq;
      qr.seq_ = get_sequence();
      get_qual(back_inserter(qr.qual_));

      if( qr.seq_.size() != qr.qual_.size() ) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("sequence-quality size mismatch")
         << Err::str1_t(sanitize(qr.id_, 60))
         << Err::int1_t(qr.seq_.size())
         << Err::int2_t(qr.qual_.size())
         << Err::line_t(line_num() - 1)
      );
      return qr;
   }

private:
   const char offset_;
   State state_;
};

}//namespace vdj_pipe
#endif /* PARSER_FASTQ_HPP_ */
