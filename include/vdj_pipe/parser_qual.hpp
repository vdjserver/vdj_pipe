/** @file "/vdj_pipe/include/vdj_pipe/parser_qual.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PARSER_QUAL_HPP_
#define PARSER_QUAL_HPP_
#include <vector>
#include "boost/lexical_cast.hpp"
#include "boost/tokenizer.hpp"
#include "vdj_pipe/detail/parser_line.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class Parser_qual : public detail::Parser_line {
public:
   typedef Qual_record record;
   typedef record::quality quality;

   explicit Parser_qual(File_input const& fi)
   : detail::Parser_line(fi)
   {
      if( fi.format() != format::Qual ) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("wrong file format for quality score parser")
         << Err::str1_t(sanitize(fi.path()))
         << Err::int1_t(fi.format())
      );
      next_record();
   }

   explicit Parser_qual(
            std::istream& is,
            const compression::Compression compr = compression::none
   )
   : detail::Parser_line(is, compr)
   {
      next_record();
   }

   const boost::string_ref get_id() {return Parser_line::get_id('>');}
   void next_record() {Parser_line::seek_line('>');}
   const boost::string_ref get_defstr() {return Parser_line::get_defstr('>');}

   quality get_qual() {
      quality q;
      get_qual(back_inserter(q));
      return q;
   }

   template<class InsertIter> void get_qual(InsertIter ii) {
      while( fis_.istream().peek() != '>' && getline(fis_.istream(), str_) ) {
         ++line_;
         typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
         boost::char_separator<char> sep(" \t");
         tokenizer tok(str_, sep);
         for(tokenizer::const_iterator ti = tok.begin(); ! ti.at_end(); ++ti) {
            try{
               const int i = boost::lexical_cast<int>(*ti);
               ii = boost::numeric_cast<quality::value_type>(i);
            } catch(std::bad_cast const&) {
               BOOST_THROW_EXCEPTION(
                        Err()
                        << Err::msg_t("invalid quality score")
                        << Err::str1_t(sanitize(*ti, 60))
                        << Err::line_t(line_num() - 1)
               );
            }
         }
      }
   }

   record get_record() {
      record qr;
      set_meta(qr, '>');
      try{
         get_qual(back_inserter(qr.qual_));
      }catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("invalid quality score")
                  << Err::str1_t(sanitize(qr.id_, 60))
                  << Err::str2_t(sanitize(qr.comm_, 60))
                  << Err::nested_t(boost::current_exception())
         );
      }
      return qr;
   }
};

}//namespace vdj_pipe
#endif /* PARSER_QUAL_HPP_ */
