/** @file "/vdj_pipe/include/vdj_pipe/pipe_environment.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef PIPE_ENVIRONMENT_HPP_
#define PIPE_ENVIRONMENT_HPP_
#include <iosfwd>
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/shared_ptr.hpp"
#include "boost/chrono/process_cpu_clocks.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/input_manager.hpp"
#include "vdj_pipe/output_manager.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{
class Seq_file_map;
class Step_variant_store;

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Pipe_environment {
   typedef boost::shared_ptr<Input_manager> in_manager_ptr;
   typedef boost::shared_ptr<Output_manager> out_manager_ptr;
   BOOST_STATIC_CONSTANT(std::size_t, unset = static_cast<std::size_t>(-1));
   static void input_csv(
            boost::property_tree::ptree const& pt,
            std::vector<std::string>& vs
   );

public:
   struct Err : public base_exception {};

   Pipe_environment(
            boost::property_tree::ptree const& pt,
            Value_map const& vm
   );
   Step_variant_store const& steps() const {return *steps_;}
   Step_variant_store& steps() {return *steps_;}
   Input_manager const& input() const {return *im_;}
   Input_manager& input() {return *im_;}
   Output_manager const& output() const {return *om_;}
   Output_manager& output() {return *om_;}
   char delimiter() const {return om_->delimiter();}
   std::size_t max_reads() const {return max_reads_;}
   std::size_t max_file_reads() const {return max_file_reads_;}

   bool process_more() {
      if(
               max_reads() <= (std::size_t)vm_.read_count() ||
               max_file_reads() <= (std::size_t)vm_.file_read_count()
      ) {
         reads_skipped_ = true;
         return false;
      }
      return true;
   }

   std::size_t read_count() const;
   void finish();
   void process_read();
   void summary();

   void write_config(
            boost::property_tree::ptree const& pt,
            std::string const& path
   ) const;

   void start_file(std::string const& fn);
   void finish_file();

private:
   boost::shared_ptr<Step_variant_store> steps_;
   in_manager_ptr im_;
   out_manager_ptr om_;
   std::size_t max_reads_;
   std::size_t max_file_reads_;
   std::string summary_output_path_;
   boost::chrono::process_real_cpu_clock::time_point t1_, t2_, t3_;
   bool is_running_;
   bool reads_skipped_;
   Value_map vm_;

private:
   std::ostream& print_summary(std::ostream& os) const;

};

}//namespace vdj_pipe
#endif /* PIPE_ENVIRONMENT_HPP_ */
