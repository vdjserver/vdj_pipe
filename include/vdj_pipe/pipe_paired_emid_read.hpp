/** @file "/vdj_pipe/include/vdj_pipe/pipe_paired_emid_read.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PIPE_PAIRED_EMID_READ_HPP_
#define PIPE_PAIRED_EMID_READ_HPP_
#include <string>
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/value_map_access_paired.hpp"

namespace vdj_pipe{
class Seq_file;
class Parser_fastq;

/**@brief process paired reads with external MIDs
*******************************************************************************/
class VDJ_PIPE_DECL Pipe_paired_emid_read {
public:
   struct Err : public base_exception {};
   explicit Pipe_paired_emid_read(boost::property_tree::ptree const& pt);
   void run();
   Pipe_environment const& pe() const {return pe_;}

private:
   Vm_access_paired_emid vma_;
   Pipe_environment pe_;

   void process_fastq(Seq_file_entry const& sfe);
   void process_fasta_qual(Seq_file_entry const& sfe);

   void store_records(
            Parser_fastq& psf,
            Parser_fastq& psr,
            Parser_fastq& pmf,
            Parser_fastq& pmr
   );
};

}//namespace vdj_pipe
#endif /* PIPE_PAIRED_EMID_READ_HPP_ */
