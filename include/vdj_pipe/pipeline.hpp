/** @file "/vdj_pipe/include/vdj_pipe/pipeline.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef PIPELINE_HPP_
#define PIPELINE_HPP_
#include <vector>
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
#include "vdj_pipe/step/step_factory.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/step/visitor.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
template<class Config> class Pipeline {
   typedef typename Config::input_step input_step;
   typedef typename Config::processing_step processing_step;
   typedef typename Config::value_map_access vm_access;
   typedef std::vector<processing_step> step_vector;
public:
   static std::string const& name() {return Config::name();}

   explicit Pipeline(Pipe_environment const& pe) : pe_(pe) {}

   Pipeline(
            boost::property_tree::ptree const& pt,
            Pipe_environment const& pe
   )
   : pe_(pe),
     sv_(),
     vma_()
   {
      BOOST_FOREACH(
               boost::property_tree::ptree::value_type const& vt,
               pt.get_child(name())
      ) {
         add_step(vt);
      }
   }

   void add_step(boost::property_tree::ptree const& pt) {
      add_step(create_step<Config>(vma_, pt, pe_));
   }

   void add_step(processing_step const& step) {sv_.push_back(step);}

   vm_access const& value_map() const {return vma_;}
   vm_access& value_map() {return vma_;}

   void process_read() {
      try{
         Run_visitor rv;
         BOOST_FOREACH(processing_step& step, sv_) {
            boost::apply_visitor(rv, step);
         }
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  base_exception()
                  << base_exception::msg_t("error processing read")
                  << base_exception::str1_t(sanitize(vma_.read_id_nothrow()))
                  << base_exception::nested_t(boost::current_exception())
         );
      }
      vma_.clear_values();
   }

   std::size_t size() const {return sv_.size();}
   processing_step const& step(const std::size_t n) const {return sv_[n];}

   void run() {while( ins_.run() ) process_read();}

   void finalize() {
      Finish_visitor finish;
      BOOST_FOREACH(processing_step& step, sv_) {
         boost::apply_visitor(finish, step);
      }
   }

private:
   Pipe_environment pe_;
   input_step ins_;
   step_vector sv_;
   vm_access vma_;
};

}//namespace vdj_pipe
#endif /* PIPELINE_HPP_ */
