/** @file "/vdj_pipe/include/vdj_pipe/pipeline_input.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef PIPELINE_INPUT_HPP_
#define PIPELINE_INPUT_HPP_

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
template<class Config> class Pipeline_input {
   typedef typename Config::step_type step_type;
   typedef typename Config::value_map_access vm_access;
   typedef std::vector<step_type> step_vector;
public:


};

}//namespace vdj_pipe
#endif /* PIPELINE_INPUT_HPP_ */
