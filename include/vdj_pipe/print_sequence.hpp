/** @file "/vdj_pipe/include/vdj_pipe/print_sequence.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef PRINT_SEQUENCE_HPP_
#define PRINT_SEQUENCE_HPP_
#include <ostream>
#include <string>
#include <vector>
#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/utility/string_ref.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{

/**@brief print string splitting it into parts of equal length
@param os output stream
@param str input string
@param delim characters to insert between the parts of the string
@param length string part length
*******************************************************************************/
template<class Ch,class Tr> inline std::basic_ostream<Ch,Tr>&
print_split(
      std::basic_ostream<Ch,Tr>& os,
      const boost::string_ref str,
      const Ch delim,
      const unsigned length
) {
   std::size_t n0 = 0;
   for( ; str.size() > n0 + length; n0 += length) {
      os << str.substr(n0, length) << delim;
   }
   os << str.substr(n0);
   return os;
}

/**@brief print FASTA record
@param os output stream
@param name FASTA description string
@param sequence FASTA sequence string
@param length sequence line length
*******************************************************************************/
template<class Ch,class Tr> inline std::basic_ostream<Ch,Tr>& print_fasta(
      std::basic_ostream<Ch,Tr>& os,
      std::string const& name,
      Seq_record::sequence const& sequence,
      const unsigned length = 80
) {
   os << '>' << name << '\n';
   print_split(os, sequence, '\n', length);
   os << '\n';
   return os;
}

/**@brief print FASTA record
@param os output stream
@param name FASTA description string
@param sequence FASTA sequence string
@param length sequence line length
*******************************************************************************/
template<class Ch,class Tr> inline std::basic_ostream<Ch,Tr>& print_fasta(
      std::basic_ostream<Ch,Tr>& os,
      const std::string& name,
      const boost::string_ref sequence,
      const unsigned length = 80
) {
   os << '>' << name << '\n';
   print_split(os, sequence, '\n', length);
   os << '\n';
   return os;
}

/**@brief print QUAL record
@param os output stream
@param name FASTA description string
@param qual quality scores
@param length quality line length
*******************************************************************************/
template<class Ch,class Tr> inline std::basic_ostream<Ch,Tr>& print_qual(
      std::basic_ostream<Ch,Tr>& os,
      std::string const& name,
      Qual_record::quality const& qual,
      const unsigned length = 80
) {
   os << '>' << name;
   unsigned pos = length;
   typedef Qual_record::quality::value_type qual_type;
   BOOST_FOREACH(const qual_type q, qual) {
      const std::string qs = boost::lexical_cast<std::string>((int)q);
      if( (pos + qs.size() + 1) > length ) {
         os << '\n';
         pos = 0;
      } else {
         os << ' ';
         ++pos;
      }
      os << qs;
      pos += qs.size();
   }
   os << '\n';
   return os;
}

/**@brief print FASTQ record
@param os output stream
@param name FASTA description string
@param sequence FASTA sequence string
@param qual quality scores
@param offset quality score to char conversion offset
*******************************************************************************/
template<class Ch,class Tr> inline std::basic_ostream<Ch,Tr>& print_fastq(
      std::basic_ostream<Ch,Tr>& os,
      std::string const& name,
      Seq_record::sequence const& sequence,
      Qual_record::quality const& qual,
      const int offset = 33
) {
   BOOST_ASSERT(sequence.size() == qual.size());
   os
   << '@' << name << '\n'
   << sequence << '\n'
   << "+\n";
   BOOST_FOREACH(const int q, qual) {
      os << (char)(q + offset);
   }
   os << '\n';
   return os;
}

}//namespace vdj_pipe
#endif /* PRINT_SEQUENCE_HPP_ */
