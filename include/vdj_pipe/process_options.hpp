/** @file "/vdj_pipe/include/vdj_pipe/process_options.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef PROCESS_OPTIONS_HPP_
#define PROCESS_OPTIONS_HPP_
#include "vdj_pipe/config.hpp"
#include "boost/property_tree/ptree_fwd.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
VDJ_PIPE_DECL void process_options(boost::property_tree::ptree const& pt);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL void process_single_reads(boost::property_tree::ptree const& pt);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL void process_paired_reads(boost::property_tree::ptree const& pt);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL void process_paired_emid_reads(boost::property_tree::ptree const& pt);

}//namespace vdj_pipe
#endif /* PROCESS_OPTIONS_HPP_ */
