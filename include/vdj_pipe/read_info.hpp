/** @file "/vdj_pipe/include/vdj_pipe/read_info.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef READ_INFO_HPP_
#define READ_INFO_HPP_
#include <string>
#include "boost/assert.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/sequence_interval.hpp"
#include "vdj_pipe/detail/string_ref.hpp"

namespace vdj_pipe{
/**
*******************************************************************************/
class Read_info {
public:
   explicit Read_info(const boost::string_ref name)
   : name_(name.to_string()),
     reverse_(false),
     seq_file_(Path_id()),
     trim_(sequence_interval::whole())
   {}

   Read_info(
            const boost::string_ref name,
            const Path_id seq_file,
            const unsigned len,
            const bool reverse
   )
   : name_(name.to_string()),
     reverse_(reverse),
     seq_file_(seq_file),
     trim_(0, (int)len)
   {}

   const std::string& name() const {return name_;}
   unsigned size() const {return width(trim_);}
   bool empty() const {return boost::numeric::empty(trim_);}
   Path_id seq_file() const {return seq_file_;}
   bool is_reverse() const {return reverse_;}
   sequence_interval const& trim() const {return trim_;}

   Read_info& reverse(const bool rev) {
      reverse_ = rev;
      return *this;
   }

   Read_info& seq_file(const Path_id pid) {
      BOOST_ASSERT((! seq_file_) || (seq_file_ == pid));
      seq_file_ = pid;
      return *this;
   }

   Read_info& trim(const unsigned lo, const unsigned hi) {
      return trim(sequence_interval(lo,hi));
   }

   Read_info& trim(sequence_interval const& si) {
      trim_ = intersect(trim_, si);
      return *this;
   }

   void combine(Read_info const& si) {
      BOOST_ASSERT( name_ == si.name_ );
      reverse_ |= si.is_reverse();
      if( si.seq_file() ) seq_file(si.seq_file());
      trim_ = intersect(trim_, si.trim());
   }

private:
   std::string name_;
   bool reverse_;
   Path_id seq_file_;
   sequence_interval trim_;
};

template<class Ostr> inline Ostr& operator<<(Ostr& ostr, const Read_info si) {
   ostr << si.name() << '[' << si.size() << ']';
   return ostr;
}

}//namespace vdj_pipe
#endif /* READ_INFO_HPP_ */
