/** @file "/vdj_pipe/include/vdj_pipe/read_info_store.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef READ_INFO_STORE_HPP_
#define READ_INFO_STORE_HPP_
#include <limits>
#include "boost/assert.hpp"
#include "boost/multi_index_container.hpp"
#include "boost/multi_index/hashed_index.hpp"
#include "boost/multi_index/ordered_index.hpp"
#include "boost/range.hpp"
#include "boost/noncopyable.hpp"

#include "vdj_pipe/detail/disable_warnings.hpp"
#include "vdj_pipe/detail/get_by_id.hpp"
#include "vdj_pipe/detail/id_map.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/read_info.hpp"
#include "vdj_pipe/sequence_file_map.hpp"

namespace vdj_pipe{

/**@brief Store sequence-related information
*******************************************************************************/
class Read_info_store : boost::noncopyable {
   typedef detail::Id_map<Read_id, Read_info> info_map_t;

   typedef detail::Getter<
            info_map_t, Read_id, Read_info,
               std::string const&, &Read_info::name
            > get_name;

   typedef detail::Getter<
            info_map_t, Read_id, Read_info,
            unsigned, &Read_info::size
            > get_info_size;

   typedef boost::multi_index_container<
            Read_id,
            boost::multi_index::indexed_by<
               boost::multi_index::hashed_non_unique<
                  boost::multi_index::tag<struct name_tag>,
                  get_name
               >,
               boost::multi_index::ordered_non_unique<
                  boost::multi_index::tag<struct size_tag>,
                  get_info_size
               >
            >
   > info_mi_t;
   typedef info_mi_t::index<name_tag>::type name_index_t;
   typedef name_index_t::const_iterator name_citer_t;
   typedef name_index_t::iterator name_iter;
   typedef boost::iterator_range<name_iter> name_range;
   typedef info_mi_t::index<size_tag>::type info_size_index_t;

   static info_mi_t::ctor_args_list info_index_init(info_map_t const& im) {
      return boost::make_tuple(
               boost::make_tuple(
                        0,
                        get_name(im),
                        boost::hash<std::string>(),
                        std::equal_to<std::string>()
               ),
               boost::make_tuple(get_info_size(im), std::less<unsigned>())
      );
   }
public:
   typedef name_citer_t const_iterator;
   typedef info_size_index_t::const_iterator size_iterator;
   typedef boost::iterator_range<size_iterator> size_range;

   struct Err : public base_exception {};

   Read_info_store()
   : info_map_(Read_id(1)),
     info_index_(info_index_init(info_map_))
   {}

   std::size_t size() const {return info_index_.size();}
   bool empty() const {return info_index_.empty();}
   const_iterator begin() const {return info_index_.begin();}
   const_iterator end() const {return info_index_.end();}

   /**@return range of all sequence info IDs ordered by size */
   size_range by_size() const {return boost::make_iterator_range(info_index_.get<size_tag>());}

   /**@return range of sequence info IDs of % size size */
   size_range by_size(const unsigned size) const {
      return boost::make_iterator_range(
               info_index_.get<size_tag>().equal_range(size)
      );
   }

   /**@return range of sequence info IDs that have % size in the range [from, to) */
   size_range by_size_range(
            const unsigned from,
            const unsigned to = std::numeric_limits<unsigned>::max()
   ) const {
      return boost::make_iterator_range(
               info_index_.get<size_tag>().lower_bound(from),
               info_index_.get<size_tag>().upper_bound(to)
      );
   }

   Read_info const& operator[](const Read_id iid) const {return info_map_[iid];}

   Read_id const* find(const boost::string_ref id, const bool reverse) const {
      name_index_t const& ind = info_index_.get<name_tag>();
      const name_citer_t i = find_iter(id, reverse);
      return i == ind.end() ? 0 : &(*i);
   }

   Read_id insert(Read_info const& ri) {
      name_index_t& ind = info_index_.get<name_tag>();
      name_iter i = find_iter(ri.name(), ri.is_reverse());
      if( i == ind.end() ) {
         const Read_id rid = info_map_.insert(ri);
         info_index_.insert(rid);
         return rid;
      }
      const Read_id rid = *i;
      info_map_[rid].combine(ri);
      ind.replace(i, rid);
      return rid;
   }

private:
   info_map_t info_map_;
   info_mi_t info_index_;

   name_iter find_iter(const boost::string_ref id, const bool reverse) const {
      name_index_t const& ind = info_index_.get<name_tag>();
      name_range r = ind.equal_range(
               id,
               boost::hash<boost::string_ref>(),
               detail::Equal_string_ref()
      );
      name_iter i = r.begin();
      if( i == ind.end() || i == r.end() ) return ind.end();
      if( info_map_[*i].is_reverse() == reverse ) return i;
      ++i;
      if( i == r.end() ) return ind.end();
      if( info_map_[*i].is_reverse() == reverse ) return i;
      BOOST_ASSERT(false && "one of two reads should match");
      return ind.end();
   }

};

}//namespace vdj_pipe
#endif /* READ_INFO_STORE_HPP_ */
