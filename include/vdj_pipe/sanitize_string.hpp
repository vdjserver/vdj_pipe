/** @file "/vdj_pipe/include/vdj_pipe/sanitize_string.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef SANITIZE_STRING_HPP_
#define SANITIZE_STRING_HPP_
#include <string>
#include "boost/utility/string_ref.hpp"
#include "vdj_pipe/config.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::string sanitize(const char c);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::string sanitize(std::string const& str);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL std::string sanitize(
         const boost::string_ref str,
         const std::size_t max_len
);

}//namespace vdj_pipe
#endif /* SANITIZE_STRING_HPP_ */
