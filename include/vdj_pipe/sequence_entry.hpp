/** @file "/vdj_pipe/include/vdj_pipe/sequence_entry.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_ENTRY_HPP_
#define SEQUENCE_ENTRY_HPP_
#include <string>
#include "boost/assert.hpp"
#include "boost/functional/hash.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/object_ids.hpp"

namespace vdj_pipe{

/** Sequence with an ID and a list of subsequences
*******************************************************************************/
class Seq_entry {
public:
   explicit Seq_entry(
            const boost::string_ref seq = "",
            const Read_id id = Read_id()
   )
   : seq_(seq.begin(), seq.size()),
     id_(id)
   {}

   unsigned size() const {return seq_.size();}
   std::string const& sequence() const {return seq_;}
   bool empty() const {return seq_.empty();}
   Read_id id() const {return id_;}

private:
   std::string seq_;
   Read_id id_;
};

/**
*******************************************************************************/
inline bool operator==(Seq_entry const& se1, Seq_entry const& se2) {
   return se1.sequence() == se2.sequence();
}

/**
*******************************************************************************/
inline bool operator==(const boost::string_ref se1, Seq_entry const& se2) {
   return se1 == se2.sequence();
}

/**
*******************************************************************************/
inline bool operator==(Seq_entry const& se1, const boost::string_ref se2) {
   return se1.sequence() == se2;
}

/**
*******************************************************************************/
inline std::size_t hash_value(Seq_entry const& se) {
   return boost::hash_value(se.sequence());
}


}//namespace vdj_pipe
#endif /* SEQUENCE_ENTRY_HPP_ */
