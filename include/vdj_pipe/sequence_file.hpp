/** @file "/vdj_pipe/include/vdj_pipe/sequence_file.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_FILE_HPP_
#define SEQUENCE_FILE_HPP_
#include <string>
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/file.hpp"

namespace vdj_pipe{
class Seq_file_map;

/**@brief 
*******************************************************************************/
class Seq_file : public File_input {
   friend class Seq_file_map;

public:
   explicit Seq_file(
            std::string const& path,
            const bool reverse = false
   )
   : File_input(path),
     pid_(),
     is_reverse_(reverse)
   {}

   Path_id id() const {return pid_;}
   bool is_reverse() const {return is_reverse_;}

   Seq_file& reverse(const bool rev) {
      is_reverse_ = rev;
      return *this;
   }

private:
   Path_id pid_;
   bool is_reverse_;
};

}//namespace vdj_pipe
#endif /* SEQUENCE_FILE_HPP_ */
