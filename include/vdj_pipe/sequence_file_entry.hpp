/** @file "/vdj_pipe/include/vdj_pipe/sequence_file_entry.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef SEQUENCE_FILE_ENTRY_HPP_
#define SEQUENCE_FILE_ENTRY_HPP_
#include <string>
#include <map>
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/value_variant.hpp"

namespace vdj_pipe{
class Input_manager;
class Value_map;

/** Tuple of sequence data files that should be processed in parallel
and associated info.
Examples:
- single FASTA file name
- two FASTA/Qual pairs for paired reads and a sample name
*******************************************************************************/
class VDJ_PIPE_DECL Seq_file_entry {
   enum qual_type {none, qual, fastq};
public:
   typedef std::map<Val_id, value_variant> map_t;
   typedef map_t::const_iterator iterator;
   typedef map_t::const_iterator const_iterator;
   typedef map_t::value_type value_type;

   struct Err : public base_exception {};

   Seq_file_entry(map_t const& m, Input_manager const& im, Value_map const& vm);

   const_iterator begin() const {return map_.begin();}
   const_iterator end() const {return map_.end();}
   bool is_paired() const {return seq_rev_;}
   bool has_emid() const {return mid_;}
   bool has_quality() const {return qt_ != none;}
   bool has_qual_file() const {return qt_ == qual;}

   std::string const& sequence() const {
      return get_str(seq_, "sequence");
   }

   std::string const& quality() const {
      return get_str(qual_, "quality");
   }

   std::string const& seq_rev() const {
      return get_str(seq_rev_, "reverse sequence");
   }

   std::string const& qual_rev() const {
      return get_str(qual_rev_, "reverse quality");
   }

   std::string const& mid() const {
      return get_str(mid_, "MID");
   }

   std::string const& mid_rev() const {
      return get_str(mid_rev_, "reverse MID");
   }

private:
   qual_type qt_;
   Val_id seq_;
   Val_id qual_;
   Val_id seq_rev_;
   Val_id qual_rev_;
   Val_id mid_;
   Val_id mid_rev_;
   map_t map_;

   void check(Input_manager const& im);

   std::string const& get_str(const Val_id id, std::string const& msg) const {
      const const_iterator i = map_.find(id);
      if( i == map_.end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t(msg + " not found")
      );
      return boost::get<std::string>(i->second);
   }
};

/**
*******************************************************************************/
VDJ_PIPE_DECL void store_values(Seq_file_entry const& sfe, Value_map& vm);

}//namespace vdj_pipe
#endif /* SEQUENCE_FILE_ENTRY_HPP_ */
