/** @file "/vdj_pipe/include/vdj_pipe/sequence_file_map.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_FILE_MAP_HPP_
#define SEQUENCE_FILE_MAP_HPP_
#include <utility>
#include "boost/assert.hpp"
#include "boost/multi_index_container.hpp"
#include "boost/multi_index/mem_fun.hpp"
#include "boost/multi_index/member.hpp"
#include "boost/multi_index/ordered_index.hpp"
#include "vdj_pipe/detail/id_iterator.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/sequence_file.hpp"
#include "vdj_pipe/exception.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class Seq_file_map {
   typedef boost::multi_index_container<
            Seq_file,
            boost::multi_index::indexed_by<
               boost::multi_index::ordered_unique<
                  boost::multi_index::tag<struct id_tag>,
                  boost::multi_index::const_mem_fun<
                     Seq_file, Path_id, &Seq_file::id
                  >
               >,
               boost::multi_index::ordered_unique<
                  boost::multi_index::tag<struct path_tag>,
                  boost::multi_index::const_mem_fun<
                     File, std::string const&, &Seq_file::path
                  >
               >
            >
   > file_map;

   typedef file_map::index<id_tag>::type id_index;
   typedef id_index::const_iterator id_iterator;
   typedef file_map::index<path_tag>::type path_index;
   typedef path_index::const_iterator path_iterator;

   //do not use std::pair - it causes problems with MSVC
   struct Pair {
      Pair(const Path_id one, const Path_id two): one_(one), two_(two) {}
      Path_id one_, two_;
   };

   typedef boost::multi_index_container<
            Pair,
            boost::multi_index::indexed_by<
               boost::multi_index::ordered_unique<
                  boost::multi_index::member<Pair, Path_id, &Pair::one_>
               >,
               boost::multi_index::ordered_unique<
                  boost::multi_index::member<Pair, Path_id, &Pair::two_>
               >
            >
   > pair_map;
   typedef pair_map::nth_index<0>::type index1;
   typedef index1::const_iterator citer1;
   typedef pair_map::nth_index<1>::type index2;
   typedef index2::const_iterator citer2;

public:
   typedef Seq_file value_type;
   typedef file_map::iterator iterator;
   typedef file_map::const_iterator const_iterator;
   struct Err : public base_exception {};

   Seq_file_map() : i_(Path_id(1)) {}
   std::size_t size() const { return map_.size(); }
   const_iterator begin() const {return map_.begin();}
   const_iterator end() const {return map_.end();}
   bool empty() const {return map_.empty();}

   Seq_file const& operator[](const Path_id id) const {
      id_index const& ind = map_.get<id_tag>();
      const id_iterator i = ind.find(id);
      BOOST_ASSERT(i != ind.end());
      return *i;
   }

   Seq_file const& at(const Path_id id) const {
      id_index const& ind = map_.get<id_tag>();
      const id_iterator i = ind.find(id);
      if(i == ind.end()) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("invalid path ID")
         << Err::int1_t(id())
      );
      return *i;
   }

   Seq_file const* find(std::string const& path) const {
      path_index const& ind = map_.get<path_tag>();
      const path_iterator i = ind.find(path);
      if( i == ind.end() ) return 0;
      return &(*i);
   }

   void erase(const Path_id id) {map_.erase(id);}
   void clear() {map_.clear();}

   std::pair<Path_id, bool> insert(Seq_file const& file) {
      typedef std::pair<Path_id,bool> pair;
      if( Seq_file const* sfp = find(file.path()) ) return pair(sfp->id(), false);
      const Path_id id = *i_++;
      Seq_file sf = file;
      sf.pid_ = id;
      map_.emplace(sf);
      return pair(id, true);
   }

   void set_seq_qual(const Path_id seq_id, const Path_id qual_id) {
      if( (!seq_id) || (!qual_id) || seq_id == qual_id ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("files should be valid and distinct")
      );

      if( (*this)[seq_id].format() != format::Fasta ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("sequence file should be in FASTA format")
      );

      if( (*this)[qual_id].format() != format::Qual ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("quality file should be in QUAL format")
      );

      sqm_.insert(Pair(seq_id, qual_id));
   }

   void set_paired(const Path_id frw_id, const Path_id rev_id) {
      if( (!frw_id) || (!rev_id) || frw_id == rev_id ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("files should be valid and distinct")
      );

      if( (*this)[frw_id].format() != (*this)[rev_id].format() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("paired reads files should be in same format")
      );

      if(
               (*this)[frw_id].format() != format::Fasta &&
               (*this)[frw_id].format() != format::Fastq
      ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("paired reads files should be in FASTA or FASTQ formats")
      );

      frm_.insert(Pair(frw_id, rev_id));
   }

   void set_mid(const Path_id seq_id, const Path_id mid_id) {
      if( (!seq_id) || (!mid_id) || seq_id == mid_id ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("files should be valid and distinct")
      );

      if( (*this)[seq_id].format() != (*this)[mid_id].format() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("sequence and eMID files should be in same format")
      );

      if(
               (*this)[seq_id].format() != format::Fasta &&
               (*this)[seq_id].format() != format::Fastq
      ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("sequence files should be in FASTA or FASTQ formats")
      );

      smm_.insert(Pair(seq_id, mid_id));
   }

   Path_id qual2seq(const Path_id qual_id) const {return get<1>(qual_id, sqm_);}
   Path_id seq2qual(const Path_id seq_id) const {return get<0>(seq_id, sqm_);}
   Path_id rev2frw(const Path_id rev_id) const {return get<1>(rev_id, frm_);}
   Path_id frw2rev(const Path_id frw_id) const {return get<0>(frw_id, frm_);}
   Path_id mid2seq(const Path_id mid_id) const {return get<1>(mid_id, smm_);}
   Path_id seq2mid(const Path_id seq_id) const {return get<0>(seq_id, smm_);}

private:
   Id_iterator<Path_id> i_;
   file_map map_;
   pair_map sqm_; /**< sequence quality pairs */
   pair_map frm_; /**< forward reverse pairs */
   pair_map smm_; /**< sequence eMID pairs */

   template<int N> static
   Path_id get(const Path_id pid, pair_map const& pm) {
      typedef typename pair_map::nth_index<N>::type index;
      typedef typename index::const_iterator iter;
      index const& ind = pm.get<N>();
      iter i = ind.find(pid);
      if( i == ind.end() ) return Path_id();
      return N ? i->one_ : i->two_;
   }
};

}//namespace vdj_pipe
#endif /* SEQUENCE_FILE_MAP_HPP_ */
