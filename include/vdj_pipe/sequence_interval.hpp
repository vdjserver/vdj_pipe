/** @file "/vdj_pipe/include/vdj_pipe/sequence_interval.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_INTERVAL_HPP_
#define SEQUENCE_INTERVAL_HPP_
#include <iosfwd>
#include <limits>
#include "boost/functional/hash/hash_fwd.hpp"
#include "boost/numeric/interval.hpp"
#include "boost/numeric/interval/checking.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief
*******************************************************************************/
template<class T> struct Interval_checking_policy;

template<> struct Interval_checking_policy<int> {
   typedef int value_type;
   static value_type pos_inf() {return std::numeric_limits<value_type>::max();}
   static value_type neg_inf() {return 0;}
   static value_type nan() {return std::numeric_limits<value_type>::min();}
   static bool is_nan(const value_type v) {return v == nan();}
   static value_type empty_lower() {return 0;}
   static value_type empty_upper() {return 0;}
   static bool is_empty(const value_type lo, const value_type up) {return lo == up;}
};

}//namespace detail

typedef boost::numeric::interval<
         int,
         boost::numeric::interval_lib::policies<
            boost::numeric::interval_lib::rounded_math<int>,
            detail::Interval_checking_policy<int>
         >
      > sequence_interval;

/**@brief
*******************************************************************************/
inline sequence_interval sequence_interval_invalid() {
/*
   typedef sequence_interval::base_type value_type;
   //impossible to create interval with nan()
   return sequence_interval(
            std::numeric_limits<value_type>::min(),
            std::numeric_limits<value_type>::min()
   );
*/
   return sequence_interval(-1, -1);
}

}//namespace vdj_pipe

namespace boost{ namespace numeric {

/**@brief
*******************************************************************************/
inline bool is_valid( vdj_pipe::sequence_interval const& si) {
   return si.upper() >= 0;
}

/**@brief
*******************************************************************************/
inline std::size_t hash_value(vdj_pipe::sequence_interval const& si) {
   std::size_t n = 0;
   boost::hash_combine(n, si.lower());
   boost::hash_combine(n, si.upper());
   return n;
}

/**@brief
*******************************************************************************/
template<class ChT, class Tr> inline
std::basic_ostream<ChT,Tr>& operator<<(
      std::basic_ostream<ChT,Tr>& os,
      vdj_pipe::sequence_interval const& si
) {
   os << '[' << si.lower() << ',' << si.upper() << ']';
   return os;
}

}//namespace numeric
}//namespace boost

#endif /* SEQUENCE_INTERVAL_HPP_ */
