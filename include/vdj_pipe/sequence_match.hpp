/** @file "/vdj_pipe/include/vdj_pipe/sequence_match.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_MATCH_HPP_
#define SEQUENCE_MATCH_HPP_
#include "vdj_pipe/detail/vector_set.hpp"
#include "vdj_pipe/sequence_position.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
struct Seq_match {
   /** length of match */
   unsigned n_;

   /** set of sequence IDs and match starting positions */
   detail::Vector_set<super_seq> seq_;
};

}//namespace vdj_pipe
#endif /* SEQUENCE_MATCH_HPP_ */
