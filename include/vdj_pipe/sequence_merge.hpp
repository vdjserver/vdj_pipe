/** @file "/vdj_pipe/include/vdj_pipe/sequence_merge.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_MERGE_HPP_
#define SEQUENCE_MERGE_HPP_
#include <string>
#include <vector>
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
struct Merge_result {
   typedef Seq_record::sequence sequence;
   typedef Qual_record::quality quality;
   /** sequence characters without whitespace */
   sequence seq_;

   /** vector of quality values */
   quality qual_;

   unsigned score_;

   std::string cigar_;
};

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL void merge(
         std::string const& s1,
         Seq_qual_record::quality const& qv1,
         std::string const& s2, //in forward direction
         Seq_qual_record::quality const& qv2, //in forward direction
         const unsigned min_score,
         Merge_result& mr
);


}//namespace vdj_pipe
#endif /* SEQUENCE_MERGE_HPP_ */
