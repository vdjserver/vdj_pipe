/** @file "/vdj_pipe/include/vdj_pipe/sequence_position.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_POSITION_HPP_
#define SEQUENCE_POSITION_HPP_
#include "vdj_pipe/object_ids.hpp"

namespace vdj_pipe{
/**@brief
*******************************************************************************/
template<class Id>
struct Seq_pos {
   Seq_pos(const Id iid, const unsigned pos)
   : id_(iid), pos_(pos)
   {}

   Id id_;
   unsigned pos_;
};

typedef Seq_pos<Read_id> sub_seq;
typedef Seq_pos<Seq_id> super_seq;

template<class Id>
inline bool operator==(Seq_pos<Id> const& rp1, Seq_pos<Id> const& rp2) {
   return rp1.id_ == rp2.id_ && rp1.pos_ == rp2.pos_;
}

template<class Id>
inline bool operator<(Seq_pos<Id> const& rp1, Seq_pos<Id> const& rp2) {
   if( rp1.id_ < rp2.id_ ) return true;
   if( rp2.id_ < rp1.id_ ) return false;
   return rp1.pos_ < rp2.pos_;
}

template<class Id>
inline bool operator<(Seq_pos<Id> const& rp1, const Id rp2) {
   return rp1.id_ < rp2;
}

template<class Id>
inline bool operator<(const Id rp1, Seq_pos<Id> const& rp2) {
   return rp1 < rp2.id_;
}

}//namespace vdj_pipe
#endif /* SEQUENCE_POSITION_HPP_ */
