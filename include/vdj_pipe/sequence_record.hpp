/** @file "/vdj_pipe/include/vdj_pipe/sequence_record.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_RECORD_HPP_
#define SEQUENCE_RECORD_HPP_
#include <iosfwd>
#include <iterator>
#include <string>
#include <vector>

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
struct Seq_meta {
   /** characters between '>' and first SPACE or TAB */
   std::string id_;

   /** characters between first space or TAB and NL */
   std::string comm_;
};

/**@brief
*******************************************************************************/
struct Seq_record : public Seq_meta {
   typedef std::string sequence;

   /** sequence characters without whitespace */
   sequence seq_;
};

class Quality : public std::vector<unsigned char> {
public:
   typedef unsigned char value_type;

private:
   typedef std::vector<value_type> vec_t;
public:
   Quality() {}

   explicit Quality(const std::size_t n, const value_type val = 0)
   : vec_t(n, val)
   {}

   Quality(vec_t const& v) : vec_t(v) {}
};

/**@brief
*******************************************************************************/
struct Qual_record : public Seq_meta {
   typedef Quality quality;

   /** vector of quality values */
   quality qual_;
};

/**@brief
*******************************************************************************/
template<class ChT, class Tr> inline
std::basic_ostream<ChT,Tr>& operator<<(
      std::basic_ostream<ChT,Tr>& os,
      Qual_record::quality const& qual
) {
   std::copy(
            qual.begin(),
            qual.end(),
            std::ostream_iterator<Qual_record::quality::value_type>(os, " ")
   );
   return os;
}

/**@brief
*******************************************************************************/
struct Seq_qual_record : public Seq_meta {
   typedef Seq_record::sequence sequence;
   typedef Qual_record::quality quality;

   /** sequence characters without whitespace */
   sequence seq_;

   /** vector of quality values */
   quality qual_;
};

}//namespace vdj_pipe
#endif /* SEQUENCE_RECORD_HPP_ */
