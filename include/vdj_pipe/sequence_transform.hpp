/** @file "/vdj_pipe/include/vdj_pipe/sequence_transform.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SEQUENCE_TRANSFORM_HPP_
#define SEQUENCE_TRANSFORM_HPP_
#include <string>
#include "boost/range.hpp"
#include "boost/range/as_literal.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/nucleotide_index.hpp"
#include "vdj_pipe/sequence_interval.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
template<class Range> inline std::string complement(Range const& r) {
   std::string rstr(boost::size(boost::as_literal(r)), 'X');
   std::string::reverse_iterator i = rstr.rbegin();
   typedef typename boost::range_iterator<const Range>::type iter;
   iter j = boost::begin(boost::as_literal(r)), end = boost::end(boost::as_literal(r));
   for(; j != end; ++i, ++j) {
      *i = complement((char)*j);
   }
   return rstr;
}

/**@brief Reverse-complement (optionally) a portion of a sequence
*******************************************************************************/
inline std::string transform(
         const boost::string_ref seq,
         sequence_interval const& si,
         const bool reverse
) {
   if(
            (std::size_t)si.lower() > seq.size() ||
            (std::size_t)si.upper() > seq.size()
   ) BOOST_THROW_EXCEPTION(
      base_exception()
      << base_exception::msg_t("sequence interval and length mismatch")
      << base_exception::int1_t(std::max(si.lower(), si.upper()))
      << base_exception::int2_t(seq.size())
   );

   return reverse ?
            complement(seq.substr(si.lower(), width(si))) :
            seq.substr(si.lower(), width(si)).to_string()
            ;
}

}//namespace vdj_pipe
#endif /* SEQUENCE_TRANSFORM_HPP_ */
