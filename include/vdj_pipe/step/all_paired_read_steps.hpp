/** @file "/vdj_pipe/include/vdj_pipe/step/all_paired_read_steps.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef ALL_PAIRED_READ_STEPS_HPP_
#define ALL_PAIRED_READ_STEPS_HPP_

#include "vdj_pipe/step/apply_to_adaptor.hpp"
#include "vdj_pipe/step/external_mid_infile.hpp"
#include "vdj_pipe/step/merge_paired_reads_step.hpp"
#include "vdj_pipe/step/write_value_step.hpp"

#endif /* ALL_PAIRED_READ_STEPS_HPP_ */
