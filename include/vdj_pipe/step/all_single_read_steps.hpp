/** @file "/vdj_pipe/include/vdj_pipe/step/all_single_read_steps.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef ALL_SINGLE_READ_STEPS_HPP_
#define ALL_SINGLE_READ_STEPS_HPP_

#include "vdj_pipe/step/blank_step.hpp"
#include "vdj_pipe/step/composition_stats_step.hpp"
#include "vdj_pipe/step/external_mid_inline.hpp"
#include "vdj_pipe/step/filter_step.hpp"
#include "vdj_pipe/step/find_shared.hpp"
#include "vdj_pipe/step/histogram_step.hpp"
#include "vdj_pipe/step/match_step.hpp"
#include "vdj_pipe/step/quality_stats_step.hpp"
#include "vdj_pipe/step/write_sequence_step.hpp"
#include "vdj_pipe/step/write_value_step.hpp"

#endif /* ALL_SINGLE_READ_STEPS_HPP_ */
