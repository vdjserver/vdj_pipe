/** @file "/vdj_pipe/include/vdj_pipe/step/all_steps.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef ALL_STEPS_HPP_
#define ALL_STEPS_HPP_

#include "vdj_pipe/step/all_single_read_steps.hpp"
#include "vdj_pipe/step/all_paired_read_steps.hpp"

#endif /* ALL_STEPS_HPP_ */
