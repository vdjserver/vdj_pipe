/** @file "/vdj_pipe/include/vdj_pipe/step/apply_to_adaptor.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef APPLY_TO_ADAPTOR_HPP_
#define APPLY_TO_ADAPTOR_HPP_
#include <vector>
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/variant/static_visitor.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/step/all_single_read_steps.hpp"
#include "vdj_pipe/step/step_maker.hpp"
#include "vdj_pipe/step/step_variant_single_read.hpp"
#include "vdj_pipe/step/visitor.hpp"
#include "vdj_pipe/value_map_access_paired.hpp"

namespace vdj_pipe{
class Pipe_environment;

/**@brief Apply enclosed processing step once to a sequence read of specified
kind: forward, reverse, or merged
*******************************************************************************/
class Apply_one {
public:
   typedef Vm_access_paired vma_type;
   VDJ_PIPE_STATIC_STRING_METHOD(name, "apply")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "paired read")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "apply specified step to either forward, reverse, or merged sequences"
   )
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Apply_one(step_variant_single const& step) : step_(step) {}
   void run() {boost::apply_visitor(Run_visitor(), step_);}
   void finish() {boost::apply_visitor(Finish_visitor(), step_);}

   void summary(std::ostream& os) const {
      Summary_visitor2 sv2(os);
      boost::apply_visitor(sv2, step_);
   }

private:
   step_variant_single step_;
};

/**@brief Apply enclosed processing step multiple times to sequence reads of
specified kind: forward, reverse, or merged
*******************************************************************************/
class Apply_many {
   typedef std::vector<Value_ids_single> vid_vector;

   class Visitor : public boost::static_visitor<> {
   public:
      Visitor(vid_vector const& idv) : idv_(idv) {}

      template<typename Step> void operator()(Step& step) const {
         BOOST_FOREACH(Value_ids_single const& vis, idv_) {
            step.reset_access(vis);
            step.run();
         }
      }

   private:
      vid_vector idv_;
   };

   public:

   VDJ_PIPE_STATIC_STRING_METHOD(name, "apply")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "paired read")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "apply specified step to either forward, reverse, or merged sequences"
   )
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   struct Err : public base_exception {};

   Apply_many(vid_vector const& idv, step_variant_single const& step)
   : av_(idv), step_(step) {}

   void run() {boost::apply_visitor(Run_visitor(), step_);}
   void finish() {boost::apply_visitor(Finish_visitor(), step_);}

   void summary(std::ostream& os) const {
      Summary_visitor2 sv2(os);
      apply_visitor(sv2, step_);
   }

private:
   Visitor av_;
   step_variant_single step_;
};

namespace step{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Apply_to_maker {
public:
   struct Err : public base_exception {};
   VDJ_PIPE_STATIC_STRING_METHOD(name, "apply")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "paired read")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "apply specified step to forward, reverse, or merged sequences"
   )

   typedef boost::variant<Apply_one, Apply_many> result_type;
   typedef Vm_access_paired vma_type;

   static result_type make(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

private:
   static Value_ids_single make_ids(
            vma_type const& vma,
            std::string const& type
   );
};

/**@brief
*******************************************************************************/
template<> struct Find_maker<Apply_one > {typedef Apply_to_maker type;};
template<> struct Find_maker<Apply_many> {typedef Apply_to_maker type;};

}//namespace step
}//namespace vdj_pipe
#endif /* APPLY_TO_ADAPTOR_HPP_ */
