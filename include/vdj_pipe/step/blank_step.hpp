/** @file "/vdj_pipe/include/vdj_pipe/step/blank_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef BLANK_STEP_HPP_
#define BLANK_STEP_HPP_
#include <iosfwd>
#include <string>
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/value_map_access_single.hpp"

namespace vdj_pipe{
class Pipe_environment;

/**@brief
*******************************************************************************/
class Blank_step {
public:
   typedef Value_map vma_type;
   VDJ_PIPE_STATIC_STRING_METHOD(name, "blank_step")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "misc")
   VDJ_PIPE_STATIC_STRING_METHOD(comment, "does nothing")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Blank_step() {}

   Blank_step(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   ){}

   void reset_access(Value_ids_single const& ids) {}
   void run() {}
   void finish() {}
   void summary(std::ostream&) const {}
};

}//namespace vdj_pipe
#endif /* BLANK_STEP_HPP_ */
