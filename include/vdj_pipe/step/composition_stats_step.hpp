/** @file "/vdj_pipe/include/vdj_pipe/step/composition_stats_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef COMPOSITION_STATS_STEP_HPP_
#define COMPOSITION_STATS_STEP_HPP_
#include <iosfwd>
#include <string>
#include <vector>
#include "boost/array.hpp"
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/histogram_1d.hpp"
#include "vdj_pipe/step/step_base_single.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Composition_stats : public detail::Step_base_single {
   typedef boost::array<unsigned, 5> letter_counts;
   typedef std::vector<letter_counts> counts_vector;

public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "composition_stats")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "statistics")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "calculate composition statistics of sequencing reads"
   )
   VDJ_PIPE_STATIC_STRING_METHOD(
      description,
      "based on trimmed reads"
   )

   Composition_stats(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish();

private:
   counts_vector cv_;
   Histogram_1d gch_;
   std::string comp_;
   std::string gc_hist_;

   void write_composition() const;
   void write_gc_hist() const;
};

}//namespace vdj_pipe
#endif /* COMPOSITION_STATS_STEP_HPP_ */
