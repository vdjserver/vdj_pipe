/** @file "/vdj_pipe/include/vdj_pipe/step/external_mid_base.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef EXTERNAL_MID_BASE_HPP_
#define EXTERNAL_MID_BASE_HPP_
#include <iosfwd>
#include <string>
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/best_match_pair.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/nucleotide_index.hpp"
#include "vdj_pipe/step/step_base.hpp"
#include "vdj_pipe/object_ids.hpp"

namespace vdj_pipe{
class Pipe_environment;
class Value_map;
class Seq_map_short;
class Seq_map_full;

namespace detail{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL External_mid_base {
   typedef Best_match_pair<Mid_id, int> match_type;
protected:
   VDJ_PIPE_KEYWORD_STRUCT(
      kwds,
      (fasta_path)
      (pairs_path)
      (value_name)
      (min_score)
      (max_mismatches)     // reject matches with more mismatching nucleotides
      (require_best)
   );

public:
   typedef Step_base::Err Err;

   VDJ_PIPE_STATIC_STRING_METHOD(name, "eMID_map")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "demultiplexing")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "identify which of the given short sequences best matches "
      "external molecular identifier"
   )
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   External_mid_base(
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe,
            const Val_id name_val_id,
            const Val_id score_val_id
   );

   void finish() {}

private:
   boost::shared_ptr<Seq_map_short> sms_;
   boost::shared_ptr<Seq_map_full> smf_;
   scoring_matrix_t const* scoring_matrix_;
   int min_score_;
   bool require_best_;
   Val_id name_val_id_;
   Val_id score_val_id_;

protected:
   std::size_t mid_size() const;

   /**Attempt to assign sequence to one of the MIDs, gather statistics;
    @return MID name or an empty string if assignment could not be made
    */
   void find_mid(
            const boost::string_ref seq,
            Value_map& vm
   );
};

}//namespace detail
}//namespace vdj_pipe
#endif /* EXTERNAL_MID_BASE_HPP_ */
