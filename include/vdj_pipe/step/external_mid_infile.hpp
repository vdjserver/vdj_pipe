/** @file "/vdj_pipe/include/vdj_pipe/step/external_mid_infile.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef EXTERNAL_MID_INFILE_HPP_
#define EXTERNAL_MID_INFILE_HPP_
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/step/external_mid_base.hpp"
#include "vdj_pipe/step/step_base_paired_emid.hpp"

namespace vdj_pipe{


/**@brief Look for external MIDs in separate files
*******************************************************************************/
class VDJ_PIPE_DECL External_mid_infile
   : public detail::Step_base_paired_emid,
     public detail::External_mid_base {
public:
   External_mid_infile(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
};

}//namespace vdj_pipe
#endif /* EXTERNAL_MID_INFILE_HPP_ */
