/** @file "/vdj_pipe/include/vdj_pipe/step/external_mid_inline.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef EXTERNAL_MID_INLINE_HPP_
#define EXTERNAL_MID_INLINE_HPP_
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/step/external_mid_base.hpp"
#include "vdj_pipe/step/step_base_single.hpp"

namespace vdj_pipe{

/**@brief Look for external MIDs in FASTA/Q description lines
*******************************************************************************/
class VDJ_PIPE_DECL External_mid_inline
   : public detail::Step_base_single,
     public detail::External_mid_base {
public:

   External_mid_inline(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
};

}//namespace vdj_pipe
#endif /* EXTERNAL_MID_INLINE_HPP_ */
