/** @file "/vdj_pipe/include/vdj_pipe/step/filter_base.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef FILTER_BASE_HPP_
#define FILTER_BASE_HPP_
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/step/step_base_single.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief 
*******************************************************************************/
class Filter_base : public Step_base_single {
public:

   void summary(std::ostream& os) const;

protected:

   VDJ_PIPE_KEYWORD_STRUCT(
            kwds,

            //name of boolean variable indicating whether read passed filter
            (passed_name)
   );

   Filter_base(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void pass(const bool b) {
      if( ! b ) {
         ++nf_;
         vma_.set_empty();
      }
      if( pass_id_ ) vma_[pass_id_] = b;
   }

   std::size_t nf_;
   Val_id pass_id_;
};

/**@brief
*******************************************************************************/
class Window_filter_base : public Filter_base {
public:
   void summary(std::ostream& os) const;

protected:
   Window_filter_base(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   )
   : Filter_base(vma, pt, pe), nt_(0)
   {}

   void trim(sequence_interval const& si) {
      pass( ! empty(si) );
      if( si.lower() > 0 || si.upper() < (int)vma_.sequence().size() ) {
         ++nt_;
         vma_.trim(si);
      }
   }

   std::size_t nt_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* FILTER_BASE_HPP_ */
