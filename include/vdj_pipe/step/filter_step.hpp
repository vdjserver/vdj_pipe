/** @file "/vdj_pipe/include/vdj_pipe/step/filter_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef FILTER_STEP_HPP_
#define FILTER_STEP_HPP_
#include <iosfwd>
#include <string>
#include <vector>
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/step/filter_base.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class Character_filter : public detail::Filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "character_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "discard reads that have nucleotides other than specified"
   )

   Character_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   std::vector<char> ch_;
};

/**@brief
*******************************************************************************/
class Length_filter : public detail::Window_filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "length_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "discard reads with length outside of [min, max] rage"
   )

   Length_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   unsigned min_;
   unsigned max_;
   bool trim_;
};

/**@brief
*******************************************************************************/
class Homopolymer_filter : public detail::Filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "homopolymer_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "discard reads that contain a homopolymer longer than minimal length"
   )

   Homopolymer_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   unsigned min_;
};

/**@brief
*******************************************************************************/
class Min_quality_filter : public detail::Filter_base {
   struct kwds;
   typedef Qual_record::quality::value_type value_type;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "min_quality_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "discard reads that contain a quality score lower than minimal"
   )

   Min_quality_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   value_type min_;
};

/**@brief
*******************************************************************************/
class Average_quality_filter : public detail::Filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "average_quality_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "discard reads with average quality score lower than minimal"
   )

   Average_quality_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   double min_;
};

/**@brief
*******************************************************************************/
class Min_quality_window_filter : public detail::Window_filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "min_quality_window_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "find longest interval with each quality score greater than "
      "minimal; discard reads where the interval found is shorter "
      "than minimal"
   )

   Min_quality_window_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   Qual_record::quality::value_type min_qual_;
   std::size_t min_len_;
};

/**@brief
*******************************************************************************/
class Average_quality_window_filter : public detail::Window_filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "average_quality_window_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "find longest interval with average quality score greater than "
      "minimal; discard reads where the interval found is shorter "
      "than minimal"
   )

   Average_quality_window_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   /** window length */
   std::size_t win_len_;

   /**minimal average quality multiplied by window length */
   std::size_t min_qual_l_;

   /** minimal read length */
   std::size_t min_len_;
};

/**@brief
*******************************************************************************/
class Ambiguous_window_filter : public detail::Window_filter_base {
   struct kwds;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "ambiguous_window_filter")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "filter")

   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "Truncate reads to bring number of ambiguous bases below maximum"
   )

   VDJ_PIPE_STATIC_STRING_METHOD(description,
      "Find read interval at least min_len nucleotides long that has "
      "at most max_ambiguous ambiguous nucleotides. "
      "Discard reads where such interval cannot be found. "
      "If min_len is zero, require that the whole read contains at most "
      "max_ambiguous ambiguous nucleotides."
   )

   Ambiguous_window_filter(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}

private:
   std::size_t min_len_;
   std::size_t max_ambiguous_;
};

}//namespace vdj_pipe
#endif /* FILTER_STEP_HPP_ */
