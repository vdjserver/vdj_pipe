/** @file "/vdj_pipe/include/vdj_pipe/step/find_shared.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef FIND_SHARED_HPP_
#define FIND_SHARED_HPP_
#include <iosfwd>
#include <string>
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/scoped_ptr.hpp"
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/detail/id_bimap.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/file_properties.hpp"
#include "vdj_pipe/min_match_length.hpp"
#include "vdj_pipe/step/step_base_single.hpp"
#include "vdj_pipe/variable_path.hpp"

namespace boost{
template<typename, std::size_t, typename> class multi_array;
}//namespace boost

namespace vdj_pipe{
class File_ostream;
class Seq_store;
class Read_ginfo_store;

/**@brief
*******************************************************************************/
class Find_shared : public detail::Step_base_single {
   struct kwds;
   typedef detail::Queable_ofstream_types::value_type value_type;
   typedef detail::Queable_ofstream_types::val_vector val_vector;
   typedef detail::Queable_ofstream_types::val_ref_vector val_ref_vector;
   typedef detail::Id_bimap<Mapped_id, val_vector> val_map_t;
   typedef std::vector<unsigned> counts_v;
   typedef std::vector<counts_v> hist_array_t;

public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "find_shared")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "post-processing")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "output sequence reads present in multiple sets"
   )

   Find_shared(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish();
   void summary(std::ostream& os) const;

private:
   Get_match_length gml_;
   std::size_t min_duplicates_;

   /** number of unique sequences not shared between groups*/
   std::size_t n_unique_;
   bool trim_;
   bool reverse_;
   bool consensus_trim_;
   val_map_t vm_;
   boost::shared_ptr<Read_ginfo_store> rgs_;
   boost::shared_ptr<Seq_store> ss_;
   counts_v cv_;
   std::size_t unique_count_;
   std::string redund_hist_;
   std::string summ_;
   value_type unset_val_;
   Variable_path g_unique_;
   Variable_path g_dups_;
   Variable_path unique_;
   Variable_path dups_;
   std::vector<Val_id> ids_;

   typedef boost::multi_array<unsigned, 2> xstats;

   void write_sharing_matrix(
            std::ostream& os,
            counts_v const& cv,
            xstats const& xs,
            std::vector<Mapped_id> const& sorted_vi
   ) const;

   void write_summary(
            std::ostream& os,
            counts_v const& uc,
            counts_v const& guc,
            std::vector<Mapped_id> const& sn
   ) const;

   void write_redundancy(
            std::ostream& os,
            hist_array_t const& ha,
            std::vector<Mapped_id> const& sorted_vi
   ) const;

   void write_redundancy_summ(
            std::ostream& os,
            hist_array_t const& ha,
            std::vector<Mapped_id> const& sorted_vi
   ) const;


   void group_stats();

   void bulk_stats();

   std::vector<Mapped_id> sorted_ids() const;

   void init_stream(
            boost::scoped_ptr<File_ostream>&,
            Variable_path const&,
            const compression::Compression,
            const format::Format
   ) const;
};

}//namespace vdj_pipe
#endif /* FIND_SHARED_HPP_ */
