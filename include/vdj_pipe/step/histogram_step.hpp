/** @file "/vdj_pipe/include/vdj_pipe/step/histogram_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef HISTOGRAM_STEP_HPP_
#define HISTOGRAM_STEP_HPP_
#include <string>
#include <vector>
#include "boost/unordered_map.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/file.hpp"
#include "vdj_pipe/step/step_base.hpp"
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/value_variant.hpp"

namespace vdj_pipe{
class Value_ids_single;
namespace detail{class File_output;}

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Histogram_step : public detail::Step_base {
   typedef std::vector<value_variant> hvalue_t;
   typedef boost::unordered_map<hvalue_t, std::size_t> map_t;
public:
   typedef Value_map vma_type;
   VDJ_PIPE_STATIC_STRING_METHOD(name, "histogram")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "statistics")
   VDJ_PIPE_STATIC_STRING_METHOD(comment,
      "build a histogram of value occurrences")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Histogram_step(
            vma_type const& vm,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void reset_access(Value_ids_single const& ids) {}
   void run();
   void finish();

private:
   Value_map vm_;
   std::vector<Val_id> ids_;
   File_output f_;
   map_t map_;

   void write_header(std::ostream& os) const;
};

}//namespace vdj_pipe
#endif /* HISTOGRAM_STEP_HPP_ */
