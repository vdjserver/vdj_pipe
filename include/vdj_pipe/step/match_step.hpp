/** @file "/vdj_pipe/include/vdj_pipe/step/match_step.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef MATCH_STEP_HPP_
#define MATCH_STEP_HPP_
#include <string>
#include <iosfwd>
#include <vector>
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/step/step_base_single.hpp"

namespace vdj_pipe{
namespace match{
class Match_element_battery;
}

/**@brief 
*******************************************************************************/
class VDJ_PIPE_DECL Match_step : public detail::Step_base_single {
   typedef boost::shared_ptr<match::Match_element_battery> me_battery_ptr;
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "match")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "alignment")
   VDJ_PIPE_STATIC_STRING_METHOD(comment, "find sequence features")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Match_step(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish();
   void summary(std::ostream& os) const;

private:
   me_battery_ptr mb_;
   bool trim_;
   bool reverse_;
};

}//namespace vdj_pipe
#endif /* MATCH_STEP_HPP_ */
