/** @file "/vdj_pipe/include/vdj_pipe/step/merge_paired_reads_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef MERGE_PAIRED_READS_STEP_HPP_
#define MERGE_PAIRED_READS_STEP_HPP_
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/step/step_base_paired.hpp"
#include "vdj_pipe/value_map_access_single.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Merge_paired : public detail::Step_base_paired {
public:
   typedef Vm_access_paired vma_type;
   VDJ_PIPE_STATIC_STRING_METHOD(name, "merge_paired")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "paired read")
   VDJ_PIPE_STATIC_STRING_METHOD(comment, "merge forward and reverse reads")
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Merge_paired(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}
   void summary(std::ostream& os) const;

private:
   Value_ids_single vis_;
   Val_id score_id_;
   Val_id cigar_id_;
   std::size_t n_merged_;
   unsigned min_score_;
   bool trimmed_;
};

}//namespace vdj_pipe
#endif /* MERGE_PAIRED_READS_STEP_HPP_ */
