/** @file "/vdj_pipe/include/vdj_pipe/step/quality_stats_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef QUALITY_STATS_STEP_HPP_
#define QUALITY_STATS_STEP_HPP_
#include <iosfwd>
#include <string>
#if defined(NDEBUG)
//fix boost::multi_array warnings
//boost/multi_array/base.hpp:503:13: warning: unused variable 'bound_adjustment'
#define BOOST_DISABLE_ASSERTS
#endif
#include "boost/array.hpp"
#include "boost/multi_array.hpp"
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/file.hpp"
#include "vdj_pipe/histogram_1d.hpp"
#include "vdj_pipe/step/step_base_single.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Qual_stats : public detail::Step_base_single {
   typedef boost::multi_array<std::size_t, 2> heat_map;

   typedef boost::array<double,5> fractions;
   static fractions const& fracts() {
      static const fractions f = {{0.1, 0.25, 0.5, 0.75, 0.9}};
      return f;
   }

public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "quality_stats")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "statistics")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "calculate quality statistics of sequencing reads"
   )

   VDJ_PIPE_STATIC_STRING_METHOD(
      description,
      "based on trimmed reads"
   )

   Qual_stats(
            Vm_access_single const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish();

private:
   heat_map hm_;
   Histogram_1d mqh_;
   Histogram_1d lh_;
   std::string out_hm_;
   std::string out_qs_;
   std::string out_mqh_;
   std::string out_len_hist_;

   void write_heat_map() const;
   void write_qstats() const;
   void write_meanq_hist() const;
   void write_len_hist() const;
};

}//namespace vdj_pipe
#endif /* QUALITY_STATS_STEP_HPP_ */
