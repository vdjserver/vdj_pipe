/** @file "/vdj_pipe/include/vdj_pipe/step/step_base.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_BASE_HPP_
#define STEP_BASE_HPP_
#include <iosfwd>
#include "boost/property_tree/ptree_fwd.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{
class Pipe_environment;

namespace detail{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Step_base {
public:
   typedef Seq_record::sequence seq_type;
   typedef Qual_record::quality qual_type;

   struct Err : public base_exception {};

   char delimiter() const {return delimiter_;}

   Step_base(
            boost::property_tree::ptree const& pt,
            Pipe_environment const& pe
   );

   void summary(std::ostream&) const {}

private:
   char delimiter_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* STEP_BASE_HPP_ */
