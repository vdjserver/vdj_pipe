/** @file "/vdj_pipe/include/vdj_pipe/step/step_base_paired.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_BASE_PAIRED_HPP_
#define STEP_BASE_PAIRED_HPP_
#include "vdj_pipe/step/step_base.hpp"
#include "vdj_pipe/value_map_access_paired.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief 
*******************************************************************************/
class Step_base_paired : public Step_base {
protected:
   Step_base_paired(
            Vm_access_paired const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   )
   : Step_base(pt, pe), vma_(vma)
   {}

   Vm_access_paired vma_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* STEP_BASE_PAIRED_HPP_ */
