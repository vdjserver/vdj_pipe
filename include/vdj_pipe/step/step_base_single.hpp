/** @file "/vdj_pipe/include/vdj_pipe/step/step_base_single.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_BASE_SINGLE_HPP_
#define STEP_BASE_SINGLE_HPP_
#include "vdj_pipe/step/step_base.hpp"
#include "vdj_pipe/value_map_access_single.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief
*******************************************************************************/
class Step_base_single : public Step_base {
public:
   typedef Vm_access_single vma_type;

   void reset_access(Value_ids_single const& ids) {vma_.reset(ids);}

protected:
   Step_base_single(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment const& pe
   )
   : Step_base(pt, pe), vma_(vma)
   {}

   Vm_access_single vma_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* STEP_BASE_SINGLE_HPP_ */
