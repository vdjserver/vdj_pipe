/** @file "/vdj_pipe/include/vdj_pipe/step/step_factory.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_FACTORY_HPP_
#define STEP_FACTORY_HPP_
#include "boost/property_tree/ptree_fwd.hpp"

namespace vdj_pipe{
class Pipe_environment;

/**@brief 
*******************************************************************************/
template<class Config> typename Config::processing_step create_step(
         typename Config::value_map_access const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
);

}//namespace vdj_pipe
#endif /* STEP_FACTORY_HPP_ */
