/** @file "/vdj_pipe/include/vdj_pipe/step/step_factory_paired.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_FACTORY_PAIRED_HPP_
#define STEP_FACTORY_PAIRED_HPP_
#include "vdj_pipe/step/all_paired_read_steps.hpp"
#include "vdj_pipe/step/step_variant_paired_read.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/step/step_factory.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
VDJ_PIPE_DECL step_variant_paired create_step_paired(
         Vm_access_paired const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
);

/**@brief
*******************************************************************************/
template<> inline Config_paired_reads::processing_step
create_step<Config_paired_reads>(
         Config_paired_reads::value_map_access const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
) {
   return create_step_paired(vma, pt, pe);
}

}//namespace vdj_pipe
#endif /* STEP_FACTORY_PAIRED_HPP_ */
