/** @file "/vdj_pipe/include/vdj_pipe/step/step_maker.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_MAKER_HPP_
#define STEP_MAKER_HPP_
#include <string>
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/static_assert.hpp"

namespace vdj_pipe{
class Pipe_environment;

namespace step{

/**@brief
*******************************************************************************/
template<class Step> struct Maker {
   typedef Step result_type;
   typedef typename result_type::vma_type vma_type;

   static std::string const& name() {return result_type::name();}
   static std::string const& category() {return result_type::category();}
   static std::string const& comment() {return result_type::comment();}
   static std::string const& description() {return result_type::description();}

   static result_type make(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   ) {
      return result_type(vma, pt, pe);
   }
};

/**@brief
*******************************************************************************/
template<class Step> struct Find_maker {
   typedef Maker<Step> type;
//   BOOST_STATIC_ASSERT(sizeof(type));
};

}//namespace step
}//namespace vdj_pipe
#endif /* STEP_MAKER_HPP_ */
