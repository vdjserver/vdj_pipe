/** @file "/vdj_pipe/include/vdj_pipe/step/step_variant_paired_emid_read.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_VARIANT_PAIRED_EMID_READ_HPP_
#define STEP_VARIANT_PAIRED_EMID_READ_HPP_
#include <string>
#include "boost/mpl/end.hpp"
#include "boost/mpl/insert_range.hpp"
#include "boost/mpl/push_back.hpp"
#include "boost/variant.hpp"
#include "vdj_pipe/step/step_variant_paired_read.hpp"
#include "vdj_pipe/step/step_variant_universal.hpp"

namespace vdj_pipe{
class External_mid_infile;
class Vm_access_paired_emid;

namespace step{

/**@brief List of steps for pipes of paired reads with eMIDs
*******************************************************************************/
typedef boost::mpl::insert_range<
         universal_steps,
         boost::mpl::end<universal_steps>::type,
         boost::mpl::push_back<paired_read_only_vector, External_mid_infile>::type
>::type paired_emid_read_vector;

}//namespace step

typedef boost::make_recursive_variant_over<step::paired_emid_read_vector>::type
         step_variant_paired_emid;

/**@brief
*******************************************************************************/
struct Config_paired_emid_reads {
   typedef void input_step;
   typedef step_variant_paired_emid processing_step;
   typedef Vm_access_paired_emid value_map_access;
};

}//namespace vdj_pipe
#endif /* STEP_VARIANT_PAIRED_EMID_READ_HPP_ */
