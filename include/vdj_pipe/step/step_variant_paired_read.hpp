/** @file "/vdj_pipe/include/vdj_pipe/step/step_variant_paired_read.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_VARIANT_PAIRED_READ_HPP_
#define STEP_VARIANT_PAIRED_READ_HPP_
#include <string>
#include "boost/mpl/end.hpp"
#include "boost/mpl/insert_range.hpp"
#include "boost/variant.hpp"
#include "vdj_pipe/step/step_variant_universal.hpp"

namespace vdj_pipe{
class Apply_one;
class Apply_many;
class Merge_paired;

class Vm_access_paired;

namespace step{

/**@brief List of steps for paired read pipes
*******************************************************************************/
typedef boost::mpl::vector<
         Apply_many,
         Apply_one,
         Merge_paired
> paired_read_only_vector;

/**@brief List of steps for paired read pipes
*******************************************************************************/
typedef boost::mpl::insert_range<
         universal_steps,
         boost::mpl::end<universal_steps>::type,
         paired_read_only_vector
>::type paired_read_vector;

}//namespace step

typedef boost::make_recursive_variant_over<step::paired_read_vector>::type
         step_variant_paired;

/**@brief
*******************************************************************************/
struct Config_paired_reads {
   typedef void input_step;
   typedef step_variant_paired processing_step;
   typedef Vm_access_paired value_map_access;
};

}//namespace vdj_pipe
#endif /* STEP_VARIANT_PAIRED_READ_HPP_ */
