/** @file "/vdj_pipe/include/vdj_pipe/step/step_variant_single_read.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_VARIANT_SINGLE_READ_HPP_
#define STEP_VARIANT_SINGLE_READ_HPP_
#include <string>
#include "boost/mpl/end.hpp"
#include "boost/mpl/insert_range.hpp"
#include "boost/mpl/vector/vector30.hpp"
#include "boost/variant.hpp"
#include "vdj_pipe/step/step_variant_universal.hpp"

namespace vdj_pipe{
class Ambiguous_window_filter;
class Average_quality_filter;
class Average_quality_window_filter;
class Character_filter;
class Composition_stats;
class External_mid_inline;
class Find_shared;
class Homopolymer_filter;
class Length_filter;
class Match_step;
class Min_quality_filter;
class Min_quality_window_filter;
class Qual_stats;
class Write_seq;

class Vm_access_single;

namespace step{

/**@brief List of steps suitable for single read pipes ONLY
*******************************************************************************/
typedef boost::mpl::vector<
         Ambiguous_window_filter,
         Average_quality_filter,
         Average_quality_window_filter,
         Character_filter,
         Composition_stats,
         External_mid_inline,
         Find_shared,
         Homopolymer_filter,
         Length_filter,
         Match_step,
         Min_quality_filter,
         Min_quality_window_filter,
         Qual_stats,
         Write_seq
> single_read_only_vector;

/**@brief List of all steps for single read pipes
*******************************************************************************/
typedef boost::mpl::insert_range<
         universal_steps,
         boost::mpl::end<universal_steps>::type,
         single_read_only_vector
>::type single_read_vector;

}//namespace step

typedef boost::make_recursive_variant_over<step::single_read_vector>::type
         step_variant_single;

/**@brief
*******************************************************************************/
struct Config_single_reads {
   typedef void input_step;
   typedef step_variant_single processing_step;
   typedef Vm_access_single value_map_access;
};

}//namespace vdj_pipe
#endif /* STEP_VARIANT_SINGLE_READ_HPP_ */
