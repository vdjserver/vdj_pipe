/** @file "/vdj_pipe/include/vdj_pipe/step/step_variant_store.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_VARIANT_STORE_HPP_
#define STEP_VARIANT_STORE_HPP_
#include <vector>
#include "boost/foreach.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/step/step_variant_paired_emid_read.hpp"
#include "vdj_pipe/step/step_variant_paired_read.hpp"
#include "vdj_pipe/step/step_variant_single_read.hpp"
#include "vdj_pipe/step/all_steps.hpp"

namespace vdj_pipe{

namespace step{

/**@brief
*******************************************************************************/
typedef boost::mpl::insert_range<
         universal_steps,
         boost::mpl::end<universal_steps>::type,
         boost::mpl::insert_range<
            single_read_only_vector,
            boost::mpl::end<single_read_only_vector>::type,
            boost::mpl::push_back<paired_read_only_vector,External_mid_infile>::type
         >::type
>::type all_steps_vector;

}//namespace step

typedef boost::make_recursive_variant_over<step::all_steps_vector>::type
         step_variant_all;

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Step_variant_store {
public:

   void push_back(step_variant_all const& sva) {svv_.push_back(sva);}

   template<class Visitor> void visit(Visitor const& visitor) {
      BOOST_FOREACH(step_variant_all& sv, svv_) {
         boost::apply_visitor(visitor, sv);
      }
   }

   template<class Visitor> void visit(Visitor& visitor) {
      BOOST_FOREACH(step_variant_all& sv, svv_) {
         boost::apply_visitor(visitor, sv);
      }
   }

   //used for testing
   step_variant_all const& step(const std::size_t n) const {return svv_[n];}
   step_variant_all& step(const std::size_t n) {return svv_[n];}

private:
   std::vector<step_variant_all> svv_;
};

}//namespace vdj_pipe
#endif /* STEP_VARIANT_STORE_HPP_ */
