/** @file "/vdj_pipe/include/vdj_pipe/step/step_variant_universal.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_VARIANT_UNIVERSAL_HPP_
#define STEP_VARIANT_UNIVERSAL_HPP_
#include "boost/mpl/vector.hpp"

namespace vdj_pipe{
class Blank_step;
class Histogram_step;
class Write_value;

namespace step{

/**@brief List of steps that can be used in all pipes
*******************************************************************************/
typedef boost::mpl::vector<
            Blank_step,
            Histogram_step,
            Write_value
         > universal_steps;

}//namespace step
}//namespace vdj_pipe
#endif /* STEP_VARIANT_UNIVERSAL_HPP_ */
