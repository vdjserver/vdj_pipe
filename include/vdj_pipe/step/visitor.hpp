/** @file "/vdj_pipe/include/vdj_pipe/step/visitor.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VISITOR_HPP_
#define VISITOR_HPP_
#include <ostream>
#include "boost/variant.hpp"
#include "vdj_pipe/exception.hpp"

namespace vdj_pipe{
struct Seq_qual_record;

/**@brief 
*******************************************************************************/
struct Run_visitor : public boost::static_visitor<> {
   template<typename Step> void operator()(Step& step) const {
      try{
         step.run();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  base_exception()
                  << base_exception::msg_t("processing step error")
                  << base_exception::str1_t(Step::name())
                  << base_exception::nested_t(boost::current_exception())
         );
      }
   }
};

/**@brief
*******************************************************************************/
struct Finish_visitor : public boost::static_visitor<> {
   template<typename Step> void operator()(Step& step) const {
      try{
         step.finish();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  base_exception()
                  << base_exception::msg_t("error finalizing processing step")
                  << base_exception::str1_t(Step::name())
                  << base_exception::nested_t(boost::current_exception())
         );
      }
   }
};

/**@brief
*******************************************************************************/
class Summary_visitor : public boost::static_visitor<> {
public:
   explicit Summary_visitor(std::ostream& os) : os_(os), n_(1) {
      os_ << "Step summaries" << '\n';
   }

   template<typename Step> void operator()(Step& step) {
      os_
      << '\n' << n_++ << ". " << Step::name() << '\n';
      step.summary(os_);
   }

private:
   std::ostream& os_;
   std::size_t n_;
};

/**@brief
*******************************************************************************/
class Summary_visitor2 : public boost::static_visitor<> {
public:
   explicit Summary_visitor2(std::ostream& os) : os_(os) {}

   template<typename Step> void operator()(Step& step) {
      step.summary(os_);
   }

private:
   std::ostream& os_;
};

}//namespace vdj_pipe
#endif /* VISITOR_HPP_ */
