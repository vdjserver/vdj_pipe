/** @file "/vdj_pipe/include/vdj_pipe/step/write_sequence_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef WRITE_SEQUENCE_STEP_HPP_
#define WRITE_SEQUENCE_STEP_HPP_
#include <iosfwd>
#include <string>
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/file_ostream_variant.hpp"
#include "vdj_pipe/step/step_base_single.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Write_seq : public detail::Step_base_single {
public:
   VDJ_PIPE_STATIC_STRING_METHOD(name, "write_sequence")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "output")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "output sequences"
   )
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Write_seq(
            Vm_access_single const& vm,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void run();
   void finish() {}
   void summary(std::ostream& os) const;

private:
   File_ostream_variant fov_;
   std::size_t n_seq_;
   bool trim_;
   bool reverse_;
   bool skip_empty_;
};

}//namespace vdj_pipe
#endif /* WRITE_SEQUENCE_STEP_HPP_ */
