/** @file "/vdj_pipe/include/vdj_pipe/step/write_value_step.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef WRITE_VALUE_STEP_HPP_
#define WRITE_VALUE_STEP_HPP_
#include <string>
#include <vector>
#include "boost/property_tree/ptree_fwd.hpp"
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/file_ostream_variant.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/step/step_base.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{
class Value_ids_single;
class File_ostream;

/**@brief
*******************************************************************************/
class VDJ_PIPE_DECL Write_value : public detail::Step_base {
public:
   typedef Value_map vma_type;
   VDJ_PIPE_STATIC_STRING_METHOD(name, "write_value")
   VDJ_PIPE_STATIC_STRING_METHOD(category, "misc")
   VDJ_PIPE_STATIC_STRING_METHOD(
      comment,
      "write specified values"
   )
   VDJ_PIPE_STATIC_STRING_METHOD(description, "XXX")

   Write_value(
            vma_type const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   );

   void reset_access(Value_ids_single const& ids) {}
   void run();
   void finish() {}
   void summary(std::ostream& os) const;

private:
   Value_map vma_;
   std::vector<std::string> v_names_;
   std::vector<Val_id> v_ids_;
   File_ostream_variant fov_;
   std::size_t n_seq_;

   static std::vector<std::string>
   get_names(boost::property_tree::ptree const& pt);

   std::vector<Val_id> get_ids();

   std::string make_header() const;
};

}//namespace vdj_pipe
#endif /* WRITE_VALUE_STEP_HPP_ */
