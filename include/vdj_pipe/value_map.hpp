/** @file "/vdj_pipe/include/vdj_pipe/value_map.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VALUE_MAP_HPP_
#define VALUE_MAP_HPP_
#include "boost/assert.hpp"
#include "boost/shared_ptr.hpp"

#include "vdj_pipe/detail/named_value_map.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/sequence_record.hpp"
#include "vdj_pipe/sequence_transform.hpp"
#include "vdj_pipe/value_variant.hpp"

namespace vdj_pipe{

/**@brief Store values mapped against name strings and value IDs
*******************************************************************************/
class Value_map {
   typedef detail::Named_value_map<Val_id, value_variant> map_type;
   typedef boost::shared_ptr<map_type> map_ptr;
   enum {
      read_count_i = 1,
      file_read_count_i = 2,
      seq_fn_i = 3,
      read_id_i = 4
   };

public:
   typedef map_type::iterator iterator;
   typedef iterator const_iterator;
   struct Err : public base_exception{};

   Value_map()
   : map_(new map_type())
   {
      const std::string rc = "read_count";
      insert_new_name(rc);
      BOOST_ASSERT(value_id(rc)() == read_count_i );

      const std::string frc = "file_read_count";
      insert_new_name(frc);
      BOOST_ASSERT(value_id(frc)() == file_read_count_i );

      const std::string sfn = "seq_file_name";
      insert_new_name(sfn);
      BOOST_ASSERT(value_id(sfn)() == seq_fn_i );

      const std::string ri = "read_id";
      insert_new_name(ri);
      BOOST_ASSERT(value_id(ri)() == read_id_i );

      read_count_reset();
      file_read_count_reset();
   }

   iterator begin() const {return map_->begin();}
   iterator end() const {return map_->end();}
   std::size_t size() const {return map_->size();}

   void clear_values() {
	   for(iterator i = Val_id(read_id_i); i != end(); ++i) {
		   (*this)[*i] = value_variant();
	   }
   }

   std::string const& name(const Val_id vid) const {
      return map_->name(vid);
   }

   Val_id insert_name(std::string const& name) {
      return map_->insert_name(name);
   }

   Val_id insert_new_name(std::string const& name) {
      return map_->insert_new_name(name);
   }

   Val_id const* find_id(std::string const& name) const {
      return map_->find_id(name);
   }

   Val_id value_id(std::string const& name) const {
      return map_->value_id(name);
   }

   value_variant const& operator[](const Val_id vid) const {
      return (*map_)[vid];
   }

   value_variant& operator[](const Val_id vid) {
      return (*map_)[vid];
   }

   long read_count() const {
      return value<long>(Val_id(read_count_i));
   }

   void read_count_reset() {
      (*this)[Val_id(read_count_i)] = (long)0;
   }

   void read_count_incr() {
      ++boost::get<long>((*map_)[Val_id(read_count_i)]);
   }

   long file_read_count() const {
      return value<long>(Val_id(file_read_count_i));
   }

   void file_read_count_reset() {
      (*this)[Val_id(file_read_count_i)] = (long)0;
   }

   void file_read_count_incr() {
      ++boost::get<long>((*map_)[Val_id(file_read_count_i)]);
   }

   std::string const& seq_file_name() const {
      return value<std::string>(Val_id(seq_fn_i));
   }

   void seq_file_name(std::string const& fn) {
      (*this)[Val_id(seq_fn_i)] = fn;
   }

   std::string const& read_id_nothrow() const {
      return str_nothrow(Val_id(read_id_i));
   }

   std::string const& read_id() const {
      return value<std::string>(Val_id(read_id_i));
   }

   void read_id(std::string const& id) {
      (*this)[Val_id(read_id_i)] = id;
   }

   template<typename T> T const& value(std::string const& name) const {
      const Val_id id = value_id(name);
      if( ! id ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unknown value name")
               << Err::str1_t(sanitize(name))
      );
      return value<T>(id);
   }

   template<typename T> T const* value_ptr(std::string const& name) const {
      const Val_id id = value_id(name);
      if( ! id ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unknown value name")
               << Err::str1_t(sanitize(name))
      );
      return value_ptr<T>(id);
   }

   template<typename T> T const& value(const Val_id id) const {
      try{
         return boost::get<T>((*map_)[id]);
      }catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("wrong value type")
                  << Err::str1_t(variable_type_str((*map_)[id]))
                  << Err::str2_t(sanitize(name(id)))
                  << Err::nested_t(boost::current_exception())
         );
      }
   }

   template<typename T> T const* value_ptr(const Val_id id) const {
      return boost::get<T>(&(*map_)[id]);
   }

   std::string const&
   str_nothrow(const Val_id id) const {
      if( std::string const* id_ptr = boost::get<std::string>(&(*map_)[id]) )
         return *id_ptr;
      static const std::string id_str = "not_available";
      return id_str;
   }

protected:

    std::string sequence(
            const Val_id seq_id,
            const Val_id trim_id,
            const bool trimmed,
            const bool reverse_compl
   ) const {
      std::string const& seq = value<std::string>(seq_id);
      return transform(
               seq,
               trimmed ?
                        value<sequence_interval>(trim_id) :
                        sequence_interval(0U, seq.size()),
               reverse_compl
      );
   }

   Qual_record::quality quality(
            const Val_id qual_id,
            const Val_id trim_id,
            const bool trimmed,
            const bool reverse_compl
   ) const {
      Qual_record::quality const& q = value<Qual_record::quality>(qual_id);
      std::size_t n1 = 0;
      std::size_t n2 = q.size();
      if( trimmed ) {
         const sequence_interval si = value<sequence_interval>(trim_id);
         n1 = lower(si);
         n2 = upper(si);
      }
      if( n1 > q.size() || n2 > q.size() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("sequence interval and quality score mismatch")
               << Err::int1_t(std::max(n1, n2))
               << Err::int2_t(q.size())
      );

      Qual_record::quality q1(n2 - n1);
      if( reverse_compl ) {
         for(std::size_t i = 0, j = n2-1; i != q1.size(); ++i, --j) q1[i] = q[j];
      } else {
         for( std::size_t i = 0, j = n1; i != q1.size(); ++i, ++j) q1[i] = q[j];
      }
      return q1;
   }

private:
   map_ptr map_;
};

}//namespace vdj_pipe
#endif /* VALUE_MAP_HPP_ */
