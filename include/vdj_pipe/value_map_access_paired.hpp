/** @file "/vdj_pipe/include/vdj_pipe/value_map_access_paired.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VALUE_MAP_ACCESS_PAIRED_HPP_
#define VALUE_MAP_ACCESS_PAIRED_HPP_
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/value_map_access_single.hpp"

namespace vdj_pipe{

/**@brief Access to value map for paired read pipeline and processing steps
*******************************************************************************/
class Vm_access_paired : public Value_map {
public:
   typedef Seq_record::sequence seq_type;
   typedef Qual_record::quality qual_type;

   struct Err : public base_exception {};

   Vm_access_paired()
   : Value_map(), ids_(*this)
   {}

   std::string const& description_fwd() const {
      return value<std::string>(ids_.description_fwd());
   }

   void description_fwd(std::string const& descr) {
      (*this)[ids_.description_fwd()] = descr;
   }

   std::string const& description_rev() const {
      return value<std::string>(ids_.description_rev());
   }

   void description_rev(std::string const& descr) {
      (*this)[ids_.description_rev()] = descr;
   }

   seq_type const& sequence_fwd() const {
      return value<std::string>(ids_.sequence_fwd());
   }

   std::string sequence_fwd(const bool trim) const {
      return sequence(ids_.sequence_fwd(), ids_.interval_fwd(), trim, false);
   }

   void sequence_fwd(seq_type const& seq) {
      (*this)[ids_.sequence_fwd()] = seq;
   }

   seq_type const& sequence_rev() const {
      return value<std::string>(ids_.sequence_rev());
   }

   std::string sequence_rev(const bool trim, const bool reverse) const {
      return sequence(ids_.sequence_rev(), ids_.interval_rev(), trim, reverse);
   }

   void sequence_rev(seq_type const& seq) {
      (*this)[ids_.sequence_rev()] = seq;
   }

   qual_type const& quality_fwd() const {
      return value<qual_type>(ids_.quality_fwd());
   }

   qual_type quality_fwd(const bool trim) const {
      return quality(ids_.quality_fwd(), ids_.interval_fwd(), trim, false);
   }

   void quality_fwd(qual_type const& qual) {
      (*this)[ids_.quality_fwd()] = qual;
   }

   qual_type const& quality_rev() const {
      return value<qual_type>(ids_.quality_rev());
   }

   qual_type quality_rev(const bool trim, const bool reverse) const {
      return quality(ids_.quality_rev(), ids_.interval_rev(), trim, reverse);
   }

   void quality_rev(qual_type const& qual) {
      (*this)[ids_.quality_rev()] = qual;
   }

   sequence_interval interval_fwd() const {
      return value<sequence_interval>(ids_.interval_fwd());
   }

   void interval_fwd(sequence_interval const& si) {
      (*this)[ids_.interval_fwd()] = si;
   }

   sequence_interval interval_rev() const {
      return value<sequence_interval>(ids_.interval_rev());
   }

   void interval_rev(sequence_interval const& si) {
      (*this)[ids_.interval_rev()] = si;
   }

   std::string const& sequence_path_fwd() const {
      return value<std::string>(ids_.sequence_path_fwd());
   }

   void sequence_path_fwd(std::string const& s) {
      (*this)[ids_.sequence_path_fwd()] = s;
   }

   std::string const& sequence_path_rev() const {
      return value<std::string>(ids_.sequence_path_rev());
   }

   void sequence_path_rev(std::string const& s) {
      (*this)[ids_.sequence_path_rev()] = s;
   }

   std::string const& quality_path_fwd() const {
      return value<std::string>(ids_.quality_path_fwd());
   }

   void quality_path_fwd(std::string const& s) {
      (*this)[ids_.quality_path_fwd()] = s;
   }

   std::string const& quality_path_rev() const {
      return value<std::string>(ids_.quality_path_rev());
   }

   void quality_path_rev(std::string const& s) {
      (*this)[ids_.quality_path_rev()] = s;
   }

protected:
   Value_ids_paired ids_;
};

/**@brief Access to value map for paired eMID read pipeline and processing steps
*******************************************************************************/
class Vm_access_paired_emid : public Vm_access_paired {
public:
   Vm_access_paired_emid()
   : Vm_access_paired(), ids_(*this)
   {}

   seq_type const& emid_fwd() const {
      return value<std::string>(ids_.emid_fwd());
   }

   void emid_fwd(seq_type const& seq) {
      (*this)[ids_.emid_fwd()] = seq;
   }

   seq_type const& emid_rev() const {
      return value<std::string>(ids_.emid_rev());
   }

   void emid_rev(seq_type const& seq) {
      (*this)[ids_.emid_rev()] = seq;
   }

   std::string const& emid_path_fwd() const {
      return value<std::string>(ids_.emid_path_fwd());
   }

   std::string const& emid_path_rev() const {
      return value<std::string>(ids_.emid_path_rev());
   }

   void emid_path_fwd(std::string const& s) {
      (*this)[ids_.emid_path_fwd()] = s;
   }

   void emid_path_rev(std::string const& s) {
      (*this)[ids_.emid_path_rev()] = s;
   }

private:
   Value_ids_emid ids_;
};

}//namespace vdj_pipe
#endif /* VALUE_MAP_ACCESS_PAIRED_HPP_ */
