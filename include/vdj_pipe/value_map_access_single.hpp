/** @file "/vdj_pipe/include/vdj_pipe/value_map_access_single.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VALUE_MAP_ACCESS_SINGLE_HPP_
#define VALUE_MAP_ACCESS_SINGLE_HPP_
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/value_names.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
class Vm_access_single : public Value_map {
public:
   typedef Seq_record::sequence seq_type;
   typedef Qual_record::quality qual_type;

   struct Err : public base_exception {};

   Vm_access_single(Value_names::single_read_names const& names)
   : Value_map(), ids_(Value_ids_single::create(*this, names))
   {}

   Vm_access_single(
            Value_map const& vm,
            Value_names::single_read_names const& names
   )
   : Value_map(vm), ids_(Value_ids_single::ensure(*this, names))
   {}

   Vm_access_single(Value_map const& vm, Value_ids_single const& ids)
   : Value_map(vm), ids_(ids)
   {}

   void reset(Value_ids_single const& ids) {ids_ = ids;}
   std::string const& description() const {return value<std::string>(ids_.description());}
   void description(std::string const& descr) {(*this)[ids_.description()] = descr;}
   seq_type const& sequence() const {return value<std::string>(ids_.sequence());}
   void sequence(seq_type const& seq) {(*this)[ids_.sequence()] = seq;}
   qual_type const& quality() const {return value<qual_type>(ids_.quality());}
   void quality(qual_type const& qual) {(*this)[ids_.quality()] = qual;}

   /**@return sequence interval */
   sequence_interval interval() const {return value<sequence_interval>(ids_.trim());}

   /**@param si set read sequence interval to \a si */
   void interval(sequence_interval const& si) {(*this)[ids_.trim()] = si;}
   void set_empty() {(*this)[ids_.trim()] = sequence_interval::empty();}

   void trim(sequence_interval const& si) {
      (*this)[ids_.trim()] = intersect(interval(), si);
   }

   /**@return true if read sequence marked as reverse */
   bool is_reverse() const {
      try{
         if( ids_.direction() ) return boost::get<bool>((*this)[ids_.direction()]);
      }catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("wrong type for current sequence direction")
                  << Err::nested_t(boost::current_exception())
         );
      }
      return ids_.is_reverse();
   }

   void set_reverse(const bool rev) {(*this)[ids_.direction()] = rev;}

   /**
    @param trimmed
    @param reverse_compl
    @return sequence complete or within #interval() if \a trimmed, and
    reverse-complemented if #is_reverse() and \a reverse_compl
    */
   std::string sequence(
            const bool trimmed,
            const bool reverse_compl
   ) const {
      return Value_map::sequence(
               ids_.sequence(),
               ids_.trim(),
               trimmed,
               reverse_compl && is_reverse()
      );
   }

   qual_type quality(
            const bool trimmed,
            const bool reverse_compl
   ) const {
      return Value_map::quality(
               ids_.quality(),
               ids_.trim(),
               trimmed,
               reverse_compl && is_reverse()
      );
   }

   std::string const& sequence_path() const {
      return value<std::string>(ids_.sequence_path());
   }

   void sequence_path(std::string const& s) {(*this)[ids_.sequence_path()] = s;}

   std::string const& quality_path() const {
      return value<std::string>(ids_.quality_path());
   }

   void quality_path(std::string const& s) {(*this)[ids_.quality_path()] = s;}

private:
   Value_ids_single ids_;
};

}//namespace vdj_pipe
#endif /* VALUE_MAP_ACCESS_SINGLE_HPP_ */
