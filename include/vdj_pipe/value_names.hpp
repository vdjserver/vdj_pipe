/** @file "/vdj_pipe/include/vdj_pipe/value_names.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VALUE_NAMES_HPP_
#define VALUE_NAMES_HPP_
#include <string>
#include <vector>
#include "boost/array.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/object_ids.hpp"

namespace vdj_pipe{
class Value_map;

/**@brief 
*******************************************************************************/
struct VDJ_PIPE_DECL Value_names {
   typedef boost::array<std::string,8> single_read_names;
   static single_read_names const& single();

   static single_read_names const& forward();

   static single_read_names const& reverse();

   static single_read_names const& merged();
};

/**@brief Provides access to standard values for single read pipeline
*******************************************************************************/
class VDJ_PIPE_DECL Value_ids_single {
   typedef boost::array<Val_id, 7> id_array;

   Value_ids_single() : is_reverse_(false) {}

public:
   /**All names are added to value map; throws if name already exists */
   static Value_ids_single create(
            Value_map& vm,
            Value_names::single_read_names const& names
   );

   /**Ensure that names are already present in value map */
   static Value_ids_single ensure(
            Value_map const& vm,
            Value_names::single_read_names const& names
   );

   /** names that are not in value map are created */
   Value_ids_single(Value_map& vm, Value_names::single_read_names const& names);

   Val_id description() const {return ida_[0];}
   Val_id sequence() const {return ida_[1];}
   Val_id quality() const {return ida_[2];}
   Val_id trim() const {return ida_[3];}
   Val_id direction() const {return ida_[4];}
   Val_id sequence_path() const {return ida_[5];}
   Val_id quality_path() const {return ida_[6];}
   bool is_reverse() const {return is_reverse_;}

private:
   id_array ida_;
   bool is_reverse_;
};

/**@brief Provides access to standard values for paired read pipeline
*******************************************************************************/
class VDJ_PIPE_DECL Value_ids_paired {
   typedef boost::array<Val_id, 12>
   id_array;

   enum {
      descr_fwd      = 0,
      descr_rev      = 1,
      seq_fwd        = 2,
      seq_rev        = 3,
      qual_fwd       = 4,
      qual_rev       = 5,
      trim_fwd       = 6,
      trim_rev       = 7,
      seq_path_fwd   = 8,
      seq_path_rev   = 9,
      qual_path_fwd  = 10,
      qual_path_rev  = 11
   };

public:
   explicit Value_ids_paired(Value_map& vm);

   Val_id description_fwd() const {return ida_[descr_fwd];}
   Val_id description_rev() const {return ida_[descr_rev];}
   Val_id sequence_fwd() const {return ida_[seq_fwd];}
   Val_id sequence_rev() const {return ida_[seq_rev];}
   Val_id quality_fwd() const {return ida_[qual_fwd];}
   Val_id quality_rev() const {return ida_[qual_rev];}
   Val_id interval_fwd() const {return ida_[trim_fwd];}
   Val_id interval_rev() const {return ida_[trim_rev];}
   Val_id sequence_path_fwd() const {return ida_[seq_path_fwd];}
   Val_id sequence_path_rev() const {return ida_[seq_path_rev];}
   Val_id quality_path_fwd() const {return ida_[qual_path_fwd];}
   Val_id quality_path_rev() const {return ida_[qual_path_rev];}

private:
   id_array ida_;
};

/**@brief Provides access to standard eMID values
*******************************************************************************/
class VDJ_PIPE_DECL Value_ids_emid {
   typedef boost::array<Val_id, 4> id_array;

   enum {
      emid_fwd_      = 0,
      emid_rev_      = 1,
      emid_path_fwd_ = 2,
      emid_path_rev_ = 3
   };

public:
   explicit Value_ids_emid(Value_map& vm);

   Val_id emid_fwd() const {return ida_[emid_fwd_];}
   Val_id emid_rev() const {return ida_[emid_rev_];}
   Val_id emid_path_fwd() const {return ida_[emid_path_fwd_];}
   Val_id emid_path_rev() const {return ida_[emid_path_rev_];}

private:
   id_array ida_;
};

}//namespace vdj_pipe
#endif /* VALUE_NAMES_HPP_ */
