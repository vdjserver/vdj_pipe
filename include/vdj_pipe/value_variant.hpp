/** @file "/vdj_pipe/include/vdj_pipe/value_variant.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VALUE_VARIANT_HPP_
#define VALUE_VARIANT_HPP_
#include <iosfwd>
#include "boost/array.hpp"
#include "boost/mpl/begin.hpp"
#include "boost/mpl/distance.hpp"
#include "boost/mpl/find.hpp"
#include "boost/mpl/size.hpp"
#include "boost/variant.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/sequence_interval.hpp"
#include "vdj_pipe/sequence_record.hpp"

namespace vdj_pipe{

/**@brief
*******************************************************************************/
struct Blank {
   bool operator==(Blank const&) const {return true;}
   bool operator<(Blank const&) const {return false;}
};

/**@brief
*******************************************************************************/
inline std::size_t hash_value(Blank const&) {return 0xd5d6573673acc2a3;}

/**@brief
*******************************************************************************/
template<class ChT, class Tr> inline
std::basic_ostream<ChT,Tr>& operator<<(
         std::basic_ostream<ChT,Tr>& os,
         Blank const&
) {
   return os << "Undetermined";
}

/**@brief
*******************************************************************************/
typedef boost::variant<
            Blank, bool, long, double, std::string,
            sequence_interval, Qual_record::quality
         >
value_variant;

/**@brief
*******************************************************************************/
inline std::string const& variable_type_str(const int which) {
   typedef boost::array<
               std::string,
               boost::mpl::size<value_variant::types>::value
            > array;
   static const array a =
   {{
            "blank", "bool", "integer", "float", "string",
            "interval", "quality"
   }};
   return a[which];
}

/**@brief
*******************************************************************************/
template<typename T> struct Type_index :
         boost::mpl::distance<
            typename boost::mpl::begin<value_variant::types>::type,
            typename boost::mpl::find<value_variant::types,T>::type
         >::type
         {};

/**@brief
*******************************************************************************/
inline bool is_blank(value_variant const& vv) {
   return boost::get<Blank>(&vv);
}

namespace detail{

struct Type_name : public boost::static_visitor<std::string const&> {
   template<typename T>
   std::string const& operator()(T const&) const {
      return variable_type_str(Type_index<T>::value);
   }
};

}//namespace detail

/**@brief
*******************************************************************************/
inline std::string const& variable_type_str(value_variant const& vv) {
   return boost::apply_visitor(detail::Type_name(), vv);
}

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL value_variant parse_variant(std::string const& s);

}//namespace vdj_pipe
#endif /* VALUE_VARIANT_HPP_ */
