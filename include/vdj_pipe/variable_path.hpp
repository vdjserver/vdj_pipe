/** @file "/vdj_pipe/include/vdj_pipe/variable_path.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_VARIABLE_PATH_HPP_
#define VDJ_PIPE_VARIABLE_PATH_HPP_
#include <algorithm>
#include "boost/foreach.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/detail/queable_ofstream_types.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"

namespace vdj_pipe{

/**@brief separate path into non-name and name areas
@details Example: out_dir/{name1}-dir/{name2}file.fna
*******************************************************************************/
VDJ_PIPE_DECL void
path_decompose(
         std::string const& path,
         std::vector<std::string>& tv,
         std::vector<std::string>& nv
);

/**@brief assemble path from template and values
@details Example: out_dir/{name1}-dir/{name2}file.fna
*******************************************************************************/
VDJ_PIPE_DECL std::string path_assemble(
         std::vector<std::string> const& templ,
         detail::Queable_ofstream_types::val_ref_vector const& vals
);

/**@brief
*******************************************************************************/
class Variable_path {
public:
   struct Err : public base_exception {};

   void init(std::string const& path, std::vector<std::string>& nv0) {
      std::vector<std::string> nv;
      path_decompose(path, pt_, nv);
      if( nv0.empty() ) nv0 = nv;
      ids_.reserve(nv.size());
      BOOST_FOREACH(std::string s, nv) {
         std::vector<std::string>::const_iterator i =
                  std::find(nv0.begin(), nv0.end(), s);
         if( i == nv0.end() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unknown variable")
                  << Err::str1_t(s)
         );
         ids_.push_back(Val_id(i - nv0.begin()));
      }
   }

   std::string operator()(
            detail::Queable_ofstream_types::val_vector const& vals
   ) const {
      detail::Queable_ofstream_types::val_ref_vector vr;
      vr.reserve(ids_.size());
      BOOST_FOREACH(const Val_id id, ids_) {
         vr.push_back(boost::cref(vals[id()]));
      }
      return path_assemble(pt_, vr);
   }

   std::string operator()() const {return pt_.front();}
   bool empty() const {return pt_.empty();}
   std::size_t size() const {return pt_.size();}

private:
   std::vector<Val_id> ids_;
   std::vector<std::string> pt_;
};

}//namespace vdj_pipe
#endif /* VDJ_PIPE_VARIABLE_PATH_HPP_ */
