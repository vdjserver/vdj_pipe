/** @file "/vdj_pipe/lib/apply_to_adaptor.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/apply_to_adaptor.hpp"

#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "vdj_pipe/step/step_factory_single.hpp"

namespace vdj_pipe{ namespace step{

VDJ_PIPE_KEYWORD_STRUCT(kwds, (to)(step)(forward)(reverse)(merged));
namespace {

typedef std::vector<Value_ids_single> vid_vector;

}//anonymous namespace

/*
*******************************************************************************/
Value_ids_single Apply_to_maker::make_ids(
         vma_type const& vma,
         std::string const& type
) {
   Value_names::single_read_names const* srn = 0;

   if( type == kwds::forward() ) {
      srn = &Value_names::forward();

   } else if( type == kwds::reverse() ) {
      srn = &Value_names::reverse();

   } else if( type == kwds::merged() ) {
      srn = &Value_names::merged();

   }
   if( ! srn ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("unknown read direction type")
            << Err::str1_t(sanitize(type))
   );

   return Value_ids_single::ensure(vma, *srn);
}

/*
*******************************************************************************/
Apply_to_maker::result_type Apply_to_maker::make(
         vma_type const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
) {
   vid_vector vv;
   bpt::ptree const* step_pt = 0;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::to() ) {
         if( vt.second.data().empty() ) {
            BOOST_FOREACH(bpt::ptree::value_type const& v2, vt.second) {
            vv.push_back(
                     make_ids(
                              vma,
                              v2.second.get_value<std::string>()
                     )
            );
            }
         } else {
            vv.push_back(
                     make_ids(
                              vma,
                              vt.second.data()
                     )
            );
         }
      } else if( vt.first == kwds::step() ) {
         if( step_pt ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("multiple \"step\" elements")
         );
         step_pt = &vt.second;
      }
   }
   if( vv.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no read directions provided")
   );

   if( ! step_pt ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no step provided")
   );

   const step_variant_single step =
            create_step_single(
                     Vm_access_single(vma, vv.front()),
                     *step_pt,
                     pe
            );

   if( vv.size() == 1 ) return Apply_one(step);

   return Apply_many(vv, step);
}

}//namespace step
}//namespace vdj_pipe
