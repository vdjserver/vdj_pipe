/** @file "/vdj_pipe/lib/combine_option_trees.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef COMBINE_OPTION_TREES_HPP_
#define COMBINE_OPTION_TREES_HPP_
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "keywords_root.hpp"

namespace vdj_pipe{

/**
*******************************************************************************/
void combine_option_trees(
         bpt::ptree& pt1,
         bpt::ptree const& pt2
) {
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt2) {

      //combine input section entries
      if( vt.first == kwds::Root::input() ) {
         if( ! pt1.count(kwds::Root::input()) ) {
            pt1.put_child(kwds::Root::input(), bpt::ptree());
         }
         bpt::ptree& in1 = pt1.get_child(kwds::Root::input());
         in1.insert(in1.end(), vt.second.begin(), vt.second.end());

      } else if( vt.first == kwds::Root::input_csv() ) {
         if( ! pt1.count(kwds::Root::input_csv()) ) {
            pt1.put_child(kwds::Root::input_csv(), bpt::ptree());
         }
         bpt::ptree& in1 = pt1.get_child(kwds::Root::input_csv());
         in1.insert(in1.end(), vt.second.begin(), vt.second.end());


      } else if( ! pt1.count(vt.first) ) {
         //insert other entries
         //entries already present in pt1 take precedence
         pt1.push_back(vt);

      }
   }
}

}//namespace vdj_pipe
#endif /* COMBINE_OPTION_TREES_HPP_ */
