/** @file "/vdj_pipe/lib/command_line_options.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3; see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/command_line_options.hpp"

#include <iostream>
#include "boost/foreach.hpp"
#include "boost/algorithm/string/case_conv.hpp"
#include "boost/program_options.hpp"
namespace bpo = boost::program_options;
#include "boost/property_tree/json_parser.hpp"
namespace bpt = boost::property_tree;

#include "combine_option_trees.hpp"
#include "keywords_root.hpp"
#include "step_registry.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/lib_info.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(
         help_kwds,
         (general)
         (steps)
);

/*
*******************************************************************************/
Command_line_options::Command_line_options(int argc, char* argv[])
: help_(),
  max_reads_(unset),
  max_file_reads_(unset),
  version_(false),
  no_stamp_(false),
  args_provided_(argc > 1)
{
   init_options();
   bpo::variables_map vm;
   try {
      bpo::store(bpo::command_line_parser(argc, argv).options(od_).run(), vm);
      notify(vm);
   } catch (const bpo::error& e) {
      std::ostringstream str;
      std::copy(argv, argv + argc, std::ostream_iterator<char const*>(str, " "));
      BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("invalid options")
            << Err::str1_t(sanitize(str.str(), 1000))
            << Err::str2_t(sanitize(e.what()))
            << Err::nested_t(boost::current_exception())
      );
   }
}

/*
*******************************************************************************/
void Command_line_options::init_options() {
   od_.add_options()
   ("help,h", bpo::value<std::string>(&help_)->implicit_value(help_kwds::general()),
            "display this help message"
   )
   ("version,v", bpo::bool_switch(&version_), "print version")
   ("no_stamp,q", bpo::bool_switch(&no_stamp_), "disable version stamp in output files")
   ("config,C", bpo::value<std::vector<std::string> >(&config_fn_),
            "configuration file")
   ("input_csv,V", bpo::value<std::vector<std::string> >(&input_csv_),
            "CSV (or TSV) file name with input descriptions")
   ("base_path_input,I", bpo::value<std::string>(&in_path_),
            "path for input files")
   ("base_path_output,O", bpo::value<std::string>(&out_path_),
            "path for output files")
   ("max_file_reads,F", bpo::value(&max_file_reads_)->value_name("NUM"),
            "process at most NUM reads from each input file")
   ("max_reads,M", bpo::value(&max_reads_)->value_name("NUM"),
            "process at most NUM reads")
   ;
}

/*
*******************************************************************************/
std::ostream& Command_line_options::print_version(std::ostream& os) {
   os << Lib_info::name() << ' ';
   Lib_info::print_version(os);
   return os;
}

/*
*******************************************************************************/
std::ostream& Command_line_options::print_info(std::ostream& os) {
   os
   << Lib_info::description() << '\n'
   << "Copyright " << Lib_info::copyright() << '\n'
   << "Distributed under " << Lib_info::license()
   ;
   return os;
}

/*
*******************************************************************************/
std::ostream& print_step_category(std::ostream& os, std::string const& cat) {
   os << boost::algorithm::to_upper_copy(cat) << " steps"<< '\n';
   BOOST_FOREACH(
            Step_registry::Meta const& m,
            Step_registry::get().category(cat)
   ) {
      os << '\t' << m.name_ << '\t' << m.comment_ << '\n';
   }
   return os << '\n';
}

/*
*******************************************************************************/
std::ostream& Command_line_options::print_help(std::ostream& os) const {
   print_version(os) << '\n' ;
   print_info(os) << '\n' ;

   if( help_ == help_kwds::general() ) {
      print_usage(os) << '\n' ;
      os
      << "options:" << '\n'
      << od_ << '\n'
      << "use \"--help steps\" for a list of processing steps" << '\n'
      << "use \"--help <step-name>\" for description of the processing step"
      << '\n'
      ;
   } else if( help_ == help_kwds::steps() ) {
      os << '\n' << "Processing steps:" << '\n';
      BOOST_FOREACH(
               std::string const& cat,
               Step_registry::get().categories()
      ) {
         print_step_category(os, cat);
      }
   } else if( Step_registry::get().categories().find(help_)) {
         print_step_category(os, help_);
   }
   return os;
}

/*
*******************************************************************************/
std::ostream& Command_line_options::print_usage(std::ostream& os) {
   os
   << "Usage: " << Lib_info::name() << ' '
   << "[options]"
   ;
   return os;
}

/*
*******************************************************************************/
bpt::ptree Command_line_options::tree() const {
   bpt::ptree pt1 = options_to_tree();
   BOOST_FOREACH(std::string const& s, config_files()) {
      bpt::ptree pt2;
      try{
         read_json(s, pt2);
      } catch(bpt::file_parser_error const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("parsing error")
                  << Err::str1_t(sanitize(s))
                  << Err::nested_t(boost::current_exception())
         );
      }

      try{
         combine_option_trees(pt1, pt2);
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("error combining options")
                  << Err::str1_t(sanitize(s))
                  << Err::nested_t(boost::current_exception())
         );
      }
   }
   return pt1;
}

/*
*******************************************************************************/
bpt::ptree Command_line_options::options_to_tree() const {
   bpt::ptree pt;
   if( ! args_provided() ) return pt;
   if( in_path_.size() ) {
      pt.put(kwds::Root::base_path_input(), in_path_);
   }

   if( out_path_.size() ) {
      pt.put(kwds::Root::base_path_output(), out_path_);
   }

   if( max_reads() != unset ) {
      pt.put(kwds::Root::max_reads(), max_reads());
   }

   if( max_file_reads() != unset ) {
      pt.put(kwds::Root::max_file_reads(), max_file_reads());
   }

   if (no_stamp_) {
     Lib_info::set_stamp_flag(!no_stamp_);
   }

   if( input_csv_.size() ) {
      bpt::ptree pt2;
      BOOST_FOREACH(std::string const& s, input_csv_) {
         pt2.put("", s);
      }
      pt.push_back(bpt::ptree::value_type(kwds::Root::input_csv(), pt2));
   }

   return pt;
}

}//namespace vdj_pipe
