/** @file "/vdj_pipe/lib/composition_stats_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/composition_stats_step.hpp"

#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/nucleotide_index.hpp"
#include "vdj_pipe/output_stamp.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(
         kwds,
         (out_prefix)(out_path_composition)(out_path_gc_hist)
);

/*
*******************************************************************************/
Composition_stats::Composition_stats(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Step_base_single(vma, pt, pe),
  cv_(),
  gch_(),
  comp_(),
  gc_hist_()
{
   std::string pref;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::out_prefix() ) {
         pref = vt.second.get_value<std::string>();

      } else if( vt.first == kwds::out_path_composition() ) {
         comp_ = pe.output().path(vt.second.data(), plot::line);

      } else if( vt.first == kwds::out_path_gc_hist() ) {
         gc_hist_ = pe.output().path(vt.second.data(), plot::bar);

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( comp_.empty() ) {
      comp_ = pe.output().path(pref + "composition.csv", plot::line);
   }

   if( gc_hist_.empty() ) {
      gc_hist_ = pe.output().path(pref + "gc_hist.csv", plot::bar);
   }
}

/*
*******************************************************************************/
void Composition_stats::run() {
   const sequence_interval si = vma_.interval();
   if( ! width(si) ) return;
   seq_type const& s = vma_.sequence();
   static const letter_counts lc0 = {{0,0,0,0,0}};
   if( cv_.size() < (std::size_t)width(si) ) cv_.resize(width(si), lc0);
   unsigned gc = 0, tot = 0;
   for(int i = 0, j = si.lower(); j != si.upper(); ++i, ++j) {
      Nucleotide n = nucleotide_index(s[j]);
      if( n > Any ) n = Any;
      ++cv_[i][n];
      if( n < Any ) ++tot;
      if( n == Cytosine || n == Guanine ) ++gc;
   }
   if (tot == 0) tot = 1;
   gch_.add((gc*100)/tot);
}

/*
*******************************************************************************/
void Composition_stats::finish() {
   write_composition();
   write_gc_hist();
}

/*
*******************************************************************************/
void Composition_stats::write_composition() const {
   File_ostream fos(File_output(comp_, compression::unknown, format::CSV));
   std::ostream& ofs = fos.ostream();

   stamp(ofs)
   << "position"
   << delimiter() << "A%" << delimiter() << "C%" << delimiter() << "G%"
   << delimiter() << "T%" << delimiter()
   << "N%" << delimiter() << "GC%" << '\n'
   ;

   for(std::size_t i = 0; i != cv_.size(); ++i) {
      letter_counts const& lc = cv_[i];
      const unsigned cg = lc[Cytosine] + lc[Guanine];
      const unsigned acgt = lc[Adenine] + cg + lc[Thymine];
      const unsigned tot = acgt + lc[Any];
      ofs << i << delimiter();
      if( ! tot ) {
         ofs
         << 0 << delimiter() << 0 << delimiter() << 0 << delimiter()
         << 0 << delimiter() << 0 << delimiter() << 0 << '\n';
         continue;
      }
      const double p = 100;
      ofs
      << lc[Adenine]    * p / tot << delimiter()
      << lc[Cytosine]   * p / tot << delimiter()
      << lc[Guanine]    * p / tot << delimiter()
      << lc[Thymine]    * p / tot << delimiter()
      << lc[Any]        * p / tot << delimiter()
      ;
      if( ! acgt ) {
         ofs << 0 << '\n';
         continue;
      }
      ofs << cg * p / acgt << '\n';
   }
}

/*
*******************************************************************************/
void Composition_stats::write_gc_hist() const {
   File_ostream fos(File_output(gc_hist_, compression::unknown, format::CSV));
   std::ostream& ofs = fos.ostream();

   stamp(ofs)
   << "GC%" << delimiter() << "read_count" << '\n';

   gch_.print(ofs, delimiter());
}

}//namespace vdj_pipe
