/** @file "/vdj_pipe/lib/depth_first_iterator.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/gdst/depth_first_iterator.hpp"
#include "vdj_pipe/gdst/gdst.hpp"

namespace vdj_pipe{ namespace gdst{

/*
*******************************************************************************/
void Depth_iter::next() {
   for( ;! s_.empty(); s_.pop() ) {
      for(unsigned n = s_.top().second; n != 4; ++n) {
         if( Branch_id bid = t_.child(s_.top().first, (Nucleotide)n) ) {
            s_.top().second = n + 1;
            s_.push(branch_t(bid, 0));
            return;
         }
      }
   }
}


}//namespace gdst
}//namespace vdj_pipe
