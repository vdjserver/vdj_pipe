/** @file "/vdj_pipe/lib/external_mid_base.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/external_mid_base.hpp"

#include "boost/foreach.hpp"
#include "boost/optional.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "mapped_sequence_properties.hpp"
#include "sequence_map_full.hpp"
#include "sequence_map_parse.hpp"
#include "sequence_map_short.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{ namespace detail{

/*
*******************************************************************************/
External_mid_base::External_mid_base(
         bpt::ptree const& pt,
         Pipe_environment& pe,
         const Val_id name_val_id,
         const Val_id score_val_id
)
: sms_(),
  smf_(),
  scoring_matrix_(0),
  min_score_(0),
  require_best_(true),
  name_val_id_(name_val_id),
  score_val_id_(score_val_id)
{
   bool track_mismatches = true;
   name_seq_vector seqs;
   string_table combinations;
   boost::optional<int> min_score;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::fasta_path() ) {
         parse_seq_files(vt.second, pe.input(), seqs);

      } else if( vt.first == kwds::pairs_path() ) {
         parse_combinations(vt.second, pe.input(), combinations);

      } else if( vt.first == kwds::min_score() ) {
         min_score_ = vt.second.get_value<int>();
         track_mismatches = false;

      } else if( vt.first == kwds::max_mismatches() ) {
         min_score_ = - vt.second.get_value<int>();

      } else if( vt.first == kwds::require_best() ) {
         require_best_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::value_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( seqs.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("external MID sequences file not provided")
   );

   if( combinations.size() ) seqs = combine(seqs, combinations);

   Seq_props sp(seqs);

   if( sp.all_short_ && sp.same_size_ && ! sp.has_ambiguous_ ) {
      sms_.reset(new Seq_map_short(false));
      BOOST_FOREACH(match_seq const& ms, seqs) {
         sms_->insert(ms.first, ms.second);
      }
   } else {
      smf_.reset(new Seq_map_full(false));
      BOOST_FOREACH(match_seq const& ms, seqs) {
         smf_->insert(ms.first, ms.second);
      }
   }

   scoring_matrix_ = track_mismatches ?
            &scoring_matrix<0,-1,0,0>() :
            &scoring_matrix<2,-2,1,0>()
            ;
}

/*
*******************************************************************************/
std::size_t External_mid_base::mid_size() const {
   return sms_ ? sms_->seq_size() : smf_->max_len();
}

/*
*******************************************************************************/
void External_mid_base::find_mid(
         const boost::string_ref seq,
         Value_map& vm
) {
   if( seq.size() != mid_size() ) return;
   match_type m;
   if( sms_ ) {
      sms_->find_closest(seq, m, *scoring_matrix_);
   } else {
      smf_->find_closest(seq, m, *scoring_matrix_);
   }

   if( score_val_id_ ) vm[score_val_id_] = (long)m.score1();
   if( m.is_acceptable(min_score_, require_best_) ) {
      vm[name_val_id_] = sms_ ? sms_->name(m.id1()) : smf_->name(m.id1());
   }
}

}//namespace detail
}//namespace vdj_pipe
