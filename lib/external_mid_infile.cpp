/** @file "/vdj_pipe/lib/external_mid_infile.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/external_mid_infile.hpp"

#include "boost/property_tree/ptree.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
External_mid_infile::External_mid_infile(
         Vm_access_paired_emid const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: Step_base_paired_emid(vma, pt, pe),
  External_mid_base(
           pt,
           pe,
           vma_.insert_new_name(pt.get(kwds::value_name(), "eMID")),
           vma_.insert_new_name(pt.get(kwds::value_name(), "eMID_score"))
  )
{}

/*
*******************************************************************************/
void External_mid_infile::run() {
   const std::string mids = vma_.emid_fwd() + vma_.emid_rev();
   find_mid(mids, vma_);
}

}//namespace vdj_pipe
