/** @file "/vdj_pipe/lib/file.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/file.hpp"

#include <fstream>
#include "boost/algorithm/string/case_conv.hpp"
namespace ba = boost::algorithm;
#include "boost/filesystem.hpp"
namespace bfs = boost::filesystem;
#include "boost/functional/hash.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
compression::Compression guess_compression_magic(std::string const& path) {
   std::ifstream ifs(path.c_str(), std::ios_base::binary);
   typedef base_exception Err;
   if( ! ifs.good() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("not readable")
            << Err::str1_t(sanitize(path))
   );
   return compression_magic(ifs);
}

/*
*******************************************************************************/
compression::Compression guess_compression_ext(bfs::path const& p) {
   std::string ext = p.extension().string();
   boost::algorithm::to_lower(ext);
   return extension_to_compression(ext);
}

/*
*******************************************************************************/
compression::Compression guess_compression_ext(std::string const& path) {
   return guess_compression_ext(bfs::path(path));
}

/*
*******************************************************************************/
format::Format
guess_format(std::string const& path, const compression::Compression c) {
   bfs::path p(path);
   std::string ext =
            (c == compression::none) ?
                     p.extension().string() :
                     p.stem().extension().string();
   ba::to_lower(ext);
   return extension_to_format(ext);
}

/*
*******************************************************************************/
std::pair<compression::Compression,format::Format>
guess_compression_format(std::string const& path) {
   std::pair<compression::Compression,format::Format>
   pair(compression::none, format::unknown);

   bfs::path p(path);
   pair.first = guess_compression_ext(p);
   pair.second = guess_format(path, pair.first);
   return pair;
}

namespace{

void make_dirs_and_open(bfs::path const& p, std::ofstream& ofs) {
	typedef base_exception Err;
	const bfs::path parent = p.parent_path();
	if( ! parent.empty() ) {
		try{
			create_directories(parent);
		} catch(bfs::filesystem_error const&) {
			BOOST_THROW_EXCEPTION(
					Err()
					<< Err::msg_t("failed creating directory")
					<< Err::str1_t(sanitize(p.string()))
					<< Err::str2_t(sanitize(parent.string()))
					<< Err::nested_t(boost::current_exception())
			);
		}
	}
   ofs.open(p.string().c_str());
   if( ! ofs ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("failed creating output file")
      << Err::str1_t(sanitize(p.string()))
   );
}

}//anonymous namespace

/*
*******************************************************************************/
std::string ensure_path_writable(std::string const& path) {
   bfs::path p(path);
   if( ! exists(p) ) {
      std::ofstream ofs;
      make_dirs_and_open(p, ofs);
      ofs.flush();
   }
   return canonical(p).string();
}

/*
*******************************************************************************/
std::string ensure_path_writable(
         std::string const& path,
         std::string const& header
) {
   bfs::path p(path);
   if( ! exists(p) ) {
      std::ofstream ofs;
      make_dirs_and_open(p, ofs);
      ofs << header;
      ofs.flush();
   }
   return canonical(p).string();
}

/*
*******************************************************************************/
bool is_path_readable(std::string const& path) {
   std::ifstream ifs(path.c_str());
   return ifs.good();
}

/*
*******************************************************************************/
std::string ensure_path_readable(std::string const& path) {
   typedef base_exception Err;
   bfs::path p(path);
   if( ! exists(p) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("input file does not exist")
            << Err::str1_t(sanitize(path))
   );
   std::ifstream ifs(path.c_str());
   if( ! ifs.good() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("input file is not readable")
            << Err::str1_t(sanitize(path))
   );
   return canonical(p).string();
}

/*
*******************************************************************************/
std::size_t hash_value(File const& f) {
   return boost::hash_value(f.path());
}

}//namespace vdj_pipe
