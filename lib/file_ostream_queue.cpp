/** @file "/vdj_pipe/lib/file_ostream_queue.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/file_ostream_queue.hpp"

#include <fstream>
#include "boost/foreach.hpp"

#include "vdj_pipe/detail/unused_variable.hpp"
#include "vdj_pipe/file.hpp"
#include "vdj_pipe/variable_path.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
std::string Queable_ofstream::make_path(
         path_template const& templ,
         val_ref_vector const& vals,
         std::string const& header
) {
      return ensure_path_writable(path_assemble(templ, vals), header);
}

/*
*******************************************************************************/
Queable_ofstream::Queable_ofstream(
         path_template const& templ,
         val_ref_vector const& vals,
         const format::Format fmt,
         std::string const& header
)
: sv_(),
  fos_(
     File_output(make_path(templ, vals, header), compression::none, fmt),
     std::ios_base::out | std::ios_base::app
  )
{
   sv_.reserve(vals.size());
   BOOST_FOREACH(value_type const& v, vals) {
      sv_.push_back(v);
   }
}

namespace{
typedef Queable_ofstream::val_vector val_vector;
typedef Queable_ofstream::val_ref_vector val_ref_vector;

/*
*******************************************************************************/
struct Equal :
         public std::binary_function<val_ref_vector,val_vector,bool> {
   bool operator()(val_ref_vector const& v1, val_vector const& v2) const {

      if( v1.size() != v2.size() ) return false;
      val_ref_vector::const_iterator i1 = v1.begin();
      val_vector::const_iterator i2 = v2.begin();
      for( ; i1 != v1.end(); ++i1, ++i2) {
         if( ! (boost::unwrap_ref(*i1) == *i2) ) return false;
      }
      return true;
   }
};

/*
*******************************************************************************/
struct Hash : public std::unary_function<val_ref_vector,std::size_t> {
   std::size_t operator()(val_ref_vector const& v) const {
      std::size_t seed = 0;
      for(val_ref_vector::const_iterator i = v.begin(); i != v.end(); ++i) {
         boost::hash_combine(seed, boost::unwrap_ref(*i));
      }
      return seed;
   }
};

}//anonymous namespace

/*
*******************************************************************************/
File_ostream& File_ostream_queue::ostream(val_ref_vector const& v) {
   if( v.size() + 1 != pt_.size() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("wrong number of values")
      << Err::int1_t(v.size())
      << Err::int2_t(pt_.size())
   );
   index const& ind = vm_.get<1>();
   hash_citerator i = ind.find(v, Hash(), Equal());
   if( i == ind.end() ) {
      std::pair<hash_iterator, bool> p =
               vm_.get<1>().emplace(pt_, v, fmt_, header_);
      unused_variable(p);
      BOOST_ASSERT(p.second);

      if( vm_.size() == sz_ ) vm_.pop_front();

   } else {
      vm_.relocate(vm_.end(), vm_.project<0>(i));
   }
   return vm_.back().fos_;
}

}//namespace vdj_pipe
