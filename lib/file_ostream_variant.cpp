/** @file "/vdj_pipe/lib/file_ostream_variant.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/file_ostream_variant.hpp"

#include "boost/foreach.hpp"

#include "vdj_pipe/detail/disable_warnings.hpp"
#include "vdj_pipe/file_ostream_queue.hpp"
#include "vdj_pipe/variable_path.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
File_ostream_variant::File_ostream_variant(const format::Format fmt)
: unset_element_(skip_empty()),
  skip_empty_(true),
  fos_(new File_ostream(fmt)),
  fosq_()
{}

/*
*******************************************************************************/
File_ostream_variant::File_ostream_variant(
   std::string const& path,
   std::string const& unset_element,
   Value_map const& vm,
   std::string const& header,
   const compression::Compression compr,
   format::Format fmt,
   const std::size_t queue_size
)
: unset_element_(unset_element),
  skip_empty_(unset_element == skip_empty()),
  idv_(),
  fos_(),
  fosq_()
{
   if( path.find('{') == std::string::npos ) {
      init_single(path, header, compr, fmt);
   } else {
      init_queue(path, vm, header, fmt, queue_size);
   }
}

/*
*******************************************************************************/
File_ostream_variant::File_ostream_variant(
            std::vector<Val_id> const& var_ids,
            path_template const& templ,
            std::string const& unset_element,
            std::string const& header,
            const compression::Compression compr,
            format::Format fmt,
            const std::size_t queue_size
)
: unset_element_(unset_element),
  skip_empty_(unset_element == skip_empty()),
  idv_(var_ids),
  fos_(),
  fosq_()
{
   if( var_ids.size() + 1 != templ.size() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("wrong number of values")
            << Err::int1_t(var_ids.size())
            << Err::int2_t(templ.size())
   );

   if( var_ids.empty() ) {
      init_single(templ.front(), header, compr, fmt);
   } else {
      init_queue(templ, header, fmt, queue_size);
   }
}

/*
*******************************************************************************/
void File_ostream_variant::init_single(
         std::string const& path,
         std::string const& header,
         const compression::Compression compr,
         const format::Format fmt
) {
   fos_.reset(
            new File_ostream(
                     File_output(path, compr, fmt)
            )
   );
   fos_->ostream() << header;
}

/*
*******************************************************************************/
void File_ostream_variant::init_queue(
         std::string const& path,
         Value_map const& vm,
         std::string const& header,
         format::Format fmt,
         const std::size_t queue_size
) {
   std::vector<std::string> tv, nv;
   path_decompose(path, tv, nv);
   BOOST_ASSERT(
            tv.size() == nv.size() + 1 &&
            "number of template elements should be greater by one"
   );

   idv_.reserve(nv.size());
   BOOST_FOREACH(std::string const& name, nv) {
      idv_.push_back(vm.value_id(name));
   }

   init_queue(tv, header, fmt, queue_size);
}

/*
*******************************************************************************/
void File_ostream_variant::init_queue(
         path_template const& templ,
         std::string const& header,
         format::Format fmt,
         const std::size_t queue_size
) {
   if( fmt == format::unknown ) {
      fmt = guess_compression_format(templ.back()).second;
   }

   //use as NULL stream
   fos_.reset(new File_ostream(fmt));
   fosq_.reset(new File_ostream_queue(templ, header, fmt, queue_size));
}

/*
*******************************************************************************/
File_ostream& File_ostream_variant::ostream(Value_map const& vm) {
   if( ! fosq_ ) return *fos_;
   val_ref_vector v;
   v.reserve(idv_.size());
   BOOST_FOREACH(const Val_id id, idv_) {
      value_type const& val = vm[id];
      if( is_blank(val) ) {
         if( skip_empty_ ) return *fos_;
         v.push_back(boost::cref(unset_element_));
      } else {
         v.push_back(boost::cref(val));
      }
   }
   return fosq_->ostream(v);
}

/*
*******************************************************************************/
File_ostream& File_ostream_variant::ostream(val_vector const& vv) {
   if( ! fosq_ ) return *fos_;
   val_ref_vector v;
   v.reserve(idv_.size());
   BOOST_FOREACH(const Val_id id, idv_) {
      BOOST_ASSERT(id() < vv.size());
      value_type const& val = vv[id()];
      if( is_blank(val) ) {
         if( skip_empty_ ) return *fos_;
         v.push_back(boost::cref(unset_element_));
      } else {
         v.push_back(boost::cref(val));
      }
   }
   return fosq_->ostream(v);
}

/*
*******************************************************************************/
std::size_t File_ostream_variant::size() const {
   if( fosq_ ) return fosq_->size();
   return 1;
}


}//namespace vdj_pipe
