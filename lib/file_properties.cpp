/** @file "/vdj_pipe/lib/file_properties.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/file_properties.hpp"

#include <istream>
#include <map>
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;

namespace vdj_pipe{

/*
*******************************************************************************/
compression::Compression
extension_to_compression(std::string const& ext) {
   typedef std::map<std::string,compression::Compression> map_t;
   static const map_t cm =
            bass::map_list_of(".gz", compression::gzip)
   (".bz2", compression::bzip2)
   (".z", compression::zlib)
   (".zip", compression::pkzip)
   (".rar", compression::rar)
   (".7z", compression::seven_z);
   map_t::const_iterator i = cm.find(ext);
   if( i == cm.end() ) return compression::none;
   return i->second;
}

/*
*******************************************************************************/
compression::Compression
compression_magic(std::istream& is) {
   static const std::size_t N = 7;
   char c[N];
   for(std::size_t n = 0; n != N; ++n) {
      is.get(c[n]);
   }

   if( c[0] == (char)0x1F && c[1] == (char)0x8B ) {
      return compression::gzip;
   }

   if( c[0] == (char)0x1F && c[1] == (char)0x9D ) {
      return compression::zlib;
   }

   if( c[0] == (char)0x42 && c[1] == (char)0x5A && c[2] == (char)0x68 ) {
      return compression::bzip2;
   }

   if(
            c[0] == (char)0x50 && c[1] == (char)0x4B && c[2] == (char)0x03 &&
            c[3] == (char)0x04
   ) {
      return compression::pkzip;
   }

   if(
            c[0] == (char)0x52 && c[1] == (char)0x61 && c[2] == (char)0x72 &&
            c[3] == (char)0x21 && c[4] == (char)0x1A && c[5] == (char)0x07 &&
            c[6] == (char)0x01
   ) {
      return compression::rar;
   }
   if(
            c[0] == (char)0x37 && c[1] == (char)0x7A && c[2] == (char)0xBC &&
            c[3] == (char)0xAF && c[4] == (char)0x27 && c[5] == (char)0x1C
   ) {
      return compression::seven_z;
   }

   return compression::none;
}

/*
*******************************************************************************/
format::Format
extension_to_format(std::string const& ext) {
   typedef std::map<std::string,format::Format> map_t;
   static const map_t cm =
               bass::map_list_of(".fasta", format::Fasta)
   (".fas", format::Fasta)
   (".fna", format::Fasta)
   (".qual", format::Qual)
   (".fastq", format::Fastq)
   (".fq", format::Fastq);
   map_t::const_iterator i = cm.find(ext);
   if( i == cm.end() ) return format::unknown;
   return i->second;
}

}//namespace vdj_pipe
