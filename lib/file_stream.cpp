/** @file "/vdj_pipe/lib/file_stream.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/file_stream.hpp"

#include <fstream>
#include <ostream>
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/iostreams/filter/bzip2.hpp"
#include "boost/iostreams/filter/gzip.hpp"
#include "boost/iostreams/filter/zlib.hpp"

#include "vdj_pipe/print_sequence.hpp"
#include "vdj_pipe/output_stamp.hpp"

namespace vdj_pipe {

struct Null_buff : public std::streambuf {
  int overflow(int c) {return c;}
};

static Null_buff null_buff;

/*
*******************************************************************************/
File_ostream::File_ostream(const format::Format fmt)
: File_output(fmt),
  ofs_(),
  fosb_(),
  fosb_os_(new std::ostream(&null_buff)),
  os_(*fosb_os_)
{}

/*
*******************************************************************************/
File_ostream::fosb_t*
File_ostream::make_fosb(std::ostream& os, const compression::Compression compr) {
   fosb_t* fosb = 0;
   switch (compr) {
      case compression::none:
         break;
      case compression::bzip2:
         fosb = new fosb_t;
         fosb->push(boost::iostreams::bzip2_compressor());
         fosb->push(os);
         break;
      case compression::gzip:
         fosb = new fosb_t;
         fosb->push(boost::iostreams::gzip_compressor());
         fosb->push(os);
         break;
      case compression::zlib:
         fosb = new fosb_t;
         fosb->push(boost::iostreams::zlib_compressor());
         fosb->push(os);
         break;
      default:
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported compression")
                  << Err::int1_t(compr)
         );
   }
   return fosb;
}

/*
*******************************************************************************/
std::ios_base::openmode File_ostream::guess_mode(
      const std::ios_base::openmode mode,
      const compression::Compression compr
) {
   if( mode & std::ios_base::out ) return mode;
   return compr == compression::none ?
            std::ios_base::out | std::ios_base::trunc :
            std::ios_base::out | std::ios_base::trunc | std::ios_base::binary
            ;
}

/*
*******************************************************************************/
File_ostream::File_ostream(
   File_output const& fout,
   const std::ios_base::openmode mode
)
: File_output(fout),
  ofs_(
         new std::ofstream(
                  path().c_str(),
                  guess_mode(mode, compression())
         )
),
  fosb_(make_fosb(*ofs_, compression())),
  fosb_os_(fosb_ ? new std::ostream(&(*fosb_)) : 0),
  os_(fosb_os_ ? *fosb_os_ : *ofs_)
{
   if( ! os_ ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("error opening file")
      << Err::str1_t(sanitize(path()))
   );
}

/*
*******************************************************************************/
File_istream::fisb_t*
File_istream::make_fisb(std::istream& is, const compression::Compression compr) {
   fisb_t* fisb = 0;
   switch(compr) {
   case compression::none:
      break;
   case compression::bzip2:
      fisb = new fisb_t;
      fisb->push(boost::iostreams::bzip2_decompressor());
      fisb->push(is);
      break;
   case compression::gzip:
      fisb = new fisb_t;
      fisb->push(boost::iostreams::gzip_decompressor());
      fisb->push(is);
      break;
   case compression::zlib:
      fisb = new fisb_t;
      fisb->push(boost::iostreams::zlib_decompressor());
      fisb->push(is);
      break;
   default:
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unsupported compression")
               << Err::int1_t(compr)
      );
   }
   return fisb;
}

/*
*******************************************************************************/
File_istream::File_istream(File_input const& fin)
: ifs_(
         new std::ifstream(
                  fin.path().c_str(),
                  fin.compression() == compression::none ?
                  std::ios_base::in :
                  std::ios_base::in | std::ios_base::binary
         )
),
  fisb_(make_fisb(*ifs_, fin.compression())),
  fisb_is_(fisb_ ? new std::istream(&(*fisb_)) : 0),
  is_(fisb_is_ ? *fisb_is_ : *ifs_)
{}

/*
*******************************************************************************/
File_istream::File_istream(
            std::istream& is,
            const compression::Compression compr
)
: ifs_(),
  fisb_(make_fisb(is, compr)),
  fisb_is_(fisb_ ? new std::istream(&(*fisb_)) : 0),
  is_(fisb_is_ ? *fisb_is_ : is)
{}

/*
*******************************************************************************/
void File_ostream::write(
         std::string const& descr,
         Seq_record::sequence const& seq
) {
   switch (format()) {
      case format::Fasta:
         print_fasta(ostream(), descr, seq);
         break;
      default:
         BOOST_THROW_EXCEPTION(
                  File::Err()
                  << File::Err::msg_t("incorrect arguments")
                  << File::Err::str1_t(sanitize(path()))
         );
   }
}

/*
*******************************************************************************/
void File_ostream::write(
         std::string const& descr,
         const boost::string_ref seq
) {
   switch (format()) {
      case format::Fasta:
         print_fasta(ostream(), descr, seq);
         break;
      default:
         BOOST_THROW_EXCEPTION(
                  File::Err()
                  << File::Err::msg_t("incorrect arguments")
                  << File::Err::str1_t(sanitize(path()))
         );
   }
}

/*
*******************************************************************************/
void File_ostream::write(
         std::string const& descr,
         Qual_record::quality const& qual
) {
   switch (format()) {
      case format::Qual:
         print_qual(ostream(), descr, qual);
         break;
      default:
         BOOST_THROW_EXCEPTION(
                  File::Err()
                  << File::Err::msg_t("incorrect arguments")
                  << File::Err::str1_t(sanitize(path()))
         );
   }
}

/*
*******************************************************************************/
void File_ostream::write(
         std::string const& descr,
         Seq_record::sequence const& seq,
         Qual_record::quality const& qual
) {
   switch (format()) {
   case format::Fasta:
      print_fasta(ostream(), descr, seq);
      break;
      case format::Qual:
         print_qual(ostream(), descr, qual);
         break;
      case format::Fastq:
         print_fastq(ostream(), descr, seq, qual);
         break;
      default:
         BOOST_THROW_EXCEPTION(
                  File::Err()
                  << File::Err::msg_t("unsupported format")
                  << File::Err::str1_t(sanitize(path()))
         );
   }
}

}//namespace vdj_pipe
