/** @file "/vdj_pipe/lib/filter_base.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/filter_base.hpp"

#include <ostream>
#include "boost/format.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

namespace vdj_pipe{ namespace detail{

namespace{
const boost::format bf("%1$d (%2$.3g%%)");
}

/*
*******************************************************************************/
Filter_base::Filter_base(
         vma_type const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: Step_base_single(vma, pt, pe), nf_(0), pass_id_()
{
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::passed_name() ) {
         pass_id_ = vma_.insert_new_name(vt.second.data());
      }
   }
}

/*
*******************************************************************************/
void Filter_base::summary(std::ostream& os) const {
   os
   << "removed: "
   << boost::format(bf) % nf_ % (100.0 * nf_ / vma_.read_count()) << "\n"
   << "total: " << vma_.read_count() << '\n'
   ;
}

/*
*******************************************************************************/
void Window_filter_base::summary(std::ostream& os) const {
   os
   << "removed: "
   << boost::format(bf) % nf_ % (100.0 * nf_ / vma_.read_count()) << '\n'
   << "trimmed: "
   << boost::format(bf) % nt_ % (100.0 * nt_ / vma_.read_count()) << '\n'
   << "total: " << vma_.read_count() << '\n'
   ;
}


}//namespace detail
}//namespace vdj_pipe
