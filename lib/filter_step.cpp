/** @file "/vdj_pipe/lib/filter_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/filter_step.hpp"

#include <limits>
#include "boost/accumulators/accumulators.hpp"
#include "boost/accumulators/statistics/mean.hpp"
#include "boost/accumulators/statistics/stats.hpp"
#include "boost/foreach.hpp"
#include "boost/numeric/conversion/cast.hpp"
#include "boost/range/algorithm/copy.hpp"
#include "boost/range/algorithm/sort.hpp"
#include "boost/range/algorithm/binary_search.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "vdj_pipe/sequence_record.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/find_interval.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Character_filter::kwds,
         (chars)
);

/*
*******************************************************************************/
Character_filter::Character_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Filter_base(vma, pt, pe),
  ch_()
{
   static const std::string def = "ACGTacgt";
   std::string const* str = &def;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == Character_filter::kwds::chars() ) {
         str = &vt.second.data();

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }
   ch_.resize(str->size());
   boost::copy(*str, ch_.begin());
   boost::sort(ch_);
}

/*
*******************************************************************************/
void Character_filter::run() {
   BOOST_FOREACH(const char c, vma_.sequence()) {
      if( boost::binary_search(ch_, c) ) continue;
      pass(false);
      return;
   }
   pass(true);
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Length_filter::kwds,
         (min)
         (max)
         (trim) //trim reads longer than max
);

/*
*******************************************************************************/
Length_filter::Length_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Window_filter_base(vma, pt, pe),
  min_(0),
  max_(std::numeric_limits<unsigned>::max()),
  trim_(false)
{
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::min() ) {
         min_ = vt.second.get_value<unsigned>();

      } else if( vt.first == kwds::max() ) {
         max_ = vt.second.get_value<unsigned>();

      } else if( vt.first == kwds::trim() ) {
         trim_ = vt.second.get_value<bool>();

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( trim_ && max_ == std::numeric_limits<unsigned>::max() ) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("maximum length has to be defined for trimming")
               << Err::str1_t(kwds::max())
      );
   }

   if( min_ > max_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("invalid min/max values")
            << Err::int1_t(min_)
            << Err::int2_t(max_)
   );
}

/*
*******************************************************************************/
void Length_filter::run() {
   const unsigned s = width(vma_.interval());
   if( s == 0 ) return;
   if( s < min_ ) pass(false);
   else if( s > max_ ) {
      if( trim_ ) {
         const sequence_interval::base_type lo = vma_.interval().lower();
         trim(sequence_interval(lo, lo + (sequence_interval::base_type)max_));
      }
      else pass(false);
   }
   pass(true);
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Homopolymer_filter::kwds,
         (max_length)
);

/*
*******************************************************************************/
Homopolymer_filter::Homopolymer_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Filter_base(vma, pt, pe),
  min_(std::numeric_limits<unsigned>::max())
{
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::max_length() ) {
         min_ = vt.second.get_value<unsigned>();

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }
   if( ! min_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("maximum homopolymer length is required")
            << Err::str1_t(kwds::max_length())
   );
}

/*
*******************************************************************************/
void Homopolymer_filter::run() {
   seq_type const& s = vma_.sequence();
   if( s.empty() ) return;
   unsigned n = 1;
   char c = s[0];
   for(unsigned i = 1; i != s.size(); ++i) {
      if( s[i] == c ) {
         ++n;
         if( n > min_ ) {
            pass(false);
            return;
         }
         continue;
      }
      c = s[i];
      n = 1;
   }
   pass(true);
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Min_quality_filter::kwds,
         (min_quality)
);

/*
*******************************************************************************/
Min_quality_filter::Min_quality_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Filter_base(vma, pt, pe),
  min_(0)
{
   bool set = false;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::min_quality() ) {
         min_ = boost::numeric_cast<value_type>(vt.second.get_value<int>());
         set = true;

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( ! set ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("minimum quality value not found")
            << Err::str1_t(kwds::min_quality())
   );
}

/*
*******************************************************************************/
void Min_quality_filter::run() {
   BOOST_FOREACH(const qual_type::value_type q, vma_.quality()) {
      if( q < min_ ) {
         pass(false);
         return;
      }
   }
   pass(true);
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Average_quality_filter::kwds,
         (min_quality)
);

/*
*******************************************************************************/
Average_quality_filter::Average_quality_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Filter_base(vma, pt, pe),
  min_(0.0)
{
   bool set = false;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::min_quality() ) {
         min_ = vt.second.get_value<double>();
         set = true;

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( ! set ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("minimum quality value not found")
            << Err::str1_t(kwds::min_quality())
   );
}

/*
*******************************************************************************/
void Average_quality_filter::run() {
   namespace bacc = boost::accumulators;
   bacc::accumulator_set<double, bacc::stats<bacc::tag::mean> > acc;
   BOOST_FOREACH(const qual_type::value_type q, vma_.quality()) {
      acc(q);
   }
   pass( min_ <= bacc::mean(acc) );
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Min_quality_window_filter::kwds,
         (min_quality)(min_length)
);

/*
*******************************************************************************/
Min_quality_window_filter::Min_quality_window_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Window_filter_base(vma, pt, pe),
  min_qual_(),
  min_len_()
{
   bool qual_set = false, len_set = false;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::min_quality() ) {
         min_qual_ = vt.second.get_value<unsigned>();
         qual_set = true;

      } else if( vt.first == kwds::min_length() ) {
         min_len_ = vt.second.get_value<unsigned>();
         len_set = true;

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( ! qual_set ) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("min_quality not defined")
      );
   }

   if( ! len_set ) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("min_length not defined")
      );
   }
}

/*
*******************************************************************************/
void Min_quality_window_filter::run() {
   qual_type const& q = vma_.quality();
   const std::pair<std::size_t,std::size_t> p =
            longest_min_interval(q, min_qual_);
   if( p.second < min_len_ ) {
      pass(false);
   } else {
      trim(sequence_interval(p.first, p.first + p.second));
   }
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Average_quality_window_filter::kwds,
         (min_quality)(min_length)(window_length)
);

/*
*******************************************************************************/
Average_quality_window_filter::Average_quality_window_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Window_filter_base(vma, pt, pe),
  win_len_(0),
  min_qual_l_(0.0),
  min_len_(0)
{
   double min_qual = 0.0;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::min_quality() ) {
         min_qual = vt.second.get_value<unsigned>();

      } else if( vt.first == kwds::window_length() ) {
         win_len_ = vt.second.get_value<unsigned>();

      } else if( vt.first == kwds::min_length() ) {
         min_len_ = vt.second.get_value<unsigned>();

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( min_qual == 0 ) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("min_quality not defined") );
   }

   if( win_len_ == 0 ) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("window_length not defined or zero") );
   }

   min_qual_l_ = min_qual * win_len_;
   if( min_len_ == 0 ) min_len_ = win_len_;

   if( win_len_ > min_len_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t(
                     "window should not be wider than minimal sequence length"
            )
            << Err::int1_t(win_len_)
            << Err::int2_t(min_len_)
   );
}

/*
*******************************************************************************/
void Average_quality_window_filter::run() {
   qual_type const& q = vma_.quality();
   if( q.size() < min_len_ ) {
      pass(false);
      return;
   }
   const std::pair<std::size_t,std::size_t> p =
            longest_average_interval(q, min_qual_l_, win_len_);
   if( p.second < min_len_ ) {
      pass(false);
   } else {
      trim(sequence_interval(p.first, p.first + p.second));
   }
}

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         Ambiguous_window_filter::kwds,
         (min_length)(max_ambiguous)
);

/*
*******************************************************************************/
Ambiguous_window_filter::Ambiguous_window_filter(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: detail::Window_filter_base(vma, pt, pe),
  min_len_(0),
  max_ambiguous_(0)
{
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::max_ambiguous() ) {
         max_ambiguous_ = vt.second.get_value<unsigned>();

      } else if( vt.first == kwds::min_length() ) {
         min_len_ = vt.second.get_value<unsigned>();

      } else if( vt.first == Filter_base::kwds::passed_name() ) {
         //do nothing

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }
}

/*
*******************************************************************************/
void Ambiguous_window_filter::run() {
   seq_type const& seq = vma_.sequence();
   if( seq.size() < min_len_ ) {
      pass(false);
      return;
   }
   const std::pair<std::size_t,std::size_t> p =
            longest_unambiguous_interval(seq, max_ambiguous_);
   const std::size_t min_len = min_len_ ? min_len_ : seq.size();
   if( p.second < min_len ) {
      pass(false);
   } else {
      trim(sequence_interval(p.first, p.first + p.second));
   }
}

}//namespace vdj_pipe
