/** @file "/vdj_pipe/lib/find_shared.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/find_shared.hpp"

#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include "boost/array.hpp"
#include "boost/assert.hpp"
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;
#include "boost/filesystem/path.hpp"
namespace bfs = boost::filesystem;
#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"
#if defined(NDEBUG)
//fix boost::multi_array warnings
//boost/multi_array/base.hpp:503:13: warning: unused variable 'bound_adjustment'
#define BOOST_DISABLE_ASSERTS
#endif
#include "boost/multi_array.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;
#include "boost/scoped_ptr.hpp"

#include "find_shared_impl.hpp"
#include "vdj_pipe/detail/vector_set.hpp"
#include "vdj_pipe/file_ostream_queue.hpp"
#include "vdj_pipe/gdst_search.hpp"
#include "vdj_pipe/gdst/gdst.hpp"
#include "vdj_pipe/output_stamp.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/sequence_store.hpp"

namespace vdj_pipe{

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
   Find_shared::kwds,
   (min_length)      // minimal matching length to consider sequences identical
   (ignore_ends)     // maximum length of mismatch at sequence end
   (fraction_match)  // fraction of sequence to match
   (consensus_trim)  // trim mis-aligned sequence ends
   (min_duplicates)  // output sequences with at least this many duplicates
   (trimmed)         // operate on trimmed sequences
   (reverse)         // reverse-complement reverse reads
   (unset_value)       // name for unset group variables
   (out_summary)         // simple file path
   (out_group_unique)         // file path that may contain {variable}-s
   (out_group_duplicates)     // file path that may contain {variable}-s
   (out_unique)               // file path that may contain {variable}-s
   (out_duplicates)           // file path that may contain {variable}-s
   (out_redundancy_histogram) //
);

/*
*******************************************************************************/
Find_shared::Find_shared(
         Vm_access_single const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: detail::Step_base_single(vma, pt, pe),
  gml_(),
  min_duplicates_(0),
  n_unique_(0),
  trim_(true),
  reverse_(true),
  consensus_trim_(false),
  vm_(Mapped_id(0)),
  rgs_(new Read_ginfo_store()),
  ss_(new Seq_store()),
  cv_(),
  unique_count_(0),
  redund_hist_(),
  summ_(pe.output().path("sharing_summary.csv")),
  unset_val_(),
  g_unique_(),
  g_dups_(),
  unique_(),
  dups_(),
  ids_()
{
   std::vector<std::string> var_names;
   int min_len = 0, ignore_ends = 0;
   double f = 0.0;
   BOOST_FOREACH(boost::property_tree::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::out_summary() ) {
         summ_ = pe.output().path(vt.second.get_value<std::string>());

      } else if( vt.first == kwds::min_length() ) {
         min_len = vt.second.get_value<int>();

      } else if( vt.first == kwds::ignore_ends() ) {
         ignore_ends = vt.second.get_value<int>();

      } else if( vt.first == kwds::fraction_match() ) {
         f = vt.second.get_value<double>();

         if( f <= 0.0 || f > 1.0 ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("fraction_match has to be within (0,1]")
                  << Err::str1_t(sanitize(vt.second.data()))
         );

      } else if( vt.first == kwds::min_duplicates() ) {
         min_duplicates_ = vt.second.get_value<std::size_t>();

      } else if( vt.first == kwds::trimmed() ) {
         trim_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::reverse() ) {
         reverse_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::unset_value() ) {
         unset_val_ = vt.second.get_value<std::string>();

      } else if( vt.first == kwds::consensus_trim() ) {
         consensus_trim_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::out_group_unique() ) {
         g_unique_.init(
                  pe.output().path(vt.second.get_value<std::string>()),
                  var_names
         );

      } else if( vt.first == kwds::out_group_duplicates() ) {
         g_dups_.init(
                  pe.output().path(vt.second.get_value<std::string>()),
                  var_names
         );

      } else if( vt.first == kwds::out_unique() ) {
         unique_.init(
                  pe.output().path(vt.second.get_value<std::string>()),
                  var_names
         );

      } else if( vt.first == kwds::out_duplicates() ) {
         dups_.init(
                  pe.output().path(vt.second.get_value<std::string>()),
                  var_names
         );

      } else if( vt.first == kwds::out_redundancy_histogram() ) {
         redund_hist_ = pe.output().path(vt.second.get_value<std::string>());

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   ids_.reserve(var_names.size());
   BOOST_FOREACH(std::string const& s, var_names) {
      ids_.push_back(vma_.value_id(s));
   }

   gml_ = Get_match_length(min_len, ignore_ends, f);
}

std::string to_string(
         detail::Queable_ofstream_types::val_vector const v
) {
   std::ostringstream os;
   if( v.size() ) {
      os << v.front();
      for( std::size_t n = 1; n != v.size(); ++n) os << "__" << v[n];
   }
   return os.str();
}

/*
*******************************************************************************/
void Find_shared::run() {
   seq_type s = vma_.sequence(trim_, reverse_);
   if( s.empty() ) return;

   Mapped_id vid;
   if( ids_.size() ) {
      val_vector vv;
      vv.reserve(ids_.size());
      BOOST_FOREACH(const Val_id id, ids_) {
         value_type const& v = vma_[id];
         if( is_blank(v) ) {
            if( is_blank(unset_val_) ) return;
            vv.push_back(unset_val_);
         } else {
            vv.push_back(v);
         }
      }
      vid = vm_.insert(vv).first;
      if( cv_.size() <= vid() ) cv_.resize(vid() + 1, 0);
      ++cv_[vid()];
   }
   const Read_id rid = rgs_->insert(vma_.read_id(), vid);
   ss_->insert(rid, s);
}

/*
*******************************************************************************/
void Find_shared::write_summary(
         std::ostream& os,
         counts_v const& uc,
         counts_v const& guc,
         std::vector<Mapped_id> const& sn
) const {
   BOOST_ASSERT( ids_.size() && "use this method if groups were defined" );

   os
   << "group" << delimiter()        // group name
   << "total" << delimiter()        // number of reads in group
   << "group-unique" << delimiter() // number of group-unique reads
   << "unique"                      // number of globally unique reads
   << '\n'
   ;

   BOOST_FOREACH(const Mapped_id mid, sn) {
      os
      << to_string(vm_[mid]) << delimiter()  // group name
      << cv_[mid()] << delimiter()           // number of reads in group
      << guc[mid()] << delimiter()           // number of group-unique reads
      << uc[mid()]                           // number of globally unique reads
      << '\n'
      ;
   }
}

/*
*******************************************************************************/
void Find_shared::write_sharing_matrix(
         std::ostream& os,
         counts_v const& cv,
         xstats const& xs,
         std::vector<Mapped_id> const& sorted_vi
) const {
   os << "group";
   BOOST_FOREACH(const Mapped_id mid, sorted_vi) {
      os << delimiter() << to_string(vm_[mid]);
   }
   os << '\n';

   BOOST_FOREACH(const Mapped_id mid1, sorted_vi) {
      os << to_string(vm_[mid1]);
      BOOST_FOREACH(const Mapped_id mid2, sorted_vi) {
         os << delimiter() << xs[mid1()][mid2()];
      }
      os << '\n';
   }
   os << '\n';

   BOOST_FOREACH(const Mapped_id mid1, sorted_vi) {
      os << to_string(vm_[mid1]);
      BOOST_FOREACH(const Mapped_id mid2, sorted_vi) {
         os << delimiter() << ( xs[mid1()][mid2()] * 100 / cv[mid1()] );
      }
      os << '\n';
   }
}

namespace{

static const int N = 8;
const boost::array<double,N> fracts =
{{0.1, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99, 1.0}};

double quantiles(std::vector<unsigned> c, std::vector<unsigned>& q) {
   std::size_t count = 0;
   double m = 0.0;
   for( std::size_t n = 0; n != c.size(); ++n) {
      count += c[n];
      m += c[n] * n;
   }

   boost::array<std::size_t,N> fc;
   for(std::size_t n = 0; n != fc.size(); ++n) fc[n] = fracts[n] * count;
   q.resize(fracts.static_size, 0);
   for(std::size_t t = 0, i = 0, n = 0; n != c.size(); ++n) {
      t += c[n];
      for( ; i != fc.size() && t >= fc[i]; ++i ) {
         q[i] = n;
      }
   }

   return m / count;
}

}//anonymous namespace


/*
*******************************************************************************/
void Find_shared::write_redundancy_summ(
         std::ostream& os,
         hist_array_t const& ha,
         std::vector<Mapped_id> const& sorted_vi
) const {
   os << "group" << delimiter() << "mean";
   BOOST_FOREACH(const double f, fracts) {
      os << delimiter() << (int)(f * 100) << '%';
   }
   os << '\n';
   BOOST_FOREACH(const Mapped_id mid1, sorted_vi) {
      counts_v cv;
      os << to_string(vm_[mid1]) << delimiter() << quantiles(ha[mid1()], cv);
      BOOST_FOREACH(const unsigned n, cv) os << delimiter() << n;
      os << '\n';
   }
}

/*
*******************************************************************************/
void Find_shared::write_redundancy(
         std::ostream& os,
         hist_array_t const& ha,
         std::vector<Mapped_id> const& sorted_vi
) const {
   std::size_t max = 0;
   BOOST_FOREACH(counts_v const& cv, ha) {
      if( cv.size() > max ) max = cv.size();
   }

   os << "count";
   BOOST_FOREACH(const Mapped_id id, sorted_vi) {
      os << delimiter() << to_string(vm_[id]);
   }
   os << '\n';
   for(std::size_t n = 1; n != max; ++n) {
      os << n;
      BOOST_FOREACH(const Mapped_id id, sorted_vi) {
         os << delimiter();
         counts_v const& cv = ha[id()];
         if( n < cv.size() ) os << cv[n];
         else os << 0;
      }
      os << '\n';
   }
}

/*
*******************************************************************************/
void Find_shared::group_stats() {
   const unsigned sz = vm_.size();
   boost::array<std::size_t,2> shape = {{sz,sz}};
   xstats xs1(shape), xs2(shape);
   counts_v unique(sz), group_unique(sz);
   hist_array_t ha(sz);
   const std::vector<Mapped_id> sorted_vi = sorted_ids();

   BOOST_FOREACH(const Mapped_id mid, vm_) {
      Group_unique gu(
               *ss_,
               *rgs_,
               vm_.size(),
               mid,
               gml_
      );

      if( g_unique_.size() ) {
         const File_output f(
                  g_unique_(vm_[mid]),
                  compression::unknown,
                  format::Fasta
         );
         File_ostream fos(f);
         gu.write_group_unique(fos, min_duplicates_, consensus_trim_, to_string(vm_[mid]));
      }

      if( redund_hist_.size() ) gu.group_redundancy_histogram(ha[mid()]);

      if( g_dups_.size() ) {
         const File_output f(
                  g_dups_(vm_[mid]),
                  compression::unknown,
                  format::CSV
         );
         File_ostream fos(f);
         gu.write_redundancy(fos.ostream(), delimiter());
      }

      counts_v v1, v2;
      unique[mid()] = gu.find_shared(v1, v2);
      group_unique[mid()] = gu.n_reads();
      for( unsigned n = 0; n != sz; ++n) {
         xs1[mid()][n] = v1[n];
         xs2[n][mid()] = v2[n];
      }

      if( unique_.size() ) {
         const File_output f(
                  unique_(vm_[mid]),
                  compression::unknown,
                  format::Fasta
         );
         File_ostream fos(f);
         gu.write_unique(fos, min_duplicates_);
      }
   }

   File_ostream fos(File_output(summ_, compression::unknown, format::CSV));
   std::ostream& os = fos.ostream();
   stamp(os);

   write_summary(os, unique, group_unique, sorted_vi);
   os << '\n';
   write_sharing_matrix(os, group_unique, xs1, sorted_vi);
   os << '\n';
   write_sharing_matrix(os, cv_, xs2, sorted_vi);

   if( redund_hist_.size() ) {
      std::ofstream ofs(redund_hist_.c_str());
      stamp(ofs);
//      write_redundancy_summ(ofs, ha, sorted_vi);
//      ofs << '\n';
      write_redundancy(ofs, ha, sorted_vi);
   }
}

/*
*******************************************************************************/
std::vector<Mapped_id> Find_shared::sorted_ids() const {
   std::vector<val_vector> sn; //sorted values
   sn.reserve(vm_.size());
   BOOST_FOREACH(const Mapped_id mid, vm_) sn.push_back(vm_[mid]);
   sort(sn.begin(), sn.end());
   std::vector<Mapped_id> vm;
   vm.reserve(vm_.size());
   BOOST_FOREACH(val_vector const& vv, sn) vm.push_back(*vm_.find(vv));
   return vm;
}


/**@brief
*******************************************************************************/
struct Sequence_bstats {

   static std::ostream& print_header(std::ostream& os, const char delim) {
      os
/* ISSUE-38: remove unused columns
      << "total_copies" << delim
      << "exact_copies" << delim
      << "mismatched_ends" << delim
      << "5'-mismatch" << delim
      << "3'-mismatch" */
      << "exact_copies"
      ;
      return os;
   }

   explicit Sequence_bstats(const unsigned n_exact = 0)
   : n_(n_exact), gem_(n_exact), gme_(0), mm5_(0), mm3_(0)
   {}

   void store(
            const unsigned s1_pos,
            const unsigned s1_len,
            const unsigned match_len,
            const unsigned count,
            super_seq const& super,
            Seq_store const& ss
   ) {
      n_ += count;
      if(
               gdst::mismatching_end(
                        s1_pos,
                        s1_len,
                        super.pos_,
                        ss[super.id_].size(),
                        match_len
               )
      ) {
         gme_ += count;
      }
      if( mm5_ < s1_pos ) mm5_ = s1_pos;
      const unsigned match_end = s1_len - s1_pos - match_len;
      if( mm3_ < match_end ) mm3_ = match_end;
   }

   std::ostream& print(
            std::ostream& os,
            const char delim
   ) const {
      os
/* ISSUE-38: remove unused columns
      << n_ << delim
      << gem_ << delim
      << gme_ << delim
      << (int)mm5_ << delim
      << (int)mm3_ */
      << n_
      ;
      return os;
   }

   unsigned n_; /**< total number of copies*/
   unsigned gem_; /**< number of exactly matching duplicates */
   unsigned gme_; /**< number of duplicates with mismatched ends */
   unsigned char mm5_; /**< mismatching or hanging 5' end length */
   unsigned char mm3_; /**< mismatching or hanging 3' end length */
};

/*
*******************************************************************************/
void Find_shared::init_stream(
         boost::scoped_ptr<File_ostream>& fos,
         Variable_path const& vp,
         const compression::Compression compr,
         const format::Format fmt
) const {
   const std::string path = vm_.size() ? vp(vm_[Mapped_id(0)]) : vp();
   const File_output fo(path, compr, fmt);
   fos.reset(new File_ostream(fo));
}

/*
*******************************************************************************/
void Find_shared::bulk_stats() {
   typedef boost::container::flat_map<Seq_id, Sequence_bstats> seq_map;
   typedef seq_map::value_type pair;
   seq_map sm;
   gdst::Gdst st(ss_->sequence_map());
   const unsigned shortest = (*ss_)[ss_->by_size().front()].size();
   for(
            unsigned len = (*ss_)[ss_->by_size().back()].size();
            len >= shortest;
            --len
   ) {
      BOOST_FOREACH(const Seq_id sid, ss_->by_size(len)) {
         std::string const& seq = (*ss_)[sid].sequence();
         const unsigned n_copies = ss_->maps_from(sid).size();
    	 sm.emplace(sid, Sequence_bstats(n_copies));
         
/* ISSUE-38: Sequences which are substrings of other sequences were
	being excluded and not properly counted. It is better to leave
	them in the results as unique sequences.

         if( all(seq, ! Is_ambiguous()) ) {
            const gdst::Common_subseq cs =
                     st.find_longest(seq, gml_(seq.size()));
            if( cs.seq_.empty() ) {
               st.insert(sid);
               sm.emplace(sid, Sequence_bstats(n_copies));
            } else {
               BOOST_FOREACH(super_seq const& super, cs.seq_) {
                  sm[super.id_].store(
                           cs.start_,
                           seq.size(),
                           cs.len_,
                           n_copies,
                           super,
                           *ss_
                  );
               }
            }
         } else {
            sm.emplace(sid, Sequence_bstats(n_copies));
         } */
      }
   }

   boost::scoped_ptr<File_ostream> fosu;
   if( unique_.size() ) {
      init_stream(fosu, unique_, compression::unknown, format::Fasta);

   } else if( g_unique_.size() ) {
      init_stream(fosu, g_unique_, compression::unknown, format::Fasta);

   }

   boost::scoped_ptr<File_ostream> fosd;
   if( dups_.size() ) {
      init_stream(fosd, dups_, compression::unknown, format::unknown);

   } else if( g_dups_.size() ) {
      init_stream(fosd, g_dups_, compression::unknown, format::unknown);

   }

   if( fosd ) {
      stamp(fosd->ostream());
      fosd->ostream() << "read_id" << delimiter();
      Sequence_bstats::print_header(fosd->ostream(), delimiter()) << '\n';
   }

   BOOST_FOREACH(pair const& p, sm) {
      if( p.second.n_ < min_duplicates_ ) continue;

      const Read_id rid = ss_->maps_from(p.first).front().id_;
      std::string const& rid_str = (*rgs_)[rid].id_str_;
      std::ostringstream stm;
      stm << p.second.n_;
      std::string descr;
      if (rid_str.data() == NULL) descr = "read|DUPCOUNT=" + stm.str();
      else descr = rid_str + "|DUPCOUNT=" + stm.str();

      if( fosu ) {
         boost::string_ref seq = (*ss_)[p.first].sequence();
         if( consensus_trim_ ) {
            seq.remove_prefix(p.second.mm5_);
            seq.remove_suffix(p.second.mm3_);
         }
         fosu->write(descr, seq);
      }

      if( fosd ) {
         fosd->ostream() << descr << delimiter();
         p.second.print(fosd->ostream(), delimiter()) << '\n';
      }
   }
   
   // count of the total unique sequences saved
   unique_count_ = sm.size();
}

/*
*******************************************************************************/
void Find_shared::finish() {
   if( ss_->empty() ) return;

   if( vm_.size() > 1 ) {
      group_stats();
   } else {
      bulk_stats();
   }
}

/*
*******************************************************************************/
void Find_shared::summary(std::ostream& os) const {
   os
   << "reads evaluated: " << rgs_->size() << '\n'
   << "unique sequences found: " << unique_count_ << '\n'
   ;

   if( vm_.size() > 1 ) {
      os
      << "unique sequences not shared between groups: " << n_unique_ << '\n'
      ;
   }
}

}//namespace vdj_pipe
