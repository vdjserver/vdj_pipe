/** @file "/vdj_pipe/lib/gdst.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/gdst/gdst.hpp"
#include "gdst_ukkonen_inserter.hpp"

namespace vdj_pipe{ namespace gdst{

/*
*******************************************************************************/
void Match::next(const boost::string_ref patt, Gdst const& st) {
   for( ; ; ) {
      BOOST_ASSERT(curr_);
      BOOST_ASSERT(next_);
      if( min_d_ ) {
         Branch const& b1 = st[curr_];
         Branch const& b2 = st[next_];
         BOOST_ASSERT( b1.n_ < b2.n_ );
         const unsigned l = b2.n_ - b1.n_;
         if( min_d_ > l ) min_d_ -= l;
         else min_d_ = 0;
      }
      curr_ = next_;
      if( min_d_ ) {
         Branch const& b1 = st[curr_];
         i_ = b1.n_;
         pn_ = nucleotide_index(patt[i_]);
         next_ = st.child(curr_, pn_);
         Branch const& b2 = st[next_];
         BOOST_ASSERT( b1.n_ < b2.n_ );
         const unsigned l = b2.n_ - b1.n_;
         if( min_d_ >= l ) continue;
      }
      break;
   }
}

/*
*******************************************************************************/
bool Match::check(const boost::string_ref patt, Gdst const& st) {
   next_ = Branch_id();
   mismatch_ = false;
   Branch const& b1 = st[curr_];
   i_ = b1.n_;
   at_pattern_end_ = i_ == patt.size();
   at_tree_end_ = ! b1.c_;
   if( at_tree_end_ || at_pattern_end_ ) return false;
   BOOST_ASSERT(i_ < patt.size());
   pn_ = nucleotide_index(patt[i_]);
   next_ = st.child(curr_, pn_);
   if( ! next_ ) return false;
   Branch const& b2 = st[next_];
   BOOST_ASSERT( b1.n_ < b2.n_ );
   if( (b2.n_ - b1.n_) == 1 ) return true;
   if( ! min_d_ ) min_d_ = 1;
   for( i_ += min_d_; i_ < b2.n_; ++i_ ) {
      en_ = st.letter(next_, i_);
      if( patt.size() == i_ ) {
         at_pattern_end_ = true;
         return false;
      }
      pn_ = nucleotide_index(patt[i_]);
      if( pn_ != en_ ) {
         mismatch_ = true;
         return false;
      }
   }
   return true;
}

/*
*******************************************************************************/
void Gdst::insert(const Seq_id sid) {
   Ukkonen_inserter::insert(*this, sid);
}

/*
*******************************************************************************/
Common_subseq Gdst::find_longest(
         const seq_type seq,
         std::size_t min_len
) const {
   if( ! min_len ) min_len = seq.size();
   Branch_id an = root();
   Match best;
   unsigned best_suff_len = 0;
   unsigned min_d = 0;
   for(seq_type suff = seq; suff.size() >= min_len; suff.remove_prefix(1)) {
      const Match m = find(suff, an, min_d);
      if( m.at_pattern_end_ ) {
         best = m;
         best_suff_len = suff.size();
         break;
      }
      if( m.i_ > min_len ) {
         min_len = m.i_;
         best = m;
         best_suff_len = suff.size();
      }
      an = m.curr_;
      BOOST_ASSERT(bm_[an].n_ <= m.i_);
      min_d = m.i_ - bm_[an].n_;
      if( an == root() ) {
         if( min_d ) --min_d;
      } else {
         if( const Branch_id bid = bm_[an].sl_ ) an = bid;
         else an = root();
      }
   }

   BOOST_ASSERT(seq.size() >= best_suff_len);
   Common_subseq cs(seq.size() - best_suff_len, best.i_);
   if( ! best.i_ ) return cs;
   for(
            Depth_iter di(*this, best.next_ ? best.next_ : best.curr_);
            ! di.at_end();
            di.next()
   ) {
      Branch const& b = bm_[di.id()];
      if( ! b.leaf_ ) continue;
      BOOST_FOREACH(const Seq_id sid, lm_[b.leaf_]) {
         cs.seq_.insert(super_seq(sid, (*ss_)[sid].size() - b.n_));
      }
   }
   return cs;
}

/*
*******************************************************************************/
void Gdst::find_overlaping(
            const boost::string_ref seq,
            detail::Vector_set<Seq_id>& vs,
            std::size_t min_overlap
) const {
   gdst::Branch_id an = root();
   unsigned min_d  = 0;
   if( ! min_overlap ) min_overlap = seq.size();
   for(std::size_t pos = 0; pos != seq.size() - min_overlap + 1; ++pos) {
      const boost::string_ref s = seq.substr(pos, min_overlap);
      const gdst::Match m = find(s, an, min_d);
      if( m.at_pattern_end_ ) {
         if( m.next_ ) collect_sequences(m.next_, vs);
         else collect_sequences(m.curr_, vs);
      }
      an = m.curr_;
      min_d = m.i_ - bm_[an].n_;
      if( an == root() ) {
         if( min_d ) --min_d;
      } else {
         if( const gdst::Branch_id bid = bm_[an].sl_ ) an = bid;
         else an = root();
      }
   }
}

}//namespace gdst
}//namespace vdj_pipe
