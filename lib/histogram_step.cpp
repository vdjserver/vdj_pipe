/** @file "/vdj_pipe/lib/histogram_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/histogram_step.hpp"

#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include <algorithm>
#include <map>
#include <sstream>
#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/output_stamp.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(kwds, (name)(out_path));

/*
*******************************************************************************/
void Histogram_step::write_header(std::ostream& os) const {
   stamp(os);
   BOOST_FOREACH(const Val_id vid, ids_) {
      os << vm_.name(vid) << delimiter();
   }
   os << "count" << '\n';
}

/*
*******************************************************************************/
Histogram_step::Histogram_step(
         Value_map const& vm,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: detail::Step_base(pt, pe),
  vm_(vm),
  ids_(),
  f_(format::CSV),
  map_()
{
   std::vector<std::string> names;
   std::string out_path;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::name() ) {
         std::string const& s = vt.second.data();
         if( s.size() ) names.push_back(s);
         else {
            BOOST_FOREACH(bpt::ptree::value_type const& vt2, vt.second) {
               names.push_back(vt2.second.data());
            }
         }

      } else if( vt.first == kwds::out_path() ) {
         out_path = vt.second.data();
      }
   }

   if( names.empty() ) BOOST_THROW_EXCEPTION(
            Err() << Err::msg_t("no variable names provided")
   );

   if( out_path.empty() ) {
      std::ostringstream os;
      copy(
               names.begin(),
               --names.end(),
               std::ostream_iterator<std::string>(os, "_")
      );
      os << names.back() << ".csv";
      out_path = os.str();
   }

   f_ = File_output(
            pe.output().path(out_path, plot::bar),
            compression::unknown,
            format::CSV
   );

   BOOST_FOREACH(std::string const& s, names) {
      ids_.push_back(vm.value_id(s));
   }
}

/*
*******************************************************************************/
void Histogram_step::run() {
   hvalue_t v;
   v.reserve(ids_.size());
   BOOST_FOREACH(const Val_id id, ids_) v.push_back(vm_[id]);

   std::pair<map_t::iterator,bool> p = map_.emplace(v, 0);
   ++p.first->second;
}

namespace{
typedef std::vector<value_variant> hvalue_t;

/**Keep undefined values last
*******************************************************************************/
struct Compare_vv
         : public std::binary_function<value_variant,value_variant,bool> {

   bool operator()(value_variant const& vv1, value_variant const& vv2) const {
      if( vv1.which() == vv2.which() ) return vv1 < vv2;
      return vv1.which() > vv2.which();
   }
};

/**Keep undefined values last
*******************************************************************************/
struct Compare_vvv
         : public std::binary_function<hvalue_t,hvalue_t,bool> {

   bool operator()(hvalue_t const& vv1, hvalue_t const& vv2) const {
      return
               lexicographical_compare(
                        vv1.begin(),
                        vv1.end(),
                        vv2.begin(),
                        vv2.end(),
                        Compare_vv()
               );
   }
};

}//anonymous namesapce

/*
*******************************************************************************/
void Histogram_step::finish() {
   typedef std::map<hvalue_t, std::size_t, Compare_vvv> map2_t;
   map2_t m(map_.begin(), map_.end(), Compare_vvv());
   File_ostream fos(f_);
   std::ostream& os = fos.ostream();
   write_header(os);
   char const delim[2] = {delimiter(), 0};
   std::size_t tot = 0;
   BOOST_FOREACH(map2_t::value_type const& v, m) {
      copy(
               v.first.begin(),
               v.first.end(),
               std::ostream_iterator<value_variant>(os, delim)
      );

      os << v.second << '\n';
      tot += v.second;
   }

   os << "total:";
   std::fill_n(std::ostream_iterator<char>(os), ids_.size(), delimiter());
   os << tot << '\n';
}

}//namespace vdj_pipe
