/** @file "/vdj_pipe/lib/input_csv.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef INPUT_CSV_HPP_
#define INPUT_CSV_HPP_
#include <istream>
#include <string>
#include <vector>
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;
#include "boost/tokenizer.hpp"
#include "vdj_pipe/input_manager.hpp"

namespace vdj_pipe{

/**@brief Convert input data descriptions in CSV format into property tree
*******************************************************************************/
void input_csv_to_tree(std::istream& is, bpt::ptree& pt) {
   std::vector<std::string> cols;
   std::string line;
   typedef Input_manager::Err Err;
   getline(is, line);
   typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
   typedef tokenizer::const_iterator iterator;
   boost::char_separator<char> separator(",\t");
   tokenizer tok(line, separator);
   BOOST_FOREACH(std::string const& col_name, tok) {
      if( col_name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("empty column name")
      );
      cols.push_back(col_name);
   }

   for( int line_num = 2; getline(is, line); ++line_num ) {
      tokenizer tok(line, separator);
      iterator i = tok.begin();
      if( i.at_end() ) continue;
      bpt::ptree pt1;
      for( std::size_t n = 0; ! i.at_end(); ++i, ++n ) {
         if( n == cols.size() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("too many values")
                  << Err::line_t(line_num)
         );
         pt1.put(cols[n], *i);
      }
      pt.push_back(bpt::ptree::value_type("", pt1));
   }
}

}//namespace vdj_pipe
#endif /* INPUT_CSV_HPP_ */
