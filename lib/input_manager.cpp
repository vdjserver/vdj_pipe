/** @file "/vdj_pipe/lib/input_manager.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/input_manager.hpp"

#include <map>
#include "boost/assert.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "input_manager_impl.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe {

typedef boost::filesystem::path path_t;

/*
*******************************************************************************/
std::string Input_manager::check_dir(std::string const& dir) {
   if( dir.empty() ) return dir;
   path_t p(dir);
   if( ! exists(p) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("input directory does not exist")
            << Err::str1_t(sanitize(dir))
            << Err::str2_t(sanitize(p.string()))
   );
   if( ! is_directory(p) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("not a directory")
            << Err::str1_t(sanitize(dir))
            << Err::str2_t(sanitize(p.string()))
   );
   return canonical(p).string();
}

/*
*******************************************************************************/
std::string Input_manager::path(std::string const& root, std::string const& fn) {
   path_t rp(root);
   path_t fnp(fn);
   if( ! fnp.is_absolute() && ! root.empty() ) fnp = rp / fnp;
   if( ! exists(fnp) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("input path does not exist")
            << Err::str1_t(sanitize(fn))
            << Err::str2_t(sanitize(root))
   );
   return fnp.string();
}

/*
*******************************************************************************/
Input_manager::Input_manager(
         std::string const& base,
         boost::property_tree::ptree const& pt,
         Value_map& vm
)
: root_(check_dir(base)),
  iv_(),
  is_paired_(true),
  has_emid_(true),
  has_qual_(true)
{
   typedef std::vector<Seq_file_entry::map_t> vvmap_t;
   vvmap_t v;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
         if( vt.first.size() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("array of objects expected")
         );
         v.push_back(input_detail::prepare_values(vt.second, vm));
   }

   input_detail::harmonize_variables(v, vm);

   iv_.reserve(v.size());
   BOOST_FOREACH(Seq_file_entry::map_t const& m, v) {
      iv_.push_back(Seq_file_entry(m, *this, vm));
   }

   BOOST_FOREACH(Seq_file_entry const& sfe, iv_) {
      if( ! sfe.is_paired() ) {
         is_paired_ = false;
      }

      if( ! sfe.has_emid() ) {
         has_emid_ = false;
      }

      if( ! sfe.has_quality() ) {
         has_qual_ = false;
      }
   }
}

}//namespace vdj_pipe
