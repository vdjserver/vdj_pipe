/** @file "/vdj_pipe/lib/input_manager_impl.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef INPUT_MANAGER_IMPL_HPP_
#define INPUT_MANAGER_IMPL_HPP_
#include <algorithm>
#include <map>
#include <set>
#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/range.hpp"
namespace bpt = boost::property_tree;

#include "keywords_variable.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/value_variant.hpp"
#include "vdj_pipe/sequence_file_entry.hpp"

namespace vdj_pipe{ namespace input_detail{

typedef Seq_file_entry::map_t val_map_t;
typedef val_map_t::value_type val_pair_t;
typedef std::vector<val_map_t> vvmap_t;

VDJ_PIPE_KEYWORD_STRUCT(
         input_kwds,
         (forward_seq)
         (reverse_seq)
         (forward_mid)
         (reverse_mid)
         (forward_qual)
         (reverse_qual)
);

/**@brief
*******************************************************************************/
val_map_t prepare_values(bpt::ptree const& pt, Value_map& vm) {
   typedef Seq_file_entry::Err Err;
   val_map_t m;
   std::set<std::string> name_set;
   bpt::ptree::value_type const* sequence = 0;
   bpt::ptree::value_type const* quality = 0;
   bpt::ptree::value_type const* reverse = 0;
   bpt::ptree::value_type const* forward_seq = 0;
   bpt::ptree::value_type const* reverse_seq = 0;
   bpt::ptree::value_type const* forward_mid = 0;
   bpt::ptree::value_type const* reverse_mid = 0;
   bpt::ptree::value_type const* forward_qual = 0;
   bpt::ptree::value_type const* reverse_qual = 0;

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {

      if( ! name_set.insert(vt.first).second ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("duplicate value name")
               << Err::str1_t(vt.first)
      );

      if( vt.first == kwds::Single::sequence() ) {
         sequence = &vt;

      } else if( vt.first == kwds::Single::quality() ) {
         quality = &vt;

      } else if( vt.first == kwds::Single::is_reverse() ) {
         reverse = &vt;

      } else if( vt.first == input_kwds::forward_seq() ) {
         forward_seq = &vt;

      } else if( vt.first == input_kwds::reverse_seq() ) {
         reverse_seq = &vt;

      } else if( vt.first == input_kwds::forward_mid() ) {
         forward_mid = &vt;

      } else if( vt.first == input_kwds::reverse_mid() ) {
         reverse_mid = &vt;

      } else if( vt.first == input_kwds::forward_qual() ) {
         forward_qual = &vt;

      } else if( vt.first == input_kwds::reverse_qual() ) {
         reverse_qual = &vt;

      } else if( vt.first.size() ) {
         const Val_id vid = vm.insert_name(vt.first);
         const value_variant vv = parse_variant(vt.second.data());
         m.insert(val_pair_t(vid, vv));

      } else {
         BOOST_THROW_EXCEPTION( Err() << Err::msg_t("empty value name") );
      }
   }

   // check for correct file name combinations
   // and translate them to variable map names
   if( sequence ) {
      if(
               forward_seq || reverse_seq || forward_mid ||
               reverse_mid || forward_qual || reverse_qual
      ) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t(
                           "\"forward_\" and \"reverse_\" cannot be used with "
                           "\"sequence\""
                  )
         );
      }
      const Val_id vid1 = vm.value_id(kwds::Single::seq_file_path());
      m.insert(val_pair_t(vid1, sequence->second.get_value<std::string>()));

      if( quality ) {
         const Val_id vid = vm.value_id(kwds::Single::qual_file_path());
         m.insert(val_pair_t(vid, quality->second.get_value<std::string>()));
      }

      const Val_id vid2 = vm.value_id(kwds::Single::is_reverse());
      if( reverse ) {
         m.insert(val_pair_t(vid2, reverse->second.get_value<bool>()));
      } else {
         m.insert(val_pair_t(vid2, false));
      }

      return m;
   }

   if( quality || reverse ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t(
                     "\"quality\" and \"reverse\" should be used with "
                     "\"sequence\" keyword"
            )
   );

   if( forward_seq && reverse_seq ) {

      if( (bool)forward_qual != (bool)reverse_qual ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t(
                        "both forward and reverse quality scores should be "
                        "provided"
               )
      );

      if( (bool)forward_mid != (bool)reverse_mid ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t(
                        "both forward and reverse eMID files should be provided"
               )
      );

      const Val_id vid1 = vm.value_id(kwds::Forward::seq_file_path_fwd());
      m.insert(val_pair_t(vid1, forward_seq->second.get_value<std::string>()));
      const Val_id vid2 = vm.value_id(kwds::Reverse::seq_file_path_rev());
      m.insert(val_pair_t(vid2, reverse_seq->second.get_value<std::string>()));

      if( forward_qual ) {
         const Val_id vid1 = vm.value_id(kwds::Forward::qual_file_path_fwd());
         m.insert(val_pair_t(vid1, forward_qual->second.get_value<std::string>()));
         const Val_id vid2 = vm.value_id(kwds::Reverse::qual_file_path_rev());
         m.insert(val_pair_t(vid2, reverse_qual->second.get_value<std::string>()));
      }

      if( forward_mid ) {
         const Val_id vid1 = vm.value_id(kwds::Emid::emid_file_path_fwd());
         m.insert(val_pair_t(vid1, forward_mid->second.get_value<std::string>()));
         const Val_id vid2 = vm.value_id(kwds::Emid::emid_file_path_rev());
         m.insert(val_pair_t(vid2, reverse_mid->second.get_value<std::string>()));
      }

      return m;
   }

   if( forward_seq ) {
      const Val_id vid1 = vm.value_id(kwds::Single::seq_file_path());
      m.insert(val_pair_t(vid1, forward_seq->second.get_value<std::string>()));

      const Val_id vid2 = vm.value_id(kwds::Single::is_reverse());
      m.insert(val_pair_t(vid2, false));

      if( forward_qual ) {
         const Val_id vid1 = vm.value_id(kwds::Single::qual_file_path());
         m.insert(val_pair_t(vid1, forward_qual->second.get_value<std::string>()));
      }

      return m;
   }

   if( reverse_seq ) {
      const Val_id vid1 = vm.value_id(kwds::Single::seq_file_path());
      m.insert(val_pair_t(vid1, reverse_seq->second.get_value<std::string>()));

      const Val_id vid2 = vm.value_id(kwds::Single::is_reverse());
      m.insert(val_pair_t(vid2, true));

      if( reverse_qual ) {
         const Val_id vid1 = vm.value_id(kwds::Single::qual_file_path());
         m.insert(val_pair_t(vid1, reverse_qual->second.get_value<std::string>()));
      }

      return m;
   }

   BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("no sequence information found")
   );

   return m;
}

typedef std::map<Val_id,int> type_map_t;

/**
@throw if variable is present in curr_types, but neither in entry_vars nor

*******************************************************************************/
void harmonize_variables_1(
         val_map_t& entry_vars,
         type_map_t& curr_types,
         type_map_t& change_to,
         std::set<Val_id> const& ign,
         Value_map const& vm
) {
   typedef Seq_file_entry::Err Err;
   BOOST_FOREACH(type_map_t::value_type& vp, curr_types) {
      BOOST_ASSERT(vp.first);

      if( ign.find(vp.first) != ign.end() ) continue;

      if( entry_vars.find(vp.first) == entry_vars.end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("value not present in all input entries")
               << Err::str1_t(vm.name(vp.first))
      );

      value_variant& vv = entry_vars[vp.first];
      if( vv.which() == vp.second ) continue;

      //current type is int and entry has float
      if(
               vp.second == Type_index<long>::value &&
               vv.which() == Type_index<double>::value
      ) {
         change_to[vp.first] = Type_index<double>::value;
         vp.second = Type_index<double>::value;
         continue;
      }

      //current type is float and entry has int
      if(
               vp.second == Type_index<double>::value &&
               vv.which() == Type_index<long>::value
      ) {
         vv = (double)boost::get<long>(vv);
         continue;
      }

      //current type is string
      if( vp.second == Type_index<std::string>::value ) {
         vv = boost::lexical_cast<std::string>(vv);
         continue;
      }

      //convert both types to string
      if( vv.which() != Type_index<std::string>::value ) {
         vv = boost::lexical_cast<std::string>(vv);
      }
      change_to[vp.first] = Type_index<std::string>::value;
      vp.second = Type_index<std::string>::value;
   }
}

/**
*******************************************************************************/
void harmonize_variables_2(
         val_map_t& entry_vars,
         type_map_t const& change_to,
         Value_map const& vm
) {
   typedef Seq_file_entry::Err Err;
   BOOST_FOREACH(type_map_t::value_type const& vp, change_to) {
      value_variant& vv = entry_vars[vp.first];
      if( vv.which() == vp.second ) continue;

      //current type is float and entry has int
      if(
               vp.second == Type_index<double>::value &&
               vv.which() == Type_index<long>::value
      ) {
         vv = (double)boost::get<long>(vv);
         continue;
      }

      //current type is string
      if( vp.second == Type_index<std::string>::value ) {
         vv = boost::lexical_cast<std::string>(vv);
         continue;
      }

      BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("value type conversion error")
                     << Err::str1_t(vm.name(vp.first))
                     << Err::str2_t(variable_type_str(vv))
                     << Err::str3_t(variable_type_str(vp.second))
      );
   }

}

/**
Make sure all required (non-ignorable) variables are present in all input
entries.
Make sure that all variables have same type
*******************************************************************************/
void harmonize_variables(vvmap_t& vvm, Value_map const& vm) {
   typedef Seq_file_entry::Err Err;

   if( vvm.empty() ) return;

   type_map_t curr_types;
   BOOST_FOREACH(
            val_map_t& entry_vars,
            boost::make_iterator_range(++vvm.begin(), vvm.end())
   ) {
      typedef type_map_t::value_type pair_t;
      BOOST_FOREACH(val_pair_t const& vp, entry_vars) {
         curr_types.insert(pair_t(vp.first, vp.second.which()));
      }
   }

   //these variables may be set only in some input entries
   std::set<Val_id> ign;

   if( Val_id const* id = vm.find_id(kwds::Single::qual_file_path()) ) {
      ign.insert(*id);
   }

   if( Val_id const* id = vm.find_id(kwds::Forward::qual_file_path_fwd()) ) {
      ign.insert(*id);
   }

   if( Val_id const* id = vm.find_id(kwds::Reverse::qual_file_path_rev()) ) {
      ign.insert(*id);
   }

   type_map_t change_to;
   BOOST_FOREACH(
            val_map_t& entry_vars,
            boost::make_iterator_range(++vvm.begin(), vvm.end())
   ) {
      harmonize_variables_1(entry_vars, curr_types, change_to, ign, vm);
   }

   BOOST_FOREACH(
            val_map_t& entry_vars,
            boost::make_iterator_range(++vvm.begin(), vvm.end())
   ) {
      harmonize_variables_2(entry_vars, change_to, vm);
   }
}

}//namespace input_detail
}//namespace vdj_pipe
#endif /* INPUT_MANAGER_IMPL_HPP_ */
