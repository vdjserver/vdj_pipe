/** @file "/vdj_pipe/lib/keywords_root.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef KEYWORDS_ROOT_HPP_
#define KEYWORDS_ROOT_HPP_
#include "vdj_pipe/detail/keyword_struct_macro.hpp"

namespace vdj_pipe{ namespace kwds{

VDJ_PIPE_KEYWORD_STRUCT(
         Root,
         (base_path_input)
         (base_path_output)
         (config_output_path)
         (csv_file_delimiter)
         (external_MIDs)
         (max_file_reads)
         (max_reads)
         (paired_reads)
         (plots_list_path)
         (quality_scores)
         (summary_output_path)
         (input_csv)
         (input)
         (steps)
);

}//namespace kwds
}//namespace vdj_pipe
#endif /* KEYWORDS_ROOT_HPP_ */
