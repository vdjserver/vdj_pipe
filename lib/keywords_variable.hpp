/** @file "/vdj_pipe/lib/keywords_variable.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef KEYWORDS_VARIABLE_HPP_
#define KEYWORDS_VARIABLE_HPP_
#include "vdj_pipe/detail/keyword_struct_macro.hpp"

namespace vdj_pipe{ namespace kwds{

VDJ_PIPE_KEYWORD_STRUCT(
         Single,
         (description)
         (sequence)
         (quality)
         (trim)
         (is_reverse)
         (seq_file_path)
         (qual_file_path)
);

VDJ_PIPE_KEYWORD_STRUCT(
         Forward,
         (description_fwd)
         (sequence_fwd)
         (quality_fwd)
         (trim_fwd)
         (seq_file_path_fwd)
         (qual_file_path_fwd)
         (forward)
);

VDJ_PIPE_KEYWORD_STRUCT(
         Reverse,
         (description_rev)
         (sequence_rev)
         (quality_rev)
         (trim_rev)
         (seq_file_path_rev)
         (qual_file_path_rev)
         (reverse)
);

VDJ_PIPE_KEYWORD_STRUCT(
         Merged,
         (sequence_merged)
         (quality_merged)
         (trim_merged)
);

VDJ_PIPE_KEYWORD_STRUCT(
         Emid,
         (emid_fwd)
         (emid_rev)
         (emid_file_path_fwd)
         (emid_file_path_rev)
);

}//namespace kwds
}//namespace vdj_pipe
#endif /* KEYWORDS_VARIABLE_HPP_ */
