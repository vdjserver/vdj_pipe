/** @file "/vdj_pipe/lib/lib_info.cpp"
part of vdj_pipe project.
Distributed under GNU General Public License, Version 3; see doc/license.txt.
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif

#include "vdj_pipe/lib_info.hpp"
#include <sstream>
#include "boost/preprocessor/stringize.hpp"
#include "boost/version.hpp"
#include "boost/predef.h"
#include "boost/predef/make.h"
#include "boost/predef/version_number.h"
#include "boost/predef/compiler.h"

#ifndef VDJ_PIPE_NAME
#define VDJ_PIPE_NAME vdj_pipe
#endif

#ifndef VDJ_PIPE_DESCRIPTION
#define VDJ_PIPE_DESCRIPTION ""
#endif

#ifndef VDJ_PIPE_COPYRIGHT
#define VDJ_PIPE_COPYRIGHT ""
#endif

#ifndef VDJ_PIPE_LICENSE
#define VDJ_PIPE_LICENSE ""
#endif

#ifndef VDJ_PIPE_VERSION_1
#define VDJ_PIPE_VERSION_1 0
#endif

#ifndef VDJ_PIPE_VERSION_2
#define VDJ_PIPE_VERSION_2 0
#endif

#ifndef VDJ_PIPE_VERSION_3
#define VDJ_PIPE_VERSION_3 0
#endif

#ifndef VDJ_PIPE_VERSION_EXTRA
#define VDJ_PIPE_VERSION_EXTRA ???
#endif

#ifndef VDJ_PIPE_VERSION_DIRTY
#define VDJ_PIPE_VERSION_DIRTY 0
#endif

#ifndef VDJ_PIPE_BUILD
#define VDJ_PIPE_BUILD 0
#endif

namespace vdj_pipe{ namespace{

std::string make_version_str() {
   std::ostringstream str;
   str
   << 'v' << VDJ_PIPE_VERSION_1 << '.'
   << VDJ_PIPE_VERSION_2 << '.' << VDJ_PIPE_VERSION_3
   ;
   const std::string e = std::string(BOOST_PP_STRINGIZE(VDJ_PIPE_VERSION_EXTRA));
   if( ! e.empty() ) str << '-' << e;
   if( VDJ_PIPE_VERSION_DIRTY ) str << '~';
   return str.str();
}

std::string make_other_libs() {
   std::ostringstream str;
   str
   << "Boost" << BOOST_VERSION / 100000 << '.' << BOOST_VERSION / 100 % 1000
   << '.' << BOOST_VERSION % 100
   ;
   return str.str();
}

}//namespace anonymous


/*
*******************************************************************************/
std::string const& Lib_info::name() {
   static const std::string s = std::string(BOOST_PP_STRINGIZE(VDJ_PIPE_NAME));
   return s;
}

/*
*******************************************************************************/
std::string const& Lib_info::version() {
   static const std::string s = make_version_str();
   return s;
}

  static bool stamp_flag_ = true;

  /*
*******************************************************************************/
  bool const& Lib_info::stamp_flag() { return stamp_flag_; }

  /*
*******************************************************************************/
  void Lib_info::set_stamp_flag(bool aFlag) { stamp_flag_ = aFlag; }

/*
*******************************************************************************/
std::string const& Lib_info::description() {
   static const std::string s = std::string(VDJ_PIPE_DESCRIPTION);
   return s;
}

/*
*******************************************************************************/
std::string const& Lib_info::copyright() {
   static const std::string s = std::string(VDJ_PIPE_COPYRIGHT);
   return s;
}

/*
*******************************************************************************/
std::string const& Lib_info::license() {
   static const std::string s = std::string(VDJ_PIPE_LICENSE);
   return s;
}

/*
*******************************************************************************/
std::string const& Lib_info::other_libs() {
   static const std::string s = make_other_libs();
   return s;
}

/*
*******************************************************************************/
std::string const& Lib_info::compiler() {
#if defined __clang__
   static const std::string s =
            "clang" BOOST_PP_STRINGIZE(__clang_major__) "."
            BOOST_PP_STRINGIZE(__clang_minor__) "."
            BOOST_PP_STRINGIZE(__clang_patchlevel__)
            ;

#elif  __INTEL_COMPILER
   static const std::string s =
            "intel" BOOST_PP_STRINGIZE(__INTEL_COMPILER)
            ;

#elif   _MSC_VER
   static const std::string s =
            "msvc" BOOST_PP_STRINGIZE(_MSC_FULL_VER)
            ;

#elif  __GNUC__
   static const std::string s =
            "gcc" BOOST_PP_STRINGIZE(__GNUC__) "."
            BOOST_PP_STRINGIZE(__GNUC_MINOR__) "."
            BOOST_PP_STRINGIZE(__GNUC_PATCHLEVEL__)
            ;
#else
   static const std::string s = "unknown compiler";
#endif
   return s;
}

/*
*******************************************************************************/
int Lib_info::version_1() {return VDJ_PIPE_VERSION_1;}

/*
*******************************************************************************/
int Lib_info::version_2() {return VDJ_PIPE_VERSION_2;}

/*
*******************************************************************************/
int Lib_info::version_3() {return VDJ_PIPE_VERSION_3;}

/*
*******************************************************************************/
std::string const& Lib_info::version_e() {
   static const std::string s = std::string(BOOST_PP_STRINGIZE(VDJ_PIPE_VERSION_EXTRA));
   return s;
}

/*
*******************************************************************************/
int Lib_info::build() {return VDJ_PIPE_BUILD;}


}//namespace vdj_pipe
