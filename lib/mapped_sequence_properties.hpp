/** @file "/vdj_pipe/lib/mapped_sequence_properties.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef LIB_MAPPED_SEQUENCE_PROPERTIES_HPP_
#define LIB_MAPPED_SEQUENCE_PROPERTIES_HPP_
#include "boost/algorithm/string/classification.hpp" //operator!
#include "boost/algorithm/string/predicate.hpp" //all
#include "boost/foreach.hpp"

#include "sequence_map_short.hpp"
#include "sequence_map_types.hpp"

namespace vdj_pipe{ namespace detail{

/**@brief
*******************************************************************************/
struct Seq_props {
   explicit Seq_props(name_seq_vector const& sv)
   : all_short_(true),
     same_size_(true),
     has_ambiguous_(false),
     min_size_(0),
     max_size_(0)
   {
      if( sv.empty() ) return;
      min_size_ = sv.front().second.size();
      max_size_ = min_size_;
      BOOST_FOREACH(match_seq const& ms, sv) {
         if( ms.second.size() < min_size_ ) min_size_ = ms.second.size();
         if( ms.second.size() > max_size_ ) max_size_ = ms.second.size();
         if( has_ambiguous_ ) continue;
         if( ! all(ms.second, ! Is_ambiguous()) ) has_ambiguous_ = true;
      }
      if( max_size_ > Seq_map_short::seq_type::length() ) all_short_ = false;
      if( min_size_ != max_size_ ) same_size_ = false;
   }

   bool all_short_;
   bool same_size_;
   bool has_ambiguous_;
   std::size_t min_size_;
   std::size_t max_size_;
};

}//namespace detail
}//namespace vdj_pipe
#endif /* LIB_MAPPED_SEQUENCE_PROPERTIES_HPP_ */
