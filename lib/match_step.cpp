/** @file "/vdj_pipe/lib/match_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/match_step.hpp"

#include <ostream>
#include <vector>
#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/format.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "me_factory.hpp"
#include "vdj_pipe/detail/disable_warnings.hpp"
#include "vdj_pipe/pipe_environment.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(
   kwds,
   (trimmed)  // search for elements relatively to trimmed read
   (reverse)   // for reverse reads, search for elements in reverse-complemented sequence
   (elements)           // matching elements description
   (combinations)       // set values to element combinations
);

/*
*******************************************************************************/
Match_step::Match_step(
         Vm_access_single const& vma,
         bpt::ptree const& pt,
         Pipe_environment& pe
)
: Step_base_single(vma, pt, pe),
  mb_(),
  trim_(false),
  reverse_(false)
{
   match::Match_element_factory mef(vma, pe.input());
   std::vector<bpt::ptree const*> elements, combinations;

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::trimmed() ) {
         trim_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::reverse() ) {
         reverse_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::elements() ) {
         BOOST_FOREACH(bpt::ptree::value_type const& vt2, vt.second) {
            if( vt2.first.size() ) BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("array of JSON objects expected")
                     << Err::str1_t(kwds::elements())
            );
            elements.push_back(&vt2.second);
         }

      } else if( vt.first == kwds::combinations() ) {
         BOOST_FOREACH(bpt::ptree::value_type const& vt2, vt.second) {
            if( vt2.first.size() ) BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("array of JSON objects expected")
                     << Err::str1_t(kwds::combinations())
            );
            combinations.push_back(&vt2.second);
         }

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   BOOST_FOREACH(bpt::ptree const* pt, elements) {
      mef.insert_element(*pt);
   }

   BOOST_FOREACH(bpt::ptree const* pt, combinations) {
      mef.insert_combination(*pt);
   }

   mb_ = mef.get();
}

/*
*******************************************************************************/
void Match_step::run() {
   const seq_type seq = vma_.sequence(trim_, reverse_);
   const qual_type qual = vma_.quality(trim_, reverse_);
   const sequence_interval si0(0U, seq.size());
   const sequence_interval si1 = (*mb_)(seq, qual, si0);
   vma_.trim(si1);
}

/*
*******************************************************************************/
void Match_step::finish() {
   mb_->finish();
}

namespace{
const boost::format bf("%1$d (%2$.3g%%)");
}

/*
*******************************************************************************/
void Match_step::summary(std::ostream& os) const {
   const std::size_t nr = mb_->n_removed();
   const double rp = 100.0 * mb_->n_removed() / vma_.read_count();
   os
   << "removed: "
   << boost::format(bf) % nr % rp << '\n'
   << "total: " << vma_.read_count() << '\n'
   ;
}

}//namespace vdj_pipe
