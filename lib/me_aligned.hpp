/** @file "/vdj_pipe/lib/me_aligned.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_ALIGNED_HPP_
#define ME_ALIGNED_HPP_
#include "boost/lexical_cast.hpp"
#include "boost/utility/string_ref.hpp"
#include "me_relative_interval.hpp"
#include "me_types.hpp"
#include "sequence_map_aligned.hpp"
#include "sequence_map_types.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/sequence_interval.hpp"
#include "vdj_pipe/sequence_record.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{ namespace match{

/**@brief Identify DNA sequence interval by finding the best alignment
*******************************************************************************/
class Match_element_aligned {
public:
   struct Err : public base_exception{};
   typedef boost::string_ref sequence;
   typedef Qual_record::quality quality;

   Match_element_aligned(
            Value_map const& vm,
            std::string const& match_value_name,
            std::string const& score_value_name,
            std::string const& identity_value_name,
            Relative_interval const& ri,
            detail::name_seq_vector const& sv,
            const int min_score,
            const unsigned min_match_len,
            const bool track_mismatches
   )
   : vm_(vm),
     name_val_id_(vm_.insert_new_name(match_value_name)),
     score_val_id_(vm_.insert_new_name(score_value_name)),
     identity_val_id_(vm_.insert_new_name(identity_value_name)),
     gsm_(),
     ri_(ri),
     min_score_(min_score),
     min_match_len_(min_match_len),
     track_mismatches_(track_mismatches)
   {
      if( sv.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("no sequences provided")
      );

      for(std::size_t n = 0; n != sv.size(); ++n) {
         static const std::string seq = "seq_";
         detail::match_seq const& ms = sv[n];
         if( ms.first.empty() ) {
            gsm_.insert(
                     seq + boost::lexical_cast<std::string>(n+1),
                     ms.second,
                     false
            );
         } else {
            gsm_.insert(
                     ms.first,
                     ms.second,
                     false
            );
         }
      }
   }

   sequence_interval operator() (
            const sequence seq,
            quality const&,
            sequence_interval const& si
   ) {
      sequence_interval res = sequence_interval_invalid();
      if( ! is_valid(si) ) return res;
      const sequence_interval si1 = ri_(si, seq.size());
      if( ! is_valid(si1) || (std::size_t)width(si1) < min_match_len_ ) return res;

      const Seq_map_aligned::Match m =
               gsm_.best_match(
                        seq.substr(si1.lower(), width(si1)),
                        min_match_len_,
                        track_mismatches_
               );

      if( m.score_ > 0 ) {
         if( score_val_id_ ) vm_[score_val_id_] = (long)m.score_;
         if( identity_val_id_ ) vm_[identity_val_id_] = m.identity_;
      }

      if(
               (   track_mismatches_ && ( (unsigned)min_score_ >= m.n_mismatches_ ) ) ||
               ( ! track_mismatches_ && ( min_score_ <= m.score_ ) )
      ) {
         if( name_val_id_ ) vm_[name_val_id_] = gsm_.seq_id(m.id_);
         res = m.si_ + si1.lower();
      }

      return res;
   }

   void finish() {}

private:
   Value_map vm_;
   Val_id name_val_id_;
   Val_id score_val_id_;
   Val_id identity_val_id_;
   Seq_map_aligned gsm_;
   Relative_interval ri_;
   int min_score_;
   std::size_t min_match_len_;
   bool track_mismatches_;
};

}//namespace match
}//namespace vdj_pipe
#endif /* ME_ALIGNED_HPP_ */
