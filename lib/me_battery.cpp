/** @file "/vdj_pipe/lib/me_battery.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#include "me_battery.hpp"

#include <map>
#include "boost/assert.hpp"
#include "boost/foreach.hpp"

namespace vdj_pipe{ namespace match{

namespace{

/**@brief
*******************************************************************************/
struct Finish_visitor : public boost::static_visitor<> {
   template<typename Step> void operator()(Step& step) const {
      try{
         step.finish();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  base_exception()
                  << base_exception::msg_t("error finalizing match element")
                  << base_exception::nested_t(boost::current_exception())
         );
      }
   }
};

}//anonymous namespace

/*
*******************************************************************************/
Match_element_battery::Match_element_battery(
   variant_vector const& vv,
   truncate_vector const& tv,
   detail::string_vector const& names,
   detail::string_vector const& dep_names,
   std::vector<bool> const& required,
   combination_vector const& cv
)
: mev_(vv),
  tv_(tv),
  me_deps_(mev_.size(), 0),
  required_(required),
  cv_(cv),
  n_removed_(0)
{
   BOOST_ASSERT(mev_.size() == tv_.size());
   BOOST_ASSERT(mev_.size() == required_.size());
   BOOST_ASSERT(names.size() == dep_names.size());

   typedef std::map<std::string,std::size_t> name_map;
   typedef name_map::value_type pair;
   typedef name_map::const_iterator iterator;
   name_map nm;

   for(std::size_t n = 0; n != names.size(); ++n) {
      if( names[n].empty() ) continue;
      if( ! nm.insert(pair(names[n], n+1)).second ) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("duplicate element name")
         << Err::str1_t(sanitize(names[n]))
      );
   }

   for(std::size_t n = 0; n != dep_names.size(); ++n) {
      if( dep_names[n].empty() ) continue;
      const iterator i = nm.find(dep_names[n]);
      if( i == nm.end() ) BOOST_THROW_EXCEPTION(
         Err()
         << Err::msg_t("undefined dependent element name")
         << Err::str1_t(sanitize(dep_names[n]))
      );
      me_deps_[n] = i->second;
   }
}

/*
*******************************************************************************/
void Match_element_battery::finish() {
   BOOST_FOREACH(me_variant& var, mev_) {
      boost::apply_visitor(Finish_visitor(), var);
   }
}

}//namespace match
}//namespace vdj_pipe
