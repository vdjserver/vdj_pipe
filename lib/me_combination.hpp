/** @file "/vdj_pipe/lib/me_combination.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_COMBINATION_HPP_
#define ME_COMBINATION_HPP_
#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/unordered_map.hpp"
#include "me_types.hpp"
#include "sequence_map_types.hpp"
#include "vdj_pipe/detail/string_ref.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{ namespace match{

/**@brief
*******************************************************************************/
class Match_combination {
   typedef boost::unordered_map<std::string, Seq_id> seq_map;
   typedef seq_map::const_iterator map_iter;
   typedef std::vector<seq_map> seq_map_vector;
   typedef std::vector<Seq_id> id_vector;
   typedef boost::unordered_map<id_vector, std::string> name_map;
   typedef name_map::const_iterator name_iter;
   typedef boost::string_ref sequence;

public:
   struct Err : public base_exception{};

   Match_combination(
            Value_map const& vm,
            std::string const& val_name,
            std::vector<std::size_t> const& mev,
            detail::string_table const& st
   )
   : vm_(vm),
     val_id_(vm_.insert_new_name(val_name)),
     mev_(mev),
     smv_(mev_.size()),
     nm_()
   {
      if( st.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("no combination sequences found")
      );
      BOOST_FOREACH(detail::string_vector const& sv, st) {
         BOOST_ASSERT(sv.size() == mev.size() + 1);
         std::string const& name = sv[0];
         id_vector idv(mev.size());
         for(std::size_t n = 0; n != mev.size(); ++n) {
            seq_map& sm = smv_[n];
            map_iter i = sm.emplace(sv[n + 1], Seq_id(sm.size() + 1)).first;
            idv[n] = i->second;
         }
         const std::pair<name_iter,bool> p = nm_.emplace(idv, name);
         if( ! p.second ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("duplicate combination")
                  << Err::str1_t(sanitize(name))
         );
      }

      // interval_vector starts with the whole read interval followed by
      // the intervals produced by each of the matching elements
      // Therefore mev_ indices should be 1-based
      BOOST_FOREACH(std::size_t& n, mev_) ++n;
   }

   sequence_interval operator()(
            const sequence seq,
            interval_vector const& iv
   ) {
      id_vector idv(mev_.size());
      for(std::size_t n = 0; n != mev_.size(); ++n) {
         sequence_interval const& si = iv[mev_[n]];
         if( ! is_valid(si) ) return sequence_interval::empty();
         const sequence s = seq.substr(si.lower(), width(si));
         map_iter i = smv_[n].find(
                  s,
                  boost::hash<sequence>(),
                  vdj_pipe::detail::Equal_string_ref()
         );
         if( i == smv_[n].end() ) return sequence_interval::empty();
         idv[n] = i->second;
      }
      name_iter i = nm_.find(idv);
      if( i == nm_.end() ) return sequence_interval::empty();
      vm_[val_id_] = i->second;
      return iv[0];
   }

private:
   Value_map vm_;
   Val_id val_id_;
   std::vector<std::size_t> mev_;
   seq_map_vector smv_;
   name_map nm_;
};

}//namespace match
}//namespace vdj_pipe
#endif /* ME_COMBINATION_HPP_ */
