/** @file "/vdj_pipe/lib/me_factory.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "me_factory.hpp"

#include <fstream>
#include <map>
#include <sstream>
#include <utility>
#include <vector>
#include "boost/foreach.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;
#include "boost/tokenizer.hpp"

#include "sequence_map_parse.hpp"
#include "vdj_pipe/detail/keyword_struct_macro.hpp"
#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/input_manager.hpp"
#include "vdj_pipe/value_map_access_single.hpp"

namespace vdj_pipe{ namespace match{

namespace {

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
typedef tokenizer::const_iterator tok_iter;
boost::char_separator<char> separator(",\t|");

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
   kwds,
   (start)
   (end)
   (length)
   (seq_file)           // path to fasta/fastq file
   (csv_file)           // CSV file description
   (sequence)           // sequence or array of sequences
   (required)           // filter out read if element is not found
   (min_score)          // reject matches with smaller scores
   (max_mismatches)     // reject matches with more mismatching nucleotides
   (require_best)       // ignore multiple equally good matches
   (min_match_length)   // minimal number of contiguous matching nucleotides
                        // used for identifying best matching sequences
                        // for gapped alignment
   (allow_gaps)         // use gapped alignment
   (cut_lower)(cut_upper)
   (value_name)         // name for value map
   (name)               // name for reference by other match elements
   (score_name)
   (identity_name)
);

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
   position_kwds,
   (pos)    //
   (before) //
   (after)  //
);

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
   csv_kwds,
   (path)               // file path
   (names_column)       // name of the column containing sequence names
   (sequences_column)   // name of the column containing sequences
   (values_column)      // array of match element name - column name pairs
   (skip_header)        //
);

}//anonymous namespace

const int Cut_proto::      unset = std::numeric_limits<int>::min();
const int Element_proto::  unset = std::numeric_limits<int>::min();

/*
factors for selecting match element type:
   match sequences provided
      NO: me_unknown
   gapped match requested
      YES: use aligner
   match sequences short
   match sequences same size
   match sequences have wild cards

*******************************************************************************/
void Match_element_factory::insert_element(
         bpt::ptree const& pt
) {
   try{
      Element_proto ep(pt, in_);

      if( ep.seqs_.empty() ) {
         vv_.push_back(
                  Match_element_positional(
                           vma_,
                           ep.value_name_,
                           ep.interval()
                  )
         );

      } else if( ep.allow_gaps_ && ! ep.sp_.get().has_ambiguous_ ) {
         vv_.push_back(
                  Match_element_aligned(
                           vma_,
                           ep.value_name_,
                           ep.score_name_,
                           ep.identity_name_,
                           ep.interval(),
                           ep.seqs_,
                           ep.min_score_.get(),
                           ep.min_match_length_.get(),
                           ep.track_mismatches_
                  )
         );

      } else if( ep.allow_gaps_ && ep.sp_.get().has_ambiguous_ ) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("gapped alignment of sequences with ambiguous "
                     "nucleotides is not supported")
         );

      } else if(
               ep.sp_.get().all_short_ &&
               ep.sp_.get().same_size_ &&
               ! ep.sp_.get().has_ambiguous_
      ) {
         vv_.push_back(
                  Match_element_short(
                           vma_,
                           ep.value_name_,
                           ep.score_name_,
                           ep.interval(),
                           ep.seqs_,
                           ep.min_score_.get(),
                           ep.ignore_dups_,
                           ep.require_best_,
                           ep.track_mismatches_
                  )
         );

      } else {
         vv_.push_back(
                  Match_element_ambiguous(
                           vma_,
                           ep.value_name_,
                           ep.score_name_,
                           ep.interval(),
                           ep.seqs_,
                           ep.min_score_.get(),
                           ep.ignore_dups_,
                           ep.require_best_,
                           ep.track_mismatches_
                  )
         );
      }

      tv_.push_back(ep.truncation());
      names_.push_back(ep.name_);
      dep_names_.push_back(ep.re_name_);
      required_.push_back(ep.required_);

   } catch(std::exception const&) {
      std::stringstream ss;
      write_json(ss, pt);
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error adding match element")
               << Err::str1_t(ss.str())
               << Err::nested_t(boost::current_exception())
      );
   }
}

/*
*******************************************************************************/
void make_element_index(
         std::vector<std::string> const& me_names,
         std::vector<std::string> const& all_me_names,
         std::vector<std::size_t>& mev
) {
   typedef base_exception Err;
   mev.reserve(me_names.size());
   BOOST_FOREACH(std::string const& s, me_names) {
      std::vector<std::string>::const_iterator i =
               find(all_me_names.begin(), all_me_names.end(), s);
      if( i == all_me_names.end() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unknown match element name")
               << Err::str1_t(sanitize(s))
      );
      mev.push_back((std::size_t)(i - all_me_names.begin()));
   }
}

/*
*******************************************************************************/
void make_column_inds(
         std::string const& line,
         std::vector<std::string> const& col_names,
         std::vector<int>& col_inds
) {
   typedef base_exception Err;
   tokenizer tok(line, separator);
   tok_iter i = tok.begin();
   for(int n = 0; ! i.at_end(); ++i, ++n) {
      if( i->empty() ) continue;
      std::vector<std::string>::const_iterator j =
               find(col_names.begin(), col_names.end(), *i);
      if( j == col_names.end() ) continue;

      if( col_inds[n] >= 0 ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("column name duplicate")
               << Err::str1_t(sanitize(*j))
      );

      col_inds[n] = j - col_names.begin();
   }

   if( col_names.front().size() && col_inds.front() < 0 ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("combination names column not found")
            << Err::str1_t(sanitize(col_names.front()))
   );

   for(int n = 1; n != (int)col_names.size(); ++n) {
         if( col_names[n].empty() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("column name for element not provided")
                  << Err::int1_t(n)
         );

         if( col_inds[n] < 0 ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("column name for element not found")
                  << Err::str1_t(sanitize(col_names[n]))
         );
   }
}

/*
*******************************************************************************/
void parse_csv_combination(
         std::istream& is,
         std::vector<int> const& col_inds,
         detail::string_table& st
) {
   std::map<int, int> cm;
   for( int i = 0; i != (int)col_inds.size(); ++i ) {
      if( col_inds[i] >= 0 ) cm.insert(std::make_pair(col_inds[i], i));
   }

   std::string line;
   for( int line_num = 1; getline(is, line); ++line_num) {
      tokenizer tok(line, separator);
      tok_iter i = tok.begin();
      if( i.at_end() ) continue; //skip blank lines

      st.push_back(std::vector<std::string>(col_inds.size(), ""));

      for(int n = 0; ! i.at_end(); ++i, ++n) {
         const std::map<int,int>::const_iterator j = cm.find(n);
         if( j == cm.end() ) continue;
         st.back()[j->second] = *i;
      }

      if( st.back().front().empty() ) {
         st.back().front() =
                  "sample_" + boost::lexical_cast<std::string>(line_num);
      }
   }
}

/*
*******************************************************************************/
void parse_csv_combination(
         bpt::ptree const& pt,
         Input_manager const& in,
         std::vector<std::string> const& all_me_names,
         std::vector<std::size_t>& mev,
         detail::string_table& st
) {
   typedef base_exception Err;
   std::vector<std::string> me_names; //names of elements
   std::vector<std::string> col_names(1, ""); //names of CSV columns (optional)
   std::vector<int> col_inds(1, -1); //indices of CSV columns
   std::string path;
   bool skip_header = false;

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == csv_kwds::path() ) {
         path = in.path(vt.second.data());

      } else if( vt.first == csv_kwds::values_column() ) {
         BOOST_FOREACH(bpt::ptree::value_type const& vt2, vt.second) {
            BOOST_FOREACH(bpt::ptree::value_type const& vt3, vt2.second) {
               me_names.push_back(vt3.first);
               col_inds.push_back(vt3.second.get_value(-1));
               if( col_inds.back() < 0 ) col_names.push_back(vt3.second.data());
               else col_names.push_back("");
            }
         }

      } else if( vt.first == csv_kwds::names_column() ) {
         col_inds.front() = vt.second.get_value(-1);
         if( col_inds.front() < 0 ) col_names.front() = vt.second.data();

      } else if( vt.first == csv_kwds::skip_header() ) {
         skip_header = vt.second.get_value<bool>();

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t(
                           "unsupported element combination description keyword"
                  )
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( me_names.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no elements provided")
            << Err::str1_t(csv_kwds::values_column())
   );

   if( path.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("CSV file not provided")
            << Err::str1_t(csv_kwds::path())
   );

   BOOST_ASSERT(me_names.size() + 1 == col_names.size() );
   BOOST_ASSERT(col_names.size() == col_inds.size() );

   make_element_index(me_names, all_me_names, mev);
   std::ifstream ifs(path.c_str());
   std::string line;
   if( col_names[1].size() ) {
      getline(ifs, line);
      make_column_inds(line, col_names, col_inds);
   } else if( skip_header ) {
      getline(ifs, line);
   }
   parse_csv_combination(ifs, col_inds, st);
}

/*
*******************************************************************************/
Match_combination make_combination(
         Value_map const& vm,
         bpt::ptree const& pt,
         Input_manager const& in,
         std::vector<std::string> const& me_names
) {
   typedef base_exception Err;
   std::string val_name;
   std::vector<std::size_t> mev;
   detail::string_table st;

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::csv_file() ) {
         parse_csv_combination(vt.second, in, me_names, mev, st);

      } else if( vt.first == kwds::value_name() ) {
         val_name = vt.second.data();

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t(
                           "unsupported element combination description keyword"
                  )
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( mev.empty() || st.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no combination sequences found")
   );

   return
            Match_combination(
                     vm,
                     val_name,
                     mev,
                     st
            );
}

/*
*******************************************************************************/
void Match_element_factory::insert_combination(bpt::ptree const& pt) {
   try{
      cv_.push_back(make_combination(vma_, pt, in_, names_));
   } catch(std::exception const&) {
         std::stringstream ss;
         write_json(ss, pt);
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("error adding match element combination")
                  << Err::str1_t(ss.str())
                  << Err::nested_t(boost::current_exception())
         );
      }
}

/*
*******************************************************************************/
void Cut_proto::parse(bpt::ptree const& pt) {
   bool found = false;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == position_kwds::before() ) {
         if( found ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple before/after definitions")
         );
         found = true;
         cut_ = true;
         re_start_ = true;
         pos_ = vt.second.get_value<int>();

      } else if( vt.first == position_kwds::after() ) {
         if( found ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple before/after definitions")
         );
         found = true;
         cut_ = true;
         re_start_ = false;
         pos_ = vt.second.get_value<int>();

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported match element keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }
}

/*
*******************************************************************************/
Element_proto::Element_proto(
         bpt::ptree const& pt,
         Input_manager const& in
)
: start_defined_(true),
  re_start_(true),
  name_(),
  value_name_(),
  score_name_(),
  identity_name_(),
  re_name_(),
  pos_(),
  len_(),
  min_score_(),
  min_match_length_(),
  track_mismatches_(true),
  allow_gaps_(false),
  required_(false),
  ignore_dups_(false),
  require_best_(true),
  seqs_(),
  sp_(),
  cut_lo_(),
  cut_up_()
{
   bool cut_lower = false;
   bool cut_upper = false;
   bool name = false;
   bool value_name = false;
   bool score_name = false;
   bool identity_name = false;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::end() ) {
         if( pos_ ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple start/end definitions")
         );
         start_defined_ = false;
         parse_position(vt.second);

      } else if( vt.first == kwds::start() ) {
         if( pos_ ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple start/end definitions")
         );
         start_defined_ = true;
         parse_position(vt.second);

      } else if( vt.first == kwds::sequence() ) {
         detail::parse_sequences(vt.second, seqs_);

      } else if( vt.first == kwds::seq_file() ) {
         detail::parse_seq_files(vt.second, in, seqs_);

      } else if( vt.first == kwds::csv_file() ) {
         parse_csv_file(vt.second, in);

      } else if( vt.first == kwds::length() ) {
         if( len_ ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple length definitions")
         );
         len_ = vt.second.get_value<int>();

      } else if( vt.first == kwds::required() ) {
         required_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::min_score() ) {
         if( min_score_ ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("multiple min_score/max_mismatches definitions")
         );
         min_score_ = vt.second.get_value<int>();
         track_mismatches_ = false;

      } else if( vt.first == kwds::max_mismatches() ) {
         if( min_score_ ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("multiple min_score/max_mismatches definitions")
         );
         min_score_ = vt.second.get_value<int>();
         track_mismatches_ = true;

      } else if( vt.first == kwds::min_match_length() ) {
         if( min_match_length_ ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple min_match_length definitions")
         );
         min_match_length_ = vt.second.get_value<int>();

      } else if( vt.first == kwds::allow_gaps() ) {
         allow_gaps_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::require_best() ) {
         require_best_ = vt.second.get_value<bool>();

      } else if( vt.first == kwds::cut_lower() ) {
         if( cut_lower ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple cut_lower definitions")
         );
         cut_lower = true;
         cut_lo_.parse(vt.second);

      } else if( vt.first == kwds::cut_upper() ) {
         if( cut_upper ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple cut_upper definitions")
         );
         cut_upper = true;
         cut_up_.parse(vt.second);

      } else if( vt.first == kwds::name() ) {
         if( name ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple name definitions")
         );
         name = true;
         name_ = vt.second.data();

      } else if( vt.first == kwds::value_name() ) {
         if( value_name ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple value_name definitions")
         );
         value_name = true;
         value_name_ = vt.second.data();
         if( name_.empty() ) name_ = value_name_;

      } else if( vt.first == kwds::score_name() ) {
         if( score_name ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple score_name definitions")
         );
         score_name = true;
         score_name_ = vt.second.data();

      } else if( vt.first == kwds::identity_name() ) {
         if( identity_name ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple identity_name definitions")
         );
         identity_name = true;
         identity_name_ = vt.second.data();

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported match element keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( ! pos_ ) pos_ = 0;
   if( ! min_score_ ) min_score_ = 0;
   if( ! min_match_length_ ) min_match_length_ = 10;

   if( ! len_ && seqs_.empty() ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("match element length or sequence is required")
            << Err::str1_t(kwds::length())
            << Err::str2_t(kwds::sequence())
   );
   sp_ = detail::Seq_props(seqs_);
   if( ! len_ ) len_ = sp_.get().max_size_;
}

/*
*******************************************************************************/
void Element_proto::parse_position(
         bpt::ptree const& pt
) {
   bool found = false;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == position_kwds::pos() ) {
         if( pos_ ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple position definitions")
         );
         pos_ = vt.second.get_value<int>();

      } else if( vt.first == position_kwds::before() ) {
         if( found ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple before/after definitions")
         );
         found = true;
         re_start_ = true;
         re_name_ = vt.second.get_value<std::string>("");

      } else if( vt.first == position_kwds::after() ) {
         if( found ) BOOST_THROW_EXCEPTION(
                  Err() << Err::msg_t("multiple before/after definitions")
         );
         found = true;
         re_start_ = false;
         re_name_ = vt.second.get_value<std::string>("");

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported position keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }
}

/*
*******************************************************************************/
void Element_proto::parse_csv_file(
         bpt::ptree const& pt,
         Input_manager const& in
) {
   std::string path;
   std::string seq_col_name;
   int seq_col_n = -1;
   std::string names_col_name;
   int names_col_n = -1;

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == csv_kwds::path() ) {
         path = in.path(vt.second.data());

      } else if( vt.first == csv_kwds::sequences_column() ) {
         seq_col_n = vt.second.get_value<int>(-1);
         if( seq_col_n == -1 ) {
            seq_col_name = vt.second.data();
         }

      } else if( vt.first == csv_kwds::names_column() ) {
         names_col_n = vt.second.get_value<int>(-1);
         if( names_col_n == -1 ) {
            names_col_name = vt.second.data();
         }

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unsupported CSV file description keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   File_istream fis((File_input(path)));
   std::string line;
   int line_num = 1;
   if( seq_col_n == -1 ) {
      getline(fis.istream(), line);
      ++line_num;
      tokenizer tok(line, separator);
      tok_iter i = tok.begin();
      for( int n = 0; ! i.at_end(); ++i, ++n) {
         if( names_col_name.size() && names_col_name == *i ) {
            names_col_n = n;
            continue;
         }
         if( seq_col_name.size() && seq_col_name == *i ) seq_col_n = n;
      }
   }

   if( seq_col_n == -1 ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("column name for sequences not found")
   );

   ignore_dups_ = true; //CSVs are allowed to have duplicate sequences

   int n_cols = -1; //expected number of columns
   for( ;getline(fis.istream(), line); ++line_num ) {
      detail::match_seq ms;
      tokenizer tok(line, separator);
      tok_iter i = tok.begin();
      if( i.at_end() ) continue;
      int n = 0;
      for( ; ! i.at_end() && n != n_cols; ++i, ++n) {
         if( i.at_end() || n == n_cols ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected number of columns")
                  << Err::int1_t(n_cols)
                  << Err::int2_t(n)
                  << Err::line_t(line_num)
         );

         if( names_col_n == n ) ms.first = *i;
         if( seq_col_n == n ) ms.second = *i;
      }
      if( n_cols == -1 ) n_cols = n;
      if( ms.second.size() ) seqs_.push_back(ms);
   }
}

/*
*******************************************************************************/
Relative_interval Element_proto::interval() const {
   BOOST_ASSERT(pos_);
   const Relative_position rp(pos_.get(), re_start_);
   int len = unset;
   if( len_ ) {
      len = start_defined_ ? len_.get() : - len_.get() ;
   }
   return Relative_interval(rp, len);
}

}//namespace match
}//namespace vdj_pipe
