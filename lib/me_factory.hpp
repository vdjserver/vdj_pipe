/** @file "/vdj_pipe/lib/me_factory.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_FACTORY_HPP_
#define ME_FACTORY_HPP_
#include <iosfwd>
#include <map>
#include <memory>
#include <string>
#include "boost/assert.hpp"
#include "boost/optional.hpp"
#include "boost/property_tree/ptree_fwd.hpp"
namespace bpt = boost::property_tree;

#include "mapped_sequence_properties.hpp"
#include "me_battery.hpp"
#include "vdj_pipe/config.hpp"
#include "vdj_pipe/exception.hpp"

namespace vdj_pipe{
class Input_manager;
class Vm_access_single;

namespace match{

/**@brief 
*******************************************************************************/
class Match_element_factory {
public:

   struct Err : public base_exception{};

   Match_element_factory(
            Vm_access_single const& vma,
            Input_manager const& in
   )
   : vma_(vma),
     in_(in)
   {}

   void insert_element(bpt::ptree const& pt);
   void insert_combination(bpt::ptree const& pt);

   std::auto_ptr<Match_element_battery> get() const {
      return std::auto_ptr<Match_element_battery>(
         new Match_element_battery(vv_, tv_, names_, dep_names_, required_, cv_)
      );
   }

private:
   Vm_access_single const& vma_;
   Input_manager const& in_;
   variant_vector vv_;
   truncate_vector tv_;
   std::vector<std::string> names_;
   std::vector<std::string> dep_names_;
   std::vector<bool> required_;
   combination_vector cv_;
};

/**@brief
*******************************************************************************/
struct Cut_proto {
   static const int unset;

   struct Err : public base_exception{};

   Cut_proto()
   : cut_(false),
     re_start_(true),
     pos_(unset)
   {}

   void parse(bpt::ptree const& pt);

   Relative_position position() const {
      return Relative_position(pos_, re_start_);
   }

   bool cut_;
   bool re_start_;
   int pos_;
};

/**@brief
*******************************************************************************/
void make_element_index(
         std::vector<std::string> const& me_names,
         std::vector<std::string> const& all_me_names,
         std::vector<std::size_t>& mev
);

/**@brief
*******************************************************************************/
void make_column_inds(
         std::string const& line,
         std::vector<std::string> const& col_names,
         std::vector<int>& col_inds
);

/**@brief
@param is
@param col_inds show where to put each column
@param st string table
*******************************************************************************/
VDJ_PIPE_DECL void parse_csv_combination(
         std::istream& is,
         std::vector<int> const& col_inds,
         detail::string_table& st
);

/**@brief
*******************************************************************************/
VDJ_PIPE_DECL void parse_csv_combination(
         bpt::ptree const& pt,
         Input_manager const& in,
         std::vector<std::string> const& me_names,
         std::vector<std::size_t>& mev,
         detail::string_table& st
);

/**@brief
*******************************************************************************/
Match_combination make_combination(
         Value_map const& vm,
         bpt::ptree const& pt,
         Input_manager const& in,
         std::vector<std::string> const& me_names
);

/**@brief
*******************************************************************************/
struct Element_proto {
   static const int unset;

   struct Err : public base_exception{};

   Element_proto(
            bpt::ptree const& pt,
            Input_manager const& in
   );

   Truncate truncation() const {
      return Truncate(
               cut_lo_.cut_,
               cut_up_.cut_,
               cut_lo_.position(),
               cut_up_.position()
      );
   }

   Relative_interval interval() const;
   void parse_position(bpt::ptree const& pt);

   void parse_csv_file(
            bpt::ptree const& pt,
            Input_manager const& in
   );

   //true if start of interval is defined
   bool start_defined_;
   //true if position is defined relative to start of reference interval
   bool re_start_;
   std::string name_;
   std::string value_name_;
   std::string score_name_;
   std::string identity_name_;
   std::string re_name_;
   boost::optional<int> pos_;
   boost::optional<int> len_;
   boost::optional<int> min_score_;
   boost::optional<unsigned> min_match_length_;

   /** interpret min_score_ as maximum number of mismatches */
   bool track_mismatches_;
   bool allow_gaps_;
   bool required_;
   bool ignore_dups_; /**< ignore duplicate sequences for short matches */
   bool require_best_;
   detail::name_seq_vector seqs_;
   boost::optional<detail::Seq_props> sp_;
   Cut_proto cut_lo_;
   Cut_proto cut_up_;
};


}//namespace match
}//namespace vdj_pipe
#endif /* ME_FACTORY_HPP_ */
