/** @file "/vdj_pipe/lib/me_positional.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_POSITIONAL_HPP_
#define ME_POSITIONAL_HPP_
#include <cstdlib> // abs(int)
#include "boost/utility/string_ref.hpp"
#include "vdj_pipe/sequence_record.hpp"
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/sequence_interval.hpp"
#include "me_relative_interval.hpp"

namespace vdj_pipe{ namespace match{

/**@brief Identify DNA sequence interval by position only
*******************************************************************************/
class Match_element_positional {
   //undefined to prevent bool to std::size_t cast
   Match_element_positional(
            Value_map const&,
            std::string const&,
            const std::size_t,
            const std::size_t,
            const std::size_t
   );

public:
   struct Err : public base_exception{};
   typedef boost::string_ref sequence;
   typedef Qual_record::quality quality;

   Match_element_positional(
            Value_map const& vm,
            std::string const& match_value_name,
            Relative_interval const& ri
   )
   : vm_(vm),
     name_val_id_(vm_.insert_new_name(match_value_name)),
     ri_(ri)
   {}

   Match_element_positional(
            Value_map const& vm,
            std::string const& match_value_name,
            const int pos,
            const bool re_start,
            const int length
   )
   : vm_(vm),
     name_val_id_(vm_.insert_new_name(match_value_name)),
     ri_(Relative_position(pos, re_start), length)
   {}

   sequence_interval operator() (
            const sequence seq,
            quality const&,
            sequence_interval const& si
   ) {
      if( ! is_valid(si) ) return result();
      const sequence_interval si1 = ri_(si, seq.size());
      if( ! is_valid(si1) || width(si1) < abs(ri_.length()) ) return result();
      return result(seq.substr(si1.lower(), width(si1)), si1);
   }

   void finish() {}

private:
   Value_map vm_;
   Val_id name_val_id_;
   Relative_interval ri_;

   sequence_interval result(
            const sequence seq = sequence(),
            sequence_interval const& si = sequence_interval_invalid()
   ) {
      if( name_val_id_ && seq.size() ) vm_[name_val_id_] = seq.to_string();
      return si;
   }
};


}//namespace match
}//namespace vdj_pipe
#endif /* ME_POSITIONAL_HPP_ */
