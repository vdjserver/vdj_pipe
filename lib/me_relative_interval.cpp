/** @file "/vdj_pipe/lib/me_relative_interval.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#include "me_relative_interval.hpp"
#include <limits>

namespace vdj_pipe{ namespace match{

/*
*******************************************************************************/
const int Relative_interval::unset = std::numeric_limits<int>::min();

}//namespace match
}//namespace vdj_pipe
