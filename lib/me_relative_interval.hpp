/** @file "/vdj_pipe/lib/me_relative_interval.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_RELATIVE_INTERVAL_HPP_
#define ME_RELATIVE_INTERVAL_HPP_
#include "me_relative_position.hpp"

namespace vdj_pipe{ namespace match{

/**@brief 
*******************************************************************************/
class Relative_interval {
   static const int unset;
public:
   Relative_interval(
            Relative_position const& rp,
            const int length = unset
   )
   : rp_(rp),
     len_(length)
   {}

   sequence_interval operator() (
            sequence_interval const& si,
            const std::size_t seq_len
   ) const {
      if( ! is_valid(si) ) return sequence_interval_invalid();
      const int pos = rp_(si);
      const sequence_interval si2 =
               len_ < 0 ?
                        sequence_interval(pos + len_, pos) :
                        sequence_interval(pos, pos + len_)
                        ;
      if( ! is_valid(si2) ) return sequence_interval_invalid();
      return intersect(si2, sequence_interval(0U, seq_len));
   }

   int length() const {return len_;}
   bool length_defined() const {return len_ != unset;}
   Relative_position const& position() const {return rp_;}

   template<class ChT, class Tr> friend
   std::basic_ostream<ChT,Tr>& operator<<(
         std::basic_ostream<ChT,Tr>& os,
         Relative_interval const& ri
   ) {
      os << ri.rp_ << " len" << ri.len_;
      return os;
   }

private:
   Relative_position rp_;
   int len_; //length is negative if end point is defined
};

}//namespace match
}//namespace vdj_pipe
#endif /* ME_RELATIVE_INTERVAL_HPP_ */
