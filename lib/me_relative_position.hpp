/** @file "/vdj_pipe/lib/me_relative_position.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_RELATIVE_POSITION_HPP_
#define ME_RELATIVE_POSITION_HPP_
#include <iosfwd>
#include "vdj_pipe/sequence_interval.hpp"

namespace vdj_pipe{ namespace match{

/**@brief Sequence position defined relatively to an interval
*******************************************************************************/
class Relative_position {
   //left undefined to avoid implicit bool->int cast
   Relative_position(const bool, const int);
public:
   Relative_position()
   : pos_(std::numeric_limits<int>::min()),
     re_start_(true)
   {}

   /**
    @param pos position relative to an edge of a reference interval
    @param re_start true if the position is relative to reference interval start
    or false if the position is relative to reference interval end
    */
   Relative_position(const int pos, const bool re_start)
   : pos_(pos), re_start_(re_start)
   {}

   int operator() (sequence_interval const& si) const {
      if( is_valid(si) ) {
         const sequence_interval::base_type n =
                  re_start_ ? si.lower() : si.upper();
         return n + pos_;
      }
      return std::numeric_limits<int>::min();
   }

   template<class ChT, class Tr> friend
   std::basic_ostream<ChT,Tr>& operator<<(
         std::basic_ostream<ChT,Tr>& os,
         Relative_position const& rp
   ) {
      os << '@';
      if( rp.re_start_ ) os << "start";
      else os << "end";
      if( rp.pos_ > 0 ) os << '+';
      if( rp.pos_ ) os << rp.pos_;
      return os;
   }

private:
   int pos_;
   bool re_start_;
};

}//namespace match
}//namespace vdj_pipe
#endif /* ME_RELATIVE_POSITION_HPP_ */
