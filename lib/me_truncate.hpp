/** @file "/vdj_pipe/lib/me_truncate.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_TRUNCATE_HPP_
#define ME_TRUNCATE_HPP_
#include "me_relative_position.hpp"

namespace vdj_pipe{ namespace match{

/**@brief
*******************************************************************************/
class Truncate {
public:
   static Truncate lower(const int pos, const bool re_start) {
      return Truncate(
               true, false,
               Relative_position(pos, re_start),
               Relative_position()
      );
   }

   static Truncate upper(const int pos, const bool re_start) {
      return Truncate(
               false, true,
               Relative_position(),
               Relative_position(pos, re_start)
      );
   }

   static Truncate none() {
      return Truncate(
               false, false,
               Relative_position(),
               Relative_position()
      );
   }

   Truncate(
            const bool cut_lo,
            const bool cut_up,
            Relative_position const& lower,
            Relative_position const& upper
   )
   : cut_lo_(cut_lo),
     cut_up_(cut_up),
     lo_(lower),
     up_(upper)
   {}

   sequence_interval operator() (sequence_interval const& si) const {
      if( is_valid(si) ) {
         return sequence_interval(
                  (cut_lo_ ? lo_(si) : 0),
                  (cut_up_ ? up_(si) : sequence_interval::whole().upper())
         );
      }
      return sequence_interval::whole();
   }

private:
   bool cut_lo_;
   bool cut_up_;
   Relative_position lo_;
   Relative_position up_;
};

}//namespace match
}//namespace vdj_pipe
#endif /* ME_TRUNCATE_HPP_ */
