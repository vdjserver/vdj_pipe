/** @file "/vdj_pipe/lib/me_types.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef ME_TYPES_HPP_
#define ME_TYPES_HPP_
#include <vector>
#include <string>
#include <utility>
#include "boost/variant.hpp"
#include "vdj_pipe/sequence_interval.hpp"
#include "sequence_map_types.hpp"

namespace vdj_pipe{ namespace match{
class Match_combination;
class Match_element_aligned;
class Match_element_ambiguous;
class Match_element_positional;
class Match_element_short;
class Truncate;

typedef boost::variant<
            Match_element_aligned,
            Match_element_ambiguous,
            Match_element_positional,
            Match_element_short
         > me_variant;

typedef std::vector<me_variant> variant_vector;
typedef std::vector<Truncate> truncate_vector;
typedef std::vector<Match_combination> combination_vector;
typedef std::vector<sequence_interval> interval_vector;

}// namespace match
}//namespace vdj_pipe
#endif /* ME_TYPES_HPP_ */
