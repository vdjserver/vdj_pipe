/** @file "/vdj_pipe/lib/merge_paired_reads_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/merge_paired_reads_step.hpp"

#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "vdj_pipe/sequence_merge.hpp"
#include "vdj_pipe/sequence_transform.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(
         kwds,
         (min_score)
         (merge_score)
         (merge_sigar)
         (trimmed)
);

/*
*******************************************************************************/
Merge_paired::Merge_paired(
         Vm_access_paired const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: detail::Step_base_paired(vma, pt, pe),
  vis_(vma_, Value_names::merged()),
  score_id_(vma_.insert_new_name(kwds::merge_score())),
  cigar_id_(vma_.insert_new_name(kwds::merge_sigar())),
  n_merged_(0),
  min_score_(0),
  trimmed_(false)
{
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::min_score() ) {
         min_score_ = pt.get<unsigned>(kwds::min_score());

      } else if( vt.first == kwds::trimmed() ) {
         trimmed_ = pt.get<bool>(kwds::trimmed());

      } else {
         BOOST_THROW_EXCEPTION(
                        Err()
                        << Err::msg_t("unexpected keyword")
                        << Err::str1_t(sanitize(vt.first))
         );
      }
   }
}

/*
*******************************************************************************/
void Merge_paired::run() {
   const std::string s1 = vma_.sequence_fwd(trimmed_);
   const std::string s2 = vma_.sequence_rev(trimmed_, true);
   Merge_result mr;
   if( s1.size() && s2.size() ) {
      merge(
               s1,
               vma_.quality_fwd(trimmed_),
               s2,
               vma_.quality_rev(trimmed_, true),
               min_score_,
               mr
      );
   }
   vma_[score_id_] = (long)mr.score_;
   vma_[cigar_id_] = mr.cigar_;
   vma_[vis_.sequence()] = mr.seq_;
   vma_[vis_.quality()] = mr.qual_;
   vma_[vis_.trim()] = sequence_interval(0U, mr.seq_.size());
   if( mr.score_ >= min_score_ ) ++n_merged_;
}

/*
*******************************************************************************/
void Merge_paired::summary(std::ostream& os) const {
   os << "merged: " << n_merged_ << '\n';
}

}//namespace vdj_pipe
