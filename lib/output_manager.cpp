/** @file "/vdj_pipe/lib/output_manager.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/output_manager.hpp"

#include <fstream>
#include "boost/filesystem/operations.hpp"
#include "boost/foreach.hpp"

#include "vdj_pipe/output_stamp.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
Output_manager::path_t Output_manager::make_dir(std::string const& dir) {
   if( dir.empty() ) return dir;
   path_t p(dir);
   create_directories(p);
   return canonical(p);
}

/*
*******************************************************************************/
void Output_manager::finish() const {
   if( plot_list_.empty() ) return;
   std::ofstream os(plot_list_.c_str());
   stamp(os) << "path" << delimiter() << "plot_type" << '\n';
   BOOST_FOREACH(plot_pair const& pp, ptm_) {
      os << pp.first << delimiter();
      switch (pp.second) {
      case plot::bar:
         os << "bar";
         break;
      case plot::bar_n_wisker:
         os << "bar_n_wisker";
         break;
      case plot::line:
         os << "line";
         break;
      case plot::heat_map:
         os << "heat_map";
         break;
      }
      os << '\n';
   }
}

}//namespace vdj_pipe
