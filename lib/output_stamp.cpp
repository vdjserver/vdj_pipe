/** @file "/vdj_pipe/lib/output_stamp.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/output_stamp.hpp"

#include <ostream>
#include <sstream>
#include "boost/date_time/posix_time/posix_time.hpp"

#include "vdj_pipe/lib_info.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
std::ostream& stamp(
         std::ostream& os,
         std::string const& pref,
         std::string const& delim,
         std::string const& suff
) {
  if (Lib_info::stamp_flag()) {
    os
      << pref << Lib_info::name() << ' ' << Lib_info::version() << delim
      << Lib_info::compiler() << delim
      << Lib_info::other_libs() << delim << "build:" << Lib_info::build() << delim
      << boost::posix_time::second_clock::universal_time() << " UTC" << suff
      ;
  }
   return os;
}

/*
*******************************************************************************/
std::string stamp(
         std::string const& pref,
         std::string const& delim,
         std::string const& suff
) {
   std::ostringstream os;
   stamp(os, pref, delim, suff);
   return os.str();
}

}//namespace vdj_pipe
