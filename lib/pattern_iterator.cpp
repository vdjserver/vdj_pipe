/** @file "/vdj_pipe/lib/pattern_iterator.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/gdst/pattern_iterator.hpp"
#include "vdj_pipe/gdst/gdst.hpp"

namespace vdj_pipe{ namespace gdst{

/*
*******************************************************************************/
void Pattern_iter::check_state() {
   next_ = Branch_id();
   at_pattern_end_ = false;
   mismatch_ = false;

   Branch const& b1 = t_[curr_];
   BOOST_ASSERT(b1.n_ <= patt_.length());
   if( (at_end_ = (b1.n_ == patt_.length())) ) return;
   nc_ = nucleotide_index(patt_[b1.n_]);
   if( ! (next_ = t_.child(curr_, nc_)) ) return;
   Branch const& b2 = t_[next_];
   BOOST_ASSERT( b1.n_ < b2.n_ );
   if( (b2.n_ - b1.n_) == 1 ) return;
   const boost::string_ref suff2 = t_.suffix(b2);
   for(pi_ = b1.n_ + 1; pi_ != b2.n_; ++pi_) {
      ec_ = nucleotide_index(suff2[pi_]);
      if( patt_.length() == pi_ ) {
         at_pattern_end_ = true;
         return;
      }
      if( nucleotide_index(patt_[pi_]) != ec_ ) {
         mismatch_ = true;
         return;
      }
   }
}


}//namespace gdst
}//namespace vdj_pipe
