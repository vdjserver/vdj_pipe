/** @file "/vdj_pipe/lib/pipe_environment.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/pipe_environment.hpp"

#include <fstream>
#include <ostream>
#include "boost/chrono/chrono_io.hpp"
#include "boost/chrono/floor.hpp"
#include "boost/chrono/round.hpp"
namespace bc = boost::chrono;
#include "boost/filesystem/path.hpp"
namespace bfs = boost::filesystem;
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
namespace bpt = boost::property_tree;

#include "input_csv.hpp"
#include "keywords_root.hpp"
#include "vdj_pipe/output_stamp.hpp"
#include "vdj_pipe/pipe_paired_emid_read.hpp"
#include "vdj_pipe/pipe_paired_read.hpp"
#include "vdj_pipe/pipe_single_read.hpp"
#include "vdj_pipe/step/step_variant_store.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
void Pipe_environment::input_csv(
            boost::property_tree::ptree const& pt,
            std::vector<std::string>& vs
) {
   if( pt.data().size() ) {
      vs.push_back(pt.data());
      return;
   }

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      input_csv(vt.second, vs);
   }
}

/*
*******************************************************************************/
Pipe_environment::Pipe_environment(bpt::ptree const& pt, Value_map const& vm)
: steps_(new Step_variant_store()),
  im_(),
  om_(),
  max_reads_(unset),
  max_file_reads_(unset),
  summary_output_path_(),
  t1_(),
  t2_(),
  t3_(),
  is_running_(false),
  reads_skipped_(false),
  vm_(vm)
{
   std::string in_base;
   std::string out_base;
   std::string plot_list_path;
   std::string summary_output_path;
   std::string config_output_path;
   std::vector<std::string> in_csvs;
   bpt::ptree const* input_pt = 0;
   char csv_delim = '\t';
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      try{
         if( vt.first == kwds::Root::base_path_input() ) {
            in_base = vt.second.get_value<std::string>();

         } else if( vt.first == kwds::Root::base_path_output() ) {
            out_base = vt.second.get_value<std::string>();

         } else if( vt.first == kwds::Root::config_output_path() ) {
            config_output_path = vt.second.get_value<std::string>();

         } else if( vt.first == kwds::Root::csv_file_delimiter() ) {
            csv_delim = vt.second.get_value<char>();

         } else if( vt.first == kwds::Root::external_MIDs() ) {
            //do nothing

         } else if( vt.first == kwds::Root::max_file_reads() ) {
            max_file_reads_ = vt.second.get_value<std::size_t>();

         } else if( vt.first == kwds::Root::max_reads() ) {
            max_reads_ = vt.second.get_value<std::size_t>();

         } else if( vt.first == kwds::Root::paired_reads() ) {
            //do nothing

         } else if( vt.first == kwds::Root::plots_list_path() ) {
            plot_list_path = vt.second.get_value<std::string>();

         } else if( vt.first == kwds::Root::quality_scores() ) {
            //do nothing

         } else if( vt.first == kwds::Root::summary_output_path() ) {
            summary_output_path = vt.second.get_value<std::string>();

         } else if( vt.first == kwds::Root::input_csv() ) {
            input_csv(vt.second, in_csvs);

         } else if( vt.first == kwds::Root::input() ) {
            input_pt = &vt.second;

         } else if( vt.first == kwds::Root::steps() ) {
            //do nothing

         } else {
            BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("unexpected keyword")
            << Err::str1_t(sanitize(vt.first))
            );
         }
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("option error")
                  << Err::str1_t(sanitize(vt.first))
                  << Err::str2_t(sanitize(vt.second.get_value("")))
                  << Err::nested_t(boost::current_exception())
         );
      }
   }

   bpt::ptree pt2;

   if( input_pt ) {
      pt2.insert(pt2.end(), input_pt->begin(), input_pt->end());
   }

   BOOST_FOREACH(std::string const& csv, in_csvs) {
      const std::string csv_path = Input_manager::path(in_base, csv);
      std::ifstream ifs(csv_path.c_str());
      input_csv_to_tree(ifs, pt2);
   }

//   if( pt2.empty() ) {
//      BOOST_THROW_EXCEPTION(Err() << Err::msg_t("no input found"));
//   }

   im_.reset(new Input_manager(in_base, pt2, vm_));
   om_.reset(new Output_manager(out_base, plot_list_path, csv_delim));

   if( summary_output_path.size() ) {
      summary_output_path_ = output().path(summary_output_path);
   }

   if( config_output_path.size() ) write_config(pt, config_output_path);
}

/*
*******************************************************************************/
void Pipe_environment::start_file(std::string const& fn) {
   if( ! is_running_ ) std::cout << '\n';
   std::cout << fn << " .." << std::flush;
   bfs::path p(fn);
   vm_.seq_file_name(p.stem().string());
}

/*
*******************************************************************************/
void Pipe_environment::finish_file() {
   std::cout  << " " << vm_.file_read_count() << " reads" << std::endl;
   vm_.file_read_count_reset();
}

/*
*******************************************************************************/
void Pipe_environment::process_read() {
   if( ! is_running_ ) {
      t1_ = bc::process_real_cpu_clock::now();
      is_running_ = true;
   }

   try{
      steps().visit(Run_visitor());
   } catch(std::exception const&) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error processing read")
               << Err::str1_t(sanitize(vm_.read_id_nothrow()))
               << Err::nested_t(boost::current_exception())
      );
   }

   vm_.clear_values();
   vm_.read_count_incr();
   vm_.file_read_count_incr();
}

/*
*******************************************************************************/
void Pipe_environment::finish() {
   t2_ = bc::process_real_cpu_clock::now();
   std::cout << "finalizing .." << std::endl;
   steps_->visit(Finish_visitor());
   t3_ = bc::process_real_cpu_clock::now();
   summary();
}

/*
*******************************************************************************/
void Pipe_environment::summary() {
   om_->finish();
   if( summary_output_path_.size() ) {
      std::ofstream os(summary_output_path_.c_str());
      stamp(os);
      Summary_visitor sv(os);
      steps_->visit(sv);
      print_summary(os);
   }
   print_summary(std::cout);
}

/*
*******************************************************************************/
void Pipe_environment::write_config(
         boost::property_tree::ptree const& pt,
         std::string const& path
) const {
   const std::string s = output().path(path);
   std::ofstream os(s.c_str());
//   stamp(os, "/*\n", ", ", "\n*/\n");
   bpt::write_json(os, pt);
}

namespace{

template<class Rep, class Period> std::ostream& print(
         std::ostream& os,
         bc::duration<Rep,Period> d
) {
   const bc::hours h = bc::floor<bc::hours>(d);
   d -= h;
   const bc::minutes m = bc::floor<bc::minutes>(d);
   d -= m;

   bool hm = false;
   if( h.count() ) {
      if( h.count() < 10 ) os << '0';
      os << h.count() << ':';
      hm = true;
   }
   if( hm || m.count() ) {
      if( m.count() < 10 ) os << '0';
      os << m.count() << ':';
      hm = true;
   }

   if( hm ) {
      const bc::seconds::rep s = bc::duration_cast<bc::seconds>(d).count();
      if( s < 10 ) os << '0';
      os << s;
   } else {
      os << bc::duration_cast<bc::duration<double> >(d).count();
   }
   return os << " s";
}

}//anonymous namespace

/*
*******************************************************************************/
std::ostream& Pipe_environment::print_summary(std::ostream& os) const {
   os << '\n';
   if( input().empty() ) {
      os << "no input data was found" << '\n';
      return os;
   }

   os << vm_.read_count() << " sequencing reads processed";
   if( reads_skipped_ ) os << " (some were skipped)";

   typedef bc::duration<double, boost::micro> micro_t;
   const micro_t d1 = bc::duration_cast<micro_t>(t2_ - t1_);

   os << '\n' << "pre-processing time: ";
   print(os, d1) << ", " << d1 / vm_.read_count() << "/read" << '\n';

   const micro_t d2 = bc::duration_cast<micro_t>(t3_ - t1_);
   os << "total processing time: ";
   print(os, d2) << ", " << d2 / vm_.read_count() << "/read" << '\n';

   return os;
}

/*
*******************************************************************************/
std::size_t Pipe_environment::read_count() const {return vm_.read_count();}

}//namespace vdj_pipe
