/** @file "/vdj_pipe/lib/pipe_paired_emid_read.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/pipe_paired_emid_read.hpp"

#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "keywords_root.hpp"
#include "vdj_pipe/parser_fastq.hpp"
#include "vdj_pipe/sequence_file_map.hpp"
#include "vdj_pipe/step/step_factory_paired_emid.hpp"
#include "vdj_pipe/step/step_variant_store.hpp"
#include "vdj_pipe/step/visitor.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
Pipe_paired_emid_read::Pipe_paired_emid_read(bpt::ptree const& pt)
: vma_(),
  pe_(pt, vma_)
{
   BOOST_FOREACH(
            bpt::ptree::value_type const& vt,
            pt.get_child(kwds::Root::steps())
   ) {
      pe_.steps().push_back(create_step_paired_emid(vma_, vt.second, pe_));
   }
}

/*
*******************************************************************************/
void Pipe_paired_emid_read::store_records(
         Parser_fastq& psf,
         Parser_fastq& psr,
         Parser_fastq& pmf,
         Parser_fastq& pmr
) {
   const Seq_qual_record rsf = psf.get_record();
   const Seq_qual_record rsr = psr.get_record();
   const Seq_qual_record rmf = pmf.get_record();
   const Seq_qual_record rmr = pmr.get_record();

   if( rsf.id_ != rsr.id_ || rsf.id_ != rmf.id_ || rsf.id_ != rmr.id_ ) {
      const std::string s =
               rsf.id_ + ' ' + rsr.id_ + ' ' + rmf.id_ + ' ' + rmr.id_;
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("paired eMID reads ID mismatch")
               << Err::str1_t(sanitize(s, 400))
      );
   }
   vma_.read_id(rsf.id_);
   vma_.description_fwd(rsf.comm_);
   vma_.sequence_fwd(rsf.seq_);
   vma_.interval_fwd(sequence_interval(0U, rsf.seq_.size()));
   vma_.quality_fwd(rsf.qual_);
   vma_.sequence_rev(rsr.seq_);
   vma_.interval_rev(sequence_interval(0U, rsr.seq_.size()));
   vma_.quality_rev(rsr.qual_);
   vma_.emid_fwd(rmf.seq_);
   vma_.emid_rev(rmr.seq_);
}

namespace{

struct Four_file_ids {
   Path_id seq_frw_, seq_rev_, mid_frw_, mid_rev_;

   Four_file_ids(const Path_id pid, Seq_file_map const& sfm);

   bool is_complete() const {
      return seq_frw_ && seq_rev_ && mid_frw_ && mid_rev_;
   }

   std::string paths(Seq_file_map const& sfm) const {
      return
               sfm[seq_frw_].path() + ' ' +
               sfm[seq_rev_].path() + ' ' +
               sfm[mid_frw_].path() + ' ' +
               sfm[mid_rev_].path() + ' '
               ;
   }
};

Four_file_ids::Four_file_ids(const Path_id pid, Seq_file_map const& sfm) {
   typedef Pipe_paired_emid_read::Err Err;
   //only use forward paired reads
   if( (seq_rev_ = sfm.frw2rev(pid)) ) seq_frw_ = pid;
   else return;
   Seq_file const& rs = sfm[seq_rev_];
   if( ! (mid_frw_ = sfm.seq2mid(pid)) ) return;

   if( ! (mid_rev_ = sfm.seq2mid(seq_rev_)) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("reverse eMID file not defined")
            << Err::str1_t(sanitize(rs.path()))
   );
   Seq_file const& fs = sfm[seq_frw_];
   if( fs.format() != format::Fastq ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("paired eMID reads should be in FASTQ format")
            << Err::str1_t(sanitize(fs.path()))
   );
   Seq_file const& fm = sfm[mid_frw_];
   Seq_file const& rm = sfm[mid_rev_];
   if(
            fs.format() != rs.format() ||
            fs.format() != fm.format() ||
            fs.format() != rm.format()
   ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("paired eMID reads should be in same format")
            << Err::str1_t(sanitize(fs.path()))
   );
}

}//anonymous namespace

/*
*******************************************************************************/
void Pipe_paired_emid_read::run() {
   BOOST_FOREACH(Seq_file_entry const& sfe, pe_.input()) {
      BOOST_ASSERT(sfe.has_quality());
      BOOST_ASSERT(sfe.has_emid());
      BOOST_ASSERT(sfe.is_paired());

      if( ! pe_.process_more() ) break;

      pe_.start_file(sfe.sequence());
      if( sfe.has_qual_file() ) {
         try{
            process_fasta_qual(sfe);
         } catch(std::exception const&) {
            BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("error processing FASTA/QUAL")
                     << Err::str1_t(
                              sanitize(sfe.sequence()) + " " +
                              sanitize(sfe.quality())
                     )
                     << Err::str2_t(
                              sanitize(sfe.seq_rev()) + " " +
                              sanitize(sfe.qual_rev())
                     )
                     << Err::nested_t(boost::current_exception())
            );
         }
      } else {
         try{
            process_fastq(sfe);
         } catch(std::exception const&) {
            BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("error processing FASTQ")
                     << Err::str1_t(sanitize(sfe.sequence()))
                     << Err::str2_t(sanitize(sfe.seq_rev()))
                     << Err::nested_t(boost::current_exception())
            );
         }
      }
      pe_.finish_file();
   }

   pe_.finish();
}

namespace{

/*
*******************************************************************************/
template<class Parser> bool has_next(
         Parser const& p1, Parser const& p2, Parser const& p3, Parser const& p4
) {
   const bool b = p1.has_next();
   if( b != p2.has_next() || b != p3.has_next() || b != p4.has_next() ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("record number mismatch")
               << base_exception::int1_t(p1.line_num())
      );
   }
   return b;
}

/*
*******************************************************************************/
template<class Parser> bool next_record(
         Parser& p1, Parser& p2, Parser& p3, Parser& p4
) {

   p1.next_record();
   p2.next_record();
   p3.next_record();
   p4.next_record();

   const bool b1 = p1.has_next();
   const bool b2 = p2.has_next();
   const bool b3 = p3.has_next();
   const bool b4 = p4.has_next();
   if( b1 != b2 || b1 != b3 || b1 != b4 ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("record number mismatch")
               << base_exception::int1_t(p1.line_num())
      );
   }
   return b1;
}

}//anonymous namespace

/*
*******************************************************************************/
void Pipe_paired_emid_read::process_fastq(Seq_file_entry const& sfe) {
   BOOST_ASSERT( sfe.is_paired() );
   BOOST_ASSERT( sfe.has_quality() );
   BOOST_ASSERT( ! sfe.has_qual_file() );

   const File_input f_fwd(pe_.input().path(sfe.sequence()));
   const File_input f_rev(pe_.input().path(sfe.seq_rev()));
   const File_input f_mid_fwd(pe_.input().path(sfe.mid()));
   const File_input f_mid_rev(pe_.input().path(sfe.mid_rev()));

   Parser_fastq p_fs(f_fwd);
   Parser_fastq p_rs(f_rev);
   Parser_fastq p_fm(f_mid_fwd);
   Parser_fastq p_rm(f_mid_rev);
   for(; has_next(p_fs, p_rs, p_fm, p_rm); next_record(p_fs, p_rs, p_fm, p_rm)) {

      if( ! pe_.process_more() ) break;

      store_values(sfe, vma_);
      store_records(p_fs, p_rs, p_fm, p_rm);

      try{
         pe_.process_read();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("error processing read")
                  << Err::int1_t(p_fs.line_num())
                  << Err::nested_t(boost::current_exception())
         );
      }
   }
}

/*
*******************************************************************************/
void Pipe_paired_emid_read::process_fasta_qual(Seq_file_entry const& sfe) {
   BOOST_ASSERT( sfe.is_paired() );
   BOOST_ASSERT( sfe.has_quality() );
   BOOST_ASSERT( ! sfe.has_qual_file() );
   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("processing paired FASTA/QUAL currently not supported")
   );
}

}//namespace vdj_pipe
