/** @file "/vdj_pipe/lib/pipe_paired_read.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/pipe_paired_read.hpp"

#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "keywords_root.hpp"
#include "vdj_pipe/parser_fastq.hpp"
#include "vdj_pipe/step/step_factory_paired.hpp"
#include "vdj_pipe/step/step_variant_store.hpp"
#include "vdj_pipe/step/visitor.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
Pipe_paired_read::Pipe_paired_read(bpt::ptree const& pt)
: vma_(),
  pe_(pt, vma_)
{
   BOOST_FOREACH(
            bpt::ptree::value_type const& vt,
            pt.get_child(kwds::Root::steps())
   ) {
      pe_.steps().push_back(create_step_paired(vma_, vt.second, pe_));
   }
}

/*
*******************************************************************************/
void Pipe_paired_read::check() const {
   if( ! pe_.input().is_paired() ) BOOST_THROW_EXCEPTION(
            Err()<< Err::msg_t("paired reads are expected")
   );

/*
   if( pe_.input().has_emid() ) BOOST_THROW_EXCEPTION(
            Err()<< Err::msg_t("eMIDs for single reads are not supported")
   );
*/

   if( ! pe_.input().has_quality() ) BOOST_THROW_EXCEPTION(
            Err()<< Err::msg_t("quality scores are required")
   );
}

/*
*******************************************************************************/
void Pipe_paired_read::run() {
   BOOST_FOREACH(Seq_file_entry const& sfe, pe_.input()) {
      BOOST_ASSERT(sfe.has_quality());
      BOOST_ASSERT(sfe.is_paired());

      if( ! pe_.process_more() ) break;

      pe_.start_file(sfe.sequence());

      if( sfe.has_qual_file() ) {
         try{
            process_fasta_qual(sfe);
         } catch(std::exception const&) {
            BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("error processing FASTA/QUAL")
                     << Err::str1_t(
                              sanitize(sfe.sequence()) + " " +
                              sanitize(sfe.quality())
                     )
                     << Err::str2_t(
                              sanitize(sfe.seq_rev()) + " " +
                              sanitize(sfe.qual_rev())
                     )
                     << Err::nested_t(boost::current_exception())
            );
         }
      } else {
         try{
            process_fastq(sfe);
         } catch(std::exception const&) {
            BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("error processing FASTQ")
                     << Err::str1_t(sanitize(sfe.sequence()))
                     << Err::str2_t(sanitize(sfe.seq_rev()))
                     << Err::nested_t(boost::current_exception())
            );
         }
      }
      pe_.finish_file();
   }

   pe_.finish();
}

namespace{

/*
*******************************************************************************/
template<class Parser> bool has_next(Parser const& p1, Parser const& p2) {
   const bool b = p1.has_next();
   if( b != p2.has_next() ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("record number mismatch")
               << base_exception::int1_t(p1.line_num())
      );
   }
   return b;
}

/*
*******************************************************************************/
template<class Parser> bool next_record(Parser& p1, Parser& p2) {
   p1.next_record();
   p2.next_record();

   const bool b1 = p1.has_next();
   const bool b2 = p2.has_next();
   if( b1 != b2 ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("record number mismatch")
               << base_exception::int1_t(p1.line_num())
      );
   }
   return b1;
}

} //anonymous namespace

/*
*******************************************************************************/
void Pipe_paired_read::process_fastq(Seq_file_entry const& sfe) {
   BOOST_ASSERT( sfe.is_paired() );
   BOOST_ASSERT( sfe.has_quality() );
   BOOST_ASSERT( ! sfe.has_qual_file() );
   const File_input f_fwd(pe_.input().path(sfe.sequence()));
   const File_input f_rev(pe_.input().path(sfe.seq_rev()));

   Parser_fastq p_fwd(f_fwd);
   Parser_fastq p_rev(f_rev);
   for( ; has_next(p_fwd, p_rev); next_record(p_fwd, p_rev)) {

      if( ! pe_.process_more() ) break;

      Seq_qual_record r_fwd;
      try{
         r_fwd = p_fwd.get_record();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("error reading")
                  << Err::str1_t(f_fwd.path())
                  << Err::nested_t(boost::current_exception())
         );
      }

      Seq_qual_record r_rev;
      try{
         r_rev = p_rev.get_record();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("error reading")
                  << Err::str1_t(f_rev.path())
                  << Err::nested_t(boost::current_exception())
         );
      }
      store_values(sfe, vma_);
      store_records(r_fwd, r_rev);
      try{
         pe_.process_read();
      } catch(std::exception const&) {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("error processing read")
                  << Err::int1_t(p_fwd.line_num())
                  << Err::int2_t(p_rev.line_num())
                  << Err::nested_t(boost::current_exception())
         );
      }
   }
}

/*
*******************************************************************************/
void Pipe_paired_read::process_fasta_qual(Seq_file_entry const& sfe) {
   BOOST_ASSERT( sfe.is_paired() );
   BOOST_ASSERT( sfe.has_quality() );
   BOOST_ASSERT( ! sfe.has_qual_file() );
   BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t(
                     "processing paired FASTA/QUAL currently not supported"
            )
   );
}

/*
*******************************************************************************/
void Pipe_paired_read::store_records(
         Seq_qual_record const& r_fwd,
         Seq_qual_record const& r_rev
) {
   if( r_fwd.id_ != r_rev.id_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("paired read ID mismatch")
            << Err::str1_t(sanitize(r_fwd.id_))
            << Err::str2_t(sanitize(r_rev.id_))
   );

   vma_.read_id(r_fwd.id_);
   vma_.description_fwd(r_fwd.comm_);
   vma_.sequence_fwd(r_fwd.seq_);
   vma_.interval_fwd(sequence_interval(0U, r_fwd.seq_.size()));
   vma_.quality_fwd(r_fwd.qual_);
   vma_.description_rev(r_rev.comm_);
   vma_.sequence_rev(r_rev.seq_);
   vma_.interval_rev(sequence_interval(0U, r_rev.seq_.size()));
   vma_.quality_rev(r_rev.qual_);
}

}//namespace vdj_pipe
