/** @file "/vdj_pipe/lib/pipe_single_read.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/pipe_single_read.hpp"

#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"

#include "keywords_root.hpp"
#include "vdj_pipe/parser_fasta.hpp"
#include "vdj_pipe/parser_fastq.hpp"
#include "vdj_pipe/parser_qual.hpp"
#include "vdj_pipe/sequence_file_map.hpp"
#include "vdj_pipe/step/step_factory_single.hpp"
#include "vdj_pipe/step/visitor.hpp"
#include "vdj_pipe/step/step_variant_store.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
Pipe_single_read::Pipe_single_read(boost::property_tree::ptree const& pt)
: vma_(Value_names::single()),
  pe_(pt, vma_)
{
   check();
   BOOST_FOREACH(
            boost::property_tree::ptree::value_type const& vt,
            pt.get_child(kwds::Root::steps())
   ) {
      pe_.steps().push_back(create_step_single(vma_, vt.second, pe_));
   }
}

/*
*******************************************************************************/
void Pipe_single_read::check() const {
   if( pe_.input().is_paired() ) BOOST_THROW_EXCEPTION(
            Err()<< Err::msg_t("single, non-paired reads are expected")
   );

/*
   if( pe_.input().has_emid() ) BOOST_THROW_EXCEPTION(
            Err()<< Err::msg_t("eMIDs for single reads are not supported")
   );
*/

   if( ! pe_.input().has_quality() ) BOOST_THROW_EXCEPTION(
            Err()<< Err::msg_t("quality scores are required")
   );
}

/*
*******************************************************************************/
void Pipe_single_read::run() {
   BOOST_FOREACH(Seq_file_entry const& sfe, pe_.input()) {

      if( ! pe_.process_more() ) break;

      pe_.start_file(sfe.sequence());
      if( sfe.has_qual_file() ) {
         process_fasta_qual(sfe);
      } else {
         process_fastq(sfe);
      }
      pe_.finish_file();
   }

   pe_.finish();
}

/*
*******************************************************************************/
void Pipe_single_read::process_fastq(Seq_file_entry const& sfe) {
   const File_input fis(pe_.input().path(sfe.sequence()));
   Parser_fastq p(fis);
   try{
      for( ; p.has_next(); p.next_record() ) {
         if( ! pe_.process_more() ) break;
         Seq_qual_record r;
         try{
            r = p.get_record();
         } catch(std::exception const&) {
            BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("error reading")
                     << Err::nested_t(boost::current_exception())
            );
         }
         store_values(sfe, vma_);
         vma_.read_id(r.id_);
         vma_.description(r.comm_);
         vma_.sequence(r.seq_);
         vma_.interval(sequence_interval(0U, r.seq_.size()));
         BOOST_ASSERT(
        		 vma_.sequence().size() == (std::size_t)width(vma_.interval())
         );

         vma_.quality(r.qual_);
         BOOST_ASSERT(
        		 vma_.quality().size() == (std::size_t)width(vma_.interval())
         );

         pe_.process_read();
      }
   } catch(std::exception const&) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error processing read")
               << Err::int1_t(p.line_num())
               << Err::nested_t(boost::current_exception())
      );
   }
}

namespace{

/*
*******************************************************************************/
bool has_next(Parser_fasta const& pf, Parser_qual const& pq) {
   const bool b = pf.has_next();
   if( b != pq.has_next() ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("record number mismatch")
               << base_exception::int1_t(pf.line_num())
               << base_exception::int2_t(pq.line_num())
      );
   }
   return b;
}

/*
*******************************************************************************/
bool next_record(Parser_fasta& pf, Parser_qual& pq) {
   pf.next_record();
   pq.next_record();
   const bool b1 = pf.has_next();
   const bool b2 = pq.has_next();
   if( b1 != b2 ) {
      BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("record number mismatch")
               << base_exception::int1_t(pf.line_num())
               << base_exception::int2_t(pq.line_num())
      );
   }
   return b1;
}

}//anonymous namespace

/*
*******************************************************************************/
void Pipe_single_read::process_fasta_qual(Seq_file_entry const& sfe) {
   const File_input fis(pe_.input().path(sfe.sequence()));
   const File_input fiq(pe_.input().path(sfe.quality()));
   Parser_fasta p_seq(fis);
   Parser_qual p_qual(fiq);
   try{
      for( ; has_next(p_seq, p_qual); next_record(p_seq, p_qual) ) {

         if( ! pe_.process_more() ) break;

         store_values(sfe, vma_);
         const Seq_record sr = p_seq.get_record();
         const Qual_record qr = p_qual.get_record();
         if( sr.id_ != sr.id_ ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("sequence ID mismatch")
                  << Err::str1_t(sanitize(sr.id_))
                  << Err::str2_t(sanitize(qr.id_))
         );
         vma_.read_id(sr.id_);
         vma_.description(sr.comm_);
         vma_.sequence(sr.seq_);
         vma_.interval(sequence_interval(0U, sr.seq_.size()));
         BOOST_ASSERT(vma_.sequence().size() == (std::size_t)width(vma_.interval()));
         vma_.quality(qr.qual_);
         BOOST_ASSERT(vma_.quality().size() == (std::size_t)width(vma_.interval()));
         pe_.process_read();
      }
   } catch(std::exception const&) {
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("error processing read")
               << Err::int1_t(p_seq.line_num())
               << Err::int2_t(p_qual.line_num())
               << Err::nested_t(boost::current_exception())
      );
   }
}

}//namespace vdj_pipe
