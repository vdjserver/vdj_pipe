/** @file "/vdj_pipe/lib/print_gdst.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/gdst/print_gdst.hpp"
#include "vdj_pipe/gdst/gdst.hpp"
#include <iostream>

namespace vdj_pipe{ namespace gdst{

/*
*******************************************************************************/
std::ostream& print(std::ostream& os, const Branch_id bid, Gdst const& st) {
   Branch const& b = st[bid];
   os
   << bid << '{'
   << "n:" << b.n_ << ", "
   << "sl:" << b.sl_() << ", "
   << "sb:" << b.sb_() << ", ["
   ;
   if( b.c_ ) {
      os
      << st.child(bid, Adenine)() << ',' << st.child(bid, Cytosine)() << ','
      << st.child(bid, Guanine)() << ',' << st.child(bid, Thymine)()
      ;
   }
   os << ']';
   if( b.leaf_ ) os << st.suffix(st[b.leaf_].front(), b.n_);
   os << '}';
   return os;
}

/*
*******************************************************************************/
std::ostream& operator<<(std::ostream& os, Gdst const& st) {
   for(Depth_iter di = st.depth_first(); ! di.at_end(); di.next() ) {
      print(os, di.id(), st);
      os << '\n';
   }
   return os;
}

namespace{

/*
*******************************************************************************/
void dot_edge(
         std::ostream& os,
         const Branch_id bid,
         Gdst const& st,
         const Nucleotide n,
         const unsigned nmax
) {
   if( const Branch_id c = st.child(bid, n) ) {
      os
      << bid << " -> " << c << " [weight=" << nmax + st[bid].n_ - st[c].n_ << ", "
      << "taillabel=\"" << to_capital(n) << "\"];\n";
   }
}

}//anonymous namespace

/*
*******************************************************************************/
std::ostream& dot(std::ostream& os, Gdst const& st, const bool sl) {
   os << "/* dot -O -Tpdf graph */" << '\n';
   os << "digraph ST {" << '\n' << "node [shape=box];" << '\n';
   os << "nodesep=1.0; ranksep=1.2;" << '\n';

   unsigned nmax = 0;
   for(Depth_iter di = st.depth_first(); ! di.at_end(); di.next() ) {
      Branch const& b = st[di.id()];
      if( b.n_ > nmax ) nmax = b.n_;
      os << di.id() << " [label=\"" << 'n' << di.id()() << ' ' << b.n_ << ' ' << b.sl_();
      if( b.leaf_ ) os << "\\n" << st.suffix(b);
      os << "\"];\n";
   }

   os << '\n' << "edge [dir=none];" << '\n';

   for(Depth_iter di = st.depth_first(); ! di.at_end(); di.next() ) {
      dot_edge(os, di.id(), st, Adenine, nmax);
      dot_edge(os, di.id(), st, Cytosine, nmax);
      dot_edge(os, di.id(), st, Guanine, nmax);
      dot_edge(os, di.id(), st, Thymine, nmax);
   }

   if( sl ) {
      os
      << '\n' <<
      "edge [dir=forward, penwidth=0.5, constraint=false, "
      "arrowsize=0.5, color=blue];" << '\n';

      for(Depth_iter di = st.depth_first(); ! di.at_end(); di.next() ) {
         Branch const& b = st[di.id()];
         if( b.sl_ ) os << di.id() << " -> " << b.sl_ << ";\n";
      }
   }

   os << '}' << '\n';
   return os;
}


}//namespace gdst
}//namespace vdj_pipe
