/** @file "/vdj_pipe/lib/process_options.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/process_options.hpp"

#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "keywords_root.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/pipe_paired_emid_read.hpp"
#include "vdj_pipe/pipe_paired_read.hpp"
#include "vdj_pipe/pipe_single_read.hpp"

namespace vdj_pipe{

/*
*******************************************************************************/
void process_options(bpt::ptree const& pt) {
   enum { Paired = 1, Quality = 2, Emids = 4 };
   unsigned opts = 0;

   if( pt.get<bool>(kwds::Root::paired_reads(), false) ) opts |= Paired;
   if( pt.get<bool>(kwds::Root::quality_scores(), true) ) opts |= Quality;
   if( pt.get(kwds::Root::external_MIDs(), false) ) opts |= Emids;

   typedef base_exception Err;

   if( ! (opts & Quality) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("quality scores are currently required")
            << Err::str1_t(kwds::Root::quality_scores())
   );

   switch (opts) {
   case Quality:
      process_single_reads(pt);
      break;
   case Quality | Paired:
      process_paired_reads(pt);
      break;
   case Quality | Paired | Emids:
      process_paired_emid_reads(pt);
      break;
   default:
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("unsupported options")
      );
   }
}

/*
*******************************************************************************/
void process_single_reads(bpt::ptree const& pt) {
   Pipe_single_read psr(pt);
   psr.run();
}

/*
*******************************************************************************/
void process_paired_reads(bpt::ptree const& pt) {
      Pipe_paired_read ppr(pt);
      ppr.run();
}

/*
*******************************************************************************/
void process_paired_emid_reads(bpt::ptree const& pt) {
      Pipe_paired_emid_read pper(pt);
      pper.run();
}


}//namespace vdj_pipe
