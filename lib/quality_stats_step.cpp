/** @file "/vdj_pipe/lib/quality_stats_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013-4
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/quality_stats_step.hpp"

#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/output_stamp.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{ namespace {

/*
*******************************************************************************/
VDJ_PIPE_KEYWORD_STRUCT(
         kwds,
         (out_prefix)(out_path_hm)(out_path_stats)(out_path_mq_hist)
         (out_path_len_hist)
);

}//anonymous namespace

/*
*******************************************************************************/
Qual_stats::Qual_stats(
         Vm_access_single const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: detail::Step_base_single(vma, pt, pe),
  hm_(),
  out_hm_(),
  out_qs_(),
  out_mqh_(),
  out_len_hist_()
{
   std::string pref;
   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      if( vt.first == kwds::out_prefix() ) {
         pref = vt.second.get_value<std::string>();

      } else if( vt.first == kwds::out_path_hm() ) {
         out_hm_ = pe.output().path(vt.second.data(), plot::heat_map);

      } else if( vt.first == kwds::out_path_stats() ) {
         out_qs_ = pe.output().path(vt.second.data(), plot::bar_n_wisker);

      } else if( vt.first == kwds::out_path_mq_hist() ) {
         out_mqh_ = pe.output().path(vt.second.data(), plot::bar);

      } else if( vt.first == kwds::out_path_len_hist() ) {
         out_len_hist_ = pe.output().path(vt.second.data(), plot::bar);

      } else {
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("unexpected keyword")
                  << Err::str1_t(sanitize(vt.first))
         );
      }
   }

   if( out_hm_.empty() ) {
      out_hm_ = pe.output().path(pref + "heat_map.csv", plot::heat_map);
   }

   if( out_qs_.empty() ) {
      out_qs_ = pe.output().path(pref + "qstats.csv", plot::bar_n_wisker);
   }

   if( out_mqh_.empty() ) {
      out_mqh_ = pe.output().path(pref + "mean_q_hist.csv", plot::bar);
   }

   if( out_len_hist_.empty() ) {
      out_len_hist_ = pe.output().path(pref + "len_hist.csv", plot::bar);
   }
}

/*
*******************************************************************************/
void Qual_stats::run() {
   const sequence_interval si = vma_.interval();
   if( ! width(si) ) return;
   qual_type const& q = vma_.quality();
   lh_.add(width(si));
   if( (std::size_t)width(si) > hm_.shape()[0] ) {
      boost::array<std::size_t,2> s = {{q.size(), hm_.shape()[1]}};
      hm_.resize(s);
   }
   double mq = 0;
   for(int i = 0, j = si.lower(); j != si.upper(); ++i, ++j) {
      if( q[j] >= hm_.shape()[1] ) {
         const boost::array<std::size_t,2> s = {{hm_.shape()[0], (std::size_t)q[j]+1}};
         hm_.resize(s);
      }
      ++hm_[i][q[j]];
      mq += q[j];
   }
   mq /= width(si);
   mqh_.add((unsigned)(mq + 0.5));
}

/*
*******************************************************************************/
void Qual_stats::finish() {
   write_heat_map();
   write_qstats();
   write_meanq_hist();
   write_len_hist();
}

/*
*******************************************************************************/
void Qual_stats::write_heat_map() const {
   File_ostream fos(File_output(out_hm_, compression::unknown, format::CSV));
   std::ostream& ofs = fos.ostream();
   stamp(ofs);

   ofs << delimiter();
   for(std::size_t i = 0; i != hm_.shape()[1]; ++i) {
      ofs << i << delimiter();
   }
   ofs << '\n';
   for(std::size_t i = 0; i != hm_.shape()[0]; ++i) {
      ofs << i + 1 << delimiter();
      for(std::size_t j = 0; j != hm_.shape()[1]; ++j) {
         ofs << hm_[i][j] << delimiter();
      }
      ofs << '\n';
   }
}

/*
*******************************************************************************/
void Qual_stats::write_qstats() const {
   File_ostream fos(
            File_output(out_qs_, compression::unknown, format::CSV)
   );

   std::ostream& ofs = fos.ostream();

   stamp(ofs) << "position" << delimiter() << "mean";
   BOOST_FOREACH(const double f, fracts()) {
      ofs << delimiter() << (int)(f * 100) << '%';
   }
   ofs << '\n';

   for(std::size_t i = 0; i != hm_.shape()[0]; ++i) {
      ofs << i;
      std::size_t count = 0;
      double mean = 0;
      for(std::size_t j = 0; j != hm_.shape()[1]; ++j) {
         count += hm_[i][j];
         mean += hm_[i][j] * j;
      }
      if( ! count ) continue;
      ofs << delimiter() << mean / count;
      boost::array<std::size_t,fractions::static_size> fc;
      for(std::size_t n = 0; n != fc.size(); ++n) fc[n] = fracts()[n] * count;
      for(std::size_t c = 0, n = 0, j = 0; j != hm_.shape()[1]; ++j) {
         c += hm_[i][j];
         for( ; n != fc.size() && c >= fc[n]; ++n ) {
            ofs << delimiter() << j;
         }
         if( n == fc.size() ) break;
      }
      ofs << '\n';
   }
}

/*
*******************************************************************************/
void Qual_stats::write_meanq_hist() const {
   File_ostream fos(
            File_output(out_mqh_, compression::unknown, format::CSV)
   );
   std::ostream& ofs = fos.ostream();

   stamp(ofs) << "read_quality" << delimiter() << "count" << '\n';

   mqh_.print(ofs, delimiter());
}

/*
*******************************************************************************/
void Qual_stats::write_len_hist() const {
   File_ostream fos(
            File_output(out_len_hist_, compression::unknown, format::CSV)
   );

   std::ostream& ofs = fos.ostream();

   stamp(ofs)
   << "read_length" << delimiter() << "count" << '\n';

   lh_.print(ofs, delimiter());
}

}//namespace vdj_pipe
