/** @file "/vdj_pipe/lib/sequence_file_entry.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/sequence_file_entry.hpp"

#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "keywords_variable.hpp"
#include "vdj_pipe/file.hpp"
#include "vdj_pipe/input_manager.hpp"
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/file.hpp"

namespace vdj_pipe{

/*
*******************************************************************************/
Seq_file_entry::Seq_file_entry(
         map_t const& m,
         Input_manager const& im,
         Value_map const& vm
)
: qt_(none),
  seq_(),
  qual_(),
  seq_rev_(),
  qual_rev_(),
  mid_(),
  mid_rev_(),
  map_(m)
{
   BOOST_FOREACH(map_t::value_type const& vt, map_) {
      if( vm.name(vt.first) == kwds::Single::seq_file_path() ) {
         seq_ = vt.first;

      } else if( vm.name(vt.first) == kwds::Forward::seq_file_path_fwd() ) {
         seq_ = vt.first;

      } else if( vm.name(vt.first) == kwds::Single::qual_file_path() ) {
        qual_  = vt.first;

      } else if( vm.name(vt.first) == kwds::Forward::qual_file_path_fwd() ) {
         qual_ = vt.first;

      } else if( vm.name(vt.first) == kwds::Reverse::seq_file_path_rev() ) {
         seq_rev_ = vt.first;

      } else if( vm.name(vt.first) == kwds::Reverse::qual_file_path_rev() ) {
         qual_rev_ = vt.first;

      } else if( vm.name(vt.first) == kwds::Emid::emid_file_path_fwd() ) {
         mid_ = vt.first;

      } else if( vm.name(vt.first) == kwds::Emid::emid_file_path_rev() ) {
         mid_rev_ = vt.first;

      }
   }

   check(im);
}

/*
*******************************************************************************/
void Seq_file_entry::check(Input_manager const& im) {

   if( ! seq_ ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no sequence entries found")
   );

   const std::string sf = im.path(sequence());
   if( ! is_path_readable(sf) ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("sequence file not readable")
            << Err::str1_t(sanitize(sf))
   );

   const format::Format ff = guess_compression_format(sf).second;
   if( qual_ ) {
      qt_ = qual;
   } else if( ff == format::Fastq ) {
      qt_ = fastq;
   }

   if( ! is_paired() ) {
      if( ! qual_rev_ ) return;
      BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("reverse quality provided without sequence")
               << Err::str1_t(sanitize(qual_rev()))
      );
   }

   const std::string sr = im.path(seq_rev());
   if(
            ff != guess_compression_format(sr).second ||
            (bool)qual_ != (bool)qual_rev_
   ) BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("format mismatch for forward/reverse sequence/quality")
            << Err::str1_t(sanitize(sf))
            << Err::str2_t(sanitize(sr))
   );

}

/*
*******************************************************************************/
void store_values(Seq_file_entry const& sfe, Value_map& vm) {
   BOOST_FOREACH(Seq_file_entry::value_type const& vt, sfe) {
      BOOST_ASSERT(vt.first);
      vm[vt.first] = vt.second;
   }
}

}//namespace vdj_pipe
