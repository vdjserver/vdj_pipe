/** @file "/vdj_pipe/lib/sequence_map_parse.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef SEQUENCE_MAP_PARSE_HPP_
#define SEQUENCE_MAP_PARSE_HPP_
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#include "boost/assert.hpp"
#include "boost/foreach.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/tokenizer.hpp"
namespace bpt = boost::property_tree;

#include "sequence_map_types.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/input_manager.hpp"
#include "vdj_pipe/nucleotide_index.hpp"
#include "vdj_pipe/parser_fasta.hpp"

namespace vdj_pipe{ namespace detail{

/**
*******************************************************************************/
inline void parse_sequences(bpt::ptree const& pt, name_seq_vector& nsv) {
   std::string const& seq = pt.data();
   if( seq.size() ) {
      nsv.push_back(match_seq("", seq));
      return;
   }

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      parse_sequences(vt.second, nsv);
   }
}

/**
*******************************************************************************/
inline void parse_combinations(
         bpt::ptree const& pt,
         Input_manager const& in,
         string_table& st
) {
   std::string const& fn = pt.data();
   if( fn.size() ) {
      const std::string in_path = in.path(fn);
      std::ifstream ifs(in_path.c_str());

      typedef base_exception Err;
      if( ! ifs.good() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("cannot read")
               << Err::str1_t(sanitize(in_path))
      );

      std::string line_str;
      for( int line_n = 1; getline(ifs, line_str); ++line_n ) {
         if( line_str.empty() || line_str[0] == '#' ) continue;
         typedef boost::tokenizer<> tokenizer;
         tokenizer tok(line_str);
         string_vector sv;
         std::size_t n = 0;
         BOOST_FOREACH(std::string const& s, tok) {
            sv.push_back(s);
         }
         if( sv.empty() ) continue;
         if( n == 0 ) n = sv.size();
         else {
            if( n != sv.size() ) BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("mismatched number of columns")
                     << Err::str1_t(sanitize(in_path))
                     << Err::int1_t(sv.size())
                     << Err::int2_t(n)
                     << Err::line_t(line_n)
            );
         }
         st.push_back(sv);
      }
      return;
   }

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      parse_combinations(vt.second, in, st);
   }
}

/**
*******************************************************************************/
inline void parse_seq_files(
         bpt::ptree const& pt,
         Input_manager const& in,
         name_seq_vector& nsv
) {
   std::string const& fn = pt.data();
   if( fn.size() ) {
      const std::string in_path = in.path(fn);
      for(Parser_fasta pf((File_input(in_path))); pf.has_next(); pf.next_record()) {
         const Seq_record sr = pf.get_record();
         nsv.push_back(match_seq(sr.id_, sr.seq_));
      }
      return;
   }

   BOOST_FOREACH(bpt::ptree::value_type const& vt, pt) {
      parse_seq_files(vt.second, in, nsv);
   }
}

/**@brief
*******************************************************************************/
inline name_seq_vector combine(
         name_seq_vector const& nsv,
         string_table const& st
) {
   name_seq_vector sv2;

   typedef std::map<std::string, std::size_t> map_t;
   map_t m;
   for( std::size_t n = 0; n != nsv.size(); ++n) {
      m.insert(std::make_pair(nsv[n].first, n));
   }

   typedef base_exception Err;
   BOOST_FOREACH(string_vector const& sv, st) {
      BOOST_ASSERT(sv.size() > 2);
      std::ostringstream os;
      for( std::size_t n = 1; n != sv.size(); ++n ) {
         std::string const& name = sv[n];
         map_t::const_iterator i = m.find(name);
         if( i == m.end() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("sequence name not found")
                  << Err::str1_t(sanitize(name))
         );
         os << nsv[i->second].second;
      }
      sv2.push_back(std::make_pair(sv[0], os.str()));
   }
   return sv2;
}

}//namespace detail
}//namespace vdj_pipe
#endif /* SEQUENCE_MAP_PARSE_HPP_ */
