/** @file "/vdj_pipe/lib/sequence_map_short.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef SEQUENCE_MAP_SHORT_HPP_
#define SEQUENCE_MAP_SHORT_HPP_
#include "boost/utility/string_ref.hpp"
#include "boost/foreach.hpp"

#include "vdj_pipe/best_match_pair.hpp"
#include "vdj_pipe/detail/id_bimap.hpp"
#include "vdj_pipe/detail/id_iterator.hpp"
#include "vdj_pipe/detail/id_map.hpp"
#include "vdj_pipe/detail/unused_variable.hpp"
#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/object_ids.hpp"
#include "vdj_pipe/sequence_fls.hpp"

namespace vdj_pipe{

/**@brief 
*******************************************************************************/
class Seq_map_short {
public:
   typedef Seq_fls<4, boost::uint_fast32_t> seq_type;
   typedef Best_match_pair<Mid_id, int> match_type;

private:
   typedef detail::Id_bimap<Mid_id, seq_type> seq_map;
   typedef detail::Id_map<Mid_id, std::string> name_map;

public:
   typedef Id_iterator<Mid_id> iterator;
   typedef iterator const_iterator;
   struct Err : public base_exception{};

   explicit Seq_map_short(
      const bool allow_duplicate_seq
   )
   : mid_size_(0),
     seqs_(Mid_id(1)),
     names_(Mid_id(1)),
     allow_duplicate_seq_(allow_duplicate_seq)
   {}

   iterator begin() const {return iterator(seqs_.min_id());}
   iterator end() const {return ++iterator(seqs_.max_id());}
   seq_type seq(const Mid_id id) const {return seqs_[id];}
   std::string seq_string(const Mid_id id) const {return seqs_[id].to_string(mid_size_);}
   std::string const& name(const Mid_id id) const {return names_[id];}
   std::size_t seq_size() const {return mid_size_;}
   Mid_id max_id() const {return seqs_.max_id();}
   Mid_id const* find_exact(const seq_type seq) const {return seqs_.find(seq);}

   Mid_id const* find_exact(const boost::string_ref seq) const {
      seq_type seq1;
      if( assign(seq1, seq) ) return find_exact(seq1);
      return 0;
   }

   template<class Seq> bool find_closest(
            const Seq s,
            match_type& m,
            scoring_matrix_t const& sm
   ) const {
      if( Mid_id const* id = find_exact(s) ) {
         return m.combine(*id, identity(Adenine, Adenine, sm) * seq_size());
      }

      bool found = false;
      BOOST_FOREACH(const Mid_id id, *this) {
         const int n = identity(seq(id), s, sm, seq_size());
         if( m.combine(id, n) ) found = true;
      }
      return found;
   }

   Mid_id insert(std::string name, std::string const& seq) {
      if( name.empty() ) name = seq;
      if( ! mid_size_ ) {
         if( seq_type::length() < seq.size() ) BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("match element sequence too long")
                  << Err::str1_t(sanitize(seq))
                  << Err::int1_t(seq_type::length())
         );
         mid_size_ = seq.size();
      }

      if( ! seq.size() || seq.size() != mid_size_ ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("wrong match element sequence length")
               << Err::str1_t(sanitize(seq))
               << Err::str2_t(sanitize(name))
               << Err::int1_t(mid_size_)
               << Err::int2_t(seq.size())
      );
      BOOST_ASSERT(seqs_.size() == names_.size());
      const seq_type s(seq);
      if( Mid_id const* id = seqs_.find(s) ) {
         if( allow_duplicate_seq_ ) {
            if( names_[*id] != name ) BOOST_THROW_EXCEPTION(
                     Err()
                     << Err::msg_t("same sequence, different name")
                     << Err::str1_t(sanitize(seq))
                     << Err::str2_t(sanitize(names_[*id]))
                     << Err::str3_t(sanitize(name))
            );
            return *id;
         }
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("duplicate match element sequence")
                  << Err::str1_t(sanitize(seq))
                  << Err::str2_t(sanitize(name))
         );
      }

      const std::pair<Mid_id,bool> p1 = seqs_.insert(s);
      const Mid_id id = names_.insert(name);
      unused_variable(id);
      BOOST_ASSERT(id);
      BOOST_ASSERT(p1.first == id);
      return p1.first;
   }

private:
   std::size_t mid_size_;
   seq_map seqs_;
   name_map names_;
   bool allow_duplicate_seq_;
};

}//namespace vdj_pipe
#endif /* SEQUENCE_MAP_SHORT_HPP_ */
