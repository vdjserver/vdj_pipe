/** @file "/vdj_pipe/lib/sequence_map_types.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef SEQUENCE_MAP_TYPES_HPP_
#define SEQUENCE_MAP_TYPES_HPP_

namespace vdj_pipe{ namespace detail{

/**@brief 
*******************************************************************************/
typedef std::pair<std::string,std::string> match_seq;
typedef std::vector<match_seq> name_seq_vector;
typedef std::vector<std::string> string_vector;
typedef std::vector<string_vector> string_table;

}//namespace detail
}//namespace vdj_pipe
#endif /* SEQUENCE_MAP_TYPES_HPP_ */
