/** @file "/vdj_pipe/lib/sequence_merge.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/sequence_merge.hpp"
#include "external/ssw_cpp.h"
#include "boost/foreach.hpp"
#include "boost/assert.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
void merge(
         std::string const& s1,
         Seq_qual_record::quality const& qv1,
         std::string const& s2,
         Seq_qual_record::quality const& qv2,
         const unsigned min_score,
         Merge_result& mr
) {
   BOOST_ASSERT(s1.size() && "empty string" );
   BOOST_ASSERT(s2.size() && "empty string" );
   BOOST_ASSERT(s1.size() == qv1.size() );
   BOOST_ASSERT(s2.size() == qv2.size() );
   StripedSmithWaterman::Aligner aligner;
   StripedSmithWaterman::Filter filter;
   StripedSmithWaterman::Alignment al;
   aligner.Align(s1.data(), s2.data(), s2.size(), filter, &al);

   mr.score_ = al.sw_score;
   mr.cigar_ = al.cigar_string;
   mr.seq_.clear();
   mr.qual_.clear();

   if(
            al.sw_score < min_score ||
            al.ref_begin < 0 ||
            al.ref_end < 0 ||
            al.query_begin < 0 ||
            al.query_end < 0
   ) return;

   const unsigned s10 = al.query_begin;
   const unsigned s20 = al.ref_begin;
   const unsigned max_len =
            s10 + s20 + std::max(al.query_end - s10 + 1, al.ref_end - s20 + 1);

   mr.seq_.reserve(max_len);
   mr.qual_.reserve(max_len);
   mr.seq_.append(s1.begin(), s1.begin() + s10);
   mr.qual_.insert(mr.qual_.end(), qv1.begin(), qv1.begin() + s10);

   unsigned n1 = s10, n2 = s20;
   static const uint32_t MASK = ~(~0U << 4U);
   BOOST_FOREACH(const uint32_t x, al.cigar) {
      const unsigned n = x >> 4U;
      switch (x & MASK) {
         case 0: //M, match
         case 7: //=, match
         case 8: //X, mismatch
            for(unsigned i = 0; i != n; ++i, ++n1, ++n2) {
               const char c1 = s1[n1];
               const char c2 = s2[n2];
               if( c1 == c2 ) {
                  mr.seq_.push_back(c1);
                  mr.qual_.push_back(std::max(qv1[n1], qv2[n2]));
               } else if( qv1[n1] < qv2[n2] ) {
                  mr.seq_.push_back(c2);
                  mr.qual_.push_back(qv2[n2]);
               } else {
                  mr.seq_.push_back(c1);
                  mr.qual_.push_back(qv1[n1]);
               }
            }
            break;
         case 1: //I, insertion in s1
            {
               double q1 = 0, q2 = 0;
               for(unsigned i = 0; i != n; ++i) q1 += qv1[n1 + i];
               q1 /= n;
               q2 = (qv2[n2] + qv2[n2 + 1]) * 0.5;
               if( q1 > q2 ) {
                  mr.seq_.append(s1.begin() + n1, s1.begin() + n1 + n);
                  mr.qual_.insert(mr.qual_.end(), qv1.begin() + n1, qv1.begin() + n1 + n);
               }
               n1 += n;
            }
            break;
         case 2: //D, insertion in s2
            {
               double q1 = 0, q2 = 0;
               for(unsigned i = 0; i != n; ++i) q2 += qv2[n2 + i];
               q2 /= n;
               q1 = (qv1[n1] + qv1[n1 + 1]) * 0.5;
               if( q2 > q1 ) {
                  mr.seq_.append(s2.begin() + n2, s2.begin() + n2 + n);
                  mr.qual_.insert(mr.qual_.end(), qv2.begin() + n2, qv2.begin() + n2 + n);
               }
               n2 += n;
            }

            break;
         case 4: //S, present in s1
            break;
         default:
            break;
      }
   }

   mr.seq_.append(s2.begin() + al.ref_end + 1, s2.end());
   mr.qual_.insert(mr.qual_.end(), qv2.begin() + al.ref_end + 1, qv2.end());
}

}//namespace vdj_pipe
