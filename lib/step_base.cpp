/** @file "/vdj_pipe/lib/step_base.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/step_base.hpp"

#include "boost/property_tree/ptree.hpp"

#include "vdj_pipe/pipe_environment.hpp"

namespace vdj_pipe{ namespace detail{

/*
*******************************************************************************/
Step_base::Step_base(
         boost::property_tree::ptree const& pt,
         Pipe_environment const& pe
)
: delimiter_(pe.delimiter())
{}

}//namespace detail
}//namespace vdj_pipe
