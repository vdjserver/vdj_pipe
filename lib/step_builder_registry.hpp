/** @file "/vdj_pipe/lib/step_builder_registry.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_BUILDER_REGISTRY_HPP_
#define STEP_BUILDER_REGISTRY_HPP_
#include <map>
#include <sstream>
#include "boost/function.hpp"
#include "boost/mpl/bind.hpp"
#include "boost/mpl/fold.hpp"
#include "boost/mpl/for_each.hpp"
#include "boost/mpl/insert.hpp"
#include "boost/mpl/quote.hpp"
#include "boost/mpl/set.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/ptree.hpp"

#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/step/all_steps.hpp"
#include "vdj_pipe/step/step_maker.hpp"

namespace vdj_pipe{
class Pipe_environment;

namespace step{

/**@brief
*******************************************************************************/
template<class Config> class Step_builder_registry {
   typedef typename Config::processing_step step_variant;
   typedef typename Config::value_map_access vm_access;

   typedef boost::function<
            step_variant(
                     vm_access const&,
                     boost::property_tree::ptree const&,
                     Pipe_environment&
                     )
            >
   constructor_t;

   typedef std::map<std::string, constructor_t> constructor_map_t;

   class Insert {
   public:
      explicit Insert(constructor_map_t& constr_map)
      : cm_(&constr_map)
      {}

      template<class M> static step_variant make(
               vm_access const& vma,
               boost::property_tree::ptree const& pt,
               Pipe_environment& pe
      ) {
         return M::make(vma, pt, pe);
      }

      template<class M> void operator()(M const&) const {
         const typename constructor_map_t::value_type p(
                  M::name(),
                  &make<M>
         );

         if( ! cm_->insert(p).second ) {
            BOOST_THROW_EXCEPTION(
                     base_exception()
                     << base_exception::msg_t("duplicate step name")
                     << base_exception::str1_t(sanitize(M::name()))
            );
         }
      }

   private:
      mutable constructor_map_t* cm_;
   };

   Step_builder_registry() {
      //make metafunction class
      typedef boost::mpl::quote1<Find_maker> maker;

      //use "bind" to force placeholder evaluation
      typedef typename boost::mpl::fold<
               typename step_variant::types,
               boost::mpl::set0<>,
               boost::mpl::insert<
                  boost::mpl::_1,
                  boost::mpl::bind1<maker, boost::mpl::_2>
               >
      >::type maker_tl;

      Insert ins(constr_map_);
      boost::mpl::for_each<maker_tl>(ins);
   }

   step_variant make_impl(
            std::string const& name,
            vm_access const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   ) const
   {
      typename constructor_map_t::const_iterator i = constr_map_.find(name);
      if( i == constr_map_.end() ) BOOST_THROW_EXCEPTION(
               base_exception()
               << base_exception::msg_t("unsupported step")
               << base_exception::str1_t(sanitize(name))
      );
      return i->second(vma, pt, pe);
   }

public:

   static Step_builder_registry const& get() {
      static const Step_builder_registry inst;
      return inst;
   }

   static step_variant make(
            std::string const& name,
            vm_access const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   ) {
      return get().make_impl(name, vma, pt, pe);
   }

   static step_variant make(
            vm_access const& vma,
            boost::property_tree::ptree const& pt,
            Pipe_environment& pe
   ) {
      if( pt.size() ) {
         try{
            if( pt.size() > 1 ) BOOST_THROW_EXCEPTION(
                     base_exception()
                     << base_exception::msg_t("multiple children found")
            );
            std::string const& name = pt.front().first;
            return get().make_impl(name, vma, pt.front().second, pe);
         } catch(boost::exception const&) {
            std::stringstream ss;
            write_json(ss, pt);
            BOOST_THROW_EXCEPTION(
                     base_exception()
                     << base_exception::msg_t("error creating step")
                     << base_exception::str1_t(ss.str())
                     << base_exception::nested_t(boost::current_exception())
            );
         }
      }

      return Blank_step();
   }

private:
   constructor_map_t constr_map_;
};

}//namespace step
}//namespace vdj_pipe
#endif /* STEP_BUILDER_REGISTRY_HPP_ */
