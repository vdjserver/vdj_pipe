/** @file "/vdj_pipe/lib/step_factory_paired.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/step_factory_paired.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "step_builder_registry.hpp"

namespace vdj_pipe {

/*
*******************************************************************************/
step_variant_paired create_step_paired(
         Vm_access_paired const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
) {
   typedef step::Step_builder_registry<Config_paired_reads> registry;
   return registry::make(vma, pt, pe);
}

}//namespace vdj_pipe
