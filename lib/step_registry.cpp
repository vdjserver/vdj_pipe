/** @file "/vdj_pipe/lib/step_registry.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#include "step_registry.hpp"

#include "boost/mpl/for_each.hpp"
#include "boost/mpl/quote.hpp"

#include "vdj_pipe/step/step_variant_single_read.hpp"
#include "vdj_pipe/step/step_variant_paired_read.hpp"
#include "vdj_pipe/step/step_variant_paired_emid_read.hpp"
#include "vdj_pipe/step/all_steps.hpp"

namespace vdj_pipe{


template<class Step> struct Wrap {
   typedef Step step;
};

namespace detail{

class Inserter {
public:
   explicit Inserter(
            Step_registry::mmap_t& s_descr_map,
            Step_registry::cats_t& cats
   )
   : mm_(&s_descr_map), cats_(&cats)
     {}

   template<class WStep> void operator()(WStep const&) const {
      Step_registry::Meta sm(
               WStep::step::name(),
               WStep::step::category(),
               WStep::step::comment(),
               WStep::step::description()
      );
      mm_->insert(sm);
      cats_->insert(WStep::step::category());
   }

private:
   mutable Step_registry::mmap_t* mm_;
   mutable Step_registry::cats_t* cats_;
};

}//namespace detail

/*
*******************************************************************************/
Step_registry::Step_registry() {
   detail::Inserter ins(mm_, cats_);
   boost::mpl::for_each<
      step::single_read_vector, boost::mpl::quote1<Wrap>
   >(ins);
   boost::mpl::for_each<
      step::paired_read_vector, boost::mpl::quote1<Wrap>
   >(ins);
   boost::mpl::for_each<
      step::paired_emid_read_vector, boost::mpl::quote1<Wrap>
   >(ins);
}


}//namespace vdj_pipe
