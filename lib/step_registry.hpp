/** @file "/vdj_pipe/lib/step_registry.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef STEP_REGISTRY_HPP_
#define STEP_REGISTRY_HPP_
#include <map>
#include <string>
#include <sstream>
#include "boost/multi_index_container.hpp"
#include "boost/multi_index/ordered_index.hpp"
#include "boost/multi_index/member.hpp"
#include "boost/range.hpp"

#include "vdj_pipe/exception.hpp"
#include "vdj_pipe/detail/vector_set.hpp"

namespace vdj_pipe{
namespace detail{ class Inserter; }

/**@brief 
*******************************************************************************/
class Step_registry {
public:
   struct Meta {
      Meta() {}

      Meta(
               std::string const& name,
               std::string const& category,
               std::string const& comment,
               std::string const& description
      )
      : name_(name),
        category_(category),
        comment_(comment),
        description_(description)
      {}

      std::string name_, category_, comment_, description_;
   };

private:
   typedef boost::multi_index_container<
            Meta,
            boost::multi_index::indexed_by<
               boost::multi_index::ordered_unique<
                  boost::multi_index::member<Meta, std::string, &Meta::name_>
               >,
               boost::multi_index::ordered_non_unique<
                  boost::multi_index::member<Meta, std::string, &Meta::category_>
               >
            >
   > mmap_t;
   friend class detail::Inserter;

   Step_registry();

public:
   struct Err : public base_exception {};

   typedef detail::Vector_set<std::string> cats_t;
   typedef mmap_t::const_iterator const_iterator;
   typedef mmap_t::const_iterator iterator;
   typedef boost::iterator_range<const_iterator> range;

   const_iterator begin() const {return mm_.begin();}
   const_iterator end() const {return mm_.end();}

   static Step_registry const& get() {
      static const Step_registry inst;
      return inst;
   }

   cats_t const& categories() const {return cats_;}

   boost::iterator_range<mmap_t::nth_index<1>::type::const_iterator>
   category(std::string const& cat) const {
      mmap_t::nth_index<1>::type const& ind = mm_.get<1>();
      return ind.equal_range(cat);
   }

private:
   mmap_t mm_;
   cats_t cats_;
};

}//namespace vdj_pipe
#endif /* STEP_REGISTRY_HPP_ */
