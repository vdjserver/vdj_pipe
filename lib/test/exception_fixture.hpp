/** @file "/vdj_pipe/lib/test/exception_fixture.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3; see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef EXCEPTION_FIXTURE_HPP_
#define EXCEPTION_FIXTURE_HPP_
#include "boost/test/unit_test_monitor.hpp"
#include "boost/exception/diagnostic_information.hpp"

namespace vdj_pipe{ namespace test{ namespace detail{

void translate(boost::exception const& e) {
   BOOST_FAIL(boost::diagnostic_information(e));
}
}//namespace detail

/** Test fixture for printing exception info
*******************************************************************************/
struct Exception_fixture {
   Exception_fixture() {
      ::boost::unit_test::unit_test_monitor.
      register_exception_translator<boost::exception>(&detail::translate);
   }
};

BOOST_GLOBAL_FIXTURE( Exception_fixture )

}//namespace test
}//namespace vdj_pipe
#endif /* EXCEPTION_FIXTURE_HPP_ */
