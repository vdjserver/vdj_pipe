/** @file "/vdj_pipe/lib/test/execute_options_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE execute_options_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/process_options.hpp"
#include "vdj_pipe/command_line_options.hpp"
#include "test/sample_data.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_single_filter_qc.json"));
   process_options(clo.tree());
//   BOOST_ERROR("");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_single.json"));
   process_options(clo.tree());
//   BOOST_ERROR("");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_paired_emid.json"));
   process_options(clo.tree());
//   BOOST_ERROR("");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_paired.json"));
   process_options(clo.tree());
//   BOOST_ERROR("");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case05 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_single_match.json"));
   process_options(clo.tree());
//   BOOST_ERROR("");
}

}//namespace test
}//namespace vdj_pipe
