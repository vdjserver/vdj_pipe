/** @file "/vdj_pipe/lib/test/file_map_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE file_map_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdj_pipe/sequence_file_map.hpp"

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Seq_file_map sfm;
   const Seq_file sf1(sample_file_path("smpl01_frw.fastq"));
   const Seq_file sf2(sample_file_path("smpl01_rev.fastq"));
   const Path_id pid1 = sfm.insert(sf1).first;
   const Path_id pid2 = sfm.insert(sf2).first;
   BOOST_CHECK( ! sfm.frw2rev(pid1));
   BOOST_CHECK( ! sfm.rev2frw(pid1));
   BOOST_CHECK( ! sfm.frw2rev(pid2));
   BOOST_CHECK( ! sfm.rev2frw(pid2));

   sfm.set_paired(pid1, pid2);
   BOOST_CHECK_EQUAL(sfm.frw2rev(pid1), pid2);
   BOOST_CHECK_EQUAL(sfm.rev2frw(pid2), pid1);

}

}//namespace test
}//namespace vdj_pipe
