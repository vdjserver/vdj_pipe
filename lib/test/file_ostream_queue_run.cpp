/** @file "/vdj_pipe/lib/test/file_ostream_queue_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE file_ostream_queue_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/file_ostream_queue.hpp"
#include "test/sample_data.hpp"
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;
#include "boost/lexical_cast.hpp"
#include <iostream>
#include <fstream>

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( file_ostream_queue_run_01 ) {
   const std::string p1 = temp_file_path();
   Queable_ofstream::path_template t =
            bass::list_of(p1 + "/dir")("/file")(".txt");
   File_ostream_queue fos(t);
   const Queable_ofstream::value_type v1 = (long)1;
   const Queable_ofstream::value_type v2 = (long)1;
   File_ostream_queue::val_ref_vector vv =
            bass::list_of(boost::cref(v1))(boost::cref(v2));
   const std::string p2 = fos.ostream(vv).path();
   BOOST_CHECK_EQUAL(p2, p1 + "/dir" + "1" + "/file" + "1" + ".txt");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( file_ostream_queue_run_02 ) {
   const std::string p1 = temp_file_path();
   Queable_ofstream::path_template t =
            bass::list_of(p1 + "/dir")("/file")(".txt");
   File_ostream_queue fos(t, "", format::unknown, 5);
   for(int n = 0; n != 5; ++n) {
      for(int i = 0; i != 4; ++i) {
         for(int j = 0; j != 4; ++j) {
            const Queable_ofstream::value_type v1 = (long)i;
            const Queable_ofstream::value_type v2 = (long)j;
            File_ostream_queue::val_ref_vector vv =
                     bass::list_of(boost::cref(v1))(boost::cref(v2));
            File_ostream& fo = fos.ostream(vv);
            BOOST_CHECK_EQUAL(
                     fo.path(),
                     p1 + "/dir" + boost::lexical_cast<std::string>(i) +
                     "/file" + boost::lexical_cast<std::string>(j) + ".txt"
            );
            fo.ostream()  << n << ' ' << v1 << ' ' << v2 << std::endl;
         }
      }
   }
}

}//namespace test
}//namespace vdj_pipe
