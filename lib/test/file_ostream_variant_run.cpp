/** @file "/vdj_pipe/lib/test/file_ostream_variant_run.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2015
*******************************************************************************/
#define BOOST_TEST_MODULE file_ostream_variant_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;
#include "boost/lexical_cast.hpp"

#include "vdj_pipe/file_ostream_variant.hpp"
#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/value_map_access_single.hpp"

namespace vdj_pipe{ namespace test{

const std::string id1 = "seq_id_1";
const std::string seq1 = "TTGACTACTGGGGCCAGGGAACCCTGGTCACCGT";
const Qual_record::quality qual1(seq1.size(), 42);

const std::string id2 = "seq_id_2";
const std::string seq2 = "TTGAGAACCCTGGTCACCGTCTAGAACCCTGGTCACCGTCTGGGGCCAGG";
const Qual_record::quality qual2(seq2.size(), 13);

const std::string id3 = "seq_id_3";
const std::string seq3 = "TTGACTAAGGAACCCTGGTCACCGTGGAACCCTGGTCACCGT";
const Qual_record::quality qual3(seq3.size(), 5);

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( file_ostream_variant_run_01 ) {
   const std::string p1 = temp_file_path() + "/file1.fasta.bz2";
   Vm_access_single vms(Value_names::forward());
   File_ostream_variant fov(p1, "", vms);

   File_ostream& fos = fov.ostream();
   BOOST_CHECK_EQUAL(fos.compression(), compression::bzip2);
   BOOST_CHECK_EQUAL(fos.format(), format::Fasta);

   fos.write(id1, seq1, qual1);
   fos.write(id2, seq2, qual2);
   fos.write(id3, seq3, qual3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( file_ostream_variant_run_02 ) {
   const std::string p1 = temp_file_path() + "/file1.qual.bz2";
   Vm_access_single vms(Value_names::forward());
   File_ostream_variant fov(p1, "", vms);

   File_ostream& fos = fov.ostream();
   BOOST_CHECK_EQUAL(fos.compression(), compression::bzip2);
   BOOST_CHECK_EQUAL(fos.format(), format::Qual);

   fos.write(id1, seq1, qual1);
   fos.write(id2, seq2, qual2);
   fos.write(id3, seq3, qual3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( file_ostream_variant_run_03 ) {
   const std::string p1 = temp_file_path() + "/file1.fastq.bz2";
   Vm_access_single vms(Value_names::forward());
   File_ostream_variant fov(p1, "", vms);

   File_ostream& fos = fov.ostream();
   BOOST_CHECK_EQUAL(fos.compression(), compression::bzip2);
   BOOST_CHECK_EQUAL(fos.format(), format::Fastq);

   fos.write(id1, seq1, qual1);
   fos.write(id2, seq2, qual2);
   fos.write(id3, seq3, qual3);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( file_ostream_variant_run_04 ) {
   const std::string p1 = temp_file_path() + "/file2.qual.bz2";
   const File_output fo(p1);
   File_ostream fos(fo);

   BOOST_CHECK_EQUAL(fos.compression(), compression::bzip2);
   BOOST_CHECK_EQUAL(fos.format(), format::Qual);

   fos.write(id1, "", Qual_record::quality());
   fos.write(id1, seq1, qual1);
   fos.write(id2, seq2, qual2);
   fos.write(id3, seq3, qual3);
}

}//namespace test
}//namespace vdj_pipe
