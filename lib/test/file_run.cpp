/** @file "/vdj_pipe/lib/test/file_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE file_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdj_pipe/sequence_file.hpp"

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   BOOST_CHECK_EQUAL(
            Seq_file(sample_file_path("sample_01.fasta")).format(),
            format::Fasta
   );

   BOOST_CHECK_EQUAL(
            Seq_file(sample_file_path("sample_01.fasta.gz")).format(),
            format::Fasta
   );

   BOOST_CHECK_EQUAL(
            Seq_file(sample_file_path("sample_01.qual")).format(),
            format::Qual
   );

   BOOST_CHECK_EQUAL(
            Seq_file(sample_file_path("sample_01.fasta.gz")).compression(),
            compression::gzip
   );

   BOOST_CHECK_EQUAL(
            Seq_file(sample_file_path("sample_01.qual.bz2")).compression(),
            compression::bzip2
   );
}

}//namespace test
}//namespace vdj_pipe
