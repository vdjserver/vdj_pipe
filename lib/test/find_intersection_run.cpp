/** @file "/vdj_pipe/lib/test/find_intersection_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE find_intersection_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdj_pipe/command_line_options.hpp"
#include "vdj_pipe/process_options.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( find_intersection_run_01 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("find_intersection.json"));
   process_options(clo.tree());
//   BOOST_ERROR("");
}

}//namespace test
}//namespace vdj_pipe
