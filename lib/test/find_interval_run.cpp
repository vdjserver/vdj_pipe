/** @file "/vdj_pipe/lib/test/find_interval_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE find_interval_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;

#include "vdj_pipe/find_interval.hpp"

namespace vdj_pipe{ namespace test{

typedef Qual_record::quality::value_type value;
typedef std::pair<std::size_t,std::size_t> pair;

/**@test finding longest interval with average above min
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::vector<value> q1 = bass::list_of(1)(1)(1)(1)(1)(1)(1)(1)(1)(1)(1)(1)(1);
   const pair pa1 = longest_average_interval(q1, 3, 3);
   BOOST_CHECK_EQUAL(pa1.first, 0);
   BOOST_CHECK_EQUAL(pa1.second, 13);
   const pair pm1 = longest_min_interval(q1, 1);
   BOOST_CHECK_EQUAL(pm1.first, 0);
   BOOST_CHECK_EQUAL(pm1.second, 13);

   std::vector<value> q2 = bass::list_of(0)(1)(1)(1)(1)(0)(1)(1)(1)(1)(1)(1)(1);
   const pair pa2 = longest_average_interval(q2, 3, 3);
   BOOST_CHECK_EQUAL(pa2.first, 6);
   BOOST_CHECK_EQUAL(pa2.second, 7);
   const pair pm2 = longest_min_interval(q2, 1);
   BOOST_CHECK_EQUAL(pm2.first, 6);
   BOOST_CHECK_EQUAL(pm2.second, 7);

   std::vector<value> q3 = bass::list_of(0)(1)(1)(1)(1)(1)(1)(1)(1)(1)(1)(1)(1);
   const pair pa3 =
            longest_average_interval(q3, 3, 3);
   BOOST_CHECK_EQUAL(pa3.first, 1);
   BOOST_CHECK_EQUAL(pa3.second, 12);
   const pair pm3 = longest_min_interval(q3, 1);
   BOOST_CHECK_EQUAL(pm3.first, 1);
   BOOST_CHECK_EQUAL(pm3.second, 12);

   std::vector<value> q4 = bass::list_of(1)(1)(1)(1)(1)(1)(0)(1)(1)(1)(1)(1)(1);
   const pair pa4 = longest_average_interval(q4, 3, 3);
   BOOST_CHECK_EQUAL(pa4.first, 0);
   BOOST_CHECK_EQUAL(pa4.second, 6);
   const pair pm4 = longest_min_interval(q4, 1);
   BOOST_CHECK_EQUAL(pm4.first, 0);
   BOOST_CHECK_EQUAL(pm4.second, 6);

   std::vector<value> q5 = bass::list_of(1)(1)(1)(1)(1)(0)(1)(1)(1)(1)(1)(1)(0);
   const pair pa5 = longest_average_interval(q5, 3, 3);
   BOOST_CHECK_EQUAL(pa5.first, 6);
   BOOST_CHECK_EQUAL(pa5.second, 6);
   const pair pm5 = longest_min_interval(q5, 1);
   BOOST_CHECK_EQUAL(pm5.first, 6);
   BOOST_CHECK_EQUAL(pm5.second, 6);
}

/**@test finding longest interval with fewer ambiguous bases
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   std::string seq = "aaaaaaan";
   BOOST_CHECK_EQUAL(seq.size(), 8);
   pair p1 = longest_unambiguous_interval(seq, 0);
   BOOST_CHECK_EQUAL(p1.first, 0);
   BOOST_CHECK_EQUAL(p1.second, 7);
   p1 = longest_unambiguous_interval(seq, 1);
   BOOST_CHECK_EQUAL(p1.first, 0);
   BOOST_CHECK_EQUAL(p1.second, 8);

   seq = "naaaaaa";
   BOOST_CHECK_EQUAL(seq.size(), 7);
   p1 = longest_unambiguous_interval(seq, 0);
   BOOST_CHECK_EQUAL(p1.first, 1);
   BOOST_CHECK_EQUAL(p1.second, 6);
   p1 = longest_unambiguous_interval(seq, 1);
   BOOST_CHECK_EQUAL(p1.first, 0);
   BOOST_CHECK_EQUAL(p1.second, 7);

   seq = "nanaaaa";
   BOOST_CHECK_EQUAL(seq.size(), 7);
   p1 = longest_unambiguous_interval(seq, 0);
   BOOST_CHECK_EQUAL(p1.first, 3);
   BOOST_CHECK_EQUAL(p1.second, 4);
   p1 = longest_unambiguous_interval(seq, 1);
   BOOST_CHECK_EQUAL(p1.first, 1);
   BOOST_CHECK_EQUAL(p1.second, 6);

   seq = "aaaaana";
   BOOST_CHECK_EQUAL(seq.size(), 7);
   p1 = longest_unambiguous_interval(seq, 0);
   BOOST_CHECK_EQUAL(p1.first, 0);
   BOOST_CHECK_EQUAL(p1.second, 5);
   p1 = longest_unambiguous_interval(seq, 1);
   BOOST_CHECK_EQUAL(p1.first, 0);
   BOOST_CHECK_EQUAL(p1.second, 7);
}
}//namespace test
}//namespace vdj_pipe
