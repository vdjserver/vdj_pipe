/** @file "/vdj_pipe/lib/test/gene_seq_map_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE gene_seq_map_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "sequence_map_aligned.hpp"
#include "test/sample_data.hpp"
#include "test/sample_sequences.hpp"
#include <iostream>

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Seq_map_aligned sma;
   sma.load_file(sample_file_path("genes_1.fasta"), true);
   BOOST_CHECK_EQUAL(sma.size(), 27U);

   BOOST_CHECK_GT(sma.find_overlaping(seq7, 12).size(), 1U);

   const Seq_map_aligned::Match m = sma.best_match(seq7, 10, false);
   BOOST_CHECK(m.id_);
   BOOST_CHECK_GT(width(m.si_), 200);
}

}//namespace test
}//namespace vdj_pipe
