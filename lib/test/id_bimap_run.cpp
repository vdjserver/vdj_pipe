/** @file "/vdj_pipe/lib/test/id_bimap_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE id_bimap_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_sequences.hpp"
#include "vdj_pipe/detail/id_bimap.hpp"
#include "vdj_pipe/object_ids.hpp"
#include <string>

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   typedef vdj_pipe::detail::Id_bimap<Mid_id, std::string> name_map;
   name_map nm1(Mid_id(1));
   const Mid_id id1 = nm1.insert(seq1).first;
   const Mid_id id2 = nm1.insert(seq2).first;
   const Mid_id id3 = nm1.insert(seq3).first;
   BOOST_CHECK_EQUAL(nm1[id1], seq1);
   BOOST_CHECK_EQUAL(nm1[id2], seq2);
   BOOST_CHECK_EQUAL(nm1[id3], seq3);
   BOOST_CHECK(nm1.find(seq1));
   BOOST_CHECK(nm1.find(seq2));
   BOOST_CHECK(nm1.find(seq3));

   name_map nm2(nm1);
   BOOST_CHECK_EQUAL(nm2[id1], seq1);
   BOOST_CHECK_EQUAL(nm2[id2], seq2);
   BOOST_CHECK_EQUAL(nm2[id3], seq3);
   BOOST_CHECK(nm2.find(seq1));
   BOOST_CHECK(nm2.find(seq2));
   BOOST_CHECK(nm2.find(seq3));
}

}//namespace test
}//namespace vdj_pipe
