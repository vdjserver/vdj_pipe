/** @file "/vdj_pipe/lib/test/interval_iter_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE interval_iter_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_sequences.hpp"
#include "vdj_pipe/interval_iterator.hpp"
#include <string>

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   const std::string s1 = "CTCCAAGAAATGTTAACTGCNCNN"
            "TGGATNGTGGGAAGATGGATCNTCACTGTCTCTTATACAC";
   Unambiguous_interval_iter uii(s1, 10);
   BOOST_CHECK(uii.has_subseq());
   BOOST_CHECK_EQUAL(uii.subseq(), "CTCCAAGAAATGTTAACTGC");
   uii.next();
   BOOST_CHECK_EQUAL(uii.subseq(), "GTGGGAAGATGGATC");
   uii.next();
   BOOST_CHECK_EQUAL(uii.subseq(), "TCACTGTCTCTTATACAC");
   uii.next();
   BOOST_CHECK( ! uii.has_subseq() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   const std::string s1 = "NTCCAAGAAATNTTAACTGCNCNN"
            "TGGATNGTGGGAAGATGGATCNTCACTGTCTCTTATACAN";
   Unambiguous_interval_iter uii(s1, 10);
   BOOST_CHECK(uii.has_subseq());
   BOOST_CHECK_EQUAL(uii.subseq(), "TCCAAGAAAT");
   uii.next();
   BOOST_CHECK_EQUAL(uii.subseq(), "GTGGGAAGATGGATC");
   uii.next();
   BOOST_CHECK_EQUAL(uii.subseq(), "TCACTGTCTCTTATACA");
   uii.next();
   BOOST_CHECK( ! uii.has_subseq() );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   Unambiguous_interval_iter uii(seq6, 10);
   BOOST_CHECK(uii.has_subseq());
   BOOST_CHECK_EQUAL(uii.subseq(), seq6.substr(0, 43));
   uii.next();
   BOOST_CHECK_EQUAL(uii.subseq(), seq6.substr(53));
   uii.next();
   BOOST_CHECK( ! uii.has_subseq() );
}

}//namespace test
}//namespace vdj_pipe
