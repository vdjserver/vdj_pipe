/** @file "/vdj_pipe/lib/test/lib_info_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3; see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE lib_info_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/lib_info.hpp"
#include <iostream>

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   BOOST_CHECK_EQUAL(Lib_info::name(), "vdj_pipe");
   BOOST_MESSAGE(Lib_info::version());
   BOOST_MESSAGE(Lib_info::copyright());
   BOOST_MESSAGE(Lib_info::license());
   std::cout
   << Lib_info::name() << '\n'
   << Lib_info::version() << '\n'
   << Lib_info::copyright() << '\n'
   << Lib_info::license() << '\n'
   ;
   BOOST_CHECK( ! Lib_info::version().empty() );
   BOOST_CHECK_GT( Lib_info::build(), 1 );
}

}//namespace test
}//namespace vdj_pipe
