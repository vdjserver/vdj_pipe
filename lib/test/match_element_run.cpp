/** @file "/vdj_pipe/lib/test/match_element_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE match_element_run
#include "boost/test/unit_test.hpp"
#include <string>
#include <sstream>
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;
#include "boost/property_tree/json_parser.hpp"
namespace bpt = boost::property_tree;
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "me_battery.hpp"
#include "me_factory.hpp"
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/input_manager.hpp"

namespace vdj_pipe{ namespace test{

const std::string seq1 = "atgggcttgcctaagttc";
const sequence_interval si1(0, 18);
Qual_record::quality qual;

/**@test Match positional element
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_element_run_01 ) {
   Value_map vm;
   match::Match_element_positional meu1(vm, "e1", 3, true, 5);
   BOOST_CHECK(is_valid(si1));
   sequence_interval si = meu1(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), 3);
   BOOST_CHECK_EQUAL(si.upper(), 3 + 5);
   BOOST_CHECK_EQUAL(vm.value<std::string>("e1"), "ggctt");

   si = meu1(seq1, qual, sequence_interval(7, 18));
   BOOST_CHECK_EQUAL(si.lower(), 7 + 3);
   BOOST_CHECK_EQUAL(si.upper(), 7 + 3 + 5);
   BOOST_CHECK_EQUAL(vm.value<std::string>("e1"), "ctaag");

   match::Match_element_positional meu2(vm, "e2", -10, false, 5);
   si = meu2(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), 18 - 10);
   BOOST_CHECK_EQUAL(si.upper(), 18 - 10 + 5);
   BOOST_CHECK_EQUAL(vm.value<std::string>("e2"), "gccta");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( parse_combinations_csv_file_1 ) {
   const std::string opts =
            "{\"path\": \"mid_pairs_1.txt\","
            "\"values_column\": [ {\"MID1\": \"mid1\"}, {\"MID2\": \"mid2\"} ],"
            "\"names_column\": \"name\"}";

   std::istringstream iss(opts);

   bpt::ptree pt;
   bpt::read_json(iss, pt);
   Input_manager in(sample_file_path());
   std::vector<std::string> me_names = bass::list_of("MID1")("MID2");
   std::vector<std::size_t> mev;
   vdj_pipe::detail::string_table st;
   match::parse_csv_combination(pt, in, me_names, mev, st);
   BOOST_CHECK_EQUAL(st[0][0], "pair1");
   BOOST_CHECK_EQUAL(st[0][1], "CGCACA");
   BOOST_CHECK_EQUAL(st[0][2], "CCAGGCT");

   BOOST_CHECK_EQUAL(st[9][0], "pair10");
   BOOST_CHECK_EQUAL(st[9][1], "CCCACA");
   BOOST_CHECK_EQUAL(st[9][2], "CCAGGCT");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( parse_combinations_csv_file_2 ) {
   const std::string opts =
            "{\"path\": \"mid_pairs_1.txt\","
            "\"values_column\": [ {\"MID1\": 1}, {\"MID2\": 2} ],"
            "\"names_column\": 0, \"skip_header\": true}";

   std::istringstream iss(opts);

   bpt::ptree pt;
   bpt::read_json(iss, pt);
   Input_manager in(sample_file_path());
   std::vector<std::string> me_names = bass::list_of("MID1")("MID2");
   std::vector<std::size_t> mev;
   vdj_pipe::detail::string_table st;
   match::parse_csv_combination(pt, in, me_names, mev, st);
   BOOST_CHECK_EQUAL(st[0][0], "pair1");
   BOOST_CHECK_EQUAL(st[0][1], "CGCACA");
   BOOST_CHECK_EQUAL(st[0][2], "CCAGGCT");

   BOOST_CHECK_EQUAL(st[9][0], "pair10");
   BOOST_CHECK_EQUAL(st[9][1], "CCCACA");
   BOOST_CHECK_EQUAL(st[9][2], "CCAGGCT");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( parse_combinations_csv_file_3 ) {
   const std::string opts =
            "{\"path\": \"mid_pairs_1.txt\","
            "\"values_column\": [ {\"MID1\": 1}, {\"MID2\": 2} ],"
            "\"skip_header\": true}";

   std::istringstream iss(opts);

   bpt::ptree pt;
   bpt::read_json(iss, pt);
   Input_manager in(sample_file_path());
   std::vector<std::string> me_names = bass::list_of("MID1")("MID2");
   std::vector<std::size_t> mev;
   vdj_pipe::detail::string_table st;
   match::parse_csv_combination(pt, in, me_names, mev, st);
   BOOST_CHECK_EQUAL(st[0][0], "sample_1");
   BOOST_CHECK_EQUAL(st[0][1], "CGCACA");
   BOOST_CHECK_EQUAL(st[0][2], "CCAGGCT");

   BOOST_CHECK_EQUAL(st[9][0], "sample_10");
   BOOST_CHECK_EQUAL(st[9][1], "CCCACA");
   BOOST_CHECK_EQUAL(st[9][2], "CCAGGCT");
}

//todo:
#if 0
/**@test Match element battery
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_element_run_02 ) {
   match::Match_element_battery meb;
   BOOST_CHECK_EQUAL(meb.size(), 0);
   sequence_interval si = sequence_interval::whole();
   si = meb(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), sequence_interval::whole().lower());
   BOOST_CHECK_EQUAL(si.upper(), sequence_interval::whole().upper());

   Value_map vm;
   match::Match_element_positional meu1(vm, "e1", 3, true, 5);
   meb.insert("e1", meu1, "", match::Truncate::none());
   BOOST_CHECK_EQUAL(meb.size(), 1);
   si = meb(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), sequence_interval::whole().lower());
   BOOST_CHECK_EQUAL(si.upper(), sequence_interval::whole().upper());
   BOOST_CHECK_EQUAL(vm.value<std::string>("e1"), "ggctt");

   match::Match_element_positional meu2(vm, "e2", -3, false, 7);
   meb.insert("e2", meu2, "e1", match::Truncate::lower(5, true));
   si = meb(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), 5 + 5);
   BOOST_CHECK_EQUAL(si.upper(), sequence_interval::whole().upper());
   BOOST_CHECK_EQUAL(vm.value<std::string>("e1"), "ggctt");
   BOOST_CHECK_EQUAL(vm.value<std::string>("e2"), "cttgcct");

   match::Match_element_positional meu3(vm, "e3", -10, false, 7);
   meb.insert("e2", meu3, "e1", match::Truncate::upper(-5, true));
   si = meb(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), 5 + 5);
   BOOST_CHECK_EQUAL(si.upper(), sequence_interval::whole().upper());
   BOOST_CHECK(! vm.value_ptr<std::string>("e3"));
}

//const std::string seq1 = "atgggcttgcctaagttc";

/**@test Match short element
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_element_run_03 ) {
   Value_map vm;
   match::Match_element_short mes(
            vm,
            "se1",
            "se1-score",
            match::Relative_interval(match::Relative_position(3, true)),
            0
   );
   mes.insert("e1", "gactt");
   mes.insert("e2", "gccta");
   mes.insert("e3", "gcctt");
   mes.insert("e4", "gaatt");
   mes.insert("e5", "ggctt");

   sequence_interval si = sequence_interval::whole();
   si = mes(seq1, qual, si1);
   BOOST_CHECK(is_valid(si));
   BOOST_CHECK_EQUAL(si.lower(), 3);
   BOOST_CHECK_EQUAL(si.upper(), 8);
   BOOST_CHECK_EQUAL(vm.value<std::string>("se1"), "e5");
   BOOST_CHECK_EQUAL(vm.value<int>("se1-score"), 10);
}

/**@test Match short element
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_element_run_04 ) {
   Value_map vm;
   match::Match_element_short mes(
            vm,
            "se1",
            "se1-score",
            match::Relative_interval(match::Relative_position(-1, true), 20),
            0
   );
   mes.insert("e1", "gactt");
   mes.insert("e2", "gccta");
   mes.insert("e3", "gcctt");
   mes.insert("e4", "gaatt");
   mes.insert("e5", "ggcta");

   sequence_interval si = sequence_interval::whole();
   si = mes(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), 8);
   BOOST_CHECK_EQUAL(si.upper(), 13);
   BOOST_CHECK_EQUAL(vm.value<std::string>("se1"), "e2");
   BOOST_CHECK_EQUAL(vm.value<int>("se1-score"), 10);
}

//const std::string seq1 = "atgggcttgcctaagttc";

/**@test Match short and positional elements
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_element_run_05 ) {
   match::Match_element_battery meb;
   sequence_interval si = sequence_interval::whole();

   Value_map vm;
   match::Match_element_short mes(
            vm,
            "me1",
            "me1-score",
            match::Relative_interval(match::Relative_position(2, true), 12),
            0
   );
   mes.insert("e1", "gactt");
   mes.insert("e2", "gccta");
   mes.insert("e3", "gcctt");
   mes.insert("e4", "gaatt");
   mes.insert("e5", "ggcta");

   meb.insert("me1", mes, "", match::Truncate::none());

   si = meb(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), sequence_interval::whole().lower());
   BOOST_CHECK_EQUAL(si.upper(), sequence_interval::whole().upper());
   BOOST_CHECK_EQUAL(vm.value<std::string>("me1"), "e2");
   BOOST_CHECK_EQUAL(vm.value<int>("me1-score"), 10);

   match::Match_element_positional meu1(vm, "me2", -3, false, 7);
   meb.insert("me2", meu1, "me1", match::Truncate::none());

   si = meb(seq1, qual, si1);
   BOOST_CHECK_EQUAL(si.lower(), sequence_interval::whole().lower());
   BOOST_CHECK_EQUAL(si.upper(), sequence_interval::whole().upper());
   BOOST_CHECK_EQUAL(vm.value<std::string>("me1"), "e2");
   BOOST_CHECK_EQUAL(vm.value<int>("me1-score"), 10);
   BOOST_CHECK_EQUAL(vm.value<std::string>("me2"), "ctaagtt");
}
#endif

}//namespace test
}//namespace vdj_pipe
