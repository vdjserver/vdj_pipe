/** @file "/vdj_pipe/lib/test/match_step_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE match_step_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/step/match_step.hpp"
#include "processing_step_utils.hpp"
#include "vdj_pipe/process_options.hpp"
#include "vdj_pipe/command_line_options.hpp"
#include "test/sample_data.hpp"

namespace vdj_pipe{ namespace test{

const std::string match1 =
   "{                                                                         "
   "   \"reverse\": true, \"trimmed\": true,                                  "
   "   \"elements\": [                                                        "
   "       {                                                                  "
   "         \"start\": { \"pos\": 14 }, \"length\": 8,                       "
   "         \"seq_file\": \"imid1.fasta\",                                   "
   "         \"min_score\": 10, \"value_name\": \"iMID\",                     "
   "         \"score_name\": \"iMID_score\"                                   "
   "       },                                                                 "
   "       {                                                                  "
   "         \"end\": { \"before\": \"iMID\", \"pos\": -1 }, \"length\": 14,  "
   "         \"value_name\": \"UMI\"                                          "
   "       },                                                                 "
   "       {                                                                  "
   "         \"start\": { \"after\": \"iMID\" },                              "
   "         \"length\": 50,                                                  "
   "         \"sequence\": \"ACTCTGGGTGTCTCACCATGGCCT\", \"name\": \"primer\", "
   "         \"min_score\": 10, \"allow_gaps\": true, \"min_match_length\": 10,"
   "         \"cut_lower\": {\"before\": 0}, \"value_name\": \"seq42\"        "
   "       }                                                                  "
   "   ]                                                                      "
   "}                                                                         "
         ;

const std::string seq1 = "taaaaaaaaaaaaaaTAGAGCccttACTCTGGGTGTCTCACCATGGCCTcc";

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_3_elements_01 ) {
   Step_harness<Match_step> sh(match1);
   sh.run(seq1);
   BOOST_REQUIRE(sh.vma().value_ptr<std::string>("iMID"));
   BOOST_CHECK_EQUAL(sh.vma().value<std::string>("iMID"), "MID12");
   BOOST_REQUIRE(sh.vma().value_ptr<std::string>("UMI"));
   BOOST_CHECK_EQUAL(sh.vma().value<std::string>("UMI"), "taaaaaaaaaaaaa");
   BOOST_REQUIRE(sh.vma().value_ptr<std::string>("seq42"));
   BOOST_CHECK_EQUAL(sh.vma().value<std::string>("seq42"), "seq_1");
   BOOST_CHECK_EQUAL(sh.vma().interval().lower(), 25);
}

const std::string match2 =
   "{"
   "   \"elements\": ["
   "       {"
   "         \"end\": { \"after\": \"\" }, "
   "         \"sequence\": \"ggcctcc\","
   "         \"max_mismatches\": 1, \"value_name\": \"MID1\","
   "         \"score_name\": \"MID1_score\""
   "       }, "
   "       {"
   "         \"end\": { \"after\": \"\" }, "
   "         \"length\": 5,"
   "         \"value_name\": \"MID2\""
   "       }"
   "   ]"
   "}"
         ;

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_element_at_end ) {
   Step_harness<Match_step> sh(match2);
   sh.run(seq1);
   BOOST_REQUIRE(sh.vma().value_ptr<std::string>("MID1"));
   BOOST_CHECK_EQUAL(sh.vma().value<std::string>("MID1"), "ggcctcc");
   BOOST_CHECK_EQUAL(sh.vma().interval().lower(), 0);
   BOOST_CHECK_EQUAL(sh.vma().interval().upper(), 51);
   BOOST_REQUIRE(sh.vma().value_ptr<std::string>("MID2"));
}

const std::string seq2 = "CTGGGTGTctCACCATGGCCTcc";

const std::string match3 =
   "{"
   "   \"elements\": ["
   "       {"
   "         \"end\": { \"after\": \"\" }, \"length\": 30, "
   "         \"sequence\": \"gtctcg\", "
   "         \"max_mismatches\": 1, \"value_name\": \"MID1\","
   "         \"score_name\": \"MID1_score\", \"cut_upper\": { \"after\": 0}"
   "       }, "
   "       {"
   "         \"end\": { \"after\": \"\" }, "
   "         \"length\": 5,"
   "         \"value_name\": \"MID2\""
   "       }"
   "   ]"
   "}"
         ;

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_scan_element ) {
   Step_harness<Match_step> sh(match3);
   sh.run(seq2);
   BOOST_CHECK(sh.vma().value_ptr<std::string>("MID1"));
   BOOST_CHECK_EQUAL(sh.vma().value<long>("MID1_score"), -1);
   BOOST_CHECK_EQUAL(sh.vma().value<std::string>("MID1"), "gtctcg");
   BOOST_REQUIRE(sh.vma().value_ptr<std::string>("MID2"));
   BOOST_CHECK_EQUAL(sh.vma().interval().lower(), 0);
   BOOST_CHECK_EQUAL(sh.vma().interval().upper(), 12);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_step_run_02 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("match_combination_1.json"));
   process_options(clo.tree());
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( match_step_run_03 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("match_combination_2.json"));
   process_options(clo.tree());
}

}//namespace test
}//namespace vdj_pipe
