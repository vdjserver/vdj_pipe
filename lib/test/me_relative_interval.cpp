/** @file "/vdj_pipe/lib/test/me_relative_interval.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE me_relative_interval
#include "boost/test/unit_test.hpp"
#include <iostream>
#include "test/exception_fixture.hpp"
#include "me_relative_interval.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( me_relative_interval_01 ) {
   match::Relative_position rp(-5, false); //5 bases before interval end

   //10 base interval starting 5 bases before interval end
   match::Relative_interval ri(rp, 10);

   const sequence_interval si0(10, 20);

   sequence_interval si1 = ri(si0, 100);
   BOOST_CHECK(is_valid(si1));
   BOOST_CHECK_EQUAL(si1.lower(), 15);
   BOOST_CHECK_EQUAL(si1.upper(), 25);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( me_relative_interval_02 ) {
   sequence_interval si = sequence_interval(-5,5);
   BOOST_CHECK(is_valid(si));
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( me_relative_interval_03 ) {
   match::Relative_position rp(-5, true); //5 bases before interval start

   sequence_interval si0 = sequence_interval(10, 20);
   BOOST_CHECK_EQUAL(rp(si0), 5);

   //10 base interval ending 5 bases before reference interval start
   match::Relative_interval ri(rp, -10);

   sequence_interval si1 = ri(si0, 100);
   BOOST_CHECK(is_valid(si1));
   BOOST_CHECK_EQUAL(si1.lower(), 0);
   BOOST_CHECK_EQUAL(si1.upper(), 5);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( me_relative_interval_04 ) {
   match::Relative_position rp(0, false); //at interval end
   sequence_interval si0 = sequence_interval(0, 20);
   BOOST_CHECK_EQUAL(rp(si0), 20);

   //30 base interval ending at reference interval end
   match::Relative_interval ri(rp, -30);
   std::cout << ri << std::endl;

   sequence_interval si1 = ri(si0, 20);
   BOOST_CHECK(is_valid(si1));
   BOOST_CHECK_EQUAL(si1.lower(), 0);
   BOOST_CHECK_EQUAL(si1.upper(), 20);
}

}//namespace test
}//namespace vdj_pipe
