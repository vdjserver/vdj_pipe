/** @file "/vdj_pipe/lib/test/nucleotide_index_run.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE nucleotide_index_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"

#include "boost/algorithm/string/predicate.hpp" //all
#include "boost/algorithm/string/classification.hpp" //operator!

#include "vdj_pipe/nucleotide_index.hpp"

namespace vdj_pipe{ namespace test{

scoring_matrix_t const& sm1 = scoring_matrix<1,0,1,1>();
scoring_matrix_t const& sm2 = scoring_matrix<2,-2,1,0>();

/**@test check scoring matrix symmetry
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   for( int n1 = 0; n1 != VDJ_PIPE_N_NUCLEOTIDES; ++n1 ) {
      for( int n2 = 0; n2 != VDJ_PIPE_N_NUCLEOTIDES; ++n2 ) {
         BOOST_CHECK_EQUAL(
                  identity( (Nucleotide)n1, (Nucleotide)n2, sm1 ),
                  identity( (Nucleotide)n2, (Nucleotide)n1, sm1 )
         );

         BOOST_CHECK_EQUAL(
                  identity( (Nucleotide)n1, (Nucleotide)n2, sm2 ),
                  identity( (Nucleotide)n2, (Nucleotide)n1, sm2 )
         );
      }
   }
}

/**@test check scoring matrix
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   BOOST_CHECK_EQUAL(identity(Adenine, Adenine, sm1), 1);
   BOOST_CHECK_EQUAL(identity(Adenine, Adenine, sm2), 2);

   BOOST_CHECK_EQUAL(identity(Adenine, Cytosine, sm1), 0);
   BOOST_CHECK_EQUAL(identity(Adenine, Cytosine, sm2), -2);

   BOOST_CHECK_EQUAL(identity(Adenine, Cytosine, sm1), 0);
   BOOST_CHECK_EQUAL(identity(Adenine, Cytosine, sm2), -2);
}

/**@test test for ambiguous nucleotides
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   const std::string s1 = "attgcc";
   BOOST_CHECK( all(s1, ! Is_ambiguous()) );
   const std::string s2 = "attngcc";
   BOOST_CHECK( ! all(s2, ! Is_ambiguous()) );
}

}//namespace test
}//namespace vdj_pipe
