/** @file "/vdj_pipe/lib/test/object_id_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE object_id_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/detail/object_id_base.hpp"
#include "vdj_pipe/detail/id_iterator.hpp"
#include "boost/foreach.hpp"
#include <iostream>

namespace vdj_pipe{ namespace test{

struct Id1 : public vdj_pipe::detail::Base_id<Id1> {
   explicit Id1(const unsigned x) : base(x) {}
   Id1() : base(0) {}
};

struct Id2 : public vdj_pipe::detail::Base_id<Id2> {
   explicit Id2(const unsigned x) : base(x) {}
   Id2() : base(0) {}
};

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   BOOST_CHECK( ! Id1());
   BOOST_CHECK( ! Id1(0));
   BOOST_CHECK(   Id1(1));
   BOOST_CHECK( Id1(1) == Id1(1) );
   BOOST_CHECK( Id1(1) != Id1(0) );

//   BOOST_CHECK( Id1(1) != Id2(0) ); //does not compile
//   BOOST_CHECK( Id1(1) == Id2(1) ); //does not compile
   Id1 id1(1);
   Id1 id2;
//   unsigned n = id1; //does not compile
//   void* p = id1; //does not compile
   id2 = id1;
   Id2 id3(1);
//   id2 = id3; //does not compile
}

/**@test ID iterator
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Id_iterator<Id1> id1_iter(Id1(1));
   BOOST_CHECK_EQUAL(*id1_iter, Id1(1));
   ++id1_iter;
   BOOST_CHECK_EQUAL(*id1_iter, Id1(2));

   boost::iterator_range<Id_iterator<Id1> > r(Id1(12), Id1(20));
   BOOST_FOREACH(const Id1 id, r) std::cout << id << '\n';
}

}//namespace test
}//namespace vdj_pipe
