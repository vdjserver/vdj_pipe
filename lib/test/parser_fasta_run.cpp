/** @file "/vdj_pipe/lib/test/parser_fasta_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE parser_fasta_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdj_pipe/parser_fasta.hpp"
#include "vdj_pipe/sequence_file.hpp"
#include "boost/iostreams/filter/gzip.hpp"
#include "boost/iostreams/device/file.hpp"
#include <fstream>
#include <sstream>

namespace vdj_pipe{ namespace test{

const std::string str_1 =
         ">id1 bl>ah2" "\n"
         "ATCGGCTAAG" "\n"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::istringstream is(str_1);
   Parser_fasta p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
//   BOOST_CHECK_EQUAL(p.get_id(), "id1");
   BOOST_CHECK_EQUAL(p.get_defstr(), "id1 bl>ah2");
   BOOST_CHECK_EQUAL(p.line_num(), 2);
   BOOST_CHECK_EQUAL(p.get_sequence(), "ATCGGCTAAG");

   BOOST_CHECK( ! p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

const std::string str_2 =
         "\n\n>id1 blah2" "\n" "SEQI" "\n"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   std::istringstream is(str_2);
   Parser_fasta p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 3);
   BOOST_CHECK_EQUAL(p.get_id(), "id1");
//   BOOST_CHECK_EQUAL(p.get_defstr(), "id1 blah2");
   BOOST_CHECK_EQUAL(p.line_num(), 4);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

const std::string str_3 =
         ">id2 blah1 blah2 blah3 " "\n"
         "SEQ SEQII" "\n"
         "seq" "\n"
         "\n"
         ">id3" "\n"
         "SEQI SEQII" "\n"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   std::istringstream is(str_3);
   Parser_fasta p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "id2");
//   BOOST_CHECK_EQUAL(p.get_defstr(), "id2 blah1 blah2 blah3 ");
   BOOST_CHECK_EQUAL(p.line_num(), 2);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( p.has_next() );

   BOOST_CHECK_EQUAL(p.line_num(), 5);
   BOOST_CHECK_EQUAL(p.get_defstr(), "id3");

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

/**@test Parse file
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   Parser_fasta p((File_input(sample_file_path("sample_01.fasta"))));
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "HQE6ANJ01B4HMS");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 107u);
}

/**@test Parse file.gz
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case05 ) {
   std::ifstream f(
            sample_file_path("sample_01.fasta.gz").c_str(),
            std::ios_base::in | std::ios_base::binary
   );
   boost::iostreams::filtering_istreambuf fis;
   fis.push(boost::iostreams::gzip_decompressor());
   fis.push(f);
   std::istream is(&fis);
   Parser_fasta p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "HQE6ANJ01B4HMS");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 107u);
}

/**@test Parse file.gz
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case06 ) {
   Seq_file sf(sample_file_path("sample_01.fasta.gz"));
   Parser_fasta p(sf);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "HQE6ANJ01B4HMS");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 107u);
}

}//namespace test
}//namespace vdj_pipe
