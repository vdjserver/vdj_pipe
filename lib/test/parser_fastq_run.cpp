/** @file "/vdj_pipe/lib/test/parser_fastq_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE parser_fastq_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/parser_fastq.hpp"
#include <fstream>
#include <sstream>
#include "test/sample_data.hpp"

namespace vdj_pipe{ namespace test{

const std::string str_1 =
         "@id1 blah2" "\n"
         "ATCGGCTAAG" "\n"
         "+\n"
         "!#$%&')*;@" "\n"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::istringstream is(str_1);
   Parser_fastq p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "id1");
   BOOST_CHECK_EQUAL(p.line_num(), 2);
   BOOST_CHECK_EQUAL(p.get_sequence(), "ATCGGCTAAG");
   BOOST_CHECK_EQUAL(p.line_num(), 3);
   Seq_qual_record::quality q = p.get_qual();
   BOOST_CHECK_EQUAL(p.line_num(), 5);
   BOOST_REQUIRE_EQUAL(q.size(), 10U);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );

   BOOST_CHECK_EQUAL(q[0], 0);
   BOOST_CHECK_EQUAL(q[1], 2);
   BOOST_CHECK_EQUAL(q[2], 3);
   BOOST_CHECK_EQUAL(q[3], 4);
   BOOST_CHECK_EQUAL(q[4], 5);
   BOOST_CHECK_EQUAL(q[5], 6);
   BOOST_CHECK_EQUAL(q[6], 8);
   BOOST_CHECK_EQUAL(q[7], 9);
   BOOST_CHECK_EQUAL(q[8], 26);
   BOOST_CHECK_EQUAL(q[9], 31);
}

const std::string str_2 =
         /*1*/ "@HWUSI-EAS100R:6:73:941:1973#0/1" "\n"
         /*2*/ "ATCGGCTAAG" "\n"
         /*3*/ "+\n"
         /*4*/ "!#$%&')*;@" "\n"
         /*5*/ "@SRR001666.1 071112_SLXA-EAS1_s_7:5:1:817:345 length=36" "\n"
         /*6*/ "GGGTGATGGCCGCTGCCGATGGCGTCAAATCCCACC" "\n"
         /*7*/ "+SRR001666.1 071112_SLXA-EAS1_s_7:5:1:817:345 length=36" "\n"
         /*8*/ "IIIIIIIIIIIIIIIIIIIIIIIIIIIIII9IG9IC"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   std::istringstream is(str_2);
   Parser_fastq p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "HWUSI-EAS100R:6:73:941:1973#0/1");
   BOOST_CHECK_EQUAL(p.line_num(), 2);
   BOOST_CHECK_EQUAL(p.get_sequence(), "ATCGGCTAAG");
   BOOST_CHECK_EQUAL(p.line_num(), 3);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 5);
   BOOST_CHECK_EQUAL(p.get_id(), "SRR001666.1");
   BOOST_CHECK_EQUAL(p.line_num(), 6);
   BOOST_CHECK_EQUAL(p.get_sequence(), "GGGTGATGGCCGCTGCCGATGGCGTCAAATCCCACC");
   BOOST_CHECK_EQUAL(p.line_num(), 7);
   Seq_qual_record::quality q = p.get_qual();
   BOOST_CHECK_EQUAL(p.line_num(), 9);
   BOOST_REQUIRE_EQUAL(q.size(), 36U);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );

   BOOST_CHECK_EQUAL(q[0], 40);
   BOOST_CHECK_EQUAL(q[35], 34);
}

/**@test Parse file - get IDs only
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   std::ifstream fs(sample_file_path("sample_03.fastq").c_str());
   Parser_fastq p(fs);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:22#0/1");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 6);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:1018#0/1");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 11);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:348#0/1");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 15);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:14968:1806");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 19);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:16942:1807");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 23);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:14313:1808");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 27);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:13351:1642");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 31);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:15228:1645");
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 35);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:16599:1646");

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

/**@test Parse file - get IDs and sequence only
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   std::ifstream fs(sample_file_path("sample_03.fastq").c_str());
   Parser_fastq p(fs);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:22#0/1");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 216U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 6);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:1018#0/1");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 288U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 11);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:348#0/1");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 108U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 15);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:14968:1806");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 19);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:16942:1807");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 23);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:14313:1808");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 27);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:13351:1642");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 31);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:15228:1645");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 35);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:16599:1646");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

/**@test Parse file - get ID, sequence, and quality
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case05 ) {
   std::ifstream fs(sample_file_path("sample_03.fastq").c_str());
   Parser_fastq p(fs);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:22#0/1");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 216U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 216U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 6);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:1018#0/1");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 288U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 288U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 11);
   BOOST_CHECK_EQUAL(p.get_id(), "IRIS:7:1:1753:348#0/1");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 108U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 108U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 15);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:14968:1806");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 19);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:16942:1807");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 23);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:14313:1808");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 27);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:13351:1642");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 31);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:15228:1645");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 250U);
   p.next_record();
   BOOST_CHECK_EQUAL(p.line_num(), 35);
   BOOST_CHECK_EQUAL(p.get_id(), "M00922:18:000000000-A3HRM:1:1101:16599:1646");
   BOOST_CHECK_EQUAL(p.get_sequence().size(), 250U);
   BOOST_CHECK_EQUAL(p.get_qual().size(), 250U);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

/**@test Parse file - get record
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case06 ) {
   std::ifstream fs(sample_file_path("smpl01_frw.fastq").c_str());
   Parser_fastq p(fs);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   const Parser_fastq::record r1 = p.get_record();
   BOOST_CHECK_EQUAL(p.line_num(), 5);
   BOOST_CHECK_EQUAL(r1.id_, "M00922:18:000000000-A3HRM:1:2114:12664:28693");
   BOOST_CHECK_EQUAL(r1.comm_, "1:N:0:0");
   BOOST_CHECK_EQUAL(r1.seq_.size(), 250U);
   BOOST_CHECK_EQUAL(r1.seq_.substr(0,10), "GGCTTCCCAT");
   BOOST_CHECK_EQUAL(r1.qual_.size(), 250U);
   BOOST_CHECK_EQUAL(r1.qual_[0], 32U);
   BOOST_CHECK_EQUAL(r1.qual_[5], 37U);
   BOOST_CHECK_EQUAL(r1.qual_[10], 16U);
}

}//namespace test
}//namespace vdj_pipe
