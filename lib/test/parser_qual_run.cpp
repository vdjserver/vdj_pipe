/** @file "/vdj_pipe/lib/test/parser_qual_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE parser_qual_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/parser_qual.hpp"
#include "test/sample_data.hpp"
#include "vdj_pipe/file.hpp"
#include <sstream>

namespace vdj_pipe{ namespace test{

const std::string str_1 =
         ">id1 bl>ah2" "\n"
         "10 20 42 13" "\n"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::istringstream is(str_1);
   Parser_qual p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
//   BOOST_CHECK_EQUAL(p.get_id(), "id1");
   BOOST_CHECK_EQUAL(p.get_defstr(), "id1 bl>ah2");
   BOOST_CHECK_EQUAL(p.line_num(), 2);
   Parser_qual::quality q = p.get_qual();
   BOOST_REQUIRE_EQUAL(q.size(), 4U);
   BOOST_CHECK_EQUAL(q[0], 10);
   BOOST_CHECK_EQUAL(q[1], 20);
   BOOST_CHECK_EQUAL(q[2], 42);
   BOOST_CHECK_EQUAL(q[3], 13);

   BOOST_CHECK( ! p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

const std::string str_2 =
         ">id2 blah1 blah2 blah3 " "\n"
         " 1 2 3 4 " "\n"
         "10 20 42 13" "\n"
         "\n"
         ">id3" "\n"
         "SEQI SEQII" "\n"
         ">id4" "\n"
         "5 10.5" "\n"
         ;

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   std::istringstream is(str_2);
   Parser_qual p(is);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "id2");
   BOOST_CHECK_EQUAL(p.line_num(), 2);
   Parser_qual::quality q = p.get_qual();
   BOOST_REQUIRE_EQUAL(q.size(), 8U);
   BOOST_CHECK_EQUAL(q[0], 1);
   BOOST_CHECK_EQUAL(q[1], 2);
   BOOST_CHECK_EQUAL(q[2], 3);
   BOOST_CHECK_EQUAL(q[6], 42);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( p.has_next() );
   BOOST_CHECK_EQUAL(p.get_id(), "id3");
   BOOST_CHECK_EQUAL(p.line_num(), 6);
   BOOST_CHECK_THROW(p.get_qual(), Parser_qual::Err);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( p.has_next() );
   BOOST_CHECK_EQUAL(p.get_id(), "id4");
   BOOST_CHECK_EQUAL(p.line_num(), 8);
   BOOST_CHECK_THROW(p.get_qual(), Parser_qual::Err);

   BOOST_CHECK( p.has_next() );
   p.next_record();
   BOOST_CHECK( ! p.has_next() );
}

/**@test Parse file.qual.bz2
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   File_input sf(sample_file_path("sample_01.qual.bz2"));
   Parser_qual p(sf);
   BOOST_CHECK_EQUAL(p.line_num(), 1);
   BOOST_CHECK_EQUAL(p.get_id(), "HQE6ANJ01B4HMS");
   BOOST_CHECK_EQUAL(p.get_qual().size(), 107u);
}

}//namespace test
}//namespace vdj_pipe
