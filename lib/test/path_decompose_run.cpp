/** @file "/vdj_pipe/lib/test/path_decompose_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE path_decompose_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/variable_path.hpp"

namespace vdj_pipe{ namespace test{

typedef std::vector<std::string> p_vector;

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( path_decompose_run_01 ) {
   const std::string p1 = "out_dir/{name1}-dir/{name2}file.fna";
   p_vector tv, nv;
   path_decompose(p1, tv, nv);
   BOOST_CHECK_EQUAL(tv.size(), 3U);
   BOOST_CHECK_EQUAL(nv.size(), 2U);
   BOOST_CHECK_EQUAL(tv[0], "out_dir/");
   BOOST_CHECK_EQUAL(nv[0], "name1");
   BOOST_CHECK_EQUAL(tv[1], "-dir/");
   BOOST_CHECK_EQUAL(nv[1], "name2");
   BOOST_CHECK_EQUAL(tv[2], "file.fna");

   const std::string p2 = "{name1}-dir/{name2}";
   tv.clear();
   nv.clear();
   path_decompose(p2, tv, nv);
   BOOST_CHECK_EQUAL(tv.size(), 3U);
   BOOST_CHECK_EQUAL(nv.size(), 2U);
   BOOST_CHECK_EQUAL(tv[0], "");
   BOOST_CHECK_EQUAL(nv[0], "name1");
   BOOST_CHECK_EQUAL(tv[1], "-dir/");
   BOOST_CHECK_EQUAL(nv[1], "name2");
   BOOST_CHECK_EQUAL(tv[2], "");
}

}//namespace test
}//namespace vdj_pipe
