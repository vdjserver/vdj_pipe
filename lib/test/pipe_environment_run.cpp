/** @file "/vdj_pipe/lib/test/pipe_environment_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE pipe_environment_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "vdj_pipe/command_line_options.hpp"
#include "vdj_pipe/pipe_paired_read.hpp"
#include "vdj_pipe/pipe_single_read.hpp"
#include "vdj_pipe/process_options.hpp"
#include "vdj_pipe/sequence_file_map.hpp"

namespace vdj_pipe{ namespace test{

/**@test input files
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Command_line_options clo;
   BOOST_CHECK_EQUAL(clo.max_reads(), std::numeric_limits<std::size_t>::max());
   clo.add_config_file(sample_file_path("config_single_filter_qc.json"));
   Vm_access_single vma(Value_names::single());
   Pipe_environment pe(clo.tree(), vma);
   BOOST_CHECK_EQUAL(pe.max_reads(), std::numeric_limits<std::size_t>::max());
   BOOST_CHECK_EQUAL(pe.input().size(), 3U);
}

/**@test max reads
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   char* c[0] = {};
   Command_line_options clo(0, c);
   BOOST_CHECK_EQUAL(clo.max_reads(), std::numeric_limits<std::size_t>::max());
}

/**@test test delimiter
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_single.json"));
   Vm_access_single vma(Value_names::single());
   Pipe_environment pe(clo.tree(), vma);
   BOOST_CHECK_EQUAL(pe.delimiter(), '\t');
}

/**@test max reads
*******************************************************************************/
BOOST_AUTO_TEST_CASE( max_reads_2 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_paired.json"));
   const boost::property_tree::ptree pt = clo.tree();
   Pipe_paired_read ppr(pt);
   BOOST_CHECK_EQUAL(ppr.pe().max_reads(), std::numeric_limits<std::size_t>::max());
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( seq_files ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("config_paired_emid.json"));
   Vm_access_paired_emid vma;
   Pipe_environment pe(clo.tree(), vma);

   BOOST_CHECK_EQUAL(pe.input().size(), 1U);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case06 ) {
   Command_line_options clo;
   const std::string p = sample_file_path("find_intersection.json");
   clo.add_config_file(p);
   Vm_access_single vma(Value_names::single());
   Pipe_environment pe(clo.tree(), vma);
   BOOST_CHECK_EQUAL(pe.input().size(), 1U);
   BOOST_CHECK_EQUAL(
            pe.input().begin()->sequence(),
            "intersection.fastq"
   );
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case07 ) {
   Command_line_options clo;
   clo.add_config_file(sample_file_path("find_intersection.json"));
   Pipe_single_read psr(clo.tree());
   BOOST_CHECK_EQUAL(psr.pe().input().size(), 1U);
   BOOST_CHECK_EQUAL(
            psr.pe().input().begin()->sequence(),
            "intersection.fastq"
   );
   psr.run();
   BOOST_CHECK_GE(psr.pe().read_count(), 5U);
}

}//namespace test
}//namespace vdj_pipe
