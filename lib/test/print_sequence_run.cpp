/** @file "/vdj_pipe/lib/test/print_sequence_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE print_sequence_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/print_sequence.hpp"
#include <iostream>

namespace vdj_pipe{ namespace test{

/** Test print QUAL
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Qual_record::quality v;
   for(int i = 0; i != 200; ++i) v.push_back(i);
   print_qual(std::cout, "seq_id", v, 21);
}

/** Test print FASTQ
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Qual_record::quality v;
   Seq_record::sequence seq;
   for(int i = 0; i != 94; ++i) {
      v.push_back(i);
      seq.push_back('A');
   }
   print_fastq(std::cout, "seq_id", seq, v, 33);
}

}//namespace test
}//namespace vdj_pipe
