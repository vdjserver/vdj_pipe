/** @file "/vdj_pipe/lib/test/processing_step_utils.hpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef PROCESSING_STEP_UTILS_HPP_
#define PROCESSING_STEP_UTILS_HPP_
#include <string>
#include <sstream>
#include "boost/property_tree/json_parser.hpp"
#include "boost/shared_ptr.hpp"
#include "vdj_pipe/value_map_access_single.hpp"
#include "vdj_pipe/pipe_environment.hpp"

namespace vdj_pipe{ namespace test{

namespace bpt = boost::property_tree;
typedef bpt::ptree ptree;

/**@brief
*******************************************************************************/
Pipe_environment make_pe(Value_map const& vm) {
   static const std::string pe_opts_str =
            " {                                       "
            " \"base_path_input\": \"sample_data\",   "
            " \"base_path_output\": \"out/temp\",     "
            "\"input\": {}                            "
            " }                                       ";
   std::istringstream is1(pe_opts_str);

   ptree pt1;
   bpt::read_json(is1, pt1);
   return Pipe_environment(pt1, vm);
}

/**@brief
*******************************************************************************/
template<class Step> Step make_step(
         Vm_access_single const& vma,
         std::string const& json
) {
   Pipe_environment pe = make_pe(vma);
   std::istringstream is2(json);
   ptree pt2;
   bpt::read_json(is2, pt2);

   return Step(vma, pt2, pe);
}

/**@brief
*******************************************************************************/
template<class Step> class Step_harness {
   typedef boost::shared_ptr<Step> step_ptr;

public:
   explicit Step_harness(std::string const& json)
   : vma_(Value_names::single()),
     step_(make_step<Step>(vma_, json))
   {}

   void run(std::string const& seq) {
      vma_.sequence(seq);
      Vm_access_single::qual_type qual(seq.size(), 10);
      vma_.quality(qual);
      vma_.interval(sequence_interval(0U, seq.size()));
      vma_.set_reverse(false);
      step_.run();
   }

   Vm_access_single const& vma() const {return vma_;}

private:
   Vm_access_single vma_;
   Step step_;
};


}//namespace test
}//namespace vdj_pipe
#endif /* PROCESSING_STEP_UTILS_HPP_ */
