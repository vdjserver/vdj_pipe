/** @file "/vdj_pipe/lib/test/read_info_store_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE read_info_store_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/read_info_store.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Seq_file_map sfm;
   Read_info_store sis;
   const Path_id pid1(1);
   const Path_id pid2(2);
   const Read_id iid1 = sis.insert(
            Read_info("seq1").seq_file(pid1).reverse(true)
   );
   Read_info si1 = sis[iid1];
   sis.insert(si1.trim(0,100));
   const Read_id iid2 = sis.insert(Read_info("seq2").trim(1, 10));
   const Read_id iid3 = sis.insert(Read_info("seq3").trim(0, 300));
   const Read_id iid4 = sis.insert(Read_info("seq4").trim(0, 200));
   const Read_id iid5 = sis.insert(Read_info("seq5").trim(0, 150));
   BOOST_CHECK_EQUAL(sis.by_size().front(), iid2);
   BOOST_CHECK_EQUAL(sis.by_size().back(), iid3);
   BOOST_CHECK_EQUAL(sis.by_size_range(11, 150).front(), iid1);
   BOOST_CHECK_EQUAL(sis.by_size_range(11, 199).back(), iid5);
   BOOST_CHECK_EQUAL(sis.by_size_range(11, 200).back(), iid4);
}

}//namespace test
}//namespace vdj_pipe
