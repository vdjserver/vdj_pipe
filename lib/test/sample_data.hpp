/** @file "/vdj_pipe/lib/test/sample_data.hpp"
part of vdj_pipe project.
Distributed under GNU General Public License, Version 3; see doc/license.txt.
@date 2013 @author Mikhail K Levin
*******************************************************************************/
#ifndef SAMPLE_DATA_HPP_
#define SAMPLE_DATA_HPP_
#include <string>
#include "boost/preprocessor/stringize.hpp"
#include "boost/filesystem.hpp"

#ifndef SAMPLE_DATA_DIR
#error SAMPLE_DATA_DIR needs to be defined
#endif
#ifndef TEMPORARY_DIR
#error TEMPORARY_DIR needs to be defined
#endif

namespace vdj_pipe{ namespace test{

inline std::string sample_file_path(const std::string& name = "") {
   static const boost::filesystem::path path(BOOST_PP_STRINGIZE(SAMPLE_DATA_DIR));
   return canonical(path / name).string();
}

inline std::string temp_file_path(const std::string& name = "") {
   static const boost::filesystem::path path(BOOST_PP_STRINGIZE(TEMPORARY_DIR));
   return canonical(path / name).string();
}

}//namespace test
}//namespace vdj_pipe
#endif /* SAMPLE_DATA_HPP_ */
