/** @file "/vdj_pipe/lib/test/sample_sequences.hpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef SAMPLE_SEQUENCES_HPP_
#define SAMPLE_SEQUENCES_HPP_

namespace vdj_pipe{  namespace test{

const std::string seq1 = "TTGACTACTGGGGCCAGGGAACCCTGGTCACCGT";
const std::string seq1c = "ACGGTGACCAGGGTTCCCTGGCCCCAGTAGTCAA";

const std::string seq2 =
         "ACGGTGACCAGGGTTCCTTGTGCACTCAGAGGTGAGGGCTCACAGAGTTCCTCTCTGGTT";

const std::string seq3 =
         "TCACAGAGTTCCTCTCTGGTTTCCAGGAAAGGTAACTGCAGTAATCTTGGTGATGAGAATACAGAAGTG"
         "TACAGAGGTCAGGCCACATCCT";

const std::string seq4 =
         seq2 + "TCCAGGAAA"
         "GGTAACTGCAGTAATCTTGGTGATGAGAATATCCTCCAGTGCTGGCCTATTATAGAGTTTACATATGAA"
         "ATTGTCACTGCAATTCACAATCTACTCTTTCACACAGAAGTG";

const std::string seq5 = seq3 + seq2;

const std::string seq6 =
         "CGTCGGTAGTCCACGTTCCCATCAGCCCCGATCAAGTAGTCTGNCNNTTATANACATCTGACGCTG"
         "CCGACGAGCGATCTAGTGTAGATCTCGGTGGTCGCCTCCTCATTAAAAAAAACCTCCTCTTCCCCT"
         "CCCCCCCTCCCCCTCCCCCCCCCCCCTCCCCTCCCTTCTCCCCCCCCCTCCTTCCCCTCTCCTCTC"
         "CCTCCCCCTCCCCCCCCCCCCCTCCCACTACAGCCCTCCTCCCCCCCCCCCC"
         ;

const std::string seq7 =
         "tcggcagggcacagtcacatcctggctggaattcgtgtagtgcttcacgtggcatgtcacggacttgcc"
         "gtctgggcactgtgtggccggcagggtcagctggctgctcgtggtgtacaggtccccggaggcatcctg"
         "gctaggtgggaagtttctggcggtcacgttctgtccgctttcgctccaggtcacactgagtggctcctg"
         "ggggaagaagccctggaccaggcatgcgacgaccacgttcccatcttggggggtgctgtcgaggctcag"
         "cgggaagaccttggggctggtcggggatgc"
         ;

const std::string filter1 = "{ \"type\": \"min_length\", \"min\": 100 }";
const std::string filter2 = "{ \"type\": \"min_quality\", \"min\": 10 }";
const std::string filter3 = "{ \"type\": \"nucleotides\" }";
const std::string filter4 = "{ \"type\": \"mean_quality\", \"min\": 30 }";
const std::string filter5 = "{ \"type\": \"min_quality_window\", "
         "\"min_quality\": 50, \"min_length\": 300 }";
const std::string filter6 = "{ \"type\": \"homopolymer\", \"max\": 10 }";

}//namespace test
}//namespace vdj_pipe
#endif /* SAMPLE_SEQUENCES_HPP_ */
