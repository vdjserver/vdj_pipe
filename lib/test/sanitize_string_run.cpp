/** @file "/vdj_pipe/lib/test/sanitize_string_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE sanitize_string_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"

#include "vdj_pipe/sanitize_string.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   std::string s1 = "blah";
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah" "\"");

   s1.push_back(10);
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah\\n" "\"");

   s1.push_back(0);
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah\\n\\x0" "\"");

   s1.push_back(static_cast<char>(200));
   BOOST_CHECK_EQUAL(sanitize(s1, 50), "\"" "blah\\n\\x0\\xc8" "\"");

   BOOST_CHECK_EQUAL(sanitize(s1, 4), "\"" "blah..." "\"");
}

}//namespace test
}//namespace vdj_pipe
