/** @file "/vdj_pipe/lib/test/sequence_merge_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE sequence_merge_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_data.hpp"
#include "external/ssw_cpp.h"
#include <iostream>
#include "vdj_pipe/sequence_merge.hpp"
#include "vdj_pipe/parser_fastq.hpp"
#include "vdj_pipe/sequence_transform.hpp"
#include "vdj_pipe/sequence_file.hpp"

namespace vdj_pipe{ namespace test{

const std::string s1 = /*"AAAAAAAAA"*/ "CCA" "ATGCCACAAAACATCTGTCTCTAACTGGTGTGT" "GTGT";
const std::string s2 = "CCA" "GCCCAAAATCTGTTTTAATGGTGGATTTGTGT"  "AAAAAAAAA";

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   StripedSmithWaterman::Aligner aligner;
   StripedSmithWaterman::Filter filter;
   StripedSmithWaterman::Alignment al;
   aligner.Align(s1.data(), s2.data(), s2.size(), filter, &al);

   std::cout
   << s1 << '\n'
   << s2 << '\n'
   << "score: " << al.sw_score << ' ' << al.mismatches << '\n'
   << al.query_begin << '-' << al.query_end << '\n'
   << al.ref_begin << '-' << al.ref_end << '\n'
   << al.cigar_string
   << std::endl;
//   copy(al.cigar.begin(), al.cigar.end(), std::ostream_iterator<unsigned>(std::cout, ", "));
   std::cout << std::endl;

}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Merge_result p;
   merge(
            s1, Seq_qual_record::quality(s1.size(),0),
            s2, Seq_qual_record::quality(s2.size(),1),
            10,
            p
   );
   //CCAATGCCACAAAACATCTGTCTCTAACTGGTGTG  TGTGT
   //CCA  GCC CAAAA  TCTGTTTT AA TGGTGGATTTGTGT  AAAAAAAAA
   //CCA  GCC CAAAA  TCTGTTTT AA TGGTGGATTTGTGT  AAAAAAAAA
   //CCA  GCC CAAAA  TCTGTTCT AA TGGTGTG  TGTGT  AAAAAAAAA
//   BOOST_CHECK_EQUAL(p.seq_, "CCAGCCCAAAATCTGTTTTAATGGTGGATTTGTGTAAAAAAAAA");
   BOOST_CHECK_EQUAL(p.qual_.size(), p.seq_.size());
}

const std::string s5 = "GGCTTCCCATTCATTCAGGAACCACCTTCTGGTGATTTGCAAGAACGCGTAC"
         "TTATTCGCCACCATGATTATGACCAGTGTTTCCAGTCCGTTCAGTTGTTGCAGTGGAATAGTCAGG"
         "TTAAATTTAATGTGA";

const std::string s6 = "CCGTTTA";

const std::string s7 = "CGCAATCTGCCGACCACTCGCGATTCAATCATGACTTCGTGAT"
         "AAAAGATTGAGTGTGAGGTTATAACGCCGAAGCGGTAAAAATTTTAATTTTTGCCGCTGAGGGGTT";

const std::string s8 = "GACCAAGCGAAGCGCGGTAGGTTTTCTGCTTAGTAGTTTAATCATTTTTCAG"
         "ACTTTTATTTCTCGCCATAATTCAAACTTTTTTTCTGATAAGCTGGTTCTCACTTCTGTTACTCCA"
         "GCTTCTTCGGCACCT";

const std::string s10 = s5 + s6 + "T" + s7;

const std::string s11 = s6 + "C" + s7 + s8;

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   StripedSmithWaterman::Aligner aligner;
   StripedSmithWaterman::Filter filter;
   StripedSmithWaterman::Alignment al;

   aligner.Align(s10.data(), s11.data(), s11.size(), filter, &al);

   std::cout
   << s10 << '\n'
   << s11 << '\n'
   << "score: " << al.sw_score << ' ' << al.mismatches << '\n'
   << al.query_begin << '-' << al.query_end << '\n'
   << al.ref_begin << '-' << al.ref_end << '\n'
   << al.cigar_string
   << std::endl;
//   copy(al.cigar.begin(), al.cigar.end(), std::ostream_iterator<unsigned>(std::cout, ", "));
   std::cout << std::endl;
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   Merge_result p;
   merge(
            s10, Seq_qual_record::quality(s10.size(),0),
            s11, Seq_qual_record::quality(s11.size(),1),
            10,
            p
   );
//   BOOST_CHECK_EQUAL(p.seq_, s5 + s6 + "C" + s7 + s8);
   BOOST_CHECK_EQUAL(p.qual_.size(), p.seq_.size());

   merge(
            s10, Seq_qual_record::quality(s10.size(),1),
            s11, Seq_qual_record::quality(s11.size(),0),
            10,
            p
   );
   BOOST_CHECK_EQUAL(p.seq_, s5 + s6 + "T" + s7 + s8);
}

/**@test homopolymer
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case05 ) {
   const std::string id = "M00922:18:000000000-A3HRM:1:1101:25788:16068";
   Seq_file sf1(sample_file_path("smpl01_frw.fastq"));
//   std::ifstream ifs1(sf1.path().c_str());
   Parser_fastq pf1(sf1);
   Seq_file sf2(sample_file_path("smpl01_rev.fastq"));
//   std::ifstream ifs2(sf2.path().c_str());
   Parser_fastq pf2(sf2);

   do{
      if( pf1.get_id() != id ) {
         pf2.get_id();
         continue;
      }
      const std::string s1 = pf1.get_sequence();
      const Seq_qual_record::quality v1 = pf1.get_qual();
      std::string s2 = pf2.get_sequence();
      s2 = complement(s2);
      Seq_qual_record::quality v2 = pf2.get_qual();
      std::reverse(v2.begin(), v2.end());
      std::cout
      << s1 << '\n'
      << s2 << '\n'
      << std::endl;
      std::copy(v1.begin(), v1.end(), std::ostream_iterator<int>(std::cout, ","));
      std::cout << std::endl;
      std::copy(v2.begin(), v2.end(), std::ostream_iterator<int>(std::cout, ","));
      std::cout << std::endl;

      StripedSmithWaterman::Aligner aligner;
      StripedSmithWaterman::Filter filter;
      StripedSmithWaterman::Alignment al;

      aligner.Align(s1.data(), s2.data(), s2.size(), filter, &al);

      std::cout
      << "score: " << al.sw_score << ' ' << al.mismatches << '\n'
      << "s1: " << al.query_begin << '-' << al.query_end << '\n'
      << "s2: " << al.ref_begin << '-' << al.ref_end << '\n'
      << al.cigar_string
      << std::endl;

      Merge_result p;
      merge(s1, v1, s2, v2, 10, p);
      std::cout << p.seq_ << std::endl;

      pf1.next_record();
      pf2.next_record();

   }while(pf1.has_next() && pf2.has_next());

}

}//namespace test
}//namespace vdj_pipe
