/** @file "/vdj_pipe/lib/test/sequence_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE sequence_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/sequence_fls.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   BOOST_CHECK_EQUAL((Seq_fls<4, char>::length()), 4U);
   BOOST_CHECK_EQUAL((Seq_fls<4, boost::uint_least32_t>::length()), 16U);
   BOOST_CHECK_EQUAL((Seq_fls<4, boost::uint_least64_t>::length()), 32U);

   Seq_fls<> sf;
   sf.set(0, Adenine);
   BOOST_CHECK_EQUAL(sf[0], Adenine);
   sf.set(0, Guanine);
   BOOST_CHECK_EQUAL(sf[0], Guanine);
   sf.set(1, Adenine);
   BOOST_CHECK_EQUAL(sf[0], Guanine);
   BOOST_CHECK_EQUAL(sf[1], Adenine);
   sf.set(1, Guanine);
   BOOST_CHECK_EQUAL(sf[0], Guanine);
   BOOST_CHECK_EQUAL(sf[1], Guanine);
   sf.set(2, Cytosine);
   BOOST_CHECK_EQUAL(sf[0], Guanine);
   BOOST_CHECK_EQUAL(sf[1], Guanine);
   BOOST_CHECK_EQUAL(sf[2], Cytosine);
   sf.set(3, Thymine);
   BOOST_CHECK_EQUAL(sf[0], Guanine);
   BOOST_CHECK_EQUAL(sf[1], Guanine);
   BOOST_CHECK_EQUAL(sf[2], Cytosine);
   BOOST_CHECK_EQUAL(sf[3], Thymine);
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Seq_fls<> sf1("atgctcgtgatggttg");
   BOOST_CHECK_EQUAL(sf1[0], Adenine);
   BOOST_CHECK_EQUAL(sf1[1], Thymine);
   BOOST_CHECK_EQUAL(sf1[2], Guanine);
   BOOST_CHECK_EQUAL(sf1[3], Cytosine);
   BOOST_CHECK_EQUAL(sf1[4], Thymine);
   BOOST_CHECK_EQUAL(sf1[5], Cytosine);
   BOOST_CHECK_EQUAL(sf1[6], Guanine);
   BOOST_CHECK_EQUAL(sf1[7], Thymine);
   BOOST_CHECK_EQUAL(sf1[8], Guanine);
   BOOST_CHECK_EQUAL(sf1[9], Adenine);
   BOOST_CHECK_EQUAL(sf1[10], Thymine);
   BOOST_CHECK_EQUAL(sf1[11], Guanine);
   BOOST_CHECK_EQUAL(sf1[12], Guanine);
   BOOST_CHECK_EQUAL(sf1[13], Thymine);
   BOOST_CHECK_EQUAL(sf1[14], Thymine);
   BOOST_CHECK_EQUAL(sf1[15], Guanine);

   Seq_fls<> sf2;
   sf2 = boost::string_ref("atgctcgtgatggttg");
   BOOST_CHECK(sf1 == sf2);

   Seq_fls<> sf3("atgctcgtgatggttt");
   BOOST_CHECK(sf2 != sf3);
}

}//namespace test
}//namespace vdj_pipe
