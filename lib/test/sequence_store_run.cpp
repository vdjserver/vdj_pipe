/** @file "/vdj_pipe/lib/test/sequence_store_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE sequence_store_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "boost/assign/list_of.hpp"
namespace bass = boost::assign;

#include "test/sample_data.hpp"
#include "test/sample_sequences.hpp"
#include "vdj_pipe/sequence_store.hpp"

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Seq_store ss;
   const Read_id iid1(1);
   const Seq_id sid1 = ss.insert(iid1, seq1);
   BOOST_CHECK(sid1);
   const Read_id iid1c(11);
   ss.insert(iid1c, seq1c);
   const Read_id iid2(2);
   const Seq_id sid2 = ss.insert(iid2, seq2);
   BOOST_CHECK( ss.maps_to(iid2).find(sid2) );
   const Read_id iid3(3);
   ss.insert(iid3, seq3);
   const Read_id iid4(4);
   const Seq_id sid4 = ss.insert(iid4, seq4);
   const Read_id iid5(5);
   const Seq_id sid5 = ss.insert(iid5, seq5);
   const Read_id iid5a(51);
   const Seq_id sid5a = ss.insert(iid5a, seq5);
   BOOST_CHECK_EQUAL(ss.size(), 6U);
   BOOST_CHECK_EQUAL(sid5, sid5a);
   BOOST_CHECK(ss.maps_to(iid5).find(sid5) );
   BOOST_CHECK(ss.maps_from(sid5).find(iid5) );
   BOOST_CHECK(ss.maps_from(sid5).find(iid5a) );

   //seq2 is a subsequence of seq4 and seq5
   std::vector<super_seq> v = bass::list_of(super_seq(sid4,0))
   (super_seq(sid5, seq3.size()))
   ;
   ss.remove_subsequence(sid2, v);
   BOOST_CHECK_EQUAL(ss.size(), 5U);
   BOOST_CHECK( ! ss.maps_to(iid2).find(sid2) );
   BOOST_CHECK(   ss.maps_to(iid2).find(sid4) );
   BOOST_CHECK(   ss.maps_to(iid2).find(sid5) );
   BOOST_CHECK( ss[sid2].empty() );
}


}//namespace test
}//namespace vdj_pipe
