/** @file "/vdj_pipe/lib/test/sequence_transform_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE sequence_transform_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/sequence_transform.hpp"
#include "test/sample_sequences.hpp"

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   BOOST_CHECK_EQUAL(complement("aggtc"), "gacct");
   BOOST_CHECK_EQUAL(complement(seq1), seq1c);
   BOOST_CHECK_EQUAL(
            transform(seq1, sequence_interval(10, 15), false),
            "GGGCC"
   );
   BOOST_CHECK_EQUAL(
            transform(seq1, sequence_interval(10, 16), true),
            "TGGCCC"
   );
}

}//namespace test
}//namespace vdj_pipe
