/** @file "/vdj_pipe/lib/test/short_seq_map_run.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE short_seq_map_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "sequence_map_short.hpp"

namespace vdj_pipe{ namespace test{

const std::string m1 = "mid1";
const std::string m2 = "mid2";
const std::string m3 = "mid3";
const std::string s1 = "ATGGA";
const std::string s2 = "CTGGA";
const std::string s3 = "ATGGT";

/**@test 
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Seq_map_short mm(false);
   const Mid_id id1 = mm.insert(m1, s1);
   BOOST_CHECK_THROW(mm.insert(m2, "atgga"), Seq_map_short::Err); //duplicate seq
   const Mid_id id2 = mm.insert(m2, s2);
   const Mid_id id3 = mm.insert(m3, s3);

   BOOST_CHECK_EQUAL(mm.name(id1), m1);
   BOOST_CHECK_EQUAL(mm.seq(id1).to_string(mm.seq_size()), s1);
   BOOST_CHECK_EQUAL(mm.name(id2), m2);
   BOOST_CHECK_EQUAL(mm.seq(id2).to_string(mm.seq_size()), s2);
   BOOST_CHECK_EQUAL(mm.name(id3), m3);
   BOOST_CHECK_EQUAL(mm.seq(id3).to_string(mm.seq_size()), s3);

   Seq_map_short::match_type m;
   scoring_matrix_t const& sm = scoring_matrix<2,-2,1,0>();

   BOOST_CHECK( mm.find_closest("nnnnn", m, sm) );
   BOOST_CHECK_EQUAL(m.score1(), 0);
   BOOST_CHECK_EQUAL(m.score1(), m.score2());

   BOOST_CHECK( mm.find_closest("atnnn", m, sm) );
   BOOST_CHECK_EQUAL(m.score1(), 4);
   BOOST_CHECK(m.id1() == id1 || m.id1() == id3);
   BOOST_CHECK(m.id2() == id1 || m.id2() == id3);
   BOOST_CHECK_EQUAL(m.score1(), m.score2());

   BOOST_CHECK( mm.find_closest("atggn", m, sm) );
   BOOST_CHECK_EQUAL(m.score1(), 8);
   BOOST_CHECK(m.id1() == id1 || m.id1() == id3);
   BOOST_CHECK(m.id2() == id1 || m.id2() == id3);
   BOOST_CHECK_EQUAL(m.score1(), m.score2());

   BOOST_CHECK( ! mm.find_closest("cnnna", m, sm) );
   BOOST_CHECK_EQUAL(m.score1(), 8);
   BOOST_CHECK_EQUAL(m.score1(), m.score2());

   m = Seq_map_short::match_type();

   BOOST_CHECK( mm.find_closest("cnnna", m, sm) );
   BOOST_CHECK_EQUAL(m.score1(), 4);
   BOOST_CHECK_EQUAL(m.id1(), id2);
   BOOST_CHECK_GT(m.score1(), m.score2());

   BOOST_CHECK( mm.find_closest("atnna", m, sm) );
   BOOST_CHECK_EQUAL(m.score1(), 6);
   BOOST_CHECK_EQUAL(m.score2(), 4);
   BOOST_CHECK_EQUAL(m.id1(), id1);
   BOOST_CHECK_EQUAL(m.id2(), id2);
}

/**@test copying
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Seq_map_short mm(false);
   /*const Mid_id id1 =*/ mm.insert(m1, s1);
   /*const Mid_id id2 =*/ mm.insert(m2, s2);
   /*const Mid_id id3 =*/ mm.insert(m3, s3);

   Seq_map_short mm2(false);
   mm2 = mm;

   BOOST_REQUIRE(mm2.find_exact(s1));
   BOOST_REQUIRE(mm2.find_exact(s2));
   BOOST_REQUIRE(mm2.find_exact(s3));
}

}//namespace test
}//namespace vdj_pipe
