/** @file "/vdj_pipe/lib/test/suffix_tree_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE suffix_tree_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "test/sample_sequences.hpp"
#include <iostream>
#include "boost/foreach.hpp"
#include "vdj_pipe/gdst/gdst.hpp"
#include "vdj_pipe/gdst/print_gdst.hpp"
#include "vdj_pipe/sequence_store.hpp"

namespace vdj_pipe{ namespace test{

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   Seq_store ss;
   const Seq_id sid1 = ss.insert(Read_id(1), "ATGATCATGCCC");
   const Seq_id sid2 = ss.insert(Read_id(2), "acaccaccaaccacc");
   const Seq_id sid3 = ss.insert(Read_id(3), "caaacac");
   gdst::Gdst gst(ss.sequence_map());
   gst.insert(sid1);
   gst.insert(sid2);
   gst.insert(sid3);

//   dot(std::cout, gst, false) << std::endl;

//   BOOST_ERROR("test");
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   Seq_store ss;
   const Seq_id sid1 = ss.insert(Read_id(1), seq1);
   gdst::Gdst gst(ss.sequence_map());
   gst.insert(sid1);
   BOOST_CHECK_EQUAL( gst.find_longest("ACC").seq_.size(), 2u );
   BOOST_CHECK_EQUAL( gst.find_longest("ACCGT").seq_.size(), 1u );
   BOOST_CHECK_EQUAL( gst.find_longest("ACCCT").seq_.size(), 1u );
   gdst::Common_subseq v = gst.find_longest("acc");
//   boost::copy(v, std::ostream_iterator<Seq_id>(std::cout, ", "));
//   std::cout << std::endl;
//   std::cout << gst << std::endl;
//   BOOST_ERROR("test");
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   Seq_store ss;
   ss.insert(Read_id(1), seq1);
   ss.insert(Read_id(2), seq2);
   ss.insert(Read_id(3), seq3);
   ss.insert(Read_id(4), seq4);
   gdst::Gdst gst(ss.sequence_map());
   std::size_t n_nucleotides = 0;
   BOOST_FOREACH(const Seq_id sid, ss) {
      gst.insert(sid);
      n_nucleotides += ss[sid].size();
   }
   BOOST_CHECK_EQUAL( gst.find_longest("TGAGGG").seq_.size(), 2u );
   BOOST_CHECK_EQUAL( gst.find_longest("GGGGCCAGGGAAC").seq_.size(), 1u );

   std::size_t n_leaf = 0, n_branch = 0, n_bl = 0;
   for(vdj_pipe::gdst::Depth_iter i = gst.depth_first(); ! i.at_end(); i.next()) {
      vdj_pipe::gdst::Branch const& b =gst[i.id()];
      if( b.c_ ) {
         ++n_branch;
         if( b.leaf_ ) ++n_bl;
      } else if( b.leaf_ ) ++n_leaf;
   }

   std::cout
   << "nucleotides: " << n_nucleotides << '\n'
   << "unsigned: " << sizeof(unsigned) << '\n'
   << "set: " << sizeof(vdj_pipe::detail::Vector_set<Seq_id>) << '\n'
   << "Branch: " << sizeof(vdj_pipe::gdst::Branch) << '\n'
   << "space: " << gst.size() * sizeof(vdj_pipe::gdst::Branch) << '\n'
   << "nodes: " << gst.size() << '\n'
   << "leafs: " << n_leaf << " branches: " << n_branch << " both: " << n_bl << '\n'
   ;
//   dot(std::cout, gst, false) << std::endl;
//   BOOST_ERROR("test");
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   Seq_store ss;
   ss.insert(Read_id(1), seq1);
   ss.insert(Read_id(2), seq2);
   ss.insert(Read_id(3), seq3);
   ss.insert(Read_id(4), seq4);
   gdst::Gdst gst(ss.sequence_map());
   BOOST_FOREACH(const Seq_id sid, ss) {
      gst.insert(sid);
   }

   gdst::Match m;
   m = gst.find("TGGTCACCGT", gst.root());
   BOOST_CHECK( m.at_pattern_end_ );
   BOOST_CHECK( m.at_tree_end_ );
   BOOST_CHECK( ! m.mismatch_ );
   BOOST_CHECK( ! m.next_ );
   BOOST_CHECK_EQUAL( m.i_, 10U);

   m = gst.find("TGGTCACCGTT", gst.root());
   BOOST_CHECK( ! m.at_pattern_end_ );
   BOOST_CHECK( m.at_tree_end_ );
   BOOST_CHECK( ! m.mismatch_ );
   BOOST_CHECK( ! m.next_ );
   BOOST_CHECK_EQUAL( m.i_, 10U);

   m = gst.find("TGGTCACCG", gst.root());
   BOOST_CHECK( m.at_pattern_end_ );
   BOOST_CHECK( ! m.at_tree_end_ );
   BOOST_CHECK( ! m.mismatch_ );
   BOOST_CHECK( m.next_ );
   BOOST_CHECK_EQUAL( m.i_, 9U);

   m = gst.find("GGTGACCAGGGTTCCTTGTG", gst.root());
   BOOST_CHECK( m.at_pattern_end_ );
   BOOST_CHECK( ! m.at_tree_end_ );
   BOOST_CHECK( ! m.mismatch_ );
   BOOST_CHECK( m.next_ );
   BOOST_CHECK_EQUAL( m.i_, 20U);

   m = gst.find("GGTGACgAGGGTTCCTTGTG", gst.root());
   BOOST_CHECK( ! m.at_pattern_end_ );
   BOOST_CHECK( ! m.at_tree_end_ );
   BOOST_CHECK( m.mismatch_ );
   BOOST_CHECK( m.next_ );
   BOOST_CHECK_EQUAL( m.i_, 6U);
   BOOST_CHECK_EQUAL( m.pn_, Guanine);
   BOOST_CHECK_EQUAL( m.en_, Cytosine);

   m = gst.find("GGTGACgAGGGTTCCTTGTG", gst.root(), 5);
   BOOST_CHECK( ! m.at_pattern_end_ );
   BOOST_CHECK( ! m.at_tree_end_ );
   BOOST_CHECK( m.mismatch_ );
   BOOST_CHECK( m.next_ );
   BOOST_CHECK_EQUAL( m.i_, 6U);
   BOOST_CHECK_EQUAL( m.pn_, Guanine);
   BOOST_CHECK_EQUAL( m.en_, Cytosine);
   print(std::cout, m.curr_, gst) << std::endl;
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case05 ) {
   Seq_store ss;
   const Seq_id sid1 = ss.insert(Read_id(1), seq1);
   const Seq_id sid2 = ss.insert(Read_id(2), seq2);
   const Seq_id sid3 = ss.insert(Read_id(3), seq3);
   const Seq_id sid4 = ss.insert(Read_id(4), seq4);
   gdst::Gdst gst(ss.sequence_map());
   BOOST_FOREACH(const Seq_id sid, ss) {
      gst.insert(sid);
   }

//   dot(std::cout, gst, false) << std::endl;

   gdst::Common_subseq cs;
   cs = gst.find_longest("ggCAGGGTTCCTTGTGCACTCAGAGGTGAGG");
   BOOST_CHECK_EQUAL( cs.len_, 0U);
   BOOST_CHECK( cs.seq_.empty() );

   cs = gst.find_longest("ggCAGGGTTCCTTGTGCACTCAGAGGTctatt", 10);
   BOOST_CHECK_EQUAL( cs.len_, 25U);
   BOOST_CHECK_EQUAL( cs.start_, 2U);
   BOOST_REQUIRE( cs.seq_.size() );
   BOOST_CHECK_EQUAL( cs.seq_.size(), 2U );
   BOOST_CHECK( ! cs.seq_.find(sid1) );
   BOOST_CHECK( cs.seq_.find(sid2) );
   BOOST_CHECK( ! cs.seq_.find(sid3) );
   BOOST_CHECK( cs.seq_.find(sid4) );
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case06 ) {
   Seq_store ss;
   const Seq_id sid1 = ss.insert(Read_id(1), seq1);
   const Seq_id sid1c = ss.insert(Read_id(5), seq1c);
   const Seq_id sid2 = ss.insert(Read_id(2), seq2);
   const Seq_id sid3 = ss.insert(Read_id(3), seq3);
   const Seq_id sid4 = ss.insert(Read_id(4), seq4);
   const Seq_id sid5 = ss.insert(Read_id(5), seq7);
   gdst::Gdst gst(ss.sequence_map());
   BOOST_FOREACH(const Seq_id sid, ss) {
      gst.insert(sid);
   }

   vdj_pipe::detail::Vector_set<Seq_id> vs;
   gst.find_overlaping(seq1c, vs, 10);
   BOOST_CHECK_EQUAL(vs.size(), 3u);
   BOOST_CHECK(vs.find(sid1c));
   BOOST_CHECK(vs.find(sid2));
   BOOST_CHECK(vs.find(sid4));

   vs.clear();
   gst.find_overlaping(seq1, vs, 6);
   BOOST_CHECK_EQUAL(vs.size(), 5u);
   BOOST_CHECK(vs.find(sid1));
   BOOST_CHECK(vs.find(sid1c));
   BOOST_CHECK(vs.find(sid2));
   BOOST_CHECK(vs.find(sid4));
   BOOST_CHECK( ! vs.find(sid3) );
   BOOST_CHECK(vs.find(sid5));

   vs.clear();
   gst.find_overlaping(seq6.substr(0,43), vs, 10);
   BOOST_CHECK_EQUAL(vs.size(), 1u);
   BOOST_CHECK(vs.find(sid5));
}

}//namespace test
}//namespace vdj_pipe
