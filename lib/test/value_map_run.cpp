/** @file "/vdj_pipe/lib/test/value_map_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE value_map_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{ namespace test{

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   value_variant vv1;

   BOOST_CHECK(is_blank(vv1));
   BOOST_CHECK_EQUAL(variable_type_str(vv1), "blank");
   vv1 = 42L;
   BOOST_CHECK( ! is_blank(vv1) );
   BOOST_CHECK_EQUAL(variable_type_str(vv1), "integer");
   BOOST_CHECK_EQUAL(vv1, value_variant(42L));
}

}//namespace test
}//namespace vdj_pipe
