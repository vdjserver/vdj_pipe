/** @file "/vdj_pipe/lib/test/value_names_run.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE value_names_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include "vdj_pipe/value_names.hpp"
#include "vdj_pipe/value_map.hpp"
#include "vdj_pipe/value_map_access_paired.hpp"

namespace vdj_pipe{ namespace test{

/**@test single read variable IDs
*******************************************************************************/
BOOST_AUTO_TEST_CASE( value_names_run_01 ) {
   Value_map vm;
   BOOST_CHECK_THROW(
            Value_ids_single::ensure(vm, Value_names::single()),
            base_exception
   );
   Value_ids_single vis1 = Value_ids_single::create(vm, Value_names::single());
   BOOST_CHECK(vm.value_id("read_id"));
   BOOST_CHECK_EQUAL(vm.name(vis1.trim()), "trim");
   Value_ids_single vis2 = Value_ids_single::ensure(vm, Value_names::single());
   BOOST_CHECK_EQUAL(vm.name(vis2.sequence_path()), Value_names::single()[5]);
}

/**@test paired reads variable IDs
*******************************************************************************/
BOOST_AUTO_TEST_CASE( value_names_run_02 ) {
   Value_map vm;
   const Value_ids_paired vip(vm);
   BOOST_CHECK_EQUAL(vm.name(vip.description_fwd()), "description_fwd");
   BOOST_CHECK_EQUAL(vm.name(vip.description_rev()), "description_rev");
   BOOST_CHECK_EQUAL(vm.name(vip.quality_fwd()), "quality_fwd");
   BOOST_CHECK_EQUAL(vm.name(vip.quality_path_rev()), "qual_file_path_rev");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( value_names_run_03 ) {
   Value_map vm;
   const Value_ids_emid vie(vm);
   BOOST_CHECK_EQUAL(vm.name(vie.emid_fwd()), "emid_fwd");
   BOOST_CHECK_EQUAL(vm.name(vie.emid_path_fwd()), "emid_file_path_fwd");
   BOOST_CHECK_EQUAL(vm.name(vie.emid_path_rev()), "emid_file_path_rev");
}

}//namespace test
}//namespace vdj_pipe
