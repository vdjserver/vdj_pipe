/** @file "/vdj_pipe/lib/test/value_variant_run.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#define BOOST_TEST_MODULE value_variant_run
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"

#include "vdj_pipe/value_variant.hpp"

namespace vdj_pipe{ namespace test{

/**@test 
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   value_variant vv;
   vv = parse_variant("12");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<long>::value);
   BOOST_CHECK_EQUAL(vv.type().name(), typeid(long).name());
   BOOST_CHECK_EQUAL(boost::get<long>(vv), 12);

   vv = parse_variant("1.0");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<double>::value);
   BOOST_CHECK_EQUAL(vv.type().name(), typeid(double).name());
   BOOST_CHECK_EQUAL(boost::get<double>(vv), 1.0);

   vv = parse_variant("true");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<bool>::value);
   BOOST_CHECK_EQUAL(vv.type().name(), typeid(bool).name());
   BOOST_CHECK_EQUAL(boost::get<bool>(vv), true);

   vv = parse_variant("false");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<bool>::value);
   BOOST_CHECK_EQUAL(vv.type().name(), typeid(bool).name());
   BOOST_CHECK_EQUAL(boost::get<bool>(vv), false);

   vv = parse_variant("blah");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<std::string>::value);
   BOOST_CHECK_EQUAL(vv.type().name(), typeid(std::string).name());
   BOOST_CHECK_EQUAL(boost::get<std::string>(vv), "blah");
}

/**@test
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   value_variant vv;
   vv = parse_variant("1A");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<std::string>::value);
   BOOST_CHECK_EQUAL(boost::get<std::string>(vv), "1A");

   vv = parse_variant("1E");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<std::string>::value);
   BOOST_CHECK_EQUAL(boost::get<std::string>(vv), "1E");

   vv = parse_variant("1E1");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<double>::value);
   BOOST_CHECK_EQUAL(boost::get<double>(vv), 10.0);

   vv = parse_variant("7E28N");
   BOOST_CHECK_EQUAL(vv.which(), Type_index<std::string>::value);
   BOOST_CHECK_EQUAL(boost::get<std::string>(vv), "7E28N");
}

}//namespace test
}//namespace vdj_pipe
