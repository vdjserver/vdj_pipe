/** @file "/vdj_pipe/lib/test/vector_set_test.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#define BOOST_TEST_MODULE vector_set_test
#include "boost/test/unit_test.hpp"
#include "test/exception_fixture.hpp"
#include <set>
#include <iostream>
#include "boost/unordered_set.hpp"
#include "boost/chrono/chrono.hpp"
#include "boost/chrono/chrono_io.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "boost/random/uniform_int_distribution.hpp"
#include "vdj_pipe/detail/vector_set.hpp"

namespace vdj_pipe{ namespace test{

typedef boost::chrono::high_resolution_clock clock_t;
typedef clock_t::time_point time_t;
typedef boost::chrono::duration<double, boost::ratio<1,1> > dur_t;
typedef boost::mt11213b rng;
typedef boost::random::uniform_int_distribution<unsigned> distribution;
const std::size_t n = 10L * 1000 * 1000;
const std::size_t sz = 230 * 1000;
const distribution dist(1, sz);

/**
At 230,000 elements Vector_set is approximately as fast as std::set;
pre-allocation of elements produces little, if any, acceleration;
unordered_set is over 4 times faster.
If elements are removed as well as inserted, performance of Vector_set and
std::set is same for 1,000 elements. In that case, unordered_set performs
2 times faster.
*/

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case01 ) {
   rng gen;
   vdj_pipe::detail::Vector_set<unsigned> set;
   const time_t t0 = clock_t::now();
   for(std::size_t i = 0; i != n; ++i) {
      set.insert(dist(gen));
//      set.erase(dist(gen));
   }
   const dur_t d0 = clock_t::now() - t0;
   std::cout
   << "Vector_set: "
   << d0 << ' ' << set.size()
   << std::endl;
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case02 ) {
   rng gen;
   vdj_pipe::detail::Vector_set<unsigned> set;
   set.reserve(sz);
   const time_t t0 = clock_t::now();
   for(std::size_t i = 0; i != n; ++i) {
      set.insert(dist(gen));
//      set.erase(dist(gen));
   }
   const dur_t d0 = clock_t::now() - t0;
   std::cout
   << "Vector_set (reserved): "
   << d0 << ' ' << set.size()
   << std::endl;
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case03 ) {
   rng gen;
   std::set<unsigned> set;
   const time_t t0 = clock_t::now();
   for(std::size_t i = 0; i != n; ++i) {
      set.insert(dist(gen));
//      set.erase(dist(gen));
   }
   const dur_t d0 = clock_t::now() - t0;
   std::cout
   << "std::set: "
   << d0 << ' ' << set.size()
   << std::endl;
}

/**
*******************************************************************************/
BOOST_AUTO_TEST_CASE( case04 ) {
   rng gen;
   boost::unordered_set<unsigned> set;
   const time_t t0 = clock_t::now();
   for(std::size_t i = 0; i != n; ++i) {
      set.insert(dist(gen));
//      set.erase(dist(gen));
   }
   const dur_t d0 = clock_t::now() - t0;
   std::cout
   << "boost::unordered_set: "
   << d0 << ' ' << set.size()
   << std::endl;

   BOOST_ERROR("test");
}

}//namespace test
}//namespace vdj_pipe
