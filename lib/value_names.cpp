/** @file "/vdj_pipe/lib/value_names.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/value_names.hpp"

#include <list>
#include "boost/assign/list_of.hpp"
namespace ba = boost::assign;
#include "boost/foreach.hpp"

#include "keywords_variable.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe{

/*
*******************************************************************************/
Value_names::single_read_names const& Value_names::single() {
   static const single_read_names n = {{
            kwds::Single::description(),
            kwds::Single::sequence(),
            kwds::Single::quality(),
            kwds::Single::trim(),
            kwds::Single::is_reverse(),
            kwds::Single::seq_file_path(),
            kwds::Single::qual_file_path(),
            ""
   }};
   return n;
}

/*
*******************************************************************************/
Value_names::single_read_names const& Value_names::forward() {
   static const single_read_names n = {{
            kwds::Forward::description_fwd(),
            kwds::Forward::sequence_fwd(),
            kwds::Forward::quality_fwd(),
            kwds::Forward::trim_fwd(),
            "",
            kwds::Forward::seq_file_path_fwd(),
            kwds::Forward::qual_file_path_fwd(),
            kwds::Forward::forward()
   }};
   return n;
}

/*
*******************************************************************************/
Value_names::single_read_names const& Value_names::reverse() {
   static const single_read_names n = {{
            kwds::Reverse::description_rev(),
            kwds::Reverse::sequence_rev(),
            kwds::Reverse::quality_rev(),
            kwds::Reverse::trim_rev(),
            "",
            kwds::Reverse::seq_file_path_rev(),
            kwds::Reverse::qual_file_path_rev(),
            kwds::Reverse::reverse()
   }};
   return n;
}

/*
*******************************************************************************/
Value_names::single_read_names const& Value_names::merged() {
   static const single_read_names n = {{
            kwds::Forward::description_fwd(),
            kwds::Merged::sequence_merged(),
            kwds::Merged::quality_merged(),
            kwds::Merged::trim_merged(),
            "",
            kwds::Forward::seq_file_path_fwd(),
            kwds::Forward::qual_file_path_fwd(),
            kwds::Forward::forward()
   }};
   return n;
}

/*
*******************************************************************************/
Value_ids_single Value_ids_single::create(
         Value_map& vm,
         Value_names::single_read_names const& names
) {
   Value_ids_single vi;
   vi.is_reverse_ = names.back() == Value_names::reverse().back();
   for(std::size_t i = 0; i != names.size() - 1; ++i) {
      vi.ida_[i] = vm.insert_new_name(names[i]);
   }
   return vi;
}

/*
*******************************************************************************/
Value_ids_single Value_ids_single::ensure(
         Value_map const& vm,
         Value_names::single_read_names const& names
) {
   Value_ids_single vi;
   vi.is_reverse_ = names.back() == Value_names::reverse().back();
   for(std::size_t i = 0; i != names.size() - 1; ++i) {
      vi.ida_[i] = vm.value_id(names[i]);
   }
   return vi;
}

/*
*******************************************************************************/
Value_ids_single::Value_ids_single(
         Value_map& vm,
         Value_names::single_read_names const& names
)
: is_reverse_(names.back() == Value_names::reverse().back())
{
   for(std::size_t i = 0; i != names.size() - 1; ++i) {
      ida_[i] = vm.insert_name(names[i]);
   }
}

/*
*******************************************************************************/
Value_ids_paired::Value_ids_paired(Value_map& vm)
: ida_()
{
   ida_[descr_fwd]      = vm.insert_new_name(kwds::Forward::description_fwd());
   ida_[descr_rev]      = vm.insert_new_name(kwds::Reverse::description_rev());
   ida_[seq_fwd]        = vm.insert_new_name(kwds::Forward::sequence_fwd());
   ida_[seq_rev]        = vm.insert_new_name(kwds::Reverse::sequence_rev());
   ida_[qual_fwd]       = vm.insert_new_name(kwds::Forward::quality_fwd());
   ida_[qual_rev]       = vm.insert_new_name(kwds::Reverse::quality_rev());
   ida_[trim_fwd]       = vm.insert_new_name(kwds::Forward::trim_fwd());
   ida_[trim_rev]       = vm.insert_new_name(kwds::Reverse::trim_rev());
   ida_[seq_path_fwd]   = vm.insert_new_name(kwds::Forward::seq_file_path_fwd());
   ida_[seq_path_rev]   = vm.insert_new_name(kwds::Reverse::seq_file_path_rev());
   ida_[qual_path_fwd]  = vm.insert_new_name(kwds::Forward::qual_file_path_fwd());
   ida_[qual_path_rev]  = vm.insert_new_name(kwds::Reverse::qual_file_path_rev());
}

/*
*******************************************************************************/
Value_ids_emid::Value_ids_emid(Value_map& vm)
: ida_()
{
   ida_[emid_fwd_]      = vm.insert_new_name(kwds::Emid::emid_fwd());
   ida_[emid_rev_]      = vm.insert_new_name(kwds::Emid::emid_rev());
   ida_[emid_path_fwd_] = vm.insert_new_name(kwds::Emid::emid_file_path_fwd());
   ida_[emid_path_rev_] = vm.insert_new_name(kwds::Emid::emid_file_path_rev());

}

}//namespace vdj_pipe
