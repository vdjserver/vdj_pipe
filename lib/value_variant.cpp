/** @file "/vdj_pipe/lib/value_variant.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/value_variant.hpp"

#include "boost/spirit/include/qi.hpp"
namespace qi = boost::spirit::qi;

namespace vdj_pipe{

/*
*******************************************************************************/
value_variant parse_variant(std::string const& s) {
   static const qi::real_parser<double, qi::strict_real_policies<double> >
   strict_double;
   std::string::const_iterator begin = s.begin(), end = s.end();
   typedef qi::rule<std::string::const_iterator, value_variant()> rule_t;
   static const rule_t r =
            qi::bool_ | strict_double | qi::long_;
   value_variant vv;
   if( ! qi::parse(begin, end, r, vv) || begin != end ) vv = s;
   return vv;
}

}//namespace vdj_pipe
