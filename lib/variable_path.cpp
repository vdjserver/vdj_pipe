/** @file "/vdj_pipe/lib/variable_path.cpp" 
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2014
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/variable_path.hpp"

#include <sstream>

namespace vdj_pipe{

/*
*******************************************************************************/
void path_decompose(
         std::string const& path,
         std::vector<std::string>& tv,
         std::vector<std::string>& nv
) {
   typedef base_exception Err;
   enum {templ, name};
   int state = templ;
   std::size_t pos = 0;
   for(std::size_t i = 0; i != path.size(); ++i) {
      if( path[i] == '{' ) {
         if( state == templ ) {
            tv.push_back(path.substr(pos, i - pos));
            pos = i + 1;
            state = name;
            continue;
         }
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("non-matching \"{\"")
                  << Err::str1_t(sanitize(path))
                  << Err::int1_t(i)
         );
      }

      if( path[i] == '}' ) {
         if( state == name ) {
            nv.push_back(path.substr(pos, i - pos));
            pos = i + 1;
            state = templ;
            continue;
         }
         BOOST_THROW_EXCEPTION(
                  Err()
                  << Err::msg_t("non-matching \"}\"")
                  << Err::str1_t(sanitize(path))
                  << Err::int1_t(i)
         );
      }
   }
   if( state == templ ) {
      tv.push_back(path.substr(pos, path.size() - pos));
   } else BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("\"}\" not found")
      << Err::str1_t(sanitize(path))
   );
}

/*
*******************************************************************************/
std::string path_assemble(
         std::vector<std::string> const& templ,
         detail::Queable_ofstream_types::val_ref_vector const& vals
) {
   typedef base_exception Err;
   if( vals.size() + 1 != templ.size() ) BOOST_THROW_EXCEPTION(
      Err()
      << Err::msg_t("wrong number of values")
      << Err::int1_t(vals.size())
      << Err::int2_t(templ.size())
   );
   std::ostringstream os;
   for(std::size_t i = 0; i != vals.size(); ++i) {
      os << templ[i] << boost::unwrap_ref(vals[i]);
   }
   os << templ.back();
   return os.str();
}

}//namespace vdj_pipe
