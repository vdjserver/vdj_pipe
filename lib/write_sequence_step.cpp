/** @file "/vdj_pipe/lib/write_sequence_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/write_sequence_step.hpp"

#include "boost/property_tree/ptree.hpp"

#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/print_sequence.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(
   kwds,
   (out_path)
   (trimmed)
   (reverse_complemented)
   (skip_empty)
   (unset_value)
);

/*
*******************************************************************************/
Write_seq::Write_seq(
         Vm_access_single const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: detail::Step_base_single(vma, pt, pe),
  fov_(
           pe.output().path(pt.get<std::string>(kwds::out_path())),
           pt.get(kwds::unset_value(), File_ostream_variant::skip_empty()),
           vma,
           ""
  ),
  n_seq_(0),
  trim_(pt.get(kwds::trimmed(), true)),
  reverse_(pt.get(kwds::reverse_complemented(), true)),
  skip_empty_(pt.get(kwds::skip_empty(), true))
{}

/*
*******************************************************************************/
void Write_seq::run() {
   if( skip_empty_ && vma_.sequence().empty() ) return;
   if( skip_empty_ && trim_ && empty(vma_.interval()) ) return;
   fov_.ostream(vma_).write(
            vma_.read_id(),
            vma_.sequence(trim_, reverse_),
            vma_.quality(trim_, reverse_)
   );
   ++n_seq_;
}

/*
*******************************************************************************/
void Write_seq::summary(std::ostream& os) const {
   os
   << "files created: " << fov_.size() << '\n'
   << "sequences written: " << n_seq_ << '\n'
   ;
}

}//namespace vdj_pipe
