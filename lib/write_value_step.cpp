/** @file "/vdj_pipe/lib/write_value_step.cpp"
part of vdj_pipe project.
@n @n Distributed under GNU General Public License, Version 3;
see doc/license.txt.
@n Copyright Mikhail K Levin 2013
*******************************************************************************/
#ifndef VDJ_PIPE_SOURCE
#define VDJ_PIPE_SOURCE
#endif
#include "vdj_pipe/step/write_value_step.hpp"

#include <sstream>
#include "boost/foreach.hpp"
#include "boost/numeric/interval/io.hpp"
#include "boost/property_tree/ptree.hpp"
namespace bpt = boost::property_tree;

#include "vdj_pipe/file_stream.hpp"
#include "vdj_pipe/output_stamp.hpp"
#include "vdj_pipe/pipe_environment.hpp"
#include "vdj_pipe/print_sequence.hpp"
#include "vdj_pipe/value_map.hpp"

namespace vdj_pipe {

VDJ_PIPE_KEYWORD_STRUCT(
   kwds,
   (out_path)(names)(unset_value)
);

/*
*******************************************************************************/
std::vector<std::string>
Write_value::get_names(boost::property_tree::ptree const& pt) {
   std::vector<std::string> vs;
   BOOST_FOREACH( bpt::ptree::value_type const& v, pt.get_child(kwds::names()) ) {
      std::string const& name = v.second.data();
      if( name.empty() ) BOOST_THROW_EXCEPTION(
               Err()
               << Err::msg_t("empty variable name")
      );
      vs.push_back(name);
   }

   if( vs.empty() )BOOST_THROW_EXCEPTION(
            Err()
            << Err::msg_t("no variables specified")
   );
   return vs;
}

/*
*******************************************************************************/
std::vector<Val_id> Write_value::get_ids() {
   std::vector<Val_id> vi;
   vi.reserve(v_names_.size());
   BOOST_FOREACH(std::string const& name, v_names_) {
      vi.push_back(vma_.value_id(name));
   }
   return vi;
}

/*
*******************************************************************************/
std::string Write_value::make_header() const {
   std::ostringstream os;
   os << stamp();
   BOOST_FOREACH(std::string const& name, v_names_) {
      os << name << delimiter();
   }
   os << '\n';
   return os.str();
}

/*
*******************************************************************************/
Write_value::Write_value(
         Value_map const& vma,
         boost::property_tree::ptree const& pt,
         Pipe_environment& pe
)
: Step_base(pt, pe),
  vma_(vma),
  v_names_(get_names(pt)),
  v_ids_(get_ids()),
  fov_(
           pe.output().path(pt.get<std::string>(kwds::out_path())),
           pt.get(kwds::unset_value(), File_ostream_variant::skip_empty()),
           vma,
           make_header()
  ),
  n_seq_(0)
{}

/*
*******************************************************************************/
void Write_value::run() {
   File_ostream& fos = fov_.ostream(vma_);
   BOOST_FOREACH(const Val_id vid, v_ids_) {
      fos.ostream() << vma_[vid] << delimiter();
   }
   fos.ostream() << '\n';
   ++n_seq_;
}

/*
*******************************************************************************/
void Write_value::summary(std::ostream& os) const {
   os
   << "files created: " << fov_.size() << '\n'
   << "sequences written: " << n_seq_ << '\n'
   ;
}

}//namespace vdj_pipe
